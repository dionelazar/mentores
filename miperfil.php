<?php 
    require_once __DIR__.'/class/class.helpers.php';
    $user = Cookies::getDatosUser();
    switch ($user->tipo_usuario) {
        case 1:
            require_once __DIR__.'/share/header-admin.php';
            break;
        
        case 2:
            require_once __DIR__.'/share/header-mentorDT.php';
            break;

        case 3:
            require_once __DIR__.'/share/header-mentor-con.php';
            break;

        case 4:
            require_once __DIR__.'/share/header-DT.php';
            break;

        case 5:
            require_once __DIR__.'/share/header-con.php';
            break;
    }
    require_once __DIR__.'/class/class.usuarios.php';
    $miUser = Usuarios::getMisDatos();
?>
    <div class="content home ministerio perfil">
        <!-- TODO EL CONTENIDO (LO QUE NO ES MENÚ NI HEADER) -->
        <div class="wrapper">
            <div class="col100">
                <div class="header-perfil">
                    <h1>
                       <strong> <?php echo $datosUser->nombre; ?> </strong> 
                    </h1>
                    <h3>(<?php echo $miUser->tipo_usuario ?>)</h3>
                </div>
                <hr>
                <?php if ($miUser->id_tipo_usuario == 2 || $miUser->id_tipo_usuario == 3): ?>
                <!--a href="#" class="btnCertificado">
                    <div class="ver-certificado">
                       <i data-icon="p"></i><strong>Ver Certificado</strong>
                    </div>
                </a-->
                <?php endif ?>
                <ul class="datos-personales">
                    <li>
                        <p><strong>Última actividad</strong></p>
                        <div class="ultima-actividad">
                            <p><?php echo $miUser->ult_actividad ?></p>
                        </div>
                    </li>
                    
                    <li>
                        <p><strong>E-mail</strong></p>
                        <div class="editar-email">
                            <p><?php echo $miUser->email ?></p>
                        </div>
                    </li>

                    <br>
                    <li>
                        <p><strong>Contraseña</strong></p>
                        <div class="editar-contraseña" id="editar-contraseña">
                            <p contenteditable="true" id="input_password">••••••••••••</p>
                            <!--i class="pull-right" data-icon="&#xe037;" ></i-->
                        </div>
                    </li>
                    <br>
                    <li>
                        <div class="">
                            <a href="javascript:void(0)" onclick="cambiarPass()" class="cambiar-contraseña">Cambiar contraseña</a>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="col100">
                <ul class="datos-personales">
                    <li>
                        <p><strong>Cambiar imagen de perfil</strong></p>
                        <div class="ultima-actividad">
                            <form action="javascript:cargarFotoDePerfil()" method="POST" enctype="multipart/form-data" id="fotoDePerfil">
                                <input type="file" name="foto" accept="image/*">
                                <input type="submit" value="Cargar imagen">
                            </form>
                            <div id="imagen-perfil">
                                <img src="<?php echo $miUser->imagen ?>" height="100" width="100">
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
                
    <div id="popupCertificacion" class="absolute-center" hidden="">
        <div id="overlayCertificacion"></div>
        <div class="popupCertificacion">
            <a href="#" id="closeBtn" class="closePopUp pull-right">
                <i class="close-icon" data-icon="9"></i>
            </a>
            <div class="headerCertificado">
                <img class="logoCertificado" src="img/logoMinisteriowhite.png" alt="">
            </div>
            <div class="contentCertificado">
                <p>- Certificado de capacitación -</p>
                <br>
                <h3>Se entrega este Certificado a</h3>
                <h2 class="name"><?php echo $datosUser->nombre; ?></h2>
                <h3 style="margin-top: 15px;">por haber finalizado satisfactoriamente la </h3>
                <h2>Capacitación de Mentoría.</h2>
            </div>
            <div class="firmasCertificado">
                <div class="firma1">
                    <img src="img/signature1.png" alt="">
                    <hr>
                    <p><strong>Nombre y Apellido </strong><br> - Cargo -</p>
                </div>
                <div class="firma2">
                    <img src="img/signature2.png" alt="">
                    <hr>
                    <p><strong>Nombre y Apellido </strong><br> - Cargo -</p>
                </div>
                <div class="firma3">
                    <img src="img/signature3.png" alt="">
                    <hr>
                    <p><strong>Nombre y Apellido </strong><br> - Cargo -</p>
                </div>
            </div>
        </div>
        <!--a href="#" class="descargarCertificado">
            <i data-icon=":"></i>Descargar
        </a-->
    </div>
    <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/masonry.pkgd.min.js"></script>
    <script src="js/select2.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="js/main.js"></script>
    <script src="scripts/header.js"></script>
    <script src="scripts/miperfil.js"></script>
</body>

</html>