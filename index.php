<?php 
	if(isset($_COOKIE['tipo_usuario']))
	{
		require_once 'class/class.helpers.php';

		$tipo_usuario = Encriptacion::desencriptar($_COOKIE['tipo_usuario']);

		switch ($tipo_usuario) 
		{
			case 1:
				header('location:gestion-usuarios.php');
				break;
			
			case 2:
				header('location:home-mentorDT.php');
				break;

			case 3:
				header('location:home-mentorConsolidacion.php');
				break;

			case 4:
				header('location:home-desarrolloTemp.php');
				break;

			case 5:
				header('location:home-consolidacion.php');
				break;
		}
	}else{
		header('location:login.php');
	}
?>