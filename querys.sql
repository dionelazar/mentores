CREATE PROC sp_ins_sesiones_fase_2
(
	@id_mentor INT
)
AS BEGIN
SET NOCOUNT ON
	DECLARE @id_grupo INT
		   ,@id_rol INT
	
	SELECT @id_rol = id_tipo_usuario FROM usuarios WHERE id_usuario = @id_mentor

	IF @id_rol IN(2,3) BEGIN
		IF EXISTS(SELECT 1 FROM grupos WHERE id_mentor = @id_mentor AND id_tipo_grupo = 20) BEGIN
			SELECT 'El grupo de la fase 2 ya existe' 'mensaje'
				,0 'cod_mensaje'
		END ELSE BEGIN
			SELECT @id_grupo = MAX(id_grupo) + 1 FROM grupos
		
			INSERT INTO grupos (id_grupo, id_mentor, txt_desc, id_tipo_grupo)
						VALUES (@id_grupo, @id_mentor, 'Grupo fase 2', 20)
			IF EXISTS(SELECT 1 FROM sesiones WHERE id_grupo = @id_grupo AND id_sesion > 20) BEGIN
				SELECT 'Ya tiene las sesiones creadas de la fase 2' 'mensaje'
					  ,0 'cod_mensaje'
			END ELSE BEGIN
				INSERT INTO sesiones (id_sesion, id_grupo, txt_desc)
							   VALUES(21, @id_grupo, '1° Sesion fase 2')
				INSERT INTO sesiones (id_sesion, id_grupo, txt_desc)
							   VALUES(22, @id_grupo, '2° Sesion fase 2')
				INSERT INTO sesiones (id_sesion, id_grupo, txt_desc)
							   VALUES(23, @id_grupo, '3° Sesion fase 2')

				SELECT 'Se crearon correctamente las sesiones de la fase 2' 'mensaje'
					  ,-1 'cod_mensaje'
			END
		END
	END ELSE BEGIN
		SELECT 'El usuario no es mentor' 'mensaje'
			  ,0 'cod_mensaje'
	END
END

GO

ALTER PROC [dbo].[sp_upd_aceptar_emprendedor_fase_2]
(
	@id_emprendedor INT
)
AS BEGIN
SET NOCOUNT ON
	UPDATE emprendedores_propuestos_fase_2 SET sn_aceptado = -1 WHERE id_emprendedor = @id_emprendedor
	SELECT -1 'cod_mensaje'
		  ,'El emprendedor fue aceptado exitosamente en la fase 2!' 'mensaje'
		  ,(SELECT id_mentor FROM emprendedores_propuestos_fase_2 WHERE id_emprendedor = @id_emprendedor) 'id_mentor'
END

GO

--sujeta a cambios
CREATE PROC sp_get_sesiones_fase_2
(
	@id_usuario INT
)
AS BEGIN
SET NOCOUNT ON
	DECLARE @id_grupo INT
		   ,@id_mentor INT
		   ,@id_tipo_usuario INT
		   ,@id_grupo_fase_2 INT

	SELECT @id_grupo = id_grupo, @id_tipo_usuario = id_tipo_usuario  FROM usuarios WHERE id_usuario = @id_usuario

	IF @id_tipo_usuario IN (2,3) BEGIN --MENTOR
		SELECT @id_grupo_fase_2 = id_grupo FROM grupos WHERE id_tipo_grupo = 20 AND id_mentor = @id_usuario
		
		IF @id_grupo_fase_2 IS NULL BEGIN
			SELECT 'Todavía no se aprobó ningún usuario para la fase 2' 'mensaje'
				  ,0 'cod_mensaje'
		END ELSE BEGIN
			SELECT '' 'mensaje'
				  ,-1 'cod_mensaje'
				  ,s.id_sesion 'id_sesion'
				  ,s.id_grupo 'id_grupo'
				  ,s.txt_desc 'desc'
				  ,(SELECT  Distinct Stuff((Select ', ' + txt_nombre From usuarios WHERE id_grupo = s.id_grupo AND id_tipo_usuario = 5 For Xml Path ('')), 1, 1, '')) 'emprendedores'
				  ,ISNULL((SELECT TOP 1 -1 FROM calificaciones WHERE id_usuario = @id_usuario AND id_sesion = s.id_sesion AND id_grupo = s.id_grupo), 0)'sn_califico'
				  --,falaría el tema de las calificaciones
			FROM sesiones s
			WHERE id_grupo = @id_grupo_fase_2
			ORDER BY id_grupo, id_sesion
		END
	END ELSE BEGIN --EMPRENDEDOR
		SELECT @id_mentor = id_mentor FROM grupos WHERE id_grupo = @id_grupo
		SELECT @id_grupo_fase_2 = id_grupo FROM grupos WHERE id_tipo_grupo = 20 AND id_mentor = @id_mentor

		IF @id_grupo_fase_2 IS NULL BEGIN
			SELECT 'Todavía no se aprobó ningún usuario para la fase 2' 'mensaje'
				  ,0 'cod_mensaje'
		END ELSE BEGIN
			SELECT '' 'mensaje'
				  ,-1 'cod_mensaje'
				  ,s.id_sesion 'id_sesion'
				  ,s.id_grupo 'id_grupo'
				  ,s.txt_desc 'desc'
				  ,(SELECT  Distinct Stuff((Select ', ' + txt_nombre From usuarios WHERE id_grupo = s.id_grupo AND id_tipo_usuario = 5 For Xml Path ('')), 1, 1, '')) 'emprendedores'
				  ,ISNULL((SELECT TOP 1 -1 FROM calificaciones WHERE id_usuario = @id_usuario AND id_sesion = s.id_sesion AND id_grupo = s.id_grupo), 0)'sn_califico'
				  --,falaría el tema de las calificaciones
			FROM sesiones s
			WHERE id_grupo = @id_grupo_fase_2
			ORDER BY id_grupo, id_sesion
		END
	END
END
--cambiar ??

GO

CREATE PROC sp_get_emprendedores_fase_2
(
	@id_usuario INT
)
AS BEGIN
SET NOCOUNT ON
	SELECT u.txt_nombre 'nombre'
	      ,u.txt_doc 'documento'
		  ,u.id_usuario 'id_usuario'
		  ,u.sn_abandono 'sn_abandono'
	FROM emprendedores_propuestos_fase_2 f2
	JOIN usuarios u ON f2.id_emprendedor = u.id_usuario
	WHERE   f2.sn_aceptado = -1
		AND f2.id_mentor = @id_usuario
END

GO

CREATE PROC sp_get_form_fase_2
(
	 @id_usuario INT
	,@id_sesion INT
	,@id_grupo INT
	,@id_emprendedor INT
)
AS BEGIN
SET NOCOUNT ON
	DECLARE @id_tipo_usuario INT

	SELECT @id_tipo_usuario = id_tipo_usuario FROM usuarios WHERE id_usuario = @id_usuario

	IF		   @id_sesion = 21
			OR @id_sesion <= (SELECT MAX(id_sesion) + 1 FROM calificaciones WHERE id_grupo = @id_grupo AND id_usuario = @id_usuario AND id_emprendedor = @id_emprendedor) BEGIN
		IF @id_tipo_usuario IN (2,3) BEGIN -- mentor
			SELECT p.id_pregunta
				  ,p.num_pregunta
				  ,p.txt_desc 'pregunta'
				  ,-1 'cod_mensaje'
			FROM preguntas p
			WHERE id_pregunta in (11, 12)
		END ELSE IF @id_tipo_usuario = 4 BEGIN -- DT
			SELECT p.id_pregunta
				  ,p.num_pregunta
				  ,p.txt_desc 'pregunta'
				  ,-1 'cod_mensaje'
			FROM preguntas p
			WHERE id_pregunta in (5, 6)
		END
	END ELSE BEGIN
		IF @id_emprendedor = 0 AND @id_tipo_usuario != 2 BEGIN
			SELECT  0  'cod_mensaje'
					,'No puedes calificar porque no tienes ningún emprendedor en tu grupo' 'mensaje'
		END ELSE BEGIN
			SELECT  0  'cod_mensaje'
					,'Tienes que calificar las sesiones anteriores para avanzar' 'mensaje'
		END
	END
END

GO

CREATE PROC sp_get_calificaciones_emprendedor_fase_2
(
	 @id_usuario INT
	,@id_emprendedor INT
)
AS BEGIN
SET NOCOUNT ON
	IF EXISTS(SELECT 1 FROM calificaciones WHERE id_emprendedor = @id_emprendedor AND id_sesion >= 21) BEGIN
		SELECT   '' 'mensaje'
				,-1 'cod_mensaje'
				,c.id_sesion 'id_sesion'
				,c.id_grupo 'id_grupo'
				,c.id_calificacion 'id_calificacion'
				,c.txt_comentario 'comentario'
				,c.puntuacion 'puntuacion'
				,g.txt_desc 'grupo'
				,c.id_usuario 'id_usuario'
				,c.id_emprendedor 'id_emprendedor'
		FROM calificaciones c
		JOIN grupos g ON g.id_grupo = c.id_grupo
		WHERE id_emprendedor = @id_emprendedor 
		  AND id_sesion >= 21
		ORDER BY id_sesion
	END ELSE BEGIN
		SELECT   'No hay calificaciones del emprendedor' 'mensaje'
				,0 'cod_mensaje'
	END
END

GO

CREATE PROC sp_get_emprendedores_mis_propuestos_fase_2
(
	@id_usuario INT
)
AS BEGIN
SET NOCOUNT ON
	SELECT u.id_usuario 'id_emprendedor'
		  ,u.txt_nombre 'emprendedor'
		  ,u.txt_doc 'dni'
		  ,f2.sn_aceptado 'estado'
	FROM emprendedores_propuestos_fase_2 f2
	JOIN usuarios u ON u.id_usuario = f2.id_emprendedor
	WHERE id_mentor = @id_usuario
END

GO

CREATE PROC sp_get_comprobar_aceptado_fase_2
(
	@id_usuario INT
)
AS BEGIN
SET NOCOUNT ON
	DECLARE @estado INT
	SELECT @estado = sn_aceptado FROM emprendedores_propuestos_fase_2 WHERE id_emprendedor = @id_usuario

	IF @estado IS NULL BEGIN
		SELECT 0 'cod_mensaje'
			  ,'No fue propuesto' 'mensaje'
			  ,-2 'estado'
	END ELSE IF @estado = -1 BEGIN
		SELECT -1 'cod_mensaje'
			  ,'Aceptado' 'mensaje'
			  ,@estado 'estado'
	END ELSE BEGIN
		SELECT 0 'cod_mensaje'
			  ,'Rechazado' 'mensaje'
			  ,@estado 'estado'
	END
END

GO

ALTER PROC [dbo].[sp_get_calificaciones_grupo_admin]
(
	 @id_usuario INT
	,@id_grupo INT
)
AS BEGIN
SET NOCOUNT ON
	DECLARE @id_tipo_usuario INT
		   ,@id_mentor INT
	SELECT @id_tipo_usuario = id_tipo_usuario FROM usuarios WHERE id_usuario = @id_usuario

	IF @id_tipo_usuario IN (1,7) BEGIN
		SELECT c.id_calificacion 'id_calificacion'
			  ,c.id_sesion 'id_sesion'
			  ,c.puntuacion 'puntuacion'
			  ,c.txt_comentario 'comentario'
			  ,c.fecha 'fecha'
			  ,u.txt_nombre 'usuario'
			  ,p.txt_desc 'pregunta'
			  ,r.txt_desc 'respuesta'
		FROM calificaciones c
		JOIN usuarios u ON u.id_usuario = c.id_usuario
		LEFT JOIN calificacion_respuestas cr ON cr.id_grupo = c.id_grupo AND cr.id_sesion = c.id_sesion
		LEFT JOIN respuestas r ON r.id_respuesta = cr.id_respuesta
		LEFT JOIN preguntas p ON p.id_pregunta = r.id_pregunta
		WHERE c.id_grupo = @id_grupo
		ORDER BY id_sesion, id_calificacion
	END
END

GO

CREATE PROC sp_get_sesiones_fase_2_emprendedores
(
	@id_usuario INT
)
AS BEGIN
SET NOCOUNT ON
	DECLARE @id_grupo INT
	DECLARE @id_grupo_fase_2 INT
	DECLARE @id_mentor INT
	DECLARE @nombre_mentor VARCHAR(200)

	SELECT @id_grupo = id_grupo FROM usuarios WHERE id_usuario = @id_usuario

	IF @id_grupo IS NULL BEGIN
		SELECT 'El usuario no tiene un grupo asignado' 'mensaje'
		      ,0 'cod_mensaje'
	END ELSE BEGIN
		SELECT @id_mentor = id_usuario, @nombre_mentor = txt_nombre FROM  usuarios WHERE id_grupo = @id_grupo AND id_tipo_usuario IN (2,3)
		SELECT @id_grupo_fase_2 = id_grupo FROM grupos WHERE id_mentor = @id_mentor AND id_tipo_grupo = 20

		SELECT '' 'mensaje'
		      ,-1 'cod_mensaje'
			  ,s.id_sesion 'id_sesion'
			  ,s.id_grupo 'id_grupo'
			  ,s.txt_desc 'desc'
			  ,ISNULL((SELECT TOP 1 txt_comentario FROM calificaciones WHERE id_sesion = s.id_sesion AND id_grupo = s.id_grupo AND id_usuario = @id_mentor), '') 'comentario'
			  ,@nombre_mentor 'mentor'
			  ,ISNULL((SELECT TOP 1 -1 FROM calificaciones WHERE id_usuario = @id_usuario AND id_sesion = s.id_sesion AND id_grupo = s.id_grupo), 0)'sn_califico'
			  --,faltaría el tema de las calificaciones
		FROM sesiones s
		WHERE id_grupo = @id_grupo_fase_2
		ORDER BY id_grupo, id_sesion
	END
END

GO

ALTER PROC sp_ins_calificacion
(
	 @id_usuario INT
	,@id_sesion INT
	,@id_grupo INT
	,@stars FLOAT
	,@comentario VARCHAR(MAX)
	,@id_emprendedor INT
)
AS BEGIN
SET NOCOUNT ON
	DECLARE  @id_calificacion INT
			,@hoy SMALLDATETIME
			,@id_tipo_usuario INT

	SELECT @id_calificacion = ISNULL(MAX(id_calificacion), 0) + 1 FROM calificaciones
	SET @hoy = GETDATE()
	SELECT @id_tipo_usuario = id_tipo_usuario FROM usuarios WHERE id_usuario = @id_usuario
	
	IF EXISTS(SELECT 1 FROM calificaciones WHERE id_usuario = @id_usuario AND id_grupo = @id_grupo AND id_sesion = @id_sesion AND id_emprendedor = @id_emprendedor) BEGIN
		UPDATE calificaciones SET puntuacion = @stars, txt_comentario = @comentario
		WHERE id_usuario = @id_usuario AND id_grupo = @id_grupo AND id_sesion = @id_sesion AND id_emprendedor = @id_emprendedor

		SELECT  -1  'cod_mensaje'
		      ,'Calificación editada correctamente' 'mensaje'
	END ELSE BEGIN
		-- si no existe una calificacion de la sesión y grupo
		-- o el n° de sesión es exactamente 1 mayor al último calificado
		IF     (@id_sesion = 1 OR @id_sesion = 21)
			OR @id_sesion = (SELECT MAX(id_sesion) + 1 FROM calificaciones WHERE id_grupo = @id_grupo AND id_usuario = @id_usuario AND id_emprendedor = @id_emprendedor)
		BEGIN
			INSERT INTO calificaciones (id_calificacion, id_usuario, id_sesion, id_grupo, puntuacion, txt_comentario, url_imagen, fecha, id_emprendedor)
							    VALUES (@id_calificacion, @id_usuario, @id_sesion, @id_grupo, @stars, @comentario, null, @hoy, @id_emprendedor)

			SELECT  -1  'cod_mensaje'
				   ,'Se cargó correctamente la calificación. ¡Gracias!' 'mensaje'
		END ELSE BEGIN
			IF @id_tipo_usuario = 2 BEGIN
				--DECLARE @i INT = 1
				--WHILE (@i <= @id_sesion)
				--BEGIN  
					INSERT INTO calificaciones (id_calificacion, id_usuario, id_sesion, id_grupo, puntuacion, txt_comentario, url_imagen, fecha, id_emprendedor)
							    VALUES (@id_calificacion, @id_usuario, @id_sesion, @id_grupo, @stars, @comentario, null, @hoy, @id_emprendedor)
				--	SET @i += 1  
				--END

				SELECT  -1  'cod_mensaje'
					   ,'Se cargó correctamente la calificación. ¡Gracias!' 'mensaje'
			END ELSE BEGIN
				SELECT  0  'cod_mensaje'
					   ,'Tienes que calificar las anteriores sesiones' 'mensaje'
			END
		END
	END
END

GO

/*
**************************************************************************************************
**************************************************************************************************
**************************************************************************************************
**************************************************************************************************
**************************************************************************************************
**************************************************************************************************
**************************************************************************************************
****************************************^^^^^^^^^^^^**********************************************
**************************************** 23/10/2018 **********************************************
****************************************^^^^^^^^^^^^**********************************************
**************************************************************************************************
**************************************************************************************************
**************************************************************************************************
**************************************************************************************************
**************************************************************************************************
**************************************************************************************************
**************************************************************************************************
*/

GO

CREATE PROC sp_get_reporte_usuarios_by_grupo
(
	@id_usuario INT
	,@id_grupo INT
)
AS BEGIN
SET NOCOUNT ON
	DECLARE @id_rol INT
	
	SELECT @id_rol = id_tipo_usuario FROM usuarios WHERE id_usuario = @id_usuario

	IF @id_rol IN(1,7) BEGIN
		SELECT u.id_usuario
			  ,u.txt_nombre 'nombre'
			  ,u.txt_doc 'dni'
			  ,u.id_tipo_usuario 'id_rol'
			  ,tu.txt_desc 'rol'
			  ,(SELECT COUNT(DISTINCT id_sesion) FROM calificaciones WHERE id_usuario = u.id_usuario) 'cant_sesiones'
		FROM usuarios u
		JOIN tipo_usuario tu ON tu.id_tipo_usuario = u.id_tipo_usuario
		WHERE u.id_grupo = @id_grupo
	END ELSE BEGIN
		SELECT 'usuario sin permisos' 'mensaje'
	END
END