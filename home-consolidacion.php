<?php 
    require __DIR__.'/share/header-con.php';
?>
    <div class="content home">
        <!-- TODO EL CONTENIDO (LO QUE NO ES MENÚ NI HEADER) -->
        <div class="wrapper">
            <div class="col100">
                <ul class="tabs tabs-2 oneTab clearfix">
                    <li class="current">
                        <a href="#0">
                            <h2>Sesiones</strong></h2>
                        </a>
                    </li>
                </ul>
                <div class="container home">
                    <div class="sesionA show">
                         <div class="col-100 pad10">
                            <div class="col-100 pad10">
                                <div class="feature-panel">
                                      <ul class="breadcrumb sesion3">
                                        <li id="flechaProgresoSesion1">
                                          <a class="visibleDesktop" href="#">1ª Sesión</a>
                                          <a class="visibleSmInline" href="#">1ª</a>
                                        </li>
                                        <li id="flechaProgresoSesion2">
                                          <a class="visibleDesktop" href="#">2ª Sesión</a>
                                          <a class="visibleSmInline" href="#">2ª</a>
                                        </li>
                                        <li id="flechaProgresoSesion3">
                                          <a class="visibleDesktop" href="#">3ª Sesión</a>
                                          <a class="visibleSmInline" href="#">3ª</a>
                                        </li>
                                      </ul>
                                    </div>
                                </div>  
                        <div id="sesiones">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="popupCalificacion" class="absolute-center">
        <div id="overlayCalificacion"></div>
        <div class="popupCalificacion">
            <a href="#" id="closeBtn" class="closePopUp">
                <i class="close-icon" data-icon="9"></i>
            </a>
            <div class="label100">
            <label for="rating">Calificá esta sesión</label>
            </div>
            <fieldset class="rating">
                <input type="radio" id="star5" name="rating" value="5" />
                <label class="full" for="star5" title="Awesome - 5 stars"></label>
                <input type="radio" id="star4half" name="rating" value="4.5" />
                <label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
                <input type="radio" id="star4" name="rating" value="4" />
                <label class="full" for="star4" title="Pretty good - 4 stars"></label>
                <input type="radio" id="star3half" name="rating" value="3.5" />
                <label class="half" for="star3half" title="Meh - 3.5 stars"></label>
                <input type="radio" id="star3" name="rating" value="3" />
                <label class="full" for="star3" title="Meh - 3 stars"></label>
                <input type="radio" id="star2half" name="rating" value="2.5" />
                <label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
                <input type="radio" id="star2" name="rating" value="2" />
                <label class="full" for="star2" title="Kinda bad - 2 stars"></label>
                <input type="radio" id="star1half" name="rating" value="1.5" />
                <label class="half" for="star1half" title="Meh - 1.5 stars"></label>
                <input type="radio" id="star1" name="rating" value="1" />
                <label class="full" for="star1" title="Sucks big time - 1 star"></label>
                <input type="radio" id="starhalf" name="rating" value="0.5" />
                <label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
            </fieldset>
            <fieldset class="label100">
                <label for="calificacion1" id="pregunta1">¿Pregunta #1?
                <br><em><small>Podés seleccionar varias opciones</small></em></label>
                <div id="respuestas1"></div>
            </fieldset>
            <fieldset class="label100">
                <label for="calificacion2" id="pregunta2">¿Pregunta #2?
                <br><em><small>Podés seleccionar varias opciones</small></em></label>
                <div id="respuestas2"></div>
            </fieldset>
            <fieldset class="label100">
                <label for="calificacion2">Describí brevemente como te pareció la sesión</label>
                <textarea class="label100" name="calificacion-texto" id="calificacion-texto"></textarea>
            </fieldset>
            <!--button class="cameraButton"><i data-icon="O"></i>Sacar una Selfie</button-->
            <input type="button" class="label100" name="submit-calificacion" id="submit-calificacion" value="Calificar" onclick="calificar()">
        </div>
    </div>

    <!-- POP UP SATISFACCION nuevo-->
    <div id="popupSatisfaccion" class="absolute-center" hidden="">
        <div id="overlayCalificacion"></div>
        <div class="popupCalificacion popupSatisfaccion">
            <a href="#" id="closeBtn" class="closePopUp">
                <i class="close-icon" data-icon="9"></i>
            </a>
            <h3>Por favor, te pedimos que respondas las siguientes preguntas sobre el programa de mentoría, utilizando una escala de valoración entre 1 (baja) y 5 (alta).</h3>
            <div class="label100">
                <label for="rating1">1. Estructura y contenidos de cada reunión.*</label>
            </div>
            <fieldset class="rating">
                <input type="radio" id="sstarEstr5" name="sstarEstr" value="5" />
                <label class="full" for="sstarEstr5" title="Awesome - 5 stars"></label>
                <input type="radio" id="sstarEstr4half" name="sstarEstr" value="4.5" />
                <label class="half" for="sstarEstr4half" title="Pretty good - 4.5 stars"></label>
                <input type="radio" id="sstarEstr4" name="sstarEstr" value="4" />
                <label class="full" for="sstarEstr4" title="Pretty good - 4 stars"></label>
                <input type="radio" id="sstarEstr3half" name="sstarEstr" value="3.5" />
                <label class="half" for="sstarEstr3half" title="Meh - 3.5 stars"></label>
                <input type="radio" id="sstarEstr3" name="sstarEstr" value="3" />
                <label class="full" for="sstarEstr3" title="Meh - 3 stars"></label>
                <input type="radio" id="sstarEstr2half" name="sstarEstr" value="2.5" />
                <label class="half" for="sstarEstr2half" title="Kinda bad - 2.5 stars"></label>
                <input type="radio" id="sstarEstr2" name="sstarEstr" value="2" />
                <label class="full" for="sstarEstr2" title="Kinda bad - 2 stars"></label>
                <input type="radio" id="sstarEstr1half" name="sstarEstr" value="1.5" />
                <label class="half" for="sstarEstr1half" title="Meh - 1.5 stars"></label>
                <input type="radio" id="sstarEstr1" name="sstarEstr" value="1" />
                <label class="full" for="sstarEstr1" title="Sucks big time - 1 star"></label>
                <input type="radio" id="sstarEstrhalf" name="sstarEstr" value="0.5" />
                <label class="half" for="sstarEstrhalf" title="Sucks big time - 0.5 stars"></label>
            </fieldset>
            <div class="label100">
                <label for="rating2">2. Utilidad de los temas trabajados.*</label>
            </div><br>
            <fieldset class="rating">
                <input type="radio" id="sstarUtil5" name="sstarUtil" value="5" />
                <label class="full" for="sstarUtil5" title="Awesome - 5 stars"></label>
                <input type="radio" id="sstarUtil4half" name="sstarUtil" value="4.5" />
                <label class="half" for="sstarUtil4half" title="Pretty good - 4.5 stars"></label>
                <input type="radio" id="sstarUtil4" name="sstarUtil" value="4" />
                <label class="full" for="sstarUtil4" title="Pretty good - 4 stars"></label>
                <input type="radio" id="sstarUtil3half" name="sstarUtil" value="3.5" />
                <label class="half" for="sstarUtil3half" title="Meh - 3.5 stars"></label>
                <input type="radio" id="sstarUtil3" name="sstarUtil" value="3" />
                <label class="full" for="sstarUtil3" title="Meh - 3 stars"></label>
                <input type="radio" id="sstarUtil2half" name="sstarUtil" value="2.5" />
                <label class="half" for="sstarUtil2half" title="Kinda bad - 2.5 stars"></label>
                <input type="radio" id="sstarUtil2" name="sstarUtil" value="2" />
                <label class="full" for="sstarUtil2" title="Kinda bad - 2 stars"></label>
                <input type="radio" id="sstarUtil1half" name="sstarUtil" value="1.5" />
                <label class="half" for="sstarUtil1half" title="Meh - 1.5 stars"></label>
                <input type="radio" id="sstarUtil1" name="sstarUtil" value="1" />
                <label class="full" for="sstarUtil1" title="Sucks big time - 1 star"></label>
                <input type="radio" id="sstarUtilhalf" name="sstarUtil" value="0.5" />
                <label class="half" for="sstarUtilhalf" title="Sucks big time - 0.5 stars"></label>
            </fieldset><br>
            <div class="label100">
                <label for="rating3">3. Cumplimiento de los objetivos del plan de trabajo.*</label>
            </div><br>
            <fieldset class="rating">
                <input type="radio" id="sstarCumplimiento5" name="sstarCumplimiento" value="5" />
                <label class="full" for="sstarCumplimiento5" title="Awesome - 5 stars"></label>
                <input type="radio" id="sstarCumplimiento4half" name="sstarCumplimiento" value="4.5" />
                <label class="half" for="sstarCumplimiento4half" title="Pretty good - 4.5 stars"></label>
                <input type="radio" id="sstarCumplimiento4" name="sstarCumplimiento" value="4" />
                <label class="full" for="sstarCumplimiento4" title="Pretty good - 4 stars"></label>
                <input type="radio" id="sstarCumplimiento3half" name="sstarCumplimiento" value="3.5" />
                <label class="half" for="sstarCumplimiento3half" title="Meh - 3.5 stars"></label>
                <input type="radio" id="sstarCumplimiento3" name="sstarCumplimiento" value="3" />
                <label class="full" for="sstarCumplimiento3" title="Meh - 3 stars"></label>
                <input type="radio" id="sstarCumplimiento2half" name="sstarCumplimiento" value="2.5" />
                <label class="half" for="sstarCumplimiento2half" title="Kinda bad - 2.5 stars"></label>
                <input type="radio" id="sstarCumplimiento2" name="sstarCumplimiento" value="2" />
                <label class="full" for="sstarCumplimiento2" title="Kinda bad - 2 stars"></label>
                <input type="radio" id="sstarCumplimiento1half" name="sstarCumplimiento" value="1.5" />
                <label class="half" for="sstarCumplimiento1half" title="Meh - 1.5 stars"></label>
                <input type="radio" id="sstarCumplimiento1" name="sstarCumplimiento" value="1" />
                <label class="full" for="sstarCumplimiento1" title="Sucks big time - 1 star"></label>
                <input type="radio" id="sstarCumplimientohalf" name="sstarCumplimiento" value="0.5" />
                <label class="half" for="sstarCumplimientohalf" title="Sucks big time - 0.5 stars"></label>
            </fieldset><br>
            <div class="label100">
                <label for="rating4">4. Valor de la interacción con otros emprendedores.*</label>
            </div><br>
            <fieldset class="rating">
                <input type="radio" id="sstarValor5" name="sstarValor" value="5" />
                <label class="full" for="sstarValor5" title="Awesome - 5 stars"></label>
                <input type="radio" id="sstarValor4half" name="sstarValor" value="4.5" />
                <label class="half" for="sstarValor4half" title="Pretty good - 4.5 stars"></label>
                <input type="radio" id="sstarValor4" name="sstarValor" value="4" />
                <label class="full" for="sstarValor4" title="Pretty good - 4 stars"></label>
                <input type="radio" id="sstarValor3half" name="sstarValor" value="3.5" />
                <label class="half" for="sstarValor3half" title="Meh - 3.5 stars"></label>
                <input type="radio" id="sstarValor3" name="sstarValor" value="3" />
                <label class="full" for="sstarValor3" title="Meh - 3 stars"></label>
                <input type="radio" id="sstarValor2half" name="sstarValor" value="2.5" />
                <label class="half" for="sstarValor2half" title="Kinda bad - 2.5 stars"></label>
                <input type="radio" id="sstarValor2" name="sstarValor" value="2" />
                <label class="full" for="sstarValor2" title="Kinda bad - 2 stars"></label>
                <input type="radio" id="sstarValor1half" name="sstarValor" value="1.5" />
                <label class="half" for="sstarValor1half" title="Meh - 1.5 stars"></label>
                <input type="radio" id="sstarValor1" name="sstarValor" value="1" />
                <label class="full" for="sstarValor1" title="Sucks big time - 1 star"></label>
                <input type="radio" id="sstarValorhalf" name="sstarValor" value="0.5" />
                <label class="half" for="sstarValorhalf" title="Sucks big time - 0.5 stars"></label>
            </fieldset><br>
            <div class="label100">
                <label for="rating5">5. Desempeño del mentor.*</label>
            </div><br>
            <fieldset class="rating">
                <input type="radio" id="sstarDesemp5" name="sstarDesemp" value="5" />
                <label class="full" for="sstarDesemp5" title="Awesome - 5 stars"></label>
                <input type="radio" id="sstarDesemp4half" name="sstarDesemp" value="4.5" />
                <label class="half" for="sstarDesemp4half" title="Pretty good - 4.5 stars"></label>
                <input type="radio" id="sstarDesemp4" name="sstarDesemp" value="4" />
                <label class="full" for="sstarDesemp4" title="Pretty good - 4 stars"></label>
                <input type="radio" id="sstarDesemp3half" name="sstarDesemp" value="3.5" />
                <label class="half" for="sstarDesemp3half" title="Meh - 3.5 stars"></label>
                <input type="radio" id="sstarDesemp3" name="sstarDesemp" value="3" />
                <label class="full" for="sstarDesemp3" title="Meh - 3 stars"></label>
                <input type="radio" id="sstarDesemp2half" name="sstarDesemp" value="2.5" />
                <label class="half" for="sstarDesemp2half" title="Kinda bad - 2.5 stars"></label>
                <input type="radio" id="sstarDesemp2" name="sstarDesemp" value="2" />
                <label class="full" for="sstarDesemp2" title="Kinda bad - 2 stars"></label>
                <input type="radio" id="sstarDesemp1half" name="sstarDesemp" value="1.5" />
                <label class="half" for="sstarDesemp1half" title="Meh - 1.5 stars"></label>
                <input type="radio" id="sstarDesemp1" name="sstarDesemp" value="1" />
                <label class="full" for="sstarDesemp1" title="Sucks big time - 1 star"></label>
                <input type="radio" id="sstarDesemphalf" name="sstarDesemp" value="0.5" />
                <label class="half" for="sstarDesemphalf" title="Sucks big time - 0.5 stars"></label>
            </fieldset><br>
            <div class="label100">
                <label for="rating6">6. Material de apoyo.*</label>
            </div><br>
            <fieldset class="rating">
                <input type="radio" id="sstarMaterial5" name="sstarMaterial" value="5" />
                <label class="full" for="sstarMaterial5" title="Awesome - 5 stars"></label>
                <input type="radio" id="sstarMaterial4half" name="sstarMaterial" value="4.5" />
                <label class="half" for="sstarMaterial4half" title="Pretty good - 4.5 stars"></label>
                <input type="radio" id="sstarMaterial4" name="sstarMaterial" value="4" />
                <label class="full" for="sstarMaterial4" title="Pretty good - 4 stars"></label>
                <input type="radio" id="sstarMaterial3half" name="sstarMaterial" value="3.5" />
                <label class="half" for="sstarMaterial3half" title="Meh - 3.5 stars"></label>
                <input type="radio" id="sstarMaterial3" name="sstarMaterial" value="3" />
                <label class="full" for="sstarMaterial3" title="Meh - 3 stars"></label>
                <input type="radio" id="sstarMaterial2half" name="sstarMaterial" value="2.5" />
                <label class="half" for="sstarMaterial2half" title="Kinda bad - 2.5 stars"></label>
                <input type="radio" id="sstarMaterial2" name="sstarMaterial" value="2" />
                <label class="full" for="sstarMaterial2" title="Kinda bad - 2 stars"></label>
                <input type="radio" id="sstarMaterial1half" name="sstarMaterial" value="1.5" />
                <label class="half" for="sstarMaterial1half" title="Meh - 1.5 stars"></label>
                <input type="radio" id="sstarMaterial1" name="sstarMaterial" value="1" />
                <label class="full" for="sstarMaterial1" title="Sucks big time - 1 star"></label>
                <input type="radio" id="sstarMaterialhalf" name="sstarMaterial" value="0.5" />
                <label class="half" for="sstarMaterialhalf" title="Sucks big time - 0.5 stars"></label>
            </fieldset><br>
            <div class="calificacion">
            <div class="label100">
                <label for="radio1">7. ¿Cuál es tu valoración general del programa?</label>
            </div><br>
                <fieldset class="labelWCheck">
                    <input type="radio" name="puntaje-valoracion" id="puntaje-valoracion-1" value="1">
                    <label for="puntaje-valoracion-1">1</label>
                    <input type="radio" name="puntaje-valoracion" id="puntaje-valoracion-2" value="2">
                    <label for="puntaje-valoracion-2">2</label>
                    <input type="radio" name="puntaje-valoracion" id="puntaje-valoracion-3" value="3">
                    <label for="puntaje-valoracion-3">3</label>
                    <input type="radio" name="puntaje-valoracion" id="puntaje-valoracion-4" value="4">
                    <label for="puntaje-valoracion-4">4</label>
                    <input type="radio" name="puntaje-valoracion" id="puntaje-valoracion-5" value="5">
                    <label for="puntaje-valoracion-5">5</label>
                    <input type="radio" name="puntaje-valoracion" id="puntaje-valoracion-6" value="6">
                    <label for="puntaje-valoracion-6">6</label>
                    <input type="radio" name="puntaje-valoracion" id="puntaje-valoracion-7" value="7">
                    <label for="puntaje-valoracion-7">7</label>
                    <input type="radio" name="puntaje-valoracion" id="puntaje-valoracion-8" value="8">
                    <label for="puntaje-valoracion-8">8</label>
                    <input type="radio" name="puntaje-valoracion" id="puntaje-valoracion-9" value="9">
                    <label for="puntaje-valoracion-9">9</label>
                    <input type="radio" name="puntaje-valoracion" id="puntaje-valoracion-10" value="10">
                    <label for="puntaje-valoracion-10">10</label>
                </fieldset>
            </div>
            <fieldset class="label100">
                <label for="detalleSatisfaccion">8. Por favor, ¿podrías darnos más detalles sobre tu valoración del programa?*</label>
                <textarea class="label100" name="detalleSatisfaccion" id="detalleSatisfaccion"></textarea>
            </fieldset>
             <fieldset class="label100">
                <label for="comentarioSatisfaccion">9. ¿Tenés algún comentario, sugerencia o aspecto de mejora para futuras ediciones?*</label>
                <textarea class="label100" name="comentarioSatisfaccion" id="comentarioSatisfaccion"></textarea>
            </fieldset>
            <input type="button" class="label100" name="submit-calificacion" id="submit-calificacion" value="Calificar" onclick="calificarSatisfaccion()">
        </div>
    </div>

    <!-- pop up certificacion diploma -->
    <div id="popupCertificacion" class="absolute-center" hidden="">
        <div id="overlayCertificacion"></div>
        <div class="popupCertificacion">
            <a href="#" id="closeBtn" class="closePopUp pull-right">
                <i class="close-icon" data-icon="9"></i>
            </a>
            <div class="headerCertificado">
                <img class="logoCertificado" src="img/logoMinisteriowhite.png" alt="">
            </div>
            <div class="contentCertificado">
                <p>- Certificado de capacitación -</p>
                <br>
                <h3>Se entrega este Certificado a</h3>
                <h2 class="name" id="nombreMentorCertificacion"></h2>
                <h3 style="margin-top: 15px;">por haber finalizado satisfactoriamente la </h3>
                <h2>Capacitación de Mentoría.</h2>
            </div>
            <div class="firmasCertificado">
                <div class="firma1">
                    <img src="img/signature1.png" alt="">
                    <hr>
                    <p><strong>Nombre y Apellido </strong><br> - Cargo -</p>
                </div>
                <div class="firma2">
                    <img src="img/signature2.png" alt="">
                    <hr>
                    <p><strong>Nombre y Apellido </strong><br> - Cargo -</p>
                </div>
                <div class="firma3">
                    <img src="img/signature3.png" alt="">
                    <hr>
                    <p><strong>Nombre y Apellido </strong><br> - Cargo -</p>
                </div>
            </div>
        </div>
        <a href="#" class="descargarCertificado">
            <i data-icon=":"></i>Descargar
        </a>
    </div>

    <!-- pop up sede -->
    <div id="popupSede" class="absolute-center">
        <div id="overlaySede"></div>
        <div class="popupSede">
            <a href="#" id="closeBtn" class="closePopUpSede pull-right">
                <i class="close-icon" data-icon="9"></i>
            </a>
            <div class="contentSede">
                <label for="sede">¿Cuál es tu sede de mentoría?</label>
                <br>
                <select name="sede" id="sede">
                    <option>Seleccioná una opción</option>
                    <option value="Rosario">Rosario</option>
                    <option value="Santa Fe">Santa Fe</option>
                    <option value="Panamá">Panamá</option>
                    <option value="Buenos Aires">Buenos Aires</option>
                </select><br>
                <input type="submit" class="closePopUpSede label100" name="submit-sede" id="submit-sede" value="Enviar">
            </div>
        </div>
    </div>

    <!-- POP UP SATISFACCION nuevo-->
    <div id="popupHacerSatisfaccion" class="absolute-center" hidden="">
        <div id="overlayCalificacion"></div>
        <div class="popupCalificacion popupSatisfaccion">
            <a href="#" id="closeBtn" class="closePopUp">
                <i class="close-icon" data-icon="9"></i>
            </a>
            <h3>Cuestionario de Diagnóstico - Inicial. Te pedimos que completes un breve cuestionario de diagnóstico del emprendimiento</h3>
            <input type="submit" class="label100" name="submit-calificacion" id="submit-calificacion" value="Responder más tarde" onclick="satisfaccionResponderMasTarde()">
            <input type="submit" class="label100" name="submit-calificacion" id="submit-calificacion" value="Aceptar" onclick="javascript:$('#popupEncuesta').show();javascript:$('#popupHacerSatisfaccion').hide()">
        </div>
    </div>

    <!-- POP UP ENCUESTA -->
    <?php include('popupEncuestaEmprendedor.php'); ?>

    <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/masonry.pkgd.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="js/main.js"></script>
    <script src="scripts/header.js"></script>
    <script src="scripts/home-con.js"></script>
</body>

</html>