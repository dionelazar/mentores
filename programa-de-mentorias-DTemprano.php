<?php 
    require_once __DIR__.'/class/class.helpers.php';
    $datosUser = Cookies::getDatosUser();
    if($datosUser->tipo_usuario == 8)
        require __DIR__.'/share/header-publico.php';
    else
        require __DIR__.'/share/header-DT.php';
?>
    <div class="content home">
        <!-- TODO EL CONTENIDO (LO QUE NO ES MENÚ NI HEADER) -->
        <div class="wrapper">
            <div class="col100">
                <ul class="tabs tabs-2 oneTab clearfix">
                    <li class="current">
                        <a href="#0">
                            <h2>Programa de Mentorías</h2>
                        </a>
                    </li>
                </ul>
                <div class="container home">
                    <div class="sesionA show">
                        <div class="timeline-item" name='Primera Fase'>
                            <h3>• Cada mentor tiene asignado: <strong>2 grupos de 3 emprendedores cada uno.</strong></h3>
                            <img src="img/icon4.png" alt="">
                            <h3>• Cada grupo tendrá: <strong>6 encuentros (quincenales) de 2 hs.</strong></h3>
                            <div class="images">
                                <img src="img/icon3.png" alt="">
                                <img src="img/icon7.png" alt="">
                                <img src="img/icon3.png" alt="">
                                <img src="img/icon7.png" alt="">
                                <img src="img/icon3.png" alt="">
                                <img src="img/icon7.png" alt="">
                            </div>
                            <h3>• Cada mentor tendrá: <strong>2 hs de seguimiento/acompañamiento mensual</strong> con el responsable de sede.</h3>
                            <img src="img/icon6.png" alt="">
                        </div>
                        <div class="timeline-item" name='Segunda Fase'>
                            <h3>• Cada mentor tiene asignado: <strong>2 emprendedores.</strong></h3>
                            <img src="img/icon5.png" alt="">
                            <h3>• Cada emprendedor tendrá: <strong>3 encuentros (mensuales) de 2 hs.</strong></h3>
                            <div class="images">
                                <img src="img/icon8.png" alt="">
                                <img src="img/icon9.png" alt="">
                                <img src="img/icon8.png" alt="">
                            </div>
                            <h3>• Cada mentor tendrá: <strong>2 hs de seguimiento/acompañamiento mensual con el responsable de sede.</strong></h3>
                            <img src="img/icon6.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/masonry.pkgd.min.js"></script>
    <script src="js/select2.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="js/main.js"></script>
    <script src="scripts/header-mentorDT.js"></script>
</body>

</html>