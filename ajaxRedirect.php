<?php 
	function debug_to_console( $data ) {
		$output = $data;
		if ( is_array( $output ) )
			$output = implode( ',', $output);

		echo "<script>console.log( 'Debug Objects: " . $output . "' );</script>";
	}
	
	if(isset($_POST['action']))
	{
		switch ($_POST['action']) //action seria la class , despues iria el metodo (call)
		{
			case 'importarMentoresExcel':
				if( $_FILES['myFileExcel']['size'] <= 0 )
					die();

				require 'class/class.usuarios.php';
				$result = Usuarios::insMentoresExcel($_FILES['myFileExcel']);
				break;

			case 'insEmprendedoresExcel':
				if( $_FILES['myFileExcel']['size'] <= 0 )
					die();

				require 'class/class.usuarios.php';
				$result = Usuarios::insEmprendedoresExcel($_FILES['myFileExcel']);
				break;

			case 'insRelacionamientoExcel':
				if( $_FILES['myFileExcel']['size'] <= 0 )
					die();

				require 'class/class.usuarios.php';
				$result = Usuarios::insRelacionamientoExcel($_FILES['myFileExcel']);
				break;

			case 'importarBorrarRelacionamientos':
				if( $_FILES['myFileExcel']['size'] <= 0 )
					die();

				require 'class/class.usuarios.php';
				$result = Usuarios::updBorrarRelacionamientoExcel($_FILES['myFileExcel']);
				break;

			case 'updFotoDePerfil':
				if( $_FILES['foto']['size'] <= 0 )
					die();

				require 'class/class.usuarios.php';
				$result = Usuarios::updImagenPerfil($_FILES['foto']);
				if(is_array($result) || is_object($result))
					echo json_encode($result);
				else
					echo $result;

				break;

			default:
				$class  = ucwords($_POST['action']);
				$method = $_POST['call'];
				require_once 'class/class.'.$_POST['action'].'.php';
				$result = $class::$method($_POST);
				if(is_array($result) || is_object($result))
					echo json_encode($result);
				else
					echo $result;
				break;
		}
	}
?>