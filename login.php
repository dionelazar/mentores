<!doctype html>
<html class="no-js bg-white" lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>Login</title>
		<meta name="description" content="">
		<meta name="author" content="Amedia">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
		<meta name="theme-color" content="#395597">
		
		<link rel="shortcut icon" href="img/icons/favicon.ico">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/icons/apple-touch-icon-114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/icons/apple-touch-icon-72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" href="img/icons/apple-touch-icon-57-precomposed.png">
		<!--<link rel="shortcut icon" href="img/icons/favicon.png">-->

		<link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700" rel="stylesheet">

		<link rel="stylesheet" href="css/icons.css">
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/bootstrap-theme.min.css">
		<link rel="stylesheet" href="css/main.css">
	</head>
	<body>
		<!--[if lt IE 8]>
			<p class="browserupgrade">Estás usando un navegador <strong>desactualizado</strong>. Por favor, <a href="http://browsehappy.com/">actualizá tu navegador</a> para una mejor experiencia.</p>
		<![endif]-->
		<section class="login">
			<form action="javascript:login()" name="login" id="login">
				<img src="img/2.png" alt="Ministerio de produccion de la Nacion" class="logo-login">

				<input type="text" name="login-user" id="login-user" placeholder="Nombre usuario">

				<input type="password" name="login-pass" id="login-pass" placeholder="Contraseña">

				<a href="javascript:void(0)"><input type="submit" name="login-submit" value="Iniciar sesión"></a>
				<a href="recuperarpass.php" class="subtext-it">Olvidé mi contraseña</a>
				
			</form>
		</section>

		<div id="popUpTerminos_y_Condiciones_mentor" class="absolute-center" hidden="">
	        <div id="overlayCalificacion"></div>
	        <div class="popupCalificacion popupSatisfaccion">
	            <h3>PROGRAMA DE COOPERACIÓN MUTUA DE MENTORÍA</h3>

				<h4>CONSIDERANDO:</h4>

				<p>A) Dialal S.R.L. (la “Empresa”) es una sociedad que ha diseñado un programa de Mentoría denominado “Programa de Mentorías” con el fin de desarrollar una red federal de Mentores que realicen un trabajo voluntario ad honorem para asesorar a Emprendedores, en adelante el “Programa”. </p>
				<p>B) Los Mentores son personas especializadas en el proceso de creación y puesta en marcha de una empresa, dispuestos a compartir su experiencia vivencial con Emprendedores, orientándolos en el enfoque de sus negocios y en la toma de decisiones que, en cada caso, éstos y sus emprendimientos requieran. </p>
				<p>C) Los Emprendedores son personas que construyen un capital y/o un emprendimiento y/o una empresa a través de sus propios recursos, a su propio riesgo y/o por su propia iniciativa.</p>
				<p>D) Los Emprendedores y Mentores son seleccionados por la Empresa para participar del Programa, en base a disponibilidad de cupos, teniendo en cuenta los criterios de selección del Programa que se establezcan. Los Emprendedores y Mentores seleccionados establecen una relación con el fin de que los Emprendedores reciban de los Mentores orientación y soporte para el desarrollo de sus emprendimientos. Los encuentros entre Mentores y Emprendedores son organizados por la Empresa ya sea en sesiones horarias, semanales y/o mensuales, individuales y/o grupales, online y/o presenciales.</p>
				<p>E) El Programa constará de dos tipos de mentoría, en las cuales serán asignados los Emprendedores según su perfil (Mentoría Desarrollo Temprano y Mentoría Consolidación), y de dos (2) fases: 1) Fase 1 (implementación de mentorías grupales e individuales según perfil, para los Emprendedores seleccionados); 2) Fase 2 (implementación de mentorías individuales para un cupo de los Emprendedores participantes de la Fase 1, definido por la Empresa al llegar a dicha etapa). La Empresa decidirá qué Emprendedor avanzará o no de etapa.</p>

				<h3>TÉRMINOS Y CONDICIONES:</h3>

				<p>1) El Mentor reúne conocimientos técnicos adquiridos en su propia experiencia al haber creado, por lo menos, una empresa, como ser habilidades gerenciales, competencias emprendedoras, identificación de oportunidades, etc. </p>
				<p>2) La función del Menor será escuchar, asesorar, apoyar, motivar y guiar al Emprendedor para que desarrolle las capacidades necesarias para capacitar al Emprendedor en su proyecto. El Mentor es un guía, no un consultor o experto. El Mentor no asume obligaciones de resultado respecto al proyecto del Emprendedor pues únicamente pone a su disposición su experiencia en la puesta en marcha y desarrollo inicial de empresas para guiar y cooperar a éste en el caso en concreto.</p>
				<p>3) La tarea del Mentor se realizará a través de intervenciones llevadas a cabo en sesiones, en los lugares, fechas y horarios a determinar por la Empresa, en las que realizará un seguimiento de lo trabajado por los Emprendedores, definiendo tareas y metas, fortaleciendo el modelo de negocios. Además, podrá hacer el Mentor un seguimiento a los Emprendedores mediante comunicaciones telefónicas y/o correo electrónico. </p>
				<p>4) La Empresa monitorea el progreso de las sesiones, pero no provee servicio alguno a Mentores o Emprendedores que no esté estipulado en el presente. La Empresa se limita a identificar y seleccionar al Mentor, proveer un espacio de formación, presentarlo a su/s Emprendedor/es, y monitorear y evaluar el progreso de la mentoría. La Empresa no proveerá servicios adicionales a lo aquí establecido. </p>
				<p>5) La Empresa presta al Mentor los siguientes servicios: i) espacio de formación y capacitación acerca de la metodología del Programa; ii) reuniones mensuales de seguimiento, individuales o grupales, según considere la Empresa; iii) comunicación a través de email o teléfono con equipo de profesionales asesores; vi) caja de herramientas con material de soporte para la mentoría. </p>
				<p>6) El Mentor deberá reportar los avances de sus sesiones a la Empresa en cada oportunidad, decidiendo ésta qué Emprendedor continuará con el programa y cuál no.</p>
				<p>7) El servicio de Mentoría prestado por el Mentor para el Programa es gratuito, no pudiendo el Mentor exigir retribución alguna, de la Empresa y/o del Emprendedor.</p> 
				<p>8) Este Programa tiene un plazo de tres (3) meses. Sin perjuicio de ello, Mentor y Empresa podrán resolver su relación en cualquier momento. En caso de quien resuelva sea el Mentor, deberá comunicar tal decisión por medio fehaciente a la Empresa con una antelación de como mínimo 30 días previos al cese, a fin de que la Empresa pueda conseguir un reemplazo. La comunicación al Emprendedor se hará en igual plazo, y deberá realizarla el Mentor.</p>
				<p>9) El Mentor y el Emprendedor no podrán utilizar, sin autorización por escrito previa de la Empresa, ningún logo ni mención de la Empresa en material publicitario, anuncios, etc.</p> 
				<p>10) El Mentor debe comunicar a la Empresa cualquier situación que surja durante la mentoría que pudiera presentar disputas legales. Sin perjuicio de ello, la Empresa no será responsable por ningún reclamo que pudiera surgir contra el Mentor. </p>
				<p>11) Los términos del Programa son confidenciales, obligándose Mentor y Empresa a no compartir ninguna información, con excepción de aquella que la Empresa deba informar para llevar adelante el programa de mentorías.</p>
				<p>12) Mentoría: Los servicios del Mentor al/los Emprendedor/es son:
				12.1. Mentoría Desarrollo Temprano: i) mentoría colaborativa de tres (3) Emprendedores por cada Mentor; ii) un (1) encuentro cada quince (15) días de dos (2) horas de duración, durante tres (3) meses. 
				12.2. Mentoría Consolidación: i) mentoría individual, un (1) Emprendedor por Mentor; ii) tres (3) encuentros, uno (1) por mes de dos (2) horas de duración, durante tres (3) meses.</p>
				<p>13) El Mentor y el Emprendedor mantendrán en estricta confidencialidad cualquier información relativa al Programa que se obtenga de las reuniones, sesiones, comunicaciones, etc., entre Mentor y Emprendedor. Sin perjuicio, ambos deberán otorgar dicha información a la Empresa a su solo requerimiento. </p>
				<p>14) El Mentor no realizará tareas del Emprendedor, ni contribuirá de manera directa a la operatoria de su emprendimiento.</p>
				<p>15) Mentor y Emprendedor podrán disolver la relación en cualquier momento durante el transcurso de la mentoría. El Mentor deberá comunicarlo al Emprendedor con una antelación mínima de 30 días. Finalizada la mentoría, la relación entre Mentor y Emprendedor queda finalizada sin necesidad de comunicación alguna.</p>
				<p>16) Entre Mentor, Emprendedor y/o Empresa no existe relación laboral, sociedad, asociación, o relación de ninguna índole, siendo personas completamente independientes entre sí, no pudiendo reclamar nada ninguna de la otra. </p>
				<p>17) Todas las decisiones relativas al emprendimiento son tomadas exclusivamente por el Emprendedor, quien asume entera responsabilidad por esto, manteniendo indemne al Mentor y al Empresa ante cualquier reclamo en caso de que el emprendimiento no resultare como lo deseado. </p>
				<p>18) La Empresa no es responsable por los servicios prestados por el Mentor. </p>
				<p>19) El Mentor declara y presta conformidad en que, para el caso de existir contienda alguna relativa al Programa, se someterá a la jurisdicción exclusiva de los Tribunales Ordinarios Comerciales de la Capital Federal.</p>

	            <input type="submit" class="label100" name="submit-calificacion" id="submit-calificacion" value="Aceptar" onclick="myRedirect()">
	        </div>
	    </div>

	    <div id="popUpTerminos_y_Condiciones_emprendedor" class="absolute-center" hidden="">
	        <div id="overlayCalificacion"></div>
	        <div class="popupCalificacion popupSatisfaccion">
	            <h3>PROGRAMA DE MENTORÍA PARA EMPRENDEDORES</h3>

				<h3>CONSIDERANDOS:</h3>

				<p>A) Dialal S.R.L. (la “Empresa”) es una sociedad que ha diseñado un programa de Mentoría denominado “Programa de Mentorías”, que conforma una red federal de Mentores a fin de que estos realicen un trabajo voluntario ad honorem para asesorar a Emprendedores, en adelante el “Programa”. </p>
				<p>B) Los Mentores son personas especializadas en el proceso de creación y puesta en marcha de una empresa, dispuestos a compartir su experiencia vivencial con Emprendedores, orientándolos en el enfoque de sus negocios y en la toma de decisiones. </p>
				<p>C) Los Emprendedores son personas que construyen un capital y/o un emprendimiento y/o una empresa a través de sus propios recursos, a su propio riesgo y/o su propia iniciativa.</p>
				<p>D) Los Emprendedores y Mentores son seleccionados por la Empresa para participar del Programa, en base a disponibilidad de cupos, teniendo en cuenta los criterios de selección del Programa. Los Emprendedores y Mentores seleccionados establecen una relación con el fin de que los Emprendedores reciban de los Mentores orientación y soporte para el desarrollo de sus emprendimientos. Los encuentros entre Mentores y Emprendedores son organizados por la Empresa, ya sea en sesiones horarias, semanales y/o mensuales, individuales y/o grupales, online y/o presenciales.</p>
				<p>E) El Programa constará de dos tipos de mentoría, en las cuales serán asignados los Emprendedores según su perfil (Mentoría Desarrollo Temprano y Mentoría Consolidación), y de dos (2) fases: 1) Fase 1 (implementación de mentorías grupales e individuales según perfil, para los Emprendedores seleccionados); 2) Fase 2 (implementación de mentorías individuales para un cupo de los Emprendedores participantes de la Fase 1, definido por la Empresa al llegar a dicha etapa). La Empresa decidirá qué Emprendedor avanzará o no de etapa.</p>

				<h3>TÉRMINOS Y CONDICIONES:</h3>

				<p>1) El Mentor, reuniendo conocimientos técnicos adquiridos en su propia experiencia al crear una empresa, como ser habilidades gerenciales, competencias emprendedoras, identificación de oportunidades, etc., acompañará al Emprendedor en el desarrollo de su emprendimiento.</p>
				<p>2) La función del Mentor será escuchar, asesorar, apoyar, motivar y guiar al Emprendedor para que éste desarrolle las capacidades necesarias para desplegar su proyecto. El Mentor es un guía, no un consultor o experto. El Mentor no asume obligaciones de resultado respecto al proyecto del Emprendedor pues únicamente pone a su disposición su experiencia en la puesta en marcha y desarrollo inicial de empresas para guiar y cooperar a éste en el caso en concreto. El Emprendedor es completamente libre de seguir o no la guía del Mentor, siendo el primero el único responsable por su emprendimiento.</p>
				<p>3) La tarea del Mentor con el Emprendedor se realizará a través de intervenciones llevadas a cabo en sesiones, en los lugares, fechas y horarios a determinar por la Empresa, en las que el Mentor realizará un seguimiento de lo trabajado por los Emprendedores, definiendo tareas y metas, fortaleciendo el modelo de negocios. Además, podrá hacer el Mentor un seguimiento a los Emprendedores mediante comunicaciones telefónicas y/o correo electrónico. </p>
				<p>4) La Empresa monitorea el progreso de las sesiones, pero no provee servicio alguno a Mentores o Emprendedores que no esté estipulado en el presente. La Empresa se limita a identificar y seleccionar al Mentor, proveer un espacio de formación, presentarlo a su/s Emprendedor/es, y monitorear y evaluar el progreso de la mentoría. La Empresa no proveerá servicios adicionales a lo aquí establecido. </p>
				<p>5) El Mentor deberá reportar los avances de sus sesiones a la Empresa en cada oportunidad, decidiendo ésta qué Emprendedor continuará con el programa y cuál no. En este sentido, el Emprendedor no puede negarse a que el Mentor de información a la Empresa sobre el emprendimiento.</p>
				<p>6) El servicio de Mentoría prestado por el Mentor al Emprendedor es gratuito, no pudiendo el Mentor exigir retribución alguna, de la Empresa y/o del Emprendedor.</p> 
				<p>7) Sin perjuicio de lo estipulado en el punto 5) anterior, la participación del Emprendedor en este Programa no impide la formación de un acuerdo entre el Mentor y el Emprendedor, ajeno a la Empresa, por el cual el Mentor preste servicios al Emprendedor a cambio de una retribución económica. Sin perjuicio de ello, si este acuerdo se celebra, el mismo excede el presente Programa, lo reemplaza y éste debe automáticamente finalizarse.</p>
				<p>8) Este Programa tiene un plazo de tres (3) meses. Sin perjuicio de ello, Emprendedor y Empresa podrán resolver su relación en cualquier momento, sin motivo alguno. La resolución no otorga derecho alguno a ninguna de las partes a realizar reclamo a la otra de ninguna índole. El Emprendedor tampoco podrá efectuar reclamo alguno contra el Mentor.</p>
				<p>9) El Emprendedor no podrá utilizar, sin autorización por escrito previa de la Empresa, ningún logo ni mención de la Empresa en material publicitario, anuncios, etc., sean o no relativos a su emprendimiento.</p>
				<p>10) El Emprendedor debe comunicar a la Empresa cualquier situación que surja durante la mentoría que pudiera presentar disputas legales. Sin perjuicio de ello, la Empresa no será responsable por ningún reclamo que pudiera surgir como consecuencia de esto. </p>
				<p>11) Los términos del Programa son confidenciales, obligándose Emprendedor y Empresa a no compartir ninguna información, con excepción de aquella que la Empresa deba informar para llevar adelante el programa de mentorías.</p>
				<p>12) Mentoría: Los servicios que el Emprendedor recibirá del Mentor son:
				12.1. Mentoría Desarrollo Temprano: i) mentoría colaborativa de tres (3) Emprendedores por cada Mentor; ii) un (1) encuentro cada quince (15) días de dos (2) horas de duración, durante tres (3) meses.
				12.2. Mentoría Consolidación: i) mentoría individual, un (1) Emprendedor por Mentor; ii) tres (3) encuentros, uno (1) por mes de dos (2) horas de duración, durante tres (3) meses.</p>
				<p>13) El Mentor no realizará tareas del Emprendedor, ni contribuirá de manera directa a la operatoria de su emprendimiento.</p>
				<p>14) Mentor y Emprendedor podrán disolver la relación en cualquier momento durante el transcurso de la mentoría, sin que ello pueda generar reclamo alguno de las partes entre sí o hacia la Empresa.</p>
				<p>15) Entre Mentor, Emprendedor y/o Empresa no existe relación laboral, sociedad, asociación, o relación de ninguna índole, siendo todas personas completamente independientes entre sí, no pudiendo reclamar nada ninguna de la otra.</p>
				<p>16) Todas las decisiones relativas al emprendimiento son tomadas exclusivamente por el Emprendedor, quien asume entera responsabilidad por esto, manteniendo indemne al Mentor y a la Empresa ante cualquier reclamo en caso de que el emprendimiento no resultare como lo deseado. El Mentor sólo pone a disposición del Emprendedor su conocimiento y experiencia en la creación de una empresa, no resultando de ninguna forma vinculante lo que le transmita.</p>
				<p>17) El Emprendedor declara que la Empresa no es responsable por los servicios prestados por el Mentor al Emprendedor. El Emprendedor no podrá realizar reclamo alguno ni a la Empresa ni al Mentor.</p>
				<p>18) El Emprendedor declara y presta conformidad en que, para el caso de existir contienda alguna relativa al Programa, se someterá a la jurisdicción exclusiva de los Tribunales Ordinarios Comerciales de la Capital Federal. </p>
	            <input type="submit" class="label100" name="submit-calificacion" id="submit-calificacion" value="Aceptar" onclick="myRedirect()">
	        </div>
	    </div>

		<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
		<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
		<script src="js/plugins.js"></script>
		<script src="js/imagesloaded.pkgd.min.js"></script>
		<script src="js/masonry.pkgd.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/bootbox.min.js"></script>
		<script src="js/main.js"></script>
		<script type="text/javascript">
			var linkRedirect = '';

			function myRedirect()
			{
				location.replace(linkRedirect);
			}

			function login()
			{
				var user = $('#login-user').val();
				var pass = $('#login-pass').val();

				$.ajax({
			        type: 'POST',
			        url: 'ajaxRedirect.php',
			        datatype: 'html',
			        async : false,
			        data: {
			            action: 'usuarios',
			            call: 'login',
			            usuario : user,
			            password : pass,
			        },
			        success : function(Data){
			        	console.log(Data);
			        	var miData = JSON.parse(Data);
			            if(miData.cod_mensaje != -1){
			            	bootbox.alert({
								message: miData.mensaje,
								backdrop: true,
							});
			            	return;
			            }

		            	if(miData.mensaje == "Primer login"){
		            		linkRedirect = miData.redirect;
		            		if(miData.tipo_usuario == 2 || miData.tipo_usuario == 3)
		            			$('#popUpTerminos_y_Condiciones_mentor').show();
		            		else
		            			$('#popUpTerminos_y_Condiciones_emprendedor').show();
		            	}else{
		            		location.replace(miData.redirect);	
		            	}
			        }
			    });
			}
		</script>
	</body>
</html>
