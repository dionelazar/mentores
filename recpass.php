<?php 
	if(isset($_GET['id']))
	{
		require_once __DIR__.'/class/class.helpers.php';
		require_once __DIR__.'/class/class.usuarios.php';
		$randomString = $_GET['id'];
		$usuario = Usuarios::getIdUsuarioByRandomString($randomString);
	}else{
		die();
	}
	
?>
<!doctype html>
<html class="no-js bg-white" lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>Recuperar contraseña</title>
		<meta name="description" content="">
		<meta name="author" content="Amedia">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
		<meta name="theme-color" content="#395597">

		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/icons/apple-touch-icon-114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/icons/apple-touch-icon-72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" href="img/icons/apple-touch-icon-57-precomposed.png">
		<!--<link rel="shortcut icon" href="img/icons/favicon.png">-->

		<link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700" rel="stylesheet">

		<link rel="stylesheet" href="css/icons.css">
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/bootstrap-theme.min.css">
		<link rel="stylesheet" href="css/main.css">
	</head>
	<body>
		<!--[if lt IE 8]>
			<p class="browserupgrade">Estás usando un navegador <strong>desactualizado</strong>. Por favor, <a href="http://browsehappy.com/">actualizá tu navegador</a> para una mejor experiencia.</p>
		<![endif]-->
		<section class="login">
			<form action="javascript:void(0)" name="login" id="login">
				<img src="img/logoMinisteriovert.png" alt="Ministerio de produccion de la Nacion" class="logo-login">

				<input type="password" name="pass" id="pass" placeholder="Nueva contraseña">

				<a href="javascript:void(0)"><input type="button" name="recuperar-pass-btn" id="recuperar-pass-btn" value="Nueva contraseña"></a>
				<a href="login.php" class="subtext-it">Loguearse</a>
			</form>
		</section>

		<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
		<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
		<script src="js/plugins.js"></script>
		<script src="js/imagesloaded.pkgd.min.js"></script>
		<script src="js/masonry.pkgd.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/bootbox.min.js"></script>
		<script src="js/main.js"></script>
		<script type="text/javascript">
			$('#recuperar-pass-btn').click(function(){
				recuperarPass();
			});

			function recuperarPass()
			{
				var pass = $('#pass').val();

				$.ajax({
			        type: 'POST',
			        url: 'ajaxRedirect.php',
			        datatype: 'html',
			        async : false,
			        data: {
			            action: 'usuarios',
			            call: 'updPassword',
			            password : pass,
			        },
			        success : function(Data){debugger
			        	var miData = JSON.parse(Data);
			            bootbox.alert(miData.mensaje);
			        }
			    });
			}
		</script>
	</body>
</html>
