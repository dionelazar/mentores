<?php
    require_once('class/class.helpers.php');

    if(!isset($_COOKIE['tipo_usuario']))
        header('location:login.php');

    $tipo_usuario = Encriptacion::desencriptar($_COOKIE['tipo_usuario']);

    if(!is_int(intval($tipo_usuario)))
        header('location:login.php');

    switch ($tipo_usuario) {
        case 2:
            require __DIR__.'/share/header-mentorDT.php';  
            break;

        case 3:
            require __DIR__.'/share/header-mentor-con.php';  
            break;

        case 4:
            require __DIR__.'/share/header-DT.php';  
            break;

        case 5:
            require __DIR__.'/share/header-con.php';  
            break;

        default:
            header('location:login.php');
            break;
    }
?>
    <div class="content home">
        <!-- TODO EL CONTENIDO (LO QUE NO ES MENÚ NI HEADER) -->
        <div class="wrapper">
            <div class="col100">
                <ul class="tabs tabs-2 oneTab clearfix">
                    <li class="current">
                        <a href="#0">
                            <h2>Grupo <strong></strong></h2>
                        </a>
                    </li>
                </ul>
                <div class="">
                    <ul class="tabs tabs-4 clearfix" id="horizontalTabEmprendedores">

                    </ul>

                    <div class="container home">
                        <div class="sesionA show">
                            <div id="aviso-abandono"></div>
                            <div id="marcar-abandono" class="">
                                <div class="col-100 pad10">
                                    <div class="feature-panel">
                                        <ul class="breadcrumb sesiones9">
                                            <li id="flechaProgresoSesion1">
                                                <a class="visibleDesktop" href="#">1ª Sesión</a>
                                                <a class="visibleSmInline" href="#">1ª</a>
                                            </li>
                                            <li id="flechaProgresoSesion2">
                                                <a class="visibleDesktop" href="#">2ª Sesión</a>
                                                <a class="visibleSmInline" href="#">2ª</a>
                                            </li>
                                            <li id="flechaProgresoSesion3">
                                                <a class="visibleDesktop" href="#">3ª Sesión</a>
                                                <a class="visibleSmInline" href="#">3ª</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <!--div class="col100">
                                    <label class="switch">
                                        <input class="btn" type="button" onclick="marcarAbandono()" value="¿El emprendedor abandonó?">
                                        <span id="estado_emprendedor"></span>
                                    </label>
                                </div-->
                                    <br>
                                <div id="sesionesGrupo1">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="popupCalificacion" class="absolute-center">
        <div id="overlayCalificacion"></div>
        <div class="popupCalificacion">
            <a href="#" id="closeBtn" class="closePopUp">
                <i class="close-icon" data-icon="9"></i>
            </a>

            <h3 style="text-align: left;">Calificar sesión </h3>
            <div class="titleForm nombre-participante" id="nombre_emprendedor_seleccionado"></div>
            <br>
            <fieldset class="label30">
                <label for="Asistió">¿Asistió?</label>
                <br>
                <input type="radio" name="asistencia1" id="asistencia1" value="1">
                <label for="asistencia1"><i class="fa fa-check"></i></label>
                <input type="radio" name="asistencia1" id="asistencia2" value="2">
                <label for="asistencia2"><i class="fa fa-times"></i></label>
                <input type="radio" name="asistencia1" id="asistencia3" value="3" onclick="marcarAbandono()">
                <label for="asistencia3">Si el emprendedor no continúa, marcá acá</label>
            </fieldset>
            <hr>
            <div class="cuestionario" style="display: none;">
                <!--div class="label100">
                    <div class="titleForm">Marcar Asistencia (sombrea al emprendedor si asistió a la sesión)</div>
                    <fieldset class="label100">
                        <div id="calificarMarcarAsistencia"></div>
                    </fieldset>
                </div-->

                <div class="label100">
                    <label for="rating">Calificá esta sesión</label>
                </div>
                <fieldset class="rating">
                    <input type="radio" id="star5" name="rating" value="5" />
                    <label class="full" for="star5" title="Awesome - 5 stars"></label>
                    <input type="radio" id="star4half" name="rating" value="4.5" />
                    <label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
                    <input type="radio" id="star4" name="rating" value="4" />
                    <label class="full" for="star4" title="Pretty good - 4 stars"></label>
                    <input type="radio" id="star3half" name="rating" value="3.5" />
                    <label class="half" for="star3half" title="Meh - 3.5 stars"></label>
                    <input type="radio" id="star3" name="rating" value="3" />
                    <label class="full" for="star3" title="Meh - 3 stars"></label>
                    <input type="radio" id="star2half" name="rating" value="2.5" />
                    <label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
                    <input type="radio" id="star2" name="rating" value="2" />
                    <label class="full" for="star2" title="Kinda bad - 2 stars"></label>
                    <input type="radio" id="star1half" name="rating" value="1.5" />
                    <label class="half" for="star1half" title="Meh - 1.5 stars"></label>
                    <input type="radio" id="star1" name="rating" value="1" />
                    <label class="full" for="star1" title="Sucks big time - 1 star"></label>
                    <input type="radio" id="starhalf" name="rating" value="0.5" />
                    <label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
                </fieldset>
                <fieldset class="label100">
                    <label for="calificacion1"><div id="pregunta1"></div>
                    <div id="respuestas1"></div>

                </fieldset>
                <fieldset class="label100">
                    <label for="calificacion2"><div id="pregunta2"></div>
                    <div id="respuestas2"></div>

                </fieldset>
            </div>
            <fieldset class="label100">
                <label for="calificacion2">Describí brevemente como te pareció la sesión</label>
                <textarea class="label100" name="calificacion-texto" id="calificacion-texto"></textarea>
            </fieldset>
            <!--button class="cameraButton"><i data-icon="O"></i>Sacar una Selfie</button-->
            <input type="submit" class="label100" name="submit-calificacion" id="submit-calificacion" value="Calificar" onclick="calificar()">

        </div>
    </div>

    <div id="popupSede" class="absolute-center" hidden="">
        <div id="overlayCalificacion"></div>
        <div class="popupSede">
            <a href="#" id="closeBtn" class="closePopUpSede pull-right">
                <i class="close-icon" data-icon="9"></i>
            </a>
            <div class="contentSede">
                <label for="sede">¿Cuál es tu sede de mentoría?</label>
                <br>
                <select name="selectSede" id="selectSede">
                    <option>Seleccioná una sede</option>
                </select><br>

                <label for="sede">¿Cuál es tu ciudad?</label>
                <br>
                <select name="selectProvincia" id="selectProvincia">
                    <option>Seleccioná una provincia</option>
                </select><br>
                <select name="selectCiudad" id="selectCiudad">
                    <option>Seleccioná una ciudad</option>
                </select><br>
                <input type="button" class="label100" value="Enviar" onclick="elegirSede()">
            </div>
        </div>
    </div>

    <!-- POP UP SATISFACCION nuevo-->
    <div id="popupHacerSatisfaccion" class="absolute-center" hidden="">
        <div id="overlayCalificacion"></div>
        <div class="popupCalificacion popupSatisfaccion">
            <a href="#" id="closeBtn" class="closePopUp">
                <i class="close-icon" data-icon="9"></i>
            </a>
            <h3>Encuesta de Satisfacción - Formación. Te pedimos que completes una breve evaluación sobre el proceso de formación</h3>
            <input type="submit" class="label100" name="submit-calificacion" id="submit-calificacion" value="Responder más tarde" onclick="satisfaccionResponderMasTarde()">
            <input type="submit" class="label100" name="submit-calificacion" id="submit-calificacion" value="Aceptar" onclick="javascript:$('#popupSatisfaccion').show()">
        </div>
    </div>
    
    <div id="popupSatisfaccion" class="absolute-center" hidden="">
        <div id="overlayCalificacion"></div>
        <div class="popupCalificacion popupSatisfaccion">
            <a href="#" id="closeBtn" class="closePopUp">
                <i class="close-icon" data-icon="9"></i>
            </a>
            <h3>Por favor, te pedimos que completes una breve evaluación sobre el proceso de formación, utilizando una escala de valoración entre 1 (baja) y 5 (alta).</h3>
            <div class="label100">
                <label for="rating1">1. Los contenidos son relevantes para el programa de mentoría.*</label>
            </div>
            <fieldset class="rating">
                <input type="radio" id="sstarRelevancia5" name="sstarRelevancia" value="5" />
                <label class="full" for="sstarRelevancia5" title="Awesome - 5 stars"></label>
                <input type="radio" id="sstarRelevancia4half" name="sstarRelevancia" value="4.5" />
                <label class="half" for="sstarRelevancia4half" title="Pretty good - 4.5 stars"></label>
                <input type="radio" id="sstarRelevancia4" name="sstarRelevancia" value="4" />
                <label class="full" for="sstarRelevancia4" title="Pretty good - 4 stars"></label>
                <input type="radio" id="sstarRelevancia3half" name="sstarRelevancia" value="3.5" />
                <label class="half" for="sstarRelevancia3half" title="Meh - 3.5 stars"></label>
                <input type="radio" id="sstarRelevancia3" name="sstarRelevancia" value="3" />
                <label class="full" for="sstarRelevancia3" title="Meh - 3 stars"></label>
                <input type="radio" id="sstarRelevancia2half" name="sstarRelevancia" value="2.5" />
                <label class="half" for="sstarRelevancia2half" title="Kinda bad - 2.5 stars"></label>
                <input type="radio" id="sstarRelevancia2" name="sstarRelevancia" value="2" />
                <label class="full" for="sstarRelevancia2" title="Kinda bad - 2 stars"></label>
                <input type="radio" id="sstarRelevancia1half" name="sstarRelevancia" value="1.5" />
                <label class="half" for="sstarRelevancia1half" title="Meh - 1.5 stars"></label>
                <input type="radio" id="sstarRelevancia1" name="sstarRelevancia" value="1" />
                <label class="full" for="sstarRelevancia1" title="Sucks big time - 1 star"></label>
                <input type="radio" id="sstarRelevanciahalf" name="sstarRelevancia" value="0.5" />
                <label class="half" for="sstarRelevanciahalf" title="Sucks big time - 0.5 stars"></label>
            </fieldset>
            <div class="label100">
                <label for="rating2">2. Los contenidos fueron abordados con la debida profundidad.*</label>
            </div><br>
            <fieldset class="rating">
                <input type="radio" id="sstarProfundidad5" name="sstarProfundidad" value="5" />
                <label class="full" for="sstarProfundidad5" title="Awesome - 5 stars"></label>
                <input type="radio" id="sstarProfundidad4half" name="sstarProfundidad" value="4.5" />
                <label class="half" for="sstarProfundidad4half" title="Pretty good - 4.5 stars"></label>
                <input type="radio" id="sstarProfundidad4" name="sstarProfundidad" value="4" />
                <label class="full" for="sstarProfundidad4" title="Pretty good - 4 stars"></label>
                <input type="radio" id="sstarProfundidad3half" name="sstarProfundidad" value="3.5" />
                <label class="half" for="sstarProfundidad3half" title="Meh - 3.5 stars"></label>
                <input type="radio" id="sstarProfundidad3" name="sstarProfundidad" value="3" />
                <label class="full" for="sstarProfundidad3" title="Meh - 3 stars"></label>
                <input type="radio" id="sstarProfundidad2half" name="sstarProfundidad" value="2.5" />
                <label class="half" for="sstarProfundidad2half" title="Kinda bad - 2.5 stars"></label>
                <input type="radio" id="sstarProfundidad2" name="sstarProfundidad" value="2" />
                <label class="full" for="sstarProfundidad2" title="Kinda bad - 2 stars"></label>
                <input type="radio" id="sstarProfundidad1half" name="sstarProfundidad" value="1.5" />
                <label class="half" for="sstarProfundidad1half" title="Meh - 1.5 stars"></label>
                <input type="radio" id="sstarProfundidad1" name="sstarProfundidad" value="1" />
                <label class="full" for="sstarProfundidad1" title="Sucks big time - 1 star"></label>
                <input type="radio" id="sstarProfundidadhalf" name="sstarProfundidad" value="0.5" />
                <label class="half" for="sstarProfundidadhalf" title="Sucks big time - 0.5 stars"></label>
            </fieldset><br>
            <div class="label100">
                <label for="rating3">3. La metodología ha sido acorde a los contenidos.*</label>
            </div><br>
            <fieldset class="rating">
                <input type="radio" id="sstarMetodologia5" name="sstarMetodologia" value="5" />
                <label class="full" for="sstarMetodologia5" title="Awesome - 5 stars"></label>
                <input type="radio" id="sstarMetodologia4half" name="sstarMetodologia" value="4.5" />
                <label class="half" for="sstarMetodologia4half" title="Pretty good - 4.5 stars"></label>
                <input type="radio" id="sstarMetodologia4" name="sstarMetodologia" value="4" />
                <label class="full" for="sstarMetodologia4" title="Pretty good - 4 stars"></label>
                <input type="radio" id="sstarMetodologia3half" name="sstarMetodologia" value="3.5" />
                <label class="half" for="sstarMetodologia3half" title="Meh - 3.5 stars"></label>
                <input type="radio" id="sstarMetodologia3" name="sstarMetodologia" value="3" />
                <label class="full" for="sstarMetodologia3" title="Meh - 3 stars"></label>
                <input type="radio" id="sstarMetodologia2half" name="sstarMetodologia" value="2.5" />
                <label class="half" for="sstarMetodologia2half" title="Kinda bad - 2.5 stars"></label>
                <input type="radio" id="sstarMetodologia2" name="sstarMetodologia" value="2" />
                <label class="full" for="sstarMetodologia2" title="Kinda bad - 2 stars"></label>
                <input type="radio" id="sstarMetodologia1half" name="sstarMetodologia" value="1.5" />
                <label class="half" for="sstarMetodologia1half" title="Meh - 1.5 stars"></label>
                <input type="radio" id="sstarMetodologia1" name="sstarMetodologia" value="1" />
                <label class="full" for="sstarMetodologia1" title="Sucks big time - 1 star"></label>
                <input type="radio" id="sstarMetodologiahalf" name="sstarMetodologia" value="0.5" />
                <label class="half" for="sstarMetodologiahalf" title="Sucks big time - 0.5 stars"></label>
            </fieldset><br>
            <div class="label100">
                <label for="rating4">4. Los facilitadores contaban con un adecuado dominio de los temas.*</label>
            </div><br>
            <fieldset class="rating">
                <input type="radio" id="sstarFacDominio5" name="sstarFacDominio" value="5" />
                <label class="full" for="sstarFacDominio5" title="Awesome - 5 stars"></label>
                <input type="radio" id="sstarFacDominio4half" name="sstarFacDominio" value="4.5" />
                <label class="half" for="sstarFacDominio4half" title="Pretty good - 4.5 stars"></label>
                <input type="radio" id="sstarFacDominio4" name="sstarFacDominio" value="4" />
                <label class="full" for="sstarFacDominio4" title="Pretty good - 4 stars"></label>
                <input type="radio" id="sstarFacDominio3half" name="sstarFacDominio" value="3.5" />
                <label class="half" for="sstarFacDominio3half" title="Meh - 3.5 stars"></label>
                <input type="radio" id="sstarFacDominio3" name="sstarFacDominio" value="3" />
                <label class="full" for="sstarFacDominio3" title="Meh - 3 stars"></label>
                <input type="radio" id="sstarFacDominio2half" name="sstarFacDominio" value="2.5" />
                <label class="half" for="sstarFacDominio2half" title="Kinda bad - 2.5 stars"></label>
                <input type="radio" id="sstarFacDominio2" name="sstarFacDominio" value="2" />
                <label class="full" for="sstarFacDominio2" title="Kinda bad - 2 stars"></label>
                <input type="radio" id="sstarFacDominio1half" name="sstarFacDominio" value="1.5" />
                <label class="half" for="sstarFacDominio1half" title="Meh - 1.5 stars"></label>
                <input type="radio" id="sstarFacDominio1" name="sstarFacDominio" value="1" />
                <label class="full" for="sstarFacDominio1" title="Sucks big time - 1 star"></label>
                <input type="radio" id="sstarFacDominiohalf" name="sstarFacDominio" value="0.5" />
                <label class="half" for="sstarFacDominiohalf" title="Sucks big time - 0.5 stars"></label>
            </fieldset><br>
            <div class="label100">
                <label for="rating5">5. Los facilitadores han transmitido los conceptos con claridad.*</label>
            </div><br>
            <fieldset class="rating">
                <input type="radio" id="sstarFacClaridad5" name="sstarFacClaridad" value="5" />
                <label class="full" for="sstarFacClaridad5" title="Awesome - 5 stars"></label>
                <input type="radio" id="sstarFacClaridad4half" name="sstarFacClaridad" value="4.5" />
                <label class="half" for="sstarFacClaridad4half" title="Pretty good - 4.5 stars"></label>
                <input type="radio" id="sstarFacClaridad4" name="sstarFacClaridad" value="4" />
                <label class="full" for="sstarFacClaridad4" title="Pretty good - 4 stars"></label>
                <input type="radio" id="sstarFacClaridad3half" name="sstarFacClaridad" value="3.5" />
                <label class="half" for="sstarFacClaridad3half" title="Meh - 3.5 stars"></label>
                <input type="radio" id="sstarFacClaridad3" name="sstarFacClaridad" value="3" />
                <label class="full" for="sstarFacClaridad3" title="Meh - 3 stars"></label>
                <input type="radio" id="sstarFacClaridad2half" name="sstarFacClaridad" value="2.5" />
                <label class="half" for="sstarFacClaridad2half" title="Kinda bad - 2.5 stars"></label>
                <input type="radio" id="sstarFacClaridad2" name="sstarFacClaridad" value="2" />
                <label class="full" for="sstarFacClaridad2" title="Kinda bad - 2 stars"></label>
                <input type="radio" id="sstarFacClaridad1half" name="sstarFacClaridad" value="1.5" />
                <label class="half" for="sstarFacClaridad1half" title="Meh - 1.5 stars"></label>
                <input type="radio" id="sstarFacClaridad1" name="sstarFacClaridad" value="1" />
                <label class="full" for="sstarFacClaridad1" title="Sucks big time - 1 star"></label>
                <input type="radio" id="sstarFacClaridadhalf" name="sstarFacClaridad" value="0.5" />
                <label class="half" for="sstarFacClaridadhalf" title="Sucks big time - 0.5 stars"></label>
            </fieldset><br>
            <div class="label100">
                <label for="rating6">6. La duración de la formación fue la adecuada.*</label>
            </div><br>
            <fieldset class="rating">
                <input type="radio" id="ssstarDuracion5" name="sstarDuracion" value="5" />
                <label class="full" for="sstarDuracion5" title="Awesome - 5 stars"></label>
                <input type="radio" id="sstarDuracion4half" name="sstarDuracion" value="4.5" />
                <label class="half" for="sstarDuracion4half" title="Pretty good - 4.5 stars"></label>
                <input type="radio" id="sstarDuracion4" name="sstarDuracion" value="4" />
                <label class="full" for="sstarDuracion4" title="Pretty good - 4 stars"></label>
                <input type="radio" id="sstarDuracion3half" name="sstarDuracion" value="3.5" />
                <label class="half" for="sstarDuracion3half" title="Meh - 3.5 stars"></label>
                <input type="radio" id="sstarDuracion3" name="sstarDuracion" value="3" />
                <label class="full" for="sstarDuracion3" title="Meh - 3 stars"></label>
                <input type="radio" id="sstarDuracion2half" name="sstarDuracion" value="2.5" />
                <label class="half" for="sstarDuracion2half" title="Kinda bad - 2.5 stars"></label>
                <input type="radio" id="sstarDuracion2" name="sstarDuracion" value="2" />
                <label class="full" for="sstarDuracion2" title="Kinda bad - 2 stars"></label>
                <input type="radio" id="sstarDuracion1half" name="sstarDuracion" value="1.5" />
                <label class="half" for="sstarDuracion1half" title="Meh - 1.5 stars"></label>
                <input type="radio" id="sstarDuracion1" name="sstarDuracion" value="1" />
                <label class="full" for="sstarDuracion1" title="Sucks big time - 1 star"></label>
                <input type="radio" id="sstarDuracionhalf" name="sstarDuracion" value="0.5" />
                <label class="half" for="sstarDuracionhalf" title="Sucks big time - 0.5 stars"></label>
            </fieldset><br>
            <div class="label100">
                <label for="rating6">7. Las condiciones ambientales (lugar físico, recursos) han sido adecuadas.*</label>
            </div><br>
            <fieldset class="rating">
                <input type="radio" id="sstarAmbiente5" name="sstarAmbiente" value="5" />
                <label class="full" for="sstarAmbiente5" title="Awesome - 5 stars"></label>
                <input type="radio" id="sstarAmbiente4half" name="sstarAmbiente" value="4.5" />
                <label class="half" for="sstarAmbiente4half" title="Pretty good - 4.5 stars"></label>
                <input type="radio" id="sstarAmbiente4" name="sstarAmbiente" value="4" />
                <label class="full" for="sstarAmbiente4" title="Pretty good - 4 stars"></label>
                <input type="radio" id="sstarAmbiente3half" name="sstarAmbiente" value="3.5" />
                <label class="half" for="sstarAmbiente3half" title="Meh - 3.5 stars"></label>
                <input type="radio" id="sstarAmbiente3" name="sstarAmbiente" value="3" />
                <label class="full" for="sstarAmbiente3" title="Meh - 3 stars"></label>
                <input type="radio" id="sstarAmbiente2half" name="sstarAmbiente" value="2.5" />
                <label class="half" for="sstarAmbiente2half" title="Kinda bad - 2.5 stars"></label>
                <input type="radio" id="sstarAmbiente2" name="sstarAmbiente" value="2" />
                <label class="full" for="sstarAmbiente2" title="Kinda bad - 2 stars"></label>
                <input type="radio" id="sstarAmbientehalf" name="sstarAmbiente" value="1.5" />
                <label class="half" for="sstarAmbiente1half" title="Meh - 1.5 stars"></label>
                <input type="radio" id="sstarAmbiente1" name="sstarAmbiente" value="1" />
                <label class="full" for="sstarAmbiente1" title="Sucks big time - 1 star"></label>
                <input type="radio" id="sstarAmbientehalf" name="sstarAmbiente" value="0.5" />
                <label class="half" for="sstarAmbientehalf" title="Sucks big time - 0.5 stars"></label>
            </fieldset><br>
            <fieldset class="label100">
                <label for="comentarioSatisfaccion">8. Agradecemos cualquier comentario adicional que quieras realizar</label>
                <textarea class="label100" name="comentarioSatisfaccion" id="comentarioSatisfaccion"></textarea>
            </fieldset>
            <input type="submit" class="label100" name="submit-calificacion" id="submit-calificacion" value="Calificar" onclick="calificarSatisfaccion()">
        </div>
    </div>

    <div id="popupEncuestaMentor" class="absolute-center" hidden="">
        <div id="overlayCalificacion"></div>
        <div class="popupCalificacion popupSatisfaccion">
            
            <h3>Te pedimos que valides y/o completes la siguiente información que pondremos a disposición de la Red</h3>

            <fieldset class="label100">
                <label for="nombre">Nombre</label>
                <input type="text" class="label100" id="popupMentorNombre">
            </fieldset>

            <fieldset class="label100">
                <label for="ciudad">Provincia</label>
                <select class="label100" id="popupMentorProvincia">
                    
                </select>
            </fieldset>

            <fieldset class="label100">
                <label for="ciudad">Ciudad de residencia</label>
                <select class="label100" id="popupMentorCiudad">
                    
                </select>
            </fieldset>

            <fieldset class="label100">
                <label for="email">Correo electrónico</label>
                <input type="text" class="label100" id="popupMentorEmail">
            </fieldset>

            <fieldset class="label100">
                <label for="linkedin">Perfil linkedin</label>
                <input type="text" class="label100" id="popupMentorLinkedin">
            </fieldset>

            <fieldset class="label100">
                <label for="rubro">Rubro/Industria</label>
                <select class="label100" id="popupMentorRubro">
                    
                </select>
            </fieldset>

            <fieldset class="label100">
                <label for="expertise">Área de expertise</label>
                <select class="label100" id="popupMentorExpertise">
                    
                </select>
                <div>
                    <strong>Expertises elegidas: </strong><small>(máximo 3)</small>
                    <ul id="expertisesElegidas"></ul>
                </div>
            </fieldset>

            <input type="submit" class="label100" name="submit-calificacion" id="submit-calificacion" value="Enviar" onclick="enviarEncuestaMentor()">
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/masonry.pkgd.min.js"></script>
    <script src="js/select2.js"></script>
    <script src="js/featherlight.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="js/main.js"></script>
    <script src="scripts/header.js"></script>
    <script src="scripts/sesiones-fase-2.js"></script>
</body>

</html>