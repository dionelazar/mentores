$(document).ready(function(){
    getUsuarios();
    getSedes();
    getProvincias(1);
});

var sedesElegidas = [];

$('#crearSede').change(function(e){
    var id_sede = $('#crearSede option:selected').val();
    var sede = $("#crearSede option:selected").text();

    sedesElegidas.push(id_sede);
    var textoSedes = $('#crearSedesElegidas').text();
    if(textoSedes != '')
        textoSedes += ', ';
    else
        textoSedes = 'Sedes elegidas: '

    $('#crearSedesElegidas').text(textoSedes + sede);
});

function getUsuarios()
{
	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'usuarios',
            call: 'getUsuariosCoordinador',
        },
        success : function(Data){
        	var miData = JSON.parse(Data);
        	armarListaUsuarios(miData);
        }
    });
}

function armarListaUsuarios(data)
{
    var html = '';

    for (var i = 0; i < data.length; i++) 
    {
        html += '<tr>';
        html += '    <td data-label="Nombre: "><a href="javascript:void(0)">'+data[i].nombre+'</a></td>';
        html += '    <td data-label="DNI: " style="white-space: nowrap;">'+data[i].dni+'</td>';
        html += '    <td data-label="Sedes: "><a href="javascript:void(0)">'+data[i].sedes+'</a></td>';
        html += '    <td data-label="Última actividad: ">'+data[i].ult_login+'</td>';
        //html += '    <td data-label="Sesiones: ">'+data[i].sesiones+'</td>';
        //html += '    <td data-label="E-mail: " style="white-space: nowrap;">'+data[i].email+'</td>';
        html += '    <td data-label="Contraseña: ">'+data[i].password+'</td>';
        html += '    <td data-label="Editar: "><a title="Editar" href="javascript:void(0)" onclick="abrirPopUpEditar('+ data[i].id_usuario +')"><i data-icon="*"></i></a></td>';
        html += '    <td data-label="Estado: ">';
        html += '        <label class="switch">';
        if(data[i].estado == -1)
            html += '        <input type="checkbox" onclick="bloquearUsuario('+data[i].id_usuario+')">';
        else
            html += '        <input type="checkbox" checked onclick="desbloquearUsuario('+data[i].id_usuario+')">';
        html += '            <span class="slider round"></span>';
        html += '        </label>';
        html += '    </td>';
        html += '</tr>';
    }

    $('#listaUsuarios').html(html);
}

function abrirPopUpEditar(id_usuario)
{
    $('#popUpEditarUsuario').show();
    $('#editarIdUsuario').val(id_usuario);
    var id_ciudad = 0;

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'usuarios',
            call: 'getUsuarioEspecifico',
            id_usuario: id_usuario,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            console.log(miData);
            $('#editarNombre').val(miData.nombre);
            $('#editarEmail').val(miData.email);
            $('#editarDni').val(miData.dni);
            $('#editarPassword').val(miData.password);
            $('#editarSede').val(miData.id_sede);
        }
    });
}

function editarUsuario()
{
    var id_usuario = $('#editarIdUsuario').val();
    var nombre = $('#editarNombre').val();
    var email = $('#editarEmail').val();
    var dni = $('#editarDni').val();
    var password = $('#editarPassword').val();
    
    var id_provincia = $('#editarProvincia').val();
    var id_ciudad = $('#editarCiudad').val();
    var id_sede = $('#editarSede').val();

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'usuarios',
            call: 'editarUsuario',
            id_usuario : id_usuario,
            nombre : nombre,
            email : email,
            dni : dni,
            password : password,
            id_rol : 7,
            id_provincia : id_provincia,
            id_ciudad : id_ciudad,
            id_sede : id_sede,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            if(miData.cod_mensaje == -1)
                $("#popUpEditarUsuario").fadeOut(250);

            bootbox.alert(miData.mensaje);
        }
    });
}

function getProvincias(id_pais)
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'localidades',
            call: 'getProvincias',
            id_pais : id_pais,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            var options = '';
            for (var i = 0; i < miData.length; i++) {
                options += '<option value="'+miData[i].id_provincia+'">'+miData[i].provincia+'</option>';
            }
            $('#editarProvincia').html(options);
            $('#crearProvincia').html(options);
        }
    });
}

function getCiudades(id_pais, id_provincia)
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : true,
        data: {
            action: 'localidades',
            call: 'getCiudades',
            id_pais : id_pais,
            id_provincia : id_provincia,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            var options = '';
            for (var i = 0; i < miData.length; i++) {
                options += '<option value="'+miData[i].id_ciudad+'">'+miData[i].ciudad+'</option>';
            }
            $('#editarCiudad').html(options);
            $('#crearCiudad').html(options);
        }
    });
}

function getSedes()
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : true,
        data: {
            action: 'localidades',
            call: 'getSedes',
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            var options = '';
            for (var i = 0; i < miData.length; i++) {
                options += '<option value="'+miData[i].id_sede+'">'+miData[i].sede+'</option>';
            }
            $('#editarSede').html(options);
            $('#crearSede').html(options);
        }
    });
}

function crearUsuario()
{
    var nombre = $('#crearNombre').val();
    var email = $('#crearEmail').val();
    var dni = $('#crearDni').val();
    var password = $('#crearPassword').val();
    
    var id_provincia = $('#crearProvincia').val();
    var id_ciudad = $('#crearCiudad').val();
    var id_sede = $('#crearSede').val();

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'usuarios',
            call: 'insUsuarioCoordinador',
            nombre : nombre,
            email : email,
            dni : dni,
            password : password,
            id_provincia : id_provincia,
            id_ciudad : id_ciudad,
            sedes : sedesElegidas,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            if(miData.cod_mensaje == -1)
                $("#popUpEditarUsuario").fadeOut(250);

            bootbox.alert(miData.mensaje);
        }
    });
}

function bloquearUsuario(id_usuario)
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'usuarios',
            call: 'bloquearUsuario',
            id_usuario: id_usuario,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            console.log(miData);
        }
    });
}

function desbloquearUsuario(id_usuario)
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'usuarios',
            call: 'desbloquearUsuario',
            id_usuario: id_usuario,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            console.log(miData);
        }
    });
}

function isJson(str) 
{
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}