$(document).ready(function(){
	getGrupos();
});

var okMentor = 0;
var okEmprendedor = 0;
var id_usuario_seleccionado = 0;


function getGrupos()
{

}

function buscarMentor()
{
	var dni_mentor = $('#dniMentor').val();
	if(dni_mentor.length <= 1)
		return;

	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : true,
        data: {
            action: 'usuarios',
            call: 'getMentorRelacionamiento',
            dni_mentor : dni_mentor,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            if(miData.cod_mensaje != -1){
            	var li  = '<li>'+miData.mensaje+'</li>';
            	okMentor = 0;
            	$('#btnEditarMentor').html('');
            }else{
	            var li  = '<li>'+miData.nombre+'</li>';
	            	li += '<li>'+miData.rol+'</li>' ;
	            	li += '<li>Emprendedores:</li>' ;
	            	li += '<pre>';
	            	li += '<ul style="list-style-type:circle">';
	            	for (var i = 0; i < miData.emprendedores.length; i++) {
	            		li += '<li>'+miData.emprendedores[i].nombre+' ('+miData.emprendedores[i].grupo+') - '+miData.emprendedores[i].dni+'</li>' ;
	            	}
	            	li += '</ul>'
	            	li += '</pre>';

	            	okMentor = -1;
	            	$('#btnEditarMentor').html('<button class="btn" onclick="popUpCambiarRol('+miData.id_usuario+', \''+miData.rol+'\')">Cambiar rol de '+miData.nombre+'</button>');
	        }
            $('#dataMentor').html(li);
        }
    });
}

function buscarEmprendedor()
{
	var dni_emprendedor = $('#dniEmprendedor').val();
	if(dni_emprendedor.length <= 1)
		return;

	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : true,
        data: {
            action: 'usuarios',
            call: 'getEmprendedorRelacionamiento',
            dni_emprendedor : dni_emprendedor,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            if(miData.cod_mensaje != -1){
            	var li  = '<li>'+miData.mensaje+'</li>';
            	okEmprendedor = 0;
            	$('#btnEditarEmprendedor').html('');
            }else{
	            var li  = '<li>'+miData.nombre+'</li>';
	            	li += '<li>'+miData.rol+'</li>' ;
	            	li += '<li>Mentor: '+miData.mentor+' ('+miData.dni_mentor+')</li>' ;
	            	li += '<li>Cantidad de calificaciones hechas: '+miData.cant_calificaciones_propias+'</li>' ;
	            	li += '<li>Cantidad de calificaciones que le dio el mentor: '+miData.cant_calificaciones_mentor+'</li>' ;

	            	okEmprendedor = -1;
	            	$('#btnEditarEmprendedor').html('<button class="btn" onclick="popUpCambiarRol('+miData.id_usuario+', \''+miData.rol+'\')">Cambiar rol de '+miData.nombre+'</button>');
	            	$('#btnDesasignarEmprendedor').html('<button class="btn" onclick="popUpDesasignar('+miData.id_usuario+')">Borrar relacionamiento de '+miData.nombre+'</button>');
	        }
            $('#dataEmprendedor').html(li);
        }
    });
}

function relacionar()
{
	var dni_mentor = $('#dniMentor').val();
	var dni_emprendedor = $('#dniEmprendedor').val();
	var grupo = $('#nGrupo').val();

	if(dni_mentor.length <= 4 || okMentor != -1)
	{
		bootbox.alert('Debes buscar al mentor');
		return;
	}

	if(dni_emprendedor.length <= 4 || okEmprendedor != -1)
	{
		bootbox.alert('Debes buscar al emprendedor');
		return;
	}

	if(grupo.length <= 0 || grupo == undefined)
	{
		bootbox.alert('Debes seleccionar un grupo');
		return;
	}

	bootbox.confirm({
	    title: "¿Está seguro de relacionar al emprendedor con el mentor?",
	    message: "Revise bien los datos del mentor y el emprendedor. Elija también el N° del grupo al que se va a relacionar",
	    buttons: {
	        cancel: {
	            label: '<i class="fa fa-times"></i> Cancel'
	        },
	        confirm: {
	            label: '<i class="fa fa-check"></i> Confirm'
	        }
	    },
	    callback: function (result) {
	    	if(result)
	    	{
				$.ajax({
			        type: 'POST',
			        url: 'ajaxRedirect.php',
			        datatype: 'html',
			        async : true,
			        data: {
			            action: 'usuarios',
			            call: 'insRelacionamientoGestor',
			            dni_emprendedor : dni_emprendedor,
			            dni_mentor : dni_mentor,
			            grupo : grupo,
			        },
			        success : function(Data){
			            var miData = JSON.parse(Data);
			            bootbox.alert(miData.mensaje);
			        }
			    });
			}
	    }
	});
}

function getRoles()
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : true,
        data: {
            action: 'usuarios',
            call: 'getRoles',
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            var options = '';
            for (var i = 0; i < miData.length; i++) {
                options += '<option value="'+miData[i].id_rol+'">'+miData[i].rol+'</option>';
            }
            $('#selectRoles').html(options);
        }
    });
}

function popUpCambiarRol(id_usuario, rol)
{
	getRoles();
	id_usuario_seleccionado = id_usuario;
	$('#rol_actual').text(rol);
	$('#popUpCambioRol').show();
}

function popUpDesasignar(id_usuario)
{
	bootbox.confirm({
	    title: "¿Está seguro de desasignar a este emprendedor?",
	    message: "Al desasisgnar el emprendedor de su mentor se perderán las calificaciones del mentor y su progreso.",
	    buttons: {
	        cancel: {
	            label: '<i class="fa fa-times"></i> Cancel'
	        },
	        confirm: {
	            label: '<i class="fa fa-check"></i> Confirm'
	        }
	    },
	    callback: function (result) {
	    	if(result)
	    	{
	    		$.ajax({
			        type: 'POST',
			        url: 'ajaxRedirect.php',
			        datatype: 'html',
			        async : true,
			        data: {
			            action: 'usuarios',
			            call: 'updBorarRelacionamientoEmprendedor',
			            id_usuario : id_usuario,
			        },
			        success : function(Data){
			            var miData = JSON.parse(Data);
			            bootbox.alert(miData.mensaje);
			            console.log(miData);
			        }
			    });
	    	}
	    }
	});
}

function cambiarRol()
{
	var id_rol = $('#selectRoles').val();

	bootbox.confirm({
	    title: "¿Está seguro de cambiarle el rol al usuario?",
	    message: "Puedes volver a cambiar el rol más adelante, pero ten en cuenta que pueden perderse los relacionamientos y las calificaciones del usuario.",
	    buttons: {
	        cancel: {
	            label: '<i class="fa fa-times"></i> Cancel'
	        },
	        confirm: {
	            label: '<i class="fa fa-check"></i> Confirm'
	        }
	    },
	    callback: function (result) {
	    	if(result)
	    	{
				$.ajax({
			        type: 'POST',
			        url: 'ajaxRedirect.php',
			        datatype: 'html',
			        async : true,
			        data: {
			            action: 'usuarios',
			            call: 'updRol',
			            id_usuario : id_usuario_seleccionado,
			            id_rol : id_rol,
			        },
			        success : function(Data){debugger
			            var miData = JSON.parse(Data);
			            var mensaje2 = miData.mensaje2.split("|");
			            var lista = '<ul>';
			            for (var i = 0; i < mensaje2.length; i++) {
			            	lista += '<li>'+mensaje2[i]+'</li>';
			            }
			            lista += '</ul>';

			            var dialog = bootbox.dialog({
							title: miData.mensaje,
							message: lista,
							buttons: {
							    ok: {
							        label: "OK",
							        className: 'btn-info',
							    }
							}
						});
			            console.log(miData);
			        }
			    });
			}
	    }
	});
}

function importarBorrarRelacionamiento()
{
	var f = $(this);
    var formData = new FormData(document.getElementById("formImportadorExcel"));
    formData.append("action", "importarBorrarRelacionamientos");

    $.ajax({
        xhr: function() {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function(evt) {
              if (evt.lengthComputable) {
                var percentComplete = evt.loaded / evt.total;
                percentComplete = parseInt(percentComplete * 100);
                console.log('Porcentaje subido : '+percentComplete);
              }
            }, false);
            return xhr;
          },
        url: "ajaxRedirect.php",
        type: "post",
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success : function(Data){debugger
            if (!isJson(Data)) {
            	bootbox.alert(Data);
            	return;
            }
            var data = JSON.parse(Data);
            var mensaje = '';
            
            for (var i = 0; i < data.length; i++) {
            	if(data[i].cod_mensaje == 0)
            		mensaje += data[i].mensaje + ' (línea '+data[i].linea+ ') <br> ' ;
                else if (data[i].cod_mensaje == 2)
                    mensaje += data[i].mensaje + '<a href="files/'+data[i].file+'" download >Descargar detalle</a> <br> ' ;
            }
            if(mensaje == '')
            	bootbox.alert('Todos los relacionamientos se borraron correctamente!');
            else
            	bootbox.alert(mensaje);
        }
    });
}

function isJson(str) 
{
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}