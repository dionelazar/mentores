$(document).ready(function(){
	getSesiones();
});

function getSesiones()
{
	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'reportes',
            call: 'getSesionesReunionesSeguimientoMentor',
        },
        success : function(Data){
        	var miData = JSON.parse(Data);
        	armarSesiones(miData);
        }
    });
}

function armarSesiones(data)
{
	var html = '';
	for (var i = 0; i < data.length; i++) 
	{
		html += '<div class="timeline-item" sesion="'+data[i].sesion +' ('+data[i].grupo+')">';
		html += '    <div class="ratingResult">';
		if(data[i].fecha.length > 1)
			html += '        <span>Marcado: '+data[i].fecha +'</span>';
		else
			html += '		 <span>Not checked</span>';
		html += '    </div>';
		html += '</div>	';
	}
	$('#sesiones').html(html);
}
