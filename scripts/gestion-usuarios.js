$(document).ready(function(){
	getUsuarios();
    getProvincias(1);
    getRoles();
    getSedes();
});

$('#abrir-importar-excel').click(function(){
    $('#popupImportarUsuario').show();
});

$('#importarMentores').click(function(){
    importarMentores()
});

$('#importarEmprendedores').click(function(){
    importarEmprendedores()
});

$('#inputSearch').keydown(function(){
    mySearch();
});

function getUsuarios()
{
	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'usuarios',
            call: 'getUsuarios',
        },
        success : function(Data){
        	var miData = JSON.parse(Data);
        	armarListaUsuarios(miData);
        }
    });
}

function getUsuariosPorSede()
{
    var id_sede = $('#filtrarPorSede').val();
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'usuarios',
            call: 'getUsuariosPorSede',
            id_sede : id_sede,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarListaUsuarios(miData);
        }
    });
}

function filtrarUsuarios(id_tipo_usuario, button)
{
	var tipo_usuario = ['', 'Todos', 'Mentor Desarrollo Temprano', 'Mentor Consolidación', 'Emprendedor Desarrollo temprano', 'Emprendedor Consolidación'];
	var lista = $('#listaUsuarios tr');
	$('.filtros').removeClass("current");
	button.parentElement.classList.add("current");
	
	if(id_tipo_usuario == 1)
	{
		lista.each(function( i ) {
		  	lista[i].style.display = "";
		});
	}else{
		lista.each(function( i ) {
		  	$("td", this).each(function( j ) {
		  		if(j == 3 )
		  		{
		  			if( $(this).text() == tipo_usuario[id_tipo_usuario] )
		  				lista[i].style.display = "";
		  			else
		  				lista[i].style.display = "none";
		  		}
		    	console.log("".concat("row: ", i, ", col: ", j, ", value: ", $(this).text()));
		  	});
		});
	}
}

function armarListaUsuarios(data)
{
	var html = '';

	for (var i = 0; i < data.length; i++) 
	{
        if(i == data.length - 1)
        {
            file = '<a href="files/'+data[i].file+'" download>Descargar excel</a>';
            $('#divExcelListaUsuarios').html(file);
            break;
        }else{
    		html += '<tr>';
            html += '    <td data-label="Nombre: "><a href="javascript:void(0)" onclick="mostrarDetalleUsuario('+ data[i].id_usuario +')" id="nombre_usuario_'+ data[i].id_usuario +'">'+data[i].nombre+'</a></td>';
            html += '    <td data-label="DNI: " style="white-space: nowrap;" id="dni_usuario_'+ data[i].id_usuario +'">'+data[i].dni+'</td>';
            html += '    <td data-label="DNI: " style="white-space: nowrap;">'+data[i].sede+'</td>';
            //html += '    <td data-label="DNI: " style="white-space: nowrap;">'+data[i].email+'</td>';
            html += '    <td data-label="Cargo: "><a href="javascript:void(0)">'+data[i].cargo+'</a></td>';
            html += '    <td data-label="Última actividad: ">'+data[i].ult_login+'</td>';
            //html += '    <td data-label="Sesiones: ">'+data[i].sesiones+'</td>';
            html += '    <td data-label="Contraseña: ">'+data[i].password+'</td>';
            html += '    <td data-label="Editar: "><a title="Editar" href="javascript:void(0)" onclick="abrirPopUpEditar('+ data[i].id_usuario +')"><i data-icon="*"></i></a></td>';
            html += '    <td data-label="Estado: ">';
            html += '        <label class="switch">';
            if(data[i].estado == -1)
            	html += '        <input type="checkbox" onclick="bloquearUsuario('+data[i].id_usuario+')">';
        	else
            	html += '        <input type="checkbox" checked onclick="desbloquearUsuario('+data[i].id_usuario+')">';
            html += '            <span class="slider round"></span>';
            html += '        </label>';
            html += '    </td>';
            html += '</tr>';
        }
    }

	$('#listaUsuarios').html(html);
}

function mySearch() 
{
    var input, filter, table, tr, td, i;
    input = document.getElementById("inputSearch");
    filter = input.value.toUpperCase();
    table = $('#listaUsuarios');
    var tr = $('#listaUsuarios tr');

    for (i = 0; i < tr.length; i++) 
    {
        nombre = tr[i].getElementsByTagName("td")[0];
        dni = tr[i].getElementsByTagName("td")[1];
        if (nombre || dni) {
            if (nombre.innerHTML.toUpperCase().indexOf(filter) > -1 || dni.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function importarMentores()
{
	var f = $(this);
    var formData = new FormData(document.getElementById("formImportadorExcel"));
    formData.append("action", "importarMentoresExcel");

    $.ajax({
        xhr: function() {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function(evt) {
              if (evt.lengthComputable) {
                var percentComplete = evt.loaded / evt.total;
                percentComplete = parseInt(percentComplete * 100);
                console.log('Porcentaje subido : '+percentComplete);
              }
            }, false);
            return xhr;
          },
        url: "ajaxRedirect.php",
        type: "post",
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success : function(Data){
            if (!isJson(Data)) {
            	bootbox.alert(Data);
            	return;
            }
            var data = JSON.parse(Data);
            var mensaje = '';
            
            for (var i = 0; i < data.length; i++) {
            	if(data[i].cod_mensaje == 0)
            		mensaje += data[i].mensaje + ' (línea '+data[i].linea+ ') <br> ' ;
                else if (data[i].cod_mensaje == 2)
                    mensaje += data[i].mensaje + '<a href="files/'+data[i].file+'" download >Descargar detalle</a> <br> ' ;
            }
            if(mensaje == '')
            	bootbox.alert('Todos los usuarios se cargaron correctamente!');
            else
            	bootbox.alert(mensaje);
        }
    });
}

function importarEmprendedores()
{//TODO
    var f = $(this);
    var formData = new FormData(document.getElementById("formImportadorExcel"));
    formData.append("action", "insEmprendedoresExcel");

    $.ajax({
        xhr: function() {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function(evt) {
              if (evt.lengthComputable) {
                var percentComplete = evt.loaded / evt.total;
                percentComplete = parseInt(percentComplete * 100);
                console.log('Porcentaje subido : '+percentComplete);
              }
            }, false);
            return xhr;
          },
        url: "ajaxRedirect.php",
        type: "post",
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success : function(Data){debugger
            console.log(Data);
            if (!isJson(Data)) {
                if(Data != "")
                    bootbox.alert(Data);
                return;
            }
            var data = JSON.parse(Data);
            var mensaje = '';
            for (var i = 0; i < data.length; i++) {
                if(data[i].cod_mensaje == 0)
                    mensaje += data[i].mensaje + ' (línea '+data[i].linea+ ') <br> ' ;
                else if (data[i].cod_mensaje == 2)
                    mensaje += data[i].mensaje + '<a href="files/'+data[i].file+'" download >Descargar detalle</a> <br> ' ;
            }
            if(mensaje == '')
                bootbox.alert('Todos los usuarios se cargaron correctamente!');
            else
                bootbox.alert(mensaje);
        }
    });
}

function importarRelacionamiento()
{//TODO
    var f = $(this);
    var formData = new FormData(document.getElementById("formImportadorExcel"));
    formData.append("action", "insRelacionamientoExcel");

    $.ajax({
        xhr: function() {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function(evt) {
              if (evt.lengthComputable) {
                var percentComplete = evt.loaded / evt.total;
                percentComplete = parseInt(percentComplete * 100);
                console.log('Porcentaje subido : '+percentComplete);
              }
            }, false);
            return xhr;
          },
        url: "ajaxRedirect.php",
        type: "post",
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success : function(Data){   
            console.log(Data);
            if (!isJson(Data)) {
                bootbox.alert(Data);
                return;
            }
            var data = JSON.parse(Data);
            var mensaje = '';
            for (var i = 0; i < data.length; i++) {
                if(data[i].cod_mensaje == 0)
                {
                    mensaje += data[i].mensaje + ' (línea '+data[i].linea+ ') <br> ' ;
                }
            }
            if(mensaje == '')
                bootbox.alert('Todos los relacionamientos se cargaron correctamente!');
            else
                bootbox.alert(mensaje);
        }
    });
}

function bloquearUsuario(id_usuario)
{
	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'usuarios',
            call: 'bloquearUsuario',
            id_usuario: id_usuario,
        },
        success : function(Data){
        	var miData = JSON.parse(Data);
        	console.log(miData);
        }
    });
}

function desbloquearUsuario(id_usuario)
{
	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'usuarios',
            call: 'desbloquearUsuario',
            id_usuario: id_usuario,
        },
        success : function(Data){
        	var miData = JSON.parse(Data);
        	console.log(miData);
        }
    });
}

function abrirPopUpEditar(id_usuario)
{
    $('#popUpEditarUsuario').show();
    $('#editarIdUsuario').val(id_usuario);
    var id_ciudad = 0;

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'usuarios',
            call: 'getUsuarioEspecifico',
            id_usuario: id_usuario,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            console.log(miData);
            $('#editarNombre').val(miData.nombre);
            $('#editarEmail').val(miData.email);
            $('#editarDni').val(miData.dni);
            $('#editarPassword').val(miData.password);
            
            $('#editarRol').val(miData.id_rol);
            $('#editarProvincia').val(miData.id_provincia);
            getCiudades(1, miData.id_provincia)
            id_ciudad = miData.id_ciudad;
            $('#editarSede').val(miData.id_sede);
            setTimeout(function () {
              $('#editarCiudad').val(miData.id_ciudad);
            }, 100);
        }
    });

    $('#editarCiudad').val(id_ciudad);
}

function editarUsuario()
{
    var id_usuario = $('#editarIdUsuario').val();
    var nombre = $('#editarNombre').val();
    var email = $('#editarEmail').val();
    var dni = $('#editarDni').val();
    var password = $('#editarPassword').val();
    
    var id_rol = $('#editarRol').val();
    var id_provincia = $('#editarProvincia').val();
    var id_ciudad = $('#editarCiudad').val();
    var id_sede = $('#editarSede').val();

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'usuarios',
            call: 'editarUsuario',
            id_usuario : id_usuario,
            nombre : nombre,
            email : email,
            dni : dni,
            password : password,
            id_rol : id_rol,
            id_provincia : id_provincia,
            id_ciudad : id_ciudad,
            id_sede : id_sede,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            if(miData.cod_mensaje == -1)
                $("#popUpEditarUsuario").fadeOut(250);

            bootbox.alert(miData.mensaje);
            $('#dni_usuario_'+id_usuario).html(dni);
            $('#nombre_usuario_'+id_usuario).html(nombre);
        }
    });
}

function getProvincias(id_pais)
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'localidades',
            call: 'getProvincias',
            id_pais : id_pais,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            var options = '';
            for (var i = 0; i < miData.length; i++) {
                options += '<option value="'+miData[i].id_provincia+'">'+miData[i].provincia+'</option>';
            }
            $('#editarProvincia').html(options);
        }
    });
}

function getCiudades(id_pais, id_provincia)
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : true,
        data: {
            action: 'localidades',
            call: 'getCiudades',
            id_pais : id_pais,
            id_provincia : id_provincia,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            var options = '';
            for (var i = 0; i < miData.length; i++) {
                options += '<option value="'+miData[i].id_ciudad+'">'+miData[i].ciudad+'</option>';
            }
            $('#editarCiudad').html(options);
        }
    });
}

function getRoles()
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : true,
        data: {
            action: 'usuarios',
            call: 'getRoles',
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            var options = '';
            for (var i = 0; i < miData.length; i++) {
                options += '<option value="'+miData[i].id_rol+'">'+miData[i].rol+'</option>';
            }
            $('#editarRol').html(options);
        }
    });
}

function getSedes()
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : true,
        data: {
            action: 'localidades',
            call: 'getSedes',
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            var options = '';
            for (var i = 0; i < miData.length; i++) {
                options += '<option value="'+miData[i].id_sede+'">'+miData[i].sede+'</option>';
            }
            $('#editarSede').html(options);
            $('#filtrarPorSede').html(options);
            $('#filtrarPorSede').prepend('<option disabled selected>Filtrar por sede</option>');
        }
    });
}

function mostrarDetalleUsuario(id_usuario)
{
    console.log(id_usuario);
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : true,
        data: {
            action: 'usuarios',
            call: 'getDetallesUsuario',
            id_usuario : id_usuario,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            
            if(miData[0].cod_mensaje != -1)
                return

            armarDetalleUsuario(miData);
            $('#popUpDetallesUsuario').show();
        }
    });
}

function armarDetalleUsuario(data)
{
    html = '';

    for (var i = 0; i < data.length; i++) 
    {
        if(i == data.length - 1)
        {
            var excel = '<a href="files/'+data[i].file+'" download>Descargar excel</a>';
            $('#divExcelDetalleEmprendedor').html(excel);
            break;
        }

        html += '<tr>';
        html +=     '<td>Nombre</td>';
        html +=     '<td>'+data[i].nombre+'</td>';
        html += '</tr>';

        html += '<tr>';
        html +=     '<td>Dni</td>';
        html +=     '<td>'+data[i].dni+'</td>';
        html += '</tr>';

        html += '<tr>';
        html +=     '<td>Rol</td>';
        html +=     '<td>'+data[i].rol+'</td>';
        html += '</tr>';

        html += '<tr>';
        html +=     '<td>Sede</td>';
        html +=     '<td>'+data[i].sede+'</td>';
        html += '</tr>';

        html += '<tr>';
        html +=     '<td>Actividad</td>';
        html +=     '<td>'+data[i].actividad+'</td>';
        html += '</tr>';

        html += '<tr>';
        html +=     '<td>Contraseña</td>';
        html +=     '<td>'+data[i].pass+'</td>';
        html += '</tr>';


        html += '<tr>';
        html +=     '<td>Nombre comercial</td>';
        html +=     '<td>'+data[i].nombreComercial+'</td>';
        html += '</tr>';

        html += '<tr>';
        html +=     '<td>Breve descripción de la actividad principal</td>';
        html +=     '<td>'+data[i].descripcionActividadPrincipal+'</td>';
        html += '</tr>';

        html += '<tr>';
        html +=     '<td>¿Hace cuánto que comenzaste?</td>';
        html +=     '<td>'+data[i].tiempoComenzaste+'</td>';
        html += '</tr>';

        html += '<tr>';
        html +=     '<td>Rubro</td>';
        html +=     '<td>'+data[i].rubro+'</td>';
        html += '</tr>';

        html += '<tr>';
        html +=     '<td>¿En qué etapa se encuentra tu emprendimiento?</td>';
        html +=     '<td>'+data[i].etapa+'</td>';
        html += '</tr>';

        html += '<tr>';
        html +=     '<td>Definición de la propuesta de valor del producto o servicio</td>';
        html +=     '<td>'+data[i].propuestaValor+'</td>';
        html += '</tr>';

        html += '<tr>';
        html +=     '<td>Conocimiento del mercado y segmentación de clientes</td>';
        html +=     '<td>'+data[i].conocimientoMercado+'</td>';
        html += '</tr>';

        html += '<tr>';
        html +=     '<td>Estrategia de salida al mercado: difusión, distribución y canales de venta</td>';
        html +=     '<td>'+data[i].estrategiaSalida+'</td>';
        html += '</tr>';

        html += '<tr>';
        html +=     '<td>Identificación y análisis de competidores</td>';
        html +=     '<td>'+data[i].analisisCompetencia+'</td>';
        html += '</tr>';

        html += '<tr>';
        html +=     '<td>Fidelización de clientes</td>';
        html +=     '<td>'+data[i].fidelizacionClientes+'</td>';
        html += '</tr>';

        html += '<tr>';
        html +=     '<td>Expansión hacia nuevos mercados</td>';
        html +=     '<td>'+data[i].expansionNuevosMercados+'</td>';
        html += '</tr>';

        html += '<tr>';
        html +=     '<td>Identificación y análisis de actores clave</td>';
        html +=     '<td>'+data[i].analisisActoresClave+'</td>';
        html += '</tr>';

        html += '<tr>';
        html +=     '<td>Consolidación del punto de equilibrio</td>';
        html +=     '<td>'+data[i].consolidacionPuntoEquilibrio+'</td>';
        html += '</tr>';

        html += '<tr>';
        html +=     '<td>Estabilidad financiera</td>';
        html +=     '<td>'+data[i].estabilidadFinanciera+'</td>';
        html += '</tr>';

        html += '<tr>';
        html +=     '<td>Acceso a financiamiento (propio o externo)</td>';
        html +=     '<td>'+data[i].accesoFinanciamiento+'</td>';
        html += '</tr>';

        html += '<tr>';
        html +=     '<td>Administración de insumos</td>';
        html +=     '<td>'+data[i].administracionInsumos+'</td>';
        html += '</tr>';

        html += '<tr>';
        html +=     '<td>Gestión de proveedores</td>';
        html +=     '<td>'+data[i].gestionProveedores+'</td>';
        html += '</tr>';

        html += '<tr>';
        html +=     '<td>Diseño del sistema de producción</td>';
        html +=     '<td>'+data[i].disenoSistemaProduccion+'</td>';
        html += '</tr>';

        html += '<tr>';
        html +=     '<td>Escalabilidad de la producción</td>';
        html +=     '<td>'+data[i].escalabilidadProduccion+'</td>';
        html += '</tr>';

        html += '<tr>';
        html +=     '<td>Cumplimiento de requisitos administrativos y legales para operar</td>';
        html +=     '<td>'+data[i].cumplimientoRequisitosLegales+'</td>';
        html += '</tr>';

        html += '<tr>';
        html +=     '<td>Diseño de sistemas administrativos</td>';
        html +=     '<td>'+data[i].disenoSistemasAdministrativos+'</td>';
        html += '</tr>';

        html += '<tr>';
        html +=     '<td>Diseño de sistemas de planeamiento y control</td>';
        html +=     '<td>'+data[i].disenoSistemasPlaneamiento+'</td>';
        html += '</tr>';

        html += '<tr>';
        html +=     '<td>Consolidación de estructura de personal</td>';
        html +=     '<td>'+data[i].estructuraPersonal+'</td>';
        html += '</tr>';

        html += '<tr>';
        html +=     '<td>Utilización de sistemas de gestión</td>';
        html +=     '<td>'+data[i].sistemaGestion+'</td>';
        html += '</tr>';
    }

    $('#bodyDetallesUsuario').html(html);
}

function isJson(str) 
{
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}