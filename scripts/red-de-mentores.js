$(document).ready(function(){
    getRubros();
    getExpertise();
    getSedes();
    getMentores();
});

function getMentores()
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'usuarios',
            call: 'getMentores',
        },
        success : function(Data){
            //console.log(Data);
            var miData = JSON.parse(Data);
            armarMentores(miData);
            $('#filtrarPorSede').val(0);
            $('#filtrarPorRubro').val(0);
            $('#filtrarPorExpertise').val(0);
        }
    });
}

function getUsuariosPorSede()
{
    var id_sede = $('#filtrarPorSede').val();

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'usuarios',
            call: 'getMentoresPorSede',
            id_sede : id_sede,
        },
        success : function(Data){
            //console.log(Data);
            var miData = JSON.parse(Data);
            armarMentores(miData);
        }
    });
}

function getUsuariosPorRubro()
{
    var id_rubro = $('#filtrarPorRubro').val();

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'usuarios',
            call: 'getMentoresPorRubro',
            id_rubro : id_rubro,
        },
        success : function(Data){
            //console.log(Data);
            var miData = JSON.parse(Data);
            armarMentores(miData);
        }
    });
}

function getUsuariosPorExpertise()
{
    var id_expertise = $('#filtrarPorExpertise').val();

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'usuarios',
            call: 'getMentoresPorExpertise',
            id_expertise : id_expertise,
        },
        success : function(Data){
            //console.log(Data);
            var miData = JSON.parse(Data);
            armarMentores(miData);
        }
    });
}

function getUsuariosPorFiltro()
{
    var id_expertise = $('#filtrarPorExpertise').val();
    var id_sede = $('#filtrarPorSede').val();
    var id_rubro = $('#filtrarPorRubro').val();

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'usuarios',
            call: 'getMentoresPorFiltros',
            id_expertise : id_expertise,
            id_sede : id_sede,
            id_rubro : id_rubro,
        },
        success : function(Data){
            //console.log(Data);
            var miData = JSON.parse(Data);
            armarMentores(miData);
        }
    });
}

function getMentoresBuscar()
{
    var text = $('#inputSearch').val();

    if(text.length <= 0)
        return;

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'usuarios',
            call: 'getMentoresBuscar',
            text : text,
        },
        success : function(Data){
            //console.log(Data);
            var miData = JSON.parse(Data);
            armarMentores(miData);
        }
    });
}


function armarMentores(data)
{
    var html = '';
	for (var i = 0; i < data.length; i++) 
	{
        if(data[i].cod_mensaje == 2){
            var file = '<a href="files/'+data[i].file+'" download>Descargar excel</a>';
            $('#divExcel').html(file);
            break;
        }
		    html += '<div class="timeline-item activeTimeline">';
	        html += '<h3>Mentor: <strong>'+data[i].nombre+'</strong></h3>';
            html += '   <ul>';
            
            if(data[i].imagen != null){
                html += '   <li><img src="'+data[i].imagen+'" height="100" width="100"></li>'
            }
            if(myRol == 'admin')
                html += '   <li>Dni: '+data[i].dni+'</li>';
            html += '       <li>Sede: '+data[i].sede+'</li>';
            html += '       <li>Expertise: '+data[i].expertise+'</li>';
            html += '       <li>Rubro: '+data[i].rubro+'</li>';
            if(ValidURL(data[i].linkedin))
                html += '   <li>Linkedin: <a href="'+data[i].linkedin+'">'+data[i].linkedin+'</a></li>';
            else
                html += '   <li>Linkedin: '+data[i].linkedin+'</li>';
            html += '       <li>Email: '+data[i].email+'</li>';
            html += '   </ul>';
	        html += '</div>';
	}

    $('#listaMentores').html(html);
}

function getRubros()
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'getRubros',
        },
        success : function(Data){
            var miData = JSON.parse(Data);
           var options = '<option value="0">Seleccioná un rubro</option>';
            for (var i = 0; i < miData.length; i++) {
                options += '<option value="'+miData[i].id_rubro+'">'+miData[i].rubro+'</option>';
            }
            $('#filtrarPorRubro').html(options);
        }
    });
}

function getExpertise()
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'usuarios',
            call: 'getExpertise',
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            var options = '<option value="0">Seleccioná una expertise</option>';
            for (var i = 0; i < miData.length; i++) {
                options += '<option value="'+miData[i].id_expertise+'">'+miData[i].expertise+'</option>';
            }
            $('#filtrarPorExpertise').html(options);
        }
    });
}

function getSedes()
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'localidades',
            call: 'getSedes',
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            var options = '<option value="0">Seleccioná una sede</option>';
            for (var i = 0; i < miData.length; i++) {
                options += '<option value="'+miData[i].id_sede+'">'+miData[i].sede+'</option>';
            }
            $('#filtrarPorSede').html(options);
        }
    });
}

function ValidURL(str) {
    var regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
    var regex2 = /(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
    if(!regex.test(str) || !regex2.test(str)) {
        return false;
    } else {
        return true;
    }
}
