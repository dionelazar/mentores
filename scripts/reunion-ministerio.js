$(document).ready(function(){
    getUsuarios();
    getFormCalificacion();
});

var id_sesion_selected = 0;
var id_grupo_selected = 0;
var id_mentor_selected = 0;

$('#inputSearch').keydown(function(){
    mySearch();
});

function formCalificar(id_grupo, id_sesion, id_mentor)
{
    $('#popupCalificacion').show();
    id_grupo_selected  = id_grupo;
    id_sesion_selected = id_sesion;
    id_mentor_selected = id_mentor;
}

function mySearch() 
{
    var input, filter, table, tr, td, i;
    input = document.getElementById("inputSearch");
    filter = input.value.toUpperCase();
    table = $('.tablasReporte');
    //table = document.getElementById("myTable");
    var tr = $('.tablasReporte tr');
    //tr = table.getElementsByTagName("tr");

      // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) 
    {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        } 
    }
}

function getUsuarios()
{
	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'reportes',
            call: 'getReunionSeguimientoAdmin',
        },
        success : function(Data){
        	var miData = JSON.parse(Data);
        	armarMentores(miData);
        }
    });
}

function armarMentores(data)
{
    for (var i = 0; i < data.length; i++) 
    {
        var html = '';
            html += '<div class="timeline-item activeTimeline" sesion="'+data[i].id_sesion+'ª Sesión">';
            html += '      <div class="module library reportes">';
            html += '        <table class="tablasReporte">';
            html += '            <thead>'
            html += '                <th>Nombre</th>'
            html += '                <th>Sede</th>'
            html += '                <th>E-mail</th>'
            html += '                <th>Fecha de asistencia</th>'
            html += '                <th>¿Asistió?</th>'
            html += '            </thead>'
            html += '            <tbody>';
            if(data[i+1] !== void 0)
            while( (data[i+1] !== void 0 ? data[i+1].id_sesion : -1) == data[i].id_sesion )
            {
                var grupo = data[i].id_tipo_usuario == 2 ? '('+data[i].grupo+')' : '';
                html += '                <tr>';
                html += '                    <td data-label="Nombre: "><a href="javascript:void(0)">'+data[i].nombre+' '+grupo+' </a></td>';
                html += '                    <td data-label="Sede: ">'+data[i].provincia+', '+data[i].ciudad+'</td>';
                html += '                    <td data-label="E-mail: " style="white-space: nowrap;">'+data[i].email+'</td>';
                html += '                    <td data-label="Fecha" style="white-space: nowrap;" id="fecha_'+data[i].id_grupo+'_'+data[i].id_sesion+'">'+data[i].fecha+'</td>';
                /*html += '                    <td id="asistencia_'+data[i].id_grupo+'_'+data[i].id_sesion+'">';
                if(data[i].calificacion == 0)
                {
                    html += '                     <a href="#popupCalificacion" class="calificarBtn" id="calificarBtn_'+data[i].id_grupo+'_'+data[i].id_sesion+'">';
                    html += '                         <button class="sesionCalificar" onclick="marcarAsistencia('+data[i].id_grupo+', '+data[i].id_sesion+', '+data[i].id_usuario+')">Marcar asistencia</button>';
                    html += '                     </a>';
                }else{
                    html += '                     Asistió';
                }
                html += '                    </td>';*/
                if(data[i].calificacion == 0)
                    html += '<td data-label="¿Asisitió?"><input type="checkbox" class="check-asistencia" onclick="marcarAsistencia('+data[i].id_grupo+', '+data[i].id_sesion+', '+data[i].id_usuario+')"><label></label></td>';
                else
                    html += '<td data-label="¿Asisitió?"><input type="checkbox" checked class="check-asistencia" onclick="desmarcarAsistencia('+data[i].id_grupo+', '+data[i].id_sesion+', '+data[i].id_usuario+')"><label></label></td>'; 
                html += '                </tr>';
                i++;
            }
            html += '            </tbody>';
            html += '        </table>';
            html += '    </div>';
            html += '</div>';

        if(data[i].id_tipo_usuario == 2)
            $('#tabDT').append(html);
        else
            $('#tabCON').append(html);
    }
}

function getFormCalificacion()
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'getFormCalificacionSeguimiento',
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarFormCalificacion(miData);
        }
    });
}

function marcarAsistencia(id_grupo, id_sesion, id_usuario)
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'reportes',
            call: 'marcarAsistencia',
            id_grupo : id_grupo,
            id_sesion : id_sesion,
            id_usuario : id_usuario,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            if(miData.cod_mensaje != -1)
            {
                bootbox.alert(miData.mensaje);
            }
        }
    });
}

function desmarcarAsistencia(id_grupo, id_sesion, id_usuario)
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'reportes',
            call: 'desmarcarAsistencia',
            id_grupo : id_grupo,
            id_sesion : id_sesion,
            id_usuario : id_usuario,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            if(miData.cod_mensaje != -1)
            {
                bootbox.alert(miData.mensaje);
            }
        }
    });
}

function armarFormCalificacion(data)
{
    var preguntas = '';
    for (var i = 0; i < data.preguntas.length; i++) 
    {
        var num = data.preguntas[i].num_pregunta;
        var id_pregunta = data.preguntas[i].id_pregunta;
        $('#pregunta'+num).html(data.preguntas[i].pregunta);

        var respuestas = '';
        for (var j = 0; j < data.preguntas[i].respuestas.length; j++) 
        {
            var resp = data.preguntas[i].respuestas;
            respuestas += '<input type="radio" name="calificacion'+num+'[]" id="calificacion-'+id_pregunta+'-'+resp[j].id_respuesta+'" value="'+resp[j].id_respuesta+'">'
            respuestas += '<label for="calificacion-'+id_pregunta+'-'+resp[j].id_respuesta+'">'+resp[j].respuesta+'</label>'
        }
        $('#respuestas'+num).html(respuestas);
    }
}

function calificar()
{
    var id_sesion = id_sesion_selected;
    var id_grupo = id_grupo_selected;

    var resp1       = getCheckboxValues('calificacion1');
    var resp2       = getCheckboxValues('calificacion2');
    var stars       = getRadio('rating');
    var comentario  = $('#calificacion-texto').val();

    if(!validarCalificacion())
        return;

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'reportes',
            call: 'calificarReunionSeguimiento',
            id_sesion : id_sesion,
            id_grupo : id_grupo,
            resp1 : resp1,
            resp2 : resp2,
            stars : stars,
            comentario : comentario,
        },
        success : function(Data){
            console.log(Data);
            var miData = JSON.parse(Data);
            bootbox.alert(miData.mensaje);
            $('#popupCalificacion').hide();
            if(miData.cod_mensaje == -1)
            {
                $('#calificarBtn_'+id_grupo+'_'+id_sesion).hide();
                $('#flechaProgresoSesion'+id_sesion).addClass("active");
                comprobarCalificacion(id_sesion, id_grupo);
                comprobarEncuestaSatisfaccion();
                if(id_sesion == 3)
                    location.reload();
            }
        }
    });
}


function validarCalificacion()
{
    var resp1       = getCheckboxValues('calificacion1');
    var resp2       = getCheckboxValues('calificacion2');
    var stars       = getRadio('rating');
    var comentario  = $('#calificacion-texto').val();

    if(resp1.length < 1){
        bootbox.alert('Tenés que responder la pregunta 1');
        return false;
    }

    if(resp2.length < 1){
        bootbox.alert('Tenés que responder la pregunta 1');
        return false;
    }

    if(stars.length < 1){
        bootbox.alert('Tenés que calificar la sesión');
        return false;
    }

    if(comentario.length < 1){
        bootbox.alert('Tenés que escribir un comentario');
        return false;
    }

    return true;
}


function getCheckboxValues(name)
{
    var checkboxes = document.getElementsByName(name+'[]');
    var vals = "";
    for (var i=0, n=checkboxes.length;i<n;i++) 
    {
        if (checkboxes[i].checked) 
        {
            vals += ","+checkboxes[i].value;
        }
    }
    if (vals) vals = vals.substring(1);
    return vals;
}

function getRadio(name)
{
    var radios = document.getElementsByName(name);
    for (var i = 0, length = radios.length; i < length; i++)
    {
        if (radios[i].checked)
        {
            return(radios[i].value);
            break;
        }
    }
}