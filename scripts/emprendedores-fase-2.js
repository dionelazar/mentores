$(document).ready(function(){
    getEmprendedoresPropuestosFase2();
});

function getEmprendedoresPropuestosFase2()
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'reportes',
            call: 'getEmprendedoresPropuestosFase2',
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            //-1 aceptado
            //0 solo fue propuesto
            //2 usuario rechazado
            for (var i = 0; i < miData.length; i++) {
                var row  = '<tr>';
                    row +=     '<td>'+miData[i].emprendedor+'</td>';
                    row +=     '<td>'+miData[i].mentor+'</td>';
                    row +=     '<td>'+miData[i].fecha+'</td>';
                    row +=     '<td>'+miData[i].dni_emprendedor+'</td>';
                    row +=     '<td>'+miData[i].rol+'</td>';
                    row +=     '<td>'+miData[i].sede+'</td>';
                    row +=     '<td>'+miData[i].asistencias+'</td>';
                    row +=     '<td><button class="btn" onclick="aceptar('+miData[i].id_emprendedor+')">Aceptar</button></td>';
                    row +=     '<td><button class="btn" onclick="rechazar('+miData[i].id_emprendedor+')">Rechazar</button></td>';
                    row +=     '<td>'+miData[i].sn_aceptado+'</td>';
                    row += '</tr>';
                $('#emprendedores').append(row);
            }
        }
    });
}

function aceptar(id_emprendedor)
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'usuarios',
            call: 'aceptarEmprendedorFase2',
            id_emprendedor : id_emprendedor,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            bootbox.alert(miData.mensaje);
        }
    });
}

function rechazar(id_emprendedor)
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'usuarios',
            call: 'rechazarEmprendedorFase2',
            id_emprendedor : id_emprendedor,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            bootbox.alert(miData.mensaje);
        }
    });
}