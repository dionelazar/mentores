$(document).ready(function(){
	getSesiones();
	comprobarEncuestaSatisfaccion();
	//comprobarCertificacion();
	comprobarEncuesta();
});

var sesionSeleccionada = 0;
var grupoSeleccionado  = 0;
var encuestaSatisfaccionTerminada = false;
var globalAction = '';

function getSesiones()
{
	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'getSesionesFase2Emprendedor',
        },
        success : function(Data){
        	var miData = JSON.parse(Data);
        	armarSesiones(miData);
        }
    });
}

function armarSesiones(data)
{
	for (var i = 0; i < data.length; i++) 
	{
		var html  = '<div class="timeline-item activeTimeline" sesion="'+ data[i].desc +'">';
	        //html += 	'<h3>Grupo: <strong>Nombre del grupo</strong></h3>';
	        html += '	<h3>Mentor: <strong>'+data[i].mentor+'</strong></h3>'
            html += '   <p>Mi comentarios: <strong id="comentario-'+ data[i].id_sesion + '-' + data[i].id_grupo +'"></strong></p>';
            if (data[i].sn_califico == 0 ) 
            {
            	html += '   <a href="#popupCalificacion" class="calificarBtn" id="calificarBtn_'+ data[i].id_sesion + '_' + data[i].id_grupo +'">';
	            html += '   	<button class="sesionCalificar" onclick="calificarSesion('+ data[i].id_sesion + ',' + data[i].id_grupo +')">Calificar Sesión</button>';
	            html += '   </a>';
            }else{
                html +=     '<a href="javascript:void(0)" class="calificarBtn" id="editarBtn_'+ data[i].id_sesion + '_' + data[i].id_grupo +'">';
                html +=         '<button class="sesionCalificar" onclick="calificarSesion('+ data[i].id_sesion + ',' + data[i].id_grupo +', \'editar\')">Editar calificación</button>';
                html +=     '</a>'; 
            	$('#flechaProgresoSesion'+data[i].id_sesion).addClass("active");
            }
            html +=     '<small>Mi calificación</small><br>';
	        html += 	'<div class="ratingResult">';
            html += 		'<span class="fa fa-star" id="1-starSesion-'+ data[i].id_sesion + '-' + data[i].id_grupo +'"></span>';
            html += 		'<span class="fa fa-star" id="2-starSesion-'+ data[i].id_sesion + '-' + data[i].id_grupo +'"></span>';
            html += 		'<span class="fa fa-star" id="3-starSesion-'+ data[i].id_sesion + '-' + data[i].id_grupo +'"></span>';
            html += 		'<span class="fa fa-star" id="4-starSesion-'+ data[i].id_sesion + '-' + data[i].id_grupo +'"></span>';
            html += 		'<span class="fa fa-star" id="5-starSesion-'+ data[i].id_sesion + '-' + data[i].id_grupo +'"></span>';
            html += 	'</div>';
	        html += '</div>';

			$('#sesiones').append(html);

        comprobarCalificacion( data[i].id_sesion , data[i].id_grupo );
	}
}

function calificarSesion(id_sesion, id_grupo, action = 'calificar')
{
	getFormCalificacion(id_sesion, id_grupo);
	sesionSeleccionada = id_sesion;
	grupoSeleccionado = id_grupo;
    globalAction = action;
    
    if(action == 'editar')
        getCalifiacion(id_sesion, id_grupo);
}

function getEstrellas(id_sesion, id_grupo)
{

}

function getFormCalificacion(id_sesion, id_grupo)
{
	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'getFormCalificacionEmprendedorFase2',
            id_sesion : id_sesion,
			id_grupo : id_grupo,
        },
        success : function(Data){
        	var miData = JSON.parse(Data);
        	if(miData.preguntas[0].cod_mensaje == 0)
        	{
        		bootbox.alert(miData.preguntas[0].mensaje);
        		return;
        	}
        	armarFormCalificacion(miData);
        	$('#popupCalificacion').show();
        }
    });
}

function getRubros()
{
	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'getRubros',
        },
        success : function(Data){
        	var miData = JSON.parse(Data);
        	console.log(miData);

        	var options = '<option value="">Seleccioná una opción</option>';
        	for (var i = 0; i < miData.length; i++) {
        		options += '<option value="' + miData[i].id_rubro + '">' + miData[i].rubro + '</option>';
        	}
        	$('#industria').html(options);
        }
    });
}

function comprobarEncuesta()
{
	if(getCookie('satisfaccionResponderMasTarde') == 1)
        return;

	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'getComprobarEncuestaEmprendedor',
        },
        success : function(Data){
        	var miData = JSON.parse(Data);
        	if(miData.cod_mensaje == -1){
        		getRubros();
        		$('#popupHacerSatisfaccion').show();
        		//$('#popupEncuesta').show();
        	}
        }
    });
}


function insEncuestaEmprendedor()
{
    var id_rubro                 = $('#industria').val();
    var propuesta_valor          = getRadio('propuesta-valor');
    var plan_negocio             = getRadio('plan-negocio');
    var calendario               = getRadio('calendario');
    var estructura_legal         = getRadio('estructura-legal');
    var publico_objetivo         = getRadio('publico-objetivo');
    var canales_venta            = getRadio('canales-venta');
    var estrategia_marketing     = getRadio('estrategia-marketing');
    var administracion           = getRadio('administracion');
    var situacion_financiamiento = getRadio('situacion-financiamiento');
    var plan_operaciones         = getRadio('plan-operaciones');

    if( !validarEncuestaEmprendedor(id_rubro, propuesta_valor, plan_negocio, calendario, estructura_legal, publico_objetivo, canales_venta, estrategia_marketing, administracion, situacion_financiamiento, plan_operaciones) )
    {
        alert('Debes completar todos los campos');
        $('#popupEncuesta').show();
        return;
    }

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'insEncuestaEmprendedor',
            id_rubro : id_rubro,
            propuesta_valor : propuesta_valor,
            plan_negocio : plan_negocio,
            calendario : calendario,
            estructura_legal : estructura_legal,
            publico_objetivo : publico_objetivo,
            canales_venta : canales_venta,
            estrategia_marketing : estrategia_marketing,
            administracion : administracion,
            situacion_financiamiento : situacion_financiamiento,
            plan_operaciones : plan_operaciones,
        },
        success : function(Data){debugger
            console.log(Data);
            var miData = JSON.parse(Data);

            bootbox.alert(miData.mensaje);

            if(miData.cod_mensaje == -1){
                $('#popupEncuesta').hide();
            }else{
                $('#popupEncuesta').show();
            }
        }
    });
}

function validarEncuestaEmprendedor(id_rubro, propuesta_valor, plan_negocio, calendario, estructura_legal, publico_objetivo, canales_venta, estrategia_marketing, administracion, situacion_financiamiento, plan_operaciones)
{
    if(id_rubro.length < 1)
        return false;

    if(propuesta_valor == undefined)
        return false;

    if(plan_negocio == undefined)
        return false;

    if(calendario == undefined)
        return false;

    if(estructura_legal == undefined)
        return false;

    if(publico_objetivo == undefined)
        return false;

    if(canales_venta == undefined)
        return false;

    if(estrategia_marketing == undefined)
        return false;

    if(administracion == undefined)
        return false;

    if(situacion_financiamiento == undefined)
        return false;

    if(plan_operaciones == undefined)
        return false;

    return true;
}

function incrementoOcultar()
{
    $('#incremento_num').hide();
}

function incrementoMostrar()
{
    $('#incremento_num').show();
}


function comprobarCalificacion(id_sesion, id_grupo)
{
	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'getComprobarCalificacion',
            id_sesion : id_sesion,
			id_grupo : id_grupo,
        },
        success : function(Data){
        	var miData = JSON.parse(Data);
        	if(miData.promedio > 0)
            {
                var nStar = Math.round(miData.promedio);
                for (var i = 1; i < nStar+1; i++) {
                    $('#'+ i +'-starSesion-'+ id_sesion + '-' + id_grupo).addClass('checked');
                }
                $('#comentario-'+ id_sesion + '-' + id_grupo).html(miData.comentario);
            }
        }
    });
}

function armarFormCalificacion(data)
{
	var preguntas = '';
	for (var i = 0; i < data.preguntas.length; i++) 
	{
		var num = data.preguntas[i].num_pregunta;
		var id_pregunta = data.preguntas[i].id_pregunta;
		$('#pregunta'+num).html(data.preguntas[i].pregunta);

		var respuestas = '';
		for (var j = 0; j < data.preguntas[i].respuestas.length; j++) 
		{
			var resp = data.preguntas[i].respuestas;
			respuestas += '<input type="radio" class="respuestas-pregunta-'+num+'" name="calificacion'+num+'[]" id="calificacion-'+id_pregunta+'-'+resp[j].id_respuesta+'" value="'+resp[j].id_respuesta+'">';
            respuestas += '<label for="calificacion-'+id_pregunta+'-'+resp[j].id_respuesta+'">'+resp[j].respuesta+'</label>';
		}
		$('#respuestas'+num).html(respuestas);
	}
}

function calificar()
{
	var id_sesion = sesionSeleccionada;
	var id_grupo = grupoSeleccionado;

	var asistencias = getCheckboxValues('asistencia');
	var resp1       = getCheckboxValues('calificacion1');
	var resp2       = getCheckboxValues('calificacion2');
	var stars       = getRadio('rating');
	var comentario  = $('#calificacion-texto').val();

	if(!validarCalificacion())
		return;

	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'calificar',
            id_sesion : id_sesion,
			id_grupo : id_grupo,
			asistencias : asistencias,
			resp1 : resp1,
			resp2 : resp2,
			stars : stars,
			comentario : comentario,
        },
        success : function(Data){debugger
        	console.log(Data);
        	var miData = JSON.parse(Data);
        	bootbox.alert(miData.mensaje);
        	$('#popupCalificacion').hide();
        	if(miData.cod_mensaje == -1)
        	{
        		$('#calificarBtn_'+id_sesion+'_'+id_grupo).hide();
        		$('#flechaProgresoSesion'+id_sesion).addClass("active");
        		comprobarCalificacion(id_sesion, id_grupo);
        		$('#calificacion-texto').val('');

        		if(id_sesion == 3)
        			location.reload();
        	}
        }
    });
}

function validarCalificacion()
{
	var asistencias = getCheckboxValues('asistencia');
	var resp1       = getCheckboxValues('calificacion1');
	var resp2       = getCheckboxValues('calificacion2');
	var stars       = getRadio('rating');
	var comentario  = $('#calificacion-texto').val();

	if(resp1.length < 1){
		bootbox.alert('Tenés que responder la pregunta 1');
		return false;
	}

	if(resp2.length < 1){
		bootbox.alert('Tenés que responder la pregunta 1');
		return false;
	}

	if(stars.length < 1){
		bootbox.alert('Tenés que calificar la sesión');
		return false;
	}

	if(comentario.length < 1){
		bootbox.alert('Tenés que escribir un comentario');
		return false;
	}

	return true;
}

function calificarSatisfaccion()
{
	var sstarEstr              = getRadio('sstarEstr');
	var sstarUtil              = getRadio('sstarUtil');
	var sstarCumplimiento      = getRadio('sstarCumplimiento');
	var sstarValor             = getRadio('sstarValor');
	var sstarDesemp            = getRadio('sstarDesemp');
	var sstarMaterial          = getRadio('sstarMaterial');
	var puntajeValoracion      = getRadio('puntaje-valoracion');
	var detalleSatisfaccion    = $('#detalleSatisfaccion').val();
	var comentarioSatisfaccion = $('#comentarioSatisfaccion').val();

	if(!validarSatisfaccion())
		return;

	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'calificarSatisfaccion',
            sstarEstr : sstarEstr,
			sstarUtil : sstarUtil,
			sstarCumplimiento : sstarCumplimiento,
			sstarValor : sstarValor,
			sstarDesemp : sstarDesemp,
			sstarMaterial : sstarMaterial,
			puntajeValoracion : puntajeValoracion,
			detalleSatisfaccion : detalleSatisfaccion,
			comentarioSatisfaccion : comentarioSatisfaccion,
        },
        success : function(Data){
        	console.log(Data);
        	var miData = JSON.parse(Data);
        	$('#popupSatisfaccion').hide();
        	bootbox.alert(miData.mensaje);
        }
    });
}

function validarSatisfaccion()
{
	var sstarEstr              = getRadio('sstarEstr');
	var sstarUtil              = getRadio('sstarUtil');
	var sstarCumplimiento      = getRadio('sstarCumplimiento');
	var sstarValor             = getRadio('sstarValor');
	var sstarDesemp            = getRadio('sstarDesemp');
	var sstarMaterial          = getRadio('sstarMaterial');
	var puntajeValoracion      = getRadio('puntaje-valoracion');
	var detalleSatisfaccion    = $('#detalleSatisfaccion').val();
	var comentarioSatisfaccion = $('#comentarioSatisfaccion').val();

	if(sstarEstr.length < 1){
		bootbox.alert('Tenés que calificar la pregunta 1');
		return false;
	}

	if(sstarUtil.length < 1){
		bootbox.alert('Tenés que calificar la pregunta 2');
		return false;
	}

	if(sstarCumplimiento.length < 1){
		bootbox.alert('Tenés que calificar la pregunta 3');
		return false;
	}

	if(sstarValor.length < 1){
		bootbox.alert('Tenés que calificar la pregunta 4');
		return false;
	}

	if(sstarDesemp.length < 1){
		bootbox.alert('Tenés que calificar la pregunta 5');
		return false;
	}

	if(sstarMaterial.length < 1){
		bootbox.alert('Tenés que calificar la pregunta 6');
		return false;
	}

	if(puntajeValoracion.length < 1){
		bootbox.alert('Tenés que calificar la pregunta 7');
		return false;
	}

	if(detalleSatisfaccion.length < 1){
		bootbox.alert('Tenés que escribir un comentario en la pregunta 8');
		return false;
	}

	if(comentarioSatisfaccion.length < 1){
		bootbox.alert('Tenés que escribir un comentario en la pregunta 9');
		return false;
	}

	return true;
}

function comprobarEncuestaSatisfaccion()
{
	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : true,
        data: {
            action: 'sesiones',
            call: 'comprobarEncuestaSatisfaccion',
        },
        success : function(Data){
        	console.log(Data);
        	var miData = JSON.parse(Data);
        	if(miData.cod_mensaje == -1)
        		$('#popupSatisfaccion').show();
        	else if(miData.mensaje == "Ya hizo la encuesta")
        		encuestaSatisfaccionTerminada = true;
        }
    });
}

function comprobarCertificacion()
{
	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : true,
        data: {
            action: 'sesiones',
            call: 'comprobarCertificacion',
        },
        success : function(Data){
        	console.log(Data);
        	var miData = JSON.parse(Data);
        	if(miData.cod_mensaje == -1 && encuestaSatisfaccionTerminada){
        		$('#popupCertificacion').show();
        		$('#nombreMentorCertificacion').text(miData.usuario);
        	}
        }
    });
}

function satisfaccionResponderMasTarde()
{
    setCookie('satisfaccionResponderMasTarde', 1, 1);
    $('#popupHacerSatisfaccion').hide();
}

function getCalifiacion(id_sesion, id_grupo)
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'getCalificacion',
            id_sesion : id_sesion,
            id_grupo : id_grupo,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            console.log('Calificacion');
            console.log(miData);

            if(Number.isInteger(miData[0].puntuacion))
                document.getElementById('star'+miData[0].puntuacion).click()
            else
                document.getElementById('star'+parseInt(miData[0].puntuacion)+'half').click()

            for (var i = 0; i < miData[0].respuestas.length; i++) {
                document.getElementById('calificacion-'+miData[0].respuestas[i].id_pregunta+'-'+miData[0].respuestas[i].id_respuesta).click();
            }
            $('#calificacion-texto').text(miData[0].txt_comentario);
        }
    });
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function deleteCookie( name ) {
    document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

function getCheckboxValues(name)
{
	var checkboxes = document.getElementsByName(name+'[]');
	var vals = "";
	for (var i=0, n=checkboxes.length;i<n;i++) 
	{
	    if (checkboxes[i].checked) 
	    {
	        vals += ","+checkboxes[i].value;
	    }
	}
	if (vals) vals = vals.substring(1);
	return vals;
}

function getRadio(name)
{
	var radios = document.getElementsByName(name);
	for (var i = 0, length = radios.length; i < length; i++)
	{
		if (radios[i].checked)
		{
		  	return(radios[i].value);
		  	break;
		}
	}
}