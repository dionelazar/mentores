$(document).ready(function(){
	marcarAside();
})

function logout()
{
	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'usuarios',
            call: 'logout',
        },
        success : function(Data){
        	location.replace('index.php');
        }
    });
}

function marcarAside()
{
    var aside = $('#asideMenu li a');
    var url = window.location.pathname;
    var urlArray = url.split("/");
    for (var i = 0; i < aside.length; i++) 
    {
        var href = aside[i].getAttribute("href");
        if(href == urlArray[urlArray.length-1])
        {
            $('#asideMenu li')[i].className = "current";
            return;
        }
    }

    if(urlArray[urlArray.length-1] == "listado" || urlArray[urlArray.length-1].includes("solicitud"))
    {
        $('#asideMenu li')[1].className = "current";
        return;
    }
}

function ajaxGenerico(Action, Call, myParams = null)
{
    var Data = {action : Action, call : Call};

    if( ! jQuery.isEmptyObject(myParams)){
        for (var key in myParams) {
            console.log(key, myParams[key]);
            Data.myParams[key] = myParams[key];
        }
    }

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: Data,
        success : function(Data){
            if(IsJsonString(Data))
            {
                var miData = JSON.parse(Data);
                console.log(miData);
                return miData;
            }else{
                console.log(Data);
                return Data;
            }
        }, error : function(Data){
            console.log(Data);
            return Data;
        }
    });
}

function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}