$(document).ready(function(){
	getGrupos();
	inicializarAcordion();
    getPaises();
    getEstadisticasNumeros();
    getSedes();
});

$('#filtrosPais').change(function(){
    filtrarPorPais();
    getProvincias();
});

$('#filtrosProvincia').change(function(){
    filtrarPorProvincia();
    getCiudades();
});

$('#filtrosCiudad').change(function(){
    filtrarPorCiudad();
});

$('#filtrosMentoria').change(function(){
    filtrarPorMentoria();
});

$('#filtrosSede').change(function(){
    filtrarPorSede();
});

$('#inputSearch').keydown(function(){
    mySearch();
});

$('#btnSatisfaccionDt').click(function(){
    if(globalValoracionSatisfaccionPromedioDT != 0)
        $('#popupSatisfaccionPromedio_valoracion').html(globalValoracionSatisfaccionPromedioDT);
});

$('#btnSatisfaccionConsolidacion').click(function(){
    if(globalValoracionSatisfaccionPromedioCON != 0)
        $('#popupSatisfaccionPromedio_valoracion').html(globalValoracionSatisfaccionPromedioCON);
});

var globalValoracionSatisfaccionPromedioDT = 0;
var globalValoracionSatisfaccionPromedioCON = 0;

function resetearFiltros()
{
    getGrupos();
    $('#filtrosSede').val(0);
    getEstadisticasNumeros();
}

function getGrupos()
{
	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'getGruposReporte',
        },
        success : function(Data){
        	var miData = JSON.parse(Data);
        	armarListaGrupos(miData);
        }
    });
}

function getCalificacionesEmprendedores(id_grupo)
{
	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'getCalificacionesEmprendedores',
            id_grupo : id_grupo,
        },
        success : function(Data){
        	var miData = JSON.parse(Data);
        	armarListaEmprendedoresGrupo(miData, id_grupo);
        }
    });
}

function armarListaGrupos(data)
{
    $('#desarrolloTempranoGrupos').html('');
    $('#consolidacionGrupos').html('');

	for (var i = 0; i < data.length; i++) 
	{
		var html = '';
		html += '<div class="timeline-item timeline-closed" name="'+data[i].grupo+'">';
        if(data[i].imagen != '')
        {
            html += '<img src="'+data[i].imagen+'" height="100" width="100" style="float:right;">';
            //console.log(data[i].mentor);
        }
        html += '    <div class="timeline-closed">';
        html += '        <h2>'+data[i].mentor+'</h2>';
        html += '        <h3><strong>Puntuación del grupo:</strong></h3>';
        html += '        <div class="ratingResult">';
        for (var j = 1; j < 6; j++) 
        {
        	if(data[i].promedio >= j-0.4)
        		html += '<span class="fa fa-star checked"></span>';
        	else
        		html += '<span class="fa fa-star"></span>';
        }
        html += '        </div>';
        html += '    </div>';
        html += '    <a class="verCalificacionBtn">';
        html += '        <button class="verCalificacion" data-icon="&#xe057;" onclick="getUsuariosDelGrupo('+data[i].id_grupo+')"></button>';
        html += '    </a>';
        html += '    <div class="timeline-open" id="calificacionesEmprendedores-'+data[i].id_grupo+'"></div>';
        html += '</div>';

        if(data[i].id_tipo_grupo == 2)
        	$('#desarrolloTempranoGrupos').append(html);
        else
			$('#consolidacionGrupos').append(html);
	}
}

function armarListaEmprendedoresGrupo(data, id_grupo)
{
	var	html  = '<hr>';
		html += '<ul>';
	for (var i = 0; i < data.length; i++) 
	{
		html += '    <li>';
		html += '        <i data-icon="&#xe056;"></i>';
		html += '        <h4><strong>'+data[i].nombre+'</strong></h4>';
		html += '        <div class="ratingResult">';
		for (var j = 1; j < 6; j++) 
        {
        	if(data[i].promedio >= j-0.4)
        		html += '<span class="fa fa-star checked"></span>';
        	else
        		html += '<span class="fa fa-star"></span>';
        }
		html += '        </div>';
		html += '        <p>"'+data[i].comentario+'"</p>';
		html += '    </li>';
	}
		html += '</ul>';
	
	$('#calificacionesEmprendedores-'+id_grupo).html(html);

}

function getCalificacionesGrupoAdmin(id_grupo)
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'getCalificacionesGrupoAdmin',
            id_grupo : id_grupo,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarListaCalificacionesGrupo(miData, id_grupo);
        }
    });
}

function armarListaCalificacionesGrupo(data, id_grupo)
{
    //debugger
    //TODO aca hay mucho que arreglar!
    debugger
    var html  = '<hr>';
        html += '<ul>';
    for (var i = 0; i < data.length; i++) 
    {
        if(data[i+1] != undefined && data[i].id_sesion == data[i+1].id_sesion && data[i].id_tipo_usuario == data[i+1].id_tipo_usuario)
            continue;
        
        html += '    <li>';
        html += '        <i data-icon="&#xe056;"></i>';
        html += '        <h4><strong>'+data[i].usuario+'</strong> Sesión '+data[i].id_sesion+'</h4>';
        html += '        <div class="ratingResult">';
        for (var j = 1; j < 6; j++) 
        {
            if(data[i].puntuacion >= j-0.4)
                html += '<span class="fa fa-star checked"></span>';
            else
                html += '<span class="fa fa-star"></span>';
        }
        html += '        </div>';
        
        html += '        <p>"'+data[i].comentario+'"</p>';

        /*
          Recorro cada elemento. Cuando se repite el id_calificacion del actual y el próximo
          paso al próximo elemento y solo veo las preguntas y respuestas.
          Cuando no se repite el actual y el próximo, muestro el actual y salgo.
        */

        while(data[i+1] != undefined && data[i].id_calificacion == data[i+1].id_calificacion)
        {
            html += '    <p>'+data[i].pregunta+' : '+data[i].respuesta+'</p>';
            i++;
        }
        html += '    <p>'+data[i].pregunta+' : '+data[i].respuesta+'</p>';

        html += '    </li>';
    }
        html += '</ul>';
    
    $('#calificacionesEmprendedores-'+id_grupo).html(html);

}

/***** nueva forma de traer los reportes ****/

function getUsuariosDelGrupo(id_grupo)
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'reportes',
            call: 'getUsuariosDelGrupo',
            id_grupo : id_grupo,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            mostrarUsuariosDelGrupo(miData, id_grupo);
        }
    });
}

function mostrarUsuariosDelGrupo(data, id_grupo)
{
    var html  = '<hr>';
        html += '<ul>';
    for (var i = 0; i < data.length; i++) 
    {
        html += '    <li>';
        html += '        <i data-icon="&#xe056;"></i>';
        html += '        <h4><strong>'+data[i].nombre+'</strong> (Dni: '+data[i].dni+')</h4>';
        html += '        <p>Sesiones calificadas: '+data[i].cant_sesiones+'';
        html += '        <a style="width:50%; height:50%; padding-left: 15%;">';
        html += '            <button class="verCalificacion" data-icon="&#xe057;" onclick="getSesionesDelUsuario('+data[i].id_usuario+', '+id_grupo+', '+data[i].id_rol+')"></button>';
        html += '        </a></p>';
        html += '        <div class="calificaciones_div" id="calificaciones_'+id_grupo+'_'+data[i].id_usuario+'" hidden></div>'
        html += '    </li>';
    }
        html += '</ul>';
    
    $('#calificacionesEmprendedores-'+id_grupo).html(html);
}

function getSesionesDelUsuario(id_usuario, id_grupo, id_rol)
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'reportes',
            call: 'getSesionesDelUsuario',
            id_usuario : id_usuario,
            id_grupo : id_grupo,
            id_rol : id_rol,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            mostrarSesionesDelUsuario(miData, id_grupo, id_usuario);
        }
    });
}

function mostrarSesionesDelUsuario(data, id_grupo, id_usuario)
{
    $('.calificaciones_div').fadeOut();

    if($('#calificaciones_'+id_grupo+'_'+id_usuario).is(':visible')){
        $('#calificaciones_'+id_grupo+'_'+id_usuario).fadeOut();
        return;
    }else{
        $('#calificaciones_'+id_grupo+'_'+id_usuario).fadeIn();
    }

    $('#calificaciones_'+id_grupo+'_'+id_usuario).html('');

    if(data[0].sn_califico == 0)
    {
        $('#calificaciones_'+id_grupo+'_'+id_usuario).append('<p>No calificó ninguna sesión</p>');
        return;
    }

    for (var i = 0; i < data.length; i++) {
        if(data[i].id_grupo != id_grupo)
            continue;

        var html  = '<div class="timeline-item activeTimeline" sesion="'+ data[i].desc +'">';
            html +=   '<h2><strong>'+data[i].desc+'</strong></h2>';
            html += '   <div id="calificacion_detalle_'+id_grupo+'_'+data[i].id_sesion+'_'+id_usuario+'"></div>';

            if (data[i].sn_califico == -1 ) 
            {
                html +=     '<div id="calificacion_stars_'+ data[i].id_sesion + '_' + data[i].id_grupo +'">';
                html +=         '<small>Calificación</small><br>';
                html +=         '<div class="ratingResult">';
                html +=             '<span class="fa fa-star" id="1-starSesion-'+ data[i].id_sesion + '-' + data[i].id_grupo +'"></span>';
                html +=             '<span class="fa fa-star" id="2-starSesion-'+ data[i].id_sesion + '-' + data[i].id_grupo +'"></span>';
                html +=             '<span class="fa fa-star" id="3-starSesion-'+ data[i].id_sesion + '-' + data[i].id_grupo +'"></span>';
                html +=             '<span class="fa fa-star" id="4-starSesion-'+ data[i].id_sesion + '-' + data[i].id_grupo +'"></span>';
                html +=             '<span class="fa fa-star" id="5-starSesion-'+ data[i].id_sesion + '-' + data[i].id_grupo +'"></span>';
                html +=         '</div>';
                html +=     '</div>';
                html += '</div>';
            }

            $('#calificaciones_'+id_grupo+'_'+id_usuario).append(html);
        
        getCalifiacion(data[i].id_sesion, id_grupo, id_usuario);
    }
}

function getCalifiacion(id_sesion, id_grupo, id_usuario)
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'getCalificacion',
            id_sesion : id_sesion,
            id_grupo : id_grupo,
            id_usuario : id_usuario,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
debugger
            if(miData.length > 1)
            {
                var html = '';
                for (var i = 0; i < miData.length; i++) {
                    html += '<p><strongEmprendedor:</strong> '+miData[i].txt_nombre+'</p>'
                    html += '<p><strong>Fecha:</strong> '+miData[i].fecha+'</p>';
                    html += '<p><strong>Comentario:</strong> '+miData[i].txt_comentario+'</p>';
                    for (var j = 0; j < miData[i].respuestas.length; j++) {
                        html += '<p><strong>'+miData[i].respuestas[j].pregunta+':</strong> '+miData[i].respuestas[j].respuesta+' </p>';
                    }
                    html +='<small>Calificación</small><br>';
                    html +='<div class="ratingResult">';
                    for (var j = 0; j < 5; j++) {
                        if(j <= miData[i].puntuacion)
                            html += '<span class="fa fa-star checked"></span>';
                        else
                            html += '<span class="fa fa-star"></span>';
                    }
                    html +='</div>';
                    html += '<hr>';
                    $('#calificacion_stars_'+ id_sesion + '_' + id_grupo).hide();
                }
                $('#calificacion_detalle_'+id_grupo+'_'+id_sesion+'_'+id_usuario).html(html);
            }else{
                if(miData[0].puntuacion > 0)
                {
                    var nStar = Math.round(miData[0].puntuacion);
                    for (var i = 1; i < nStar+1; i++) {
                        $('#'+ i +'-starSesion-'+ id_sesion + '-' + id_grupo).addClass('checked');
                    }
                }

                var html = '';
                    html += '<p><strong>Fecha:</strong> '+miData[0].fecha+'</p>';
                    html += '<p><strong>Comentario:</strong> '+miData[0].txt_comentario+'</p>';
                    for (var i = 0; i < miData[0].respuestas.length; i++) {
                        html += '<p><strong>'+miData[0].respuestas[i].pregunta+':</strong> '+miData[0].respuestas[i].respuesta+' </p>';
                    }

                $('#calificacion_detalle_'+id_grupo+'_'+id_sesion+'_'+id_usuario).html(html);
            }
        }
    });
}

/***************/

function getPaises()
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'localidades',
            call: 'getPaises',
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            var options = '<option value="">Filtrar por país</option>';
            for (var i = 0; i < miData.length; i++) {
                options += '<option value="'+miData[i].id_pais+'">'+miData[i].pais+'</option>';
                break;
            }
            $('#filtrosPais').html(options);
        }
    });
}

function getProvincias()
{
    var id_pais = $('#filtrosPais').val();

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'localidades',
            call: 'getProvincias',
            id_pais : id_pais,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            var options = '<option value="">Filtrar por provincia</option>';
            for (var i = 0; i < miData.length; i++) {
                options += '<option value="'+miData[i].id_pais+'_'+miData[i].id_provincia+'">'+miData[i].provincia+'</option>';
            }
            $('#filtrosProvincia').html(options);
        }
    });
}

function getCiudades()
{
    var v = $('#filtrosProvincia').val();
    var sp = v.split("_");
    var id_pais = sp[0];
    var id_provincia = sp[1];

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'localidades',
            call: 'getCiudades',
            id_pais : id_pais,
            id_provincia : id_provincia,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            var options = '<option value="">Filtrar por ciudad</option>';
            for (var i = 0; i < miData.length; i++) {
                options += '<option value="'+miData[i].id_pais+'_'+miData[i].id_provincia+'_'+miData[i].id_ciudad+'">'+miData[i].ciudad+'</option>';
            }
            $('#filtrosCiudad').html(options);
        }
    });
}

function getSedes()
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'localidades',
            call: 'getSedes',
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            var options = '<option value="">Seleccioná una sede</option>';
            for (var i = 0; i < miData.length; i++) {
                options += '<option value="'+miData[i].id_sede+'">'+miData[i].sede+'</option>';
            }
            $('#filtrosSede').html(options);
        }
    });
}

function filtrarPorPais()
{
    var id_pais = $('#filtrosPais').val();

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'getGruposPorPais',
            id_pais : id_pais,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarListaGrupos(miData);
        }
    });
}

function filtrarPorProvincia()
{
    var v = $('#filtrosProvincia').val();
    var sp = v.split("_");

    var id_pais = sp[0];
    var id_provincia = sp[1];

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'getGruposPorProvincia',
            id_pais : id_pais,
            id_provincia : id_provincia,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarListaGrupos(miData);
        }
    });
}

function filtrarPorCiudad()
{
    var v = $('#filtrosCiudad').val();
    var sp = v.split("_");

    var id_pais = sp[0];
    var id_provincia = sp[1];
    var id_ciudad = sp[2];

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'getGruposPorCiudad',
            id_pais : id_pais,
            id_provincia : id_provincia,
            id_ciudad : id_ciudad,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarListaGrupos(miData);
        }
    });
}

function filtrarPorMentoria()
{
    var id_mentoria = $('#filtrosMentoria').val();

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'getGruposPorMentoria',
            id_mentoria : id_mentoria,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarListaGrupos(miData);
        }
    });
}

function filtrarPorSede()
{
    var id_sede = $('#filtrosSede').val();

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'getGruposPorSede',
            id_sede : id_sede,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarListaGrupos(miData);
            getEstadisticasNumeros();
        }
    });
}

function getEstadisticasNumeros()
{
    var id_sede = $('#filtrosSede').val();

    if(id_sede == undefined || id_sede == '')
        id_sede = 0;

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'reportes',
            call: 'getEstadisticasNumeros',
            id_sede : id_sede,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarEstadisticasNumeros(miData);
        }
    });
}

function armarEstadisticasNumeros(data)
{
    $('#num_mentores_formacion').html(data.mentores_formacion);
    $('#num_mentores_formados').html(data.mentores_formados);
    $('#num_mentorias_activas').html(data.mentorias_activas);
    $('#num_emprendedores_en_proceso').html(data.emprendedores_en_proceso);
    $('#num_emprendedores_egresados_1').html(data.emprendedores_egresados);
    $('#num_emprendedores_egresados_2').html(0);
    $('#num_alcance_territorial').html(data.alcance_territorial);
    $('#num_atencion_por_participante').html('Ver detalle');
    $('#num_impacto_grado_satisfaccion').html(data.impacto_grado_satisfaccion);//+'<span class="fa fa-star checked"></span>'
    $('#num_genero').html('<span style="color:#FF6B1F;">'+data.generoF+'%</span>/<span style="color: #0074AD">'+data.generoM+'%</span>');
    $('#num_diversidad_productiva').html('Ver detalle');

    $('#num_alcance_territorial_popup').html(data.alcance_territorial);

    var porcentajeAvanceMentoresFormados = Math.round( (data.mentores_formados / 400) * 100 );
    $('.porcentajeAvanceMentoresFormados').html(porcentajeAvanceMentoresFormados + '%');
    $('#porcentajeAvanceMentoresFormados').html(porcentajeAvanceMentoresFormados + '%');
    $('#porcentajeAvanceMentoresFormados').attr('title', 'Obetivo 400 | Avance '+porcentajeAvanceMentoresFormados+'%');
    $('#porcentajeAvanceMentoresFormados').attr('style', 'width: '+porcentajeAvanceMentoresFormados+'%');

    var porcentajeAvanceEmprendedoresProceso = Math.round ( (data.emprendedores_en_proceso / 2000) * 100 );
    $('.porcentajeAvanceEmprendedoresProceso').html(porcentajeAvanceEmprendedoresProceso + '%');
    $('#porcentajeAvanceEmprendedoresProceso').html(porcentajeAvanceEmprendedoresProceso + '%');
    $('#porcentajeAvanceEmprendedoresProceso').attr('title', 'Obetivo 2000 | Avance '+porcentajeAvanceEmprendedoresProceso+'%');
    $('#porcentajeAvanceEmprendedoresProceso').attr('style', 'width: '+porcentajeAvanceEmprendedoresProceso+'%');

    $('#cant_emprendedores_proceso').html(data.emprendedores_en_proceso);
}

function getReporteMentoresEnFormacion()
{
    var id_sede = $('#filtrosSede').val();

    if(id_sede == undefined || id_sede == '')
        id_sede = 0;

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'reportes',
            call: 'getMentoresEnFormacion',
            id_sede : id_sede,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarReporteMentoresEnFormacion(miData);
        }
    });
}

function armarReporteMentoresEnFormacion(data)
{
    $('#mentoresEnFormacionDT').html('');
    $('#mentoresEnFormacionCON').html('');
    var cant_mentores_formacion_dt = 0
    var cant_mentores_formacion_con = 0
    var total = 0;

    for (var i = 0; i < Object.keys(data).length; i++) 
    {
        if(i == Object.keys(data).length - 1)
        {
            html = '<a href="files/'+data[i].file+'" download>Descargar excel</a>';
            $('#divExcelMentoresEnFormacion').html(html);
            break;
        }else{
            html  = '<tr>';
            html += '    <td data-label="Nombre: "><a href="javascript:void(0)">'+data[i].nombre+'</a></td>';
            //html += '    <td data-label="Ciudad: ">'+data[i].ciudad+', '+data[i].provincia+'</td>';
            html += '    <td data-label="Sede: ">'+data[i].sede+'</td>';
            html += '    <td data-label="E-mail: " style="white-space: nowrap;">'+data[i].email+'com</td>';
            html += '</tr>';

            if(data[i].id_tipo_usuario == 2){
                $('#mentoresEnFormacionDT').append(html);
                cant_mentores_formacion_dt++;
            }else{
                $('#mentoresEnFormacionCON').append(html);
                cant_mentores_formacion_con++;
            }
            total++;
        }
    }
    $('#cant_mentores_formacion_dt').html(cant_mentores_formacion_dt);
    $('#cant_mentores_formacion_con').html(cant_mentores_formacion_con);
}

function getReporteMentoresFormados()
{
    var id_sede = $('#filtrosSede').val();

    if(id_sede == undefined || id_sede == '')
        id_sede = 0;

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'reportes',
            call: 'getMentoresFormados',
            id_sede : id_sede,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarReporteMentoresFormados(miData);
        }
    });
}

function armarReporteMentoresFormados(data)
{
    $('#mentoresFormadosDT').html('');
    $('#mentoresFormadosCON').html('');
    var cant_mentores_formados_dt = 0
    var cant_mentores_formados_con = 0
    var total = 0;

    for (var i = 0; i < data.length; i++) 
    {
        if(i == Object.keys(data).length - 1)
        {
            html = '<a href="files/'+data[i].file+'" download>Descargar excel</a>';
            $('#divExcelMentoresFormados').html(html);
            break;
        }else{
            html  = '<tr>';
            html += '    <td data-label="Nombre: "><a href="javascript:void(0)">'+data[i].nombre+'</a></td>';
            //html += '    <td data-label="Ciudad: ">'+data[i].ciudad+', '+data[i].provincia+'</td>';
            html += '    <td data-label="Sede: ">'+data[i].sede+'</td>';
            html += '    <td data-label="E-mail: " style="white-space: nowrap;">'+data[i].email+'com</td>';
            html += '</tr>';

            if(data[i].id_tipo_usuario == 2){
                $('#mentoresFormadosDT').append(html);
                cant_mentores_formados_dt++;
            }else{
                $('#mentoresFormadosCON').append(html);
                cant_mentores_formados_con++;
            }
            total++;
        }
    }
    $('#cant_mentores_formados_dt').html(cant_mentores_formados_dt);
    $('#cant_mentores_formados_con').html(cant_mentores_formados_con);
    $('#cant_mentores_formados_total').html(total);
}

function getReporteGenero()
{
    var id_sede = $('#filtrosSede').val();

    if(id_sede == undefined || id_sede == '')
        id_sede = 0;

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'reportes',
            call: 'getReporteGenero',
            id_sede : id_sede,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarReporteGenero(miData);
        }
    });
}

function armarReporteGenero(data)
{
    $('#mentoresM').html(data.mentoresM+'%');
    $('#mentoresF').html(data.mentoresF+'%');
    $('#emprendedoresM').html(data.emprendedoresM+'%');
    $('#emprendedoresF').html(data.emprendedoresF+'%');
    $('#mentoresConM').html(data.mentoresConM+'%');
    $('#mentoresConF').html(data.mentoresConF+'%');
    $('#emprendedoresConM').html(data.emprendedoresConM+'%');
    $('#emprendedoresConF').html(data.emprendedoresConF+'%');
    $('#mentoresDtM').html(data.mentoresDtM+'%');
    $('#mentoresDtF').html(data.mentoresDtF+'%');
    $('#emprendedoresDtM').html(data.emprendedoresDtM+'%');
    $('#emprendedoresDtF').html(data.emprendedoresDtF+'%');
    $('#generoDescargarExcel').attr('href', 'files/'+data.excel.file);
}

function getMentoriasActivasDT()
{
    var id_sede = $('#filtrosSede').val();

    if(id_sede == undefined || id_sede == '')
        id_sede = 0;

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'reportes',
            call: 'getMentoriasActivasDT',
            id_sede : id_sede,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarMentoriasActivasDT(miData);
            getSesionesAtencion();
        }
    });    
}

function getMentoriasActivasCON()
{
    var id_sede = $('#filtrosSede').val();

    if(id_sede == undefined || id_sede == '')
        id_sede = 0;

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'reportes',
            call: 'getMentoriasActivasCON',
            id_sede : id_sede,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarMentoriasActivasCON(miData);
            getSesionesAtencion();
        }
    });    
}

function armarMentoriasActivasCON(data)
{
    var html = '';
    $('#cant_grupos_creados_mentorias_con').html(data.length - 1);

    for (var i = 0; i < data.length; i++) 
    {
        if(i == data.length - 1 || data[i].cod_mensaje == 2)
        {
            var excel = '<a href="files/'+data[i].file+'" download>Descargar excel</a>';
            $('#divExcelMentoriasActivas').html(excel);
            break;
        }

        html += '<tr>';
        html += '   <td>'+data[i].mentor+'</td>';
        html += '   <td>'+data[i].emprendedores+'</td>';
        html += '   <td>'+data[i].promedio_mentor+'</td>';
        html += '   <td>'+data[i].promedio_emprendedor+'</td>';
        html += '   <td>'+data[i].asistencias+'/3</td>';
        html += '</tr>';
    }

    $('#mentoriasActivasCON').html(html);
}

function armarMentoriasActivasDT(data)
{
    $('#mentoriasActivasDT').html('');

    var total = 0;
    var cant_DT = 0;
    var cant_CON = 0;

    for (var i = 0; i < data.length; i++) 
    {
        if(i == data.length - 1)
        {
            html = '<a href="files/'+data[i].file+'" download>Descargar excel</a>';
            $('#divExcelMentoriasActivas').html(html);
            break;
        }else{
            var emprendedores = data[i].emprendedores.split(",")
            emprendedores[0] = emprendedores[0] == undefined ? "" : emprendedores[0];
            emprendedores[1] = emprendedores[1] == undefined ? "" : emprendedores[1];
            emprendedores[2] = emprendedores[2] == undefined ? "" : emprendedores[2]

            if(emprendedores[0].split(",").length == 2)
            {
                var e = emprendedores[0].split(",")
                emprendedores[0] = e[1];
            }
            html  = '<tr>';
            if(data[i].id_tipo_usuario == 2)
                html += '<td data-label="Nombre Mentor: ">'+data[i].grupo +'</td>';
            html += '    <td data-label="Nombre Mentor: ">'+data[i].mentor +'</td>';
            html += '    <td data-label="Nombre Emprendedor 1: ">'+emprendedores[0]+'</td>';
            html += '    <td data-label="Nombre Emprendedor 2: ">'+emprendedores[1]+'</td>';
            html += '    <td data-label="Nombre Emprendedor 3: ">'+emprendedores[2]+'</td>';
            if(data[i].id_tipo_usuario == 3)
                html += '    <td data-label="Nombre Emprendedor 4: "></td>';
            html += '    <td data-label="">'+data[i].promedio_calificacion+'</td>';
            html += '    <td data-label="">'+data[i].cant_sesiones+'/6</td>';
            html += '</tr>';

            if(data[i].id_tipo_usuario == 2){
                $('#mentoriasActivasDT').append(html);
                cant_DT++;
            }else{
                $('#mentoriasActivasCON').append(html);
                cant_CON++;
            }
            total++;
        }
    }
    
    $('#cant_grupos_creados_mentorias_dt').html(cant_DT)
    $('#cant_grupos_creados_mentorias').html($('#num_mentorias_activas').text());
}

function getEmprendedoresEgresados()
{
    var id_sede = $('#filtrosSede').val();

    if(id_sede == undefined || id_sede == '')
        id_sede = 0;
    
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'reportes',
            call: 'getEmprendedoresEgresados',
            id_sede : id_sede,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarEmprendedoresEgresados(miData);
        }
    });    
}

function armarEmprendedoresEgresados(data)
{
    $('#table_egresados_dt').html('');
    $('#table_egresados_con').html('');
    var total = 0;
    var cant_DT = 0;
    var cant_CON = 0;

    for (var i = 0; i < data.length; i++) 
    {
        if(i == data.length - 1)
        {
            html = '<a href="files/'+data[i].file+'" download>Descargar excel</a>';
            $('#divExcelEmprendedoresEgresados').html(html);
            break;
        }

        html  = '<tr>';
        html += '    <td data-label="Nombre: "><a href="javascript:void(0)">'+data[i].nombre+'</a></td>';
        html += '    <td data-label="Sede: ">'+data[i].sede+'</td>';
        html += '    <td data-label="E-mail: " style="white-space: nowrap;">'+data[i].email+'</td>';
        html += '    <td data-label="Dni: " style="white-space: nowrap;">'+data[i].dni+'</td>';
        html += '    <td data-label="Mentor: " style="white-space: nowrap;">'+data[i].mentor+'</td>';
        html += '</tr>';

        if(data[i].id_tipo_usuario == 4){
            $('#table_egresados_dt').append(html);
            cant_DT++;
        }else{
            $('#table_egresados_con').append(html);
            cant_CON++;
        }
        total++;
    }

    $('#cant_emprendedores_egresados_dt').html(cant_DT);
    $('#cant_emprendedores_egresados_con').html(cant_CON);
}

function getEncuestasSatisfaccion()
{
    var id_sede = $('#filtrosSede').val();

    if(id_sede == undefined || id_sede == '')
        id_sede = 0;

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'reportes',
            call: 'getEncuestasSatisfaccion',
            id_sede : id_sede,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarEncuestasSatisfaccion(miData);
        }
    });
}

function armarEncuestasSatisfaccion(data)
{
    $('#popupSatisfaccion_dt').html('');
    $('#popupSatisfaccion_con').html('');
    var estructuraDT = 0;
    var utilidadDT = 0;
    var interaccionDT = 0;
    var desempenoDT = 0;
    var materialDT = 0;
    var valoracionDT = 0;

    var estructuraCON = 0;
    var utilidadCON = 0;
    var interaccionCON = 0;
    var desempenoCON = 0;
    var materialCON = 0;
    var valoracionCON = 0;

    var cantDT = 0;
    var cantCON = 0;
    
    for (var i = 0; i < data.length; i++) 
    {
        if(i == data.length - 1)
        {
            html = '<a href="files/'+data[i].file+'" download>Descargar excel</a>';
            $('#divExcelEncuestaSatisfaccion').html(html);
            break;
        }

        var html = '<div class="panel panel-default">';
        html += '    <div class="panel-heading" role="tab" id="headingOne">';
        html += '        <h4 class="panel-title">';
        html += '        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseM1" aria-expanded="true" aria-controls="collapseM1">';
        html += '            '+data[i].nombre;
        html += '            <i data-icon="T" class="pull-right"></i>';
        html += '        </a>';
        html += '        </h4>';
        html += '    </div>';
        html += '    <div id="collapseM1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="collapseM1">';
        html += '        <ul class="list-group">';
        html += '            <li class="list-group-item">';
        html += '                <strong>Estructura y contenidos de cada reunión:</strong>';
        html += '                <div class="ratingResult">';
                                for (var j = 1; j < 6; j++) 
                                {
                                    if(data[i].estructura >= j-0.4)
                                        html += '<span class="fa fa-star checked"></span>';
                                    else
                                        html += '<span class="fa fa-star"></span>';
                                }
        html += '                </div>';
        html += '            </li>';
        html += '            <li class="list-group-item">';
        html += '                <strong>Utilidad de los temas trabajados:</strong>';
        html += '                <div class="ratingResult">';
                                for (var j = 1; j < 6; j++) 
                                {
                                    if(data[i].utilidad >= j-0.4)
                                        html += '<span class="fa fa-star checked"></span>';
                                    else
                                        html += '<span class="fa fa-star"></span>';
                                }
        html += '                </div>';
        html += '            </li>';
        html += '            <li class="list-group-item">';
        html += '                <strong>Valor de la interacción con otros emprendedores:</strong>';
        html += '                <div class="ratingResult">';
                                for (var j = 1; j < 6; j++) 
                                {
                                    if(data[i].interaccion >= j-0.4)
                                        html += '<span class="fa fa-star checked"></span>';
                                    else
                                        html += '<span class="fa fa-star"></span>';
                                }
        html += '                </div>';
        html += '            </li>';
        html += '            <li class="list-group-item">';
        html += '                <strong>Desempeño del mentor:</strong>';
        html += '                <div class="ratingResult">';
                                for (var j = 1; j < 6; j++) 
                                {
                                    if(data[i].desempeno >= j-0.4)
                                        html += '<span class="fa fa-star checked"></span>';
                                    else
                                        html += '<span class="fa fa-star"></span>';
                                }
        html += '                </div>';
        html += '            </li>';
        html += '            <li class="list-group-item">';
        html += '                <strong>Material de apoyo:</strong>';
        html += '                <div class="ratingResult">';
                                for (var j = 1; j < 6; j++) 
                                {
                                    if(data[i].material >= j-0.4)
                                        html += '<span class="fa fa-star checked"></span>';
                                    else
                                        html += '<span class="fa fa-star"></span>';
                                }
        html += '                </div>';
        html += '            </li>';
        html += '            <li class="list-group-item"><strong>Valoración general del programa: </strong><div class="ratingResult">'+data[i].valoracion+'</div></li>';
        html += '            <li class="list-group-item"><strong>Sede: </strong><div class="ratingResult">'+data[i].sede+'</div></li>';
        html += '            <li class="list-group-item"><strong>Comentario, sugerencia o aspecto de mejora para futuras ediciones:</strong>'+data[i].comentario+'</li>';
        html += '        </ul>';
        html += '    </div>';
        html += '</div>';

        if(data[i].id_tipo_usuario == 2 || data[i].id_tipo_usuario == 4){//dt
            $('#popupSatisfaccion_dt').append(html);
            estructuraDT += data[i].estructura;
            utilidadDT += data[i].utilidad;
            interaccionDT += data[i].interaccion;
            desempenoDT += data[i].desempeno;
            materialDT += data[i].material;
            valoracionDT += data[i].valoracion;
            cantDT++;
        }else{
            $('#popupSatisfaccion_con').append(html);
            estructuraCON += data[i].estructura;
            utilidadCON += data[i].utilidad;
            interaccionCON += data[i].interaccion;
            desempenoCON += data[i].desempeno;
            materialCON += data[i].material;
            valoracionCON += data[i].valoracion;
            cantCON++;
        }
    }

    var estructuraPromedioDT  = estructuraDT / cantDT;
    var utilidadPromedioDT    = utilidadDT / cantDT;
    var interaccionPromedioDT = interaccionDT / cantDT;
    var desempenoPromedioDT   = desempenoDT / cantDT;
    var materialPromedioDT    = materialDT / cantDT;
    var valoracionPromedioDT  = valoracionDT / cantDT;

    var estructuraPromedioCON  = estructuraCON / cantCON;
    var utilidadPromedioCON    = utilidadCON / cantCON;
    var interaccionPromedioCON = interaccionCON / cantCON;
    var desempenoPromedioCON   = desempenoCON / cantCON;
    var materialPromedioCON    = materialCON / cantCON;
    var valoracionPromedioCON  = valoracionCON / cantCON;

    for (var i = 1; i <= estructuraPromedioDT; i++) {
        $('#popupSatisfaccionPromedio_'+i+'_estructura').addClass('checked');
    }

    for (var i = 1; i <= utilidadPromedioDT; i++) {
        $('#popupSatisfaccionPromedio_'+i+'_utilidad').addClass('checked');
    }

    for (var i = 1; i <= interaccionPromedioDT; i++) {
        $('#popupSatisfaccionPromedio_'+i+'_interaccion').addClass('checked');
    }

    for (var i = 1; i <= desempenoPromedioDT; i++) {
        $('#popupSatisfaccionPromedio_'+i+'_desempeno').addClass('checked');
    }

    for (var i = 1; i <= materialPromedioDT; i++) {
        $('#popupSatisfaccionPromedio_'+i+'_material').addClass('checked');
    }

    valoracionPromedioDT = valoracionPromedioDT.toFixed(2);
    valoracionPromedioCON = valoracionPromedioCON.toFixed(2);
    globalValoracionSatisfaccionPromedioDT = valoracionPromedioDT;
    globalValoracionSatisfaccionPromedioCON = valoracionPromedioCON;
    $('#popupSatisfaccionPromedio_valoracion').html(valoracionPromedioDT);
}

function getEmprendedoresProcesoMentoria()
{
    var id_sede = $('#filtrosSede').val();

    if(id_sede == undefined || id_sede == '')
        id_sede = 0;
    
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'reportes',
            call: 'getEmprendedoresProceso',
            id_sede : id_sede,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarReporteEmprendedoresProcesoMentoria(miData);
            $('#popupEmprendedoresProcesoMentoria').show();
        }
    });
}

function armarReporteEmprendedoresProcesoMentoria(data)
{
    var cant_emprendedores_formacion_dt = 0
    var cant_emprendedores_formacion_con = 0
    var total = 0;

    $('#emprendedoresProcesoMentoriaDT').html('');
    $('#emprendedoresProcesoMentoriaCON').html('');

    for (var i = 0; i < data.length; i++) 
    {
        if(i == data.length - 1)
        {
            html = '<a href="files/'+data[i].file+'" download>Descargar excel</a>';
            $('#divExcelEmprendedoresEnProceso').html(html);
            break;
        }else{
            var cant_sesiones = data[i].id_tipo_usuario == 4 ? '6' : '3';

            html  = '<tr>';
            html += '    <td data-label="Nombre: "><a href="javascript:void(0)">'+data[i].nombre+'</a></td>';
            html += '    <td data-label="E-mail: " style="white-space: nowrap;">'+data[i].sede+'</td>';
            html += '    <td data-label="E-mail: " style="white-space: nowrap;">'+data[i].nombre_mentor+'</td>';
            html += '    <td data-label="E-mail: " style="white-space: nowrap;">'+data[i].sesiones+'/'+cant_sesiones+'</td>';
            html += '    <td data-label="E-mail: " style="white-space: nowrap;">'+data[i].rol+'</td>';
            html += '</tr>';

            if(data[i].id_tipo_usuario == 4){
                $('#emprendedoresProcesoMentoriaDT').append(html);
                cant_emprendedores_formacion_dt++;
            }else{
                $('#emprendedoresProcesoMentoriaCON').append(html);
                cant_emprendedores_formacion_con++;
            }
            total++;
        }
    }
    $('#cant_emprendedores_formacion_dt').html(cant_emprendedores_formacion_dt);
    $('#cant_emprendedores_formacion_con').html(cant_emprendedores_formacion_con);
}

function getSesionesAtencion()
{
    var id_sede = $('#filtrosSede').val();

    if(id_sede == undefined || id_sede == '')
        id_sede = 0;
    
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'reportes',
            call: 'getSesionesAtencion',
            id_sede : id_sede,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarSesionesAtencion(miData);
        }
    });    
}

function armarSesionesAtencion(data)
{
    $('#DTperSes1').text(data.DTperSes1 + '%');
    $('#DTperSes2').text(data.DTperSes2 + '%');
    $('#DTperSes3').text(data.DTperSes3 + '%');
    $('#DTperSes4').text(data.DTperSes4 + '%');
    $('#DTperSes5').text(data.DTperSes5 + '%');
    $('#DTperSes6').text(data.DTperSes6 + '%');
    $('#CONperSes1').text(data.CONperSes1 + '%');
    $('#CONperSes2').text(data.CONperSes2 + '%');
    $('#CONperSes3').text(data.CONperSes3 + '%');
}

function getUsuariosAtencion()
{
    var id_sede = $('#filtrosSede').val();

    if(id_sede == undefined || id_sede == '')
        id_sede = 0;
    
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'reportes',
            call: 'getUsuariosEstadoAtencion',
            id_sede : id_sede,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarEstadoAtencion(miData);
        }
    });    
}

function armarEstadoAtencion(data)
{
    $('#usuarios_estado_atencion_dt').html('');
    $('#usuarios_estado_atencion_con').html('');

    for (var i = 0; i < data.length; i++) 
    {
        if(i == data.length - 1)
        {
            html = '<a href="files/'+data[i].file+'" download>Descargar excel</a>';
            $('#divExcelAtecionParticipante').html(html);
            break;
        }else{
            html  = '<tr>';
            html += '    <td data-label="Nombre Emprendedor: ">'+data[i].emprendedor+'</td>';
            html += '    <td data-label="DNI: ">'+data[i].dni+'</td>';
            html += '    <td data-label="Nombre Mentor 1: ">'+data[i].mentor+'</td>';
            html += '    <td data-label="Valoracion: ">'+data[i].valoracion+'</td>';
            html += '    <td data-label="Sesiones: ">'+data[i].sesiones+' / '+data[i].total_sesiones+'</td>';
            html += '    <td data-label="Activo: ">'+data[i].activo+'</td>';
            html += '</tr>';

            if(data[i].id_tipo_usuario == 2 || data[i].id_tipo_usuario == 4)//dt
                $('#usuarios_estado_atencion_dt').append(html);
            else
                $('#usuarios_estado_atencion_con').append(html);
        }
    }
}

function getDiversidadProductiva()
{
    var id_sede = $('#filtrosSede').val();

    if(id_sede == undefined || id_sede == '')
        id_sede = 0;
    //TODO
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'reportes',
            call: 'getDiversidadProductiva',
            id_sede : id_sede,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarDiversidadProductiva(miData);
        }
    });    
}

function armarDiversidadProductiva(data)
{
    var html = '';
    for (var i = 0; i < data.length; i++) 
    {
        if(i == data.length - 1)
        {
            var excel = '<a href="files/'+data[i].file+'" download>Descargar excel</a>';
            $('#divExcelDiversidadProductiva').html(excel);
            break;
        }else{
            html += '<tr>';
            html += '    <td data-label="Rubro: "><a href="javascript:void(0)">'+data[i].rubro+'</a></td>';
            html += '    <td data-label="Porcentaje: ">'+data[i].perRubro+'%</td>';
            html += '</tr>';
        }
    }

    $('#body_diversidad_productiva').html(html);
}

function getImpactoDesarrollo()
{
    var id_sede = $('#filtrosSede').val();

    if(id_sede == undefined || id_sede == '')
        id_sede = 0;
    //TODO
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'reportes',
            call: 'getImpactoDesarrollo',
            id_sede : id_sede,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarImpactoDesarrollo(miData);
        }
    });   
}

function armarImpactoDesarrollo(data)
{
    $('#impactoDT').html('');
    $('#impactoCON').html('');
    var cantCon = 0;
    var cantDT = 0;

    for (var i = 0; i < data.length; i++) 
    {
        if(i == data.length - 1)
        {
            html = '<a href="files/'+data[i].file+'" download>Descargar excel</a>';
            $('#divExcelImpactoDesarrollo').html(html);
            break;
        }else{
            html  = '<tr>';
            html += '    <td data-label="Nombre Emprendedor: ">'+data[i].emprendedor+'</td>';
            html += '    <td data-label="DNI: ">'+data[i].dni+'</td>';
            html += '    <td data-label="Nombre Mentor 1: ">'+data[i].sede+'</td>';
            html += '    <td data-label="Valoracion: ">'+data[i].diagnostico_inicial+'</td>';
            html += '    <td data-label="Sesiones: ">'+data[i].diagnostico_final+'</td>';
            html += '    <td data-label="Sesiones: ">'+data[i].impacto+'</td>';
            html += '</tr>';

            if(data[i].id_tipo_usuario == 2 || data[i].id_tipo_usuario == 4){
                $('#impactoDT').append(html);
                cantDT++;
            }else{
                $('#impactoCON').append(html);
                cantCon++;
            }
        }
    }

    $('#cant_impacto_dt').text(cantDT);
    $('#cant_impacto_con').text(cantCon);
}

function mostrarPopUpSedesOperativas()
{
    $('#popUpSedesOperativas').show();

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'reportes',
            call: 'getSedesOperativas',
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarSedesOperativas(miData);
        }
    });  
}

function armarSedesOperativas(data)
{
    
    var html = '';
    for (var i = 0; i < data.length; i++) 
    {
        if(i == data.length - 1)
        {
            var excel = '<a href="files/'+data[i].file+'" download>Descargar excel</a>';
            $('#divExcelSedesOperativas').html(excel);
            break;
        }else{
            html += '<tr>';
            html += '    <td data-label="Sede: ">'+data[i].sede+'</td>';
            html += '    <td data-label="Cantidad de emprendedores: ">'+data[i].emprendedores+'</td>';
            html += '    <td data-label="Cantidad de mentores: ">'+data[i].mentores+'</td>';
            html += '</tr>';
        }
    }
    $('#tableSedesOperativas').html(html);
}

function mySearch() 
{
    var input, filter, table, tr, td, i;
    input = document.getElementById("inputSearch");
    filter = input.value.toUpperCase();
    table = $('#listaUsuarios');
    //table = document.getElementById("myTable");
    var tr = $('#desarrolloTempranoGrupos div');
    //tr = table.getElementsByTagName("tr");

      // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) 
    {
        var t = tr[i].getElementsByTagName("div")[0];
        td = t.getElementsByTagName("h2")[0];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        } 
    }
}
//falta:
//alcance territorial
//satisfacción (hay qeu hacer la encuesta!!)