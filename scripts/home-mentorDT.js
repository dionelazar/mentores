$(document).ready(function(){
	getSesiones();
	popUpCiudad();
    comprobarEncuestaSatisfaccion();
    getEmprendedores();
    getCalificacionesGrupo();
    comprobarEncuestaMentorValidacion();
    comprobarFotoPerfil();
});

var sesionSeleccionada = 0;
var grupoSeleccionado  = 0;
var id_grupos = [];
var emprendedor_elegido = [] ;
var expertises = [];
var globalEmprendedores = [];
var globalAction = '';

$('#selectProvincia').change(function(){
    getCiudades();
});

$('#homeTab1').click(function(){
    grupoSeleccionado = id_grupos[0];
    getEmprendedores();
});

$('#homeTab2').click(function(){
    grupoSeleccionado = id_grupos[1];
    getEmprendedores();
});

function getSesiones()
{
	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'getSesiones',
        },
        success : function(Data){
            //console.log(Data);
        	var miData = JSON.parse(Data);
        	armarSesiones(miData);
        }
    });
}

function armarSesiones(data)
{
	for (var i = 0; i < data.length; i++) 
	{
		var html  = '<div class="timeline-item activeTimeline" sesion="'+ data[i].desc +'">';
	        //html += 	'<h3>Grupo: <strong>Nombre del grupo</strong></h3>';
	        //html += 	'<p>Emprendedores: <strong>'+ data[i].emprendedores +'</strong></p>';
            if (data[i].sn_califico == 0) 
            {
                html +=     '<div id="div_calificar_'+ data[i].id_sesion + '_' + data[i].id_grupo +'">';
    	        	html += 	'<a href="#popupCalificacion" class="calificarBtn" id="calificarBtn_'+ data[i].id_sesion + '_' + data[i].id_grupo +'">';
    		        html += 		'<button class="sesionCalificar" onclick="calificarSesion('+ data[i].id_sesion + ',' + data[i].id_grupo +')">Calificar Sesión</button>';
    		        html += 	'</a>';	
                html +=     '</div>';
	        }else{
                html +=     '<div id="div_editar_'+ data[i].id_sesion + '_' + data[i].id_grupo +'">';
                    html +=     '<a href="javascript:void(0)" class="calificarBtn" id="editarBtn_'+ data[i].id_sesion + '_' + data[i].id_grupo +'">';
                    html +=         '<button class="sesionCalificar" onclick="calificarSesion('+ data[i].id_sesion + ',' + data[i].id_grupo +', \'editar\')">Editar calificación</button>';
                    html +=     '</a>'; 
                html +=     '</div>';
            }
            
            html +=     '<p>Mi comentario: <strong id="miComentario-'+ data[i].id_sesion + '_' + data[i].id_grupo +'"></strong></p>';

            html +=     '<small>Mi calificación</small><br>';
            html +=     '<div class="ratingResult">';
            html +=         '<span class="fa fa-star" id="1-miStarSesion-'+ data[i].id_sesion + '-' + data[i].id_grupo +'"></span>';
            html +=         '<span class="fa fa-star" id="2-miStarSesion-'+ data[i].id_sesion + '-' + data[i].id_grupo +'"></span>';
            html +=         '<span class="fa fa-star" id="3-miStarSesion-'+ data[i].id_sesion + '-' + data[i].id_grupo +'"></span>';
            html +=         '<span class="fa fa-star" id="4-miStarSesion-'+ data[i].id_sesion + '-' + data[i].id_grupo +'"></span>';
            html +=         '<span class="fa fa-star" id="5-miStarSesion-'+ data[i].id_sesion + '-' + data[i].id_grupo +'"></span>';
            html +=     '</div>';
            html +=     '<br>';
            html +=     '<div class="calificacionesEmprendedores" id="calificacionesEmprendedores-'+ data[i].id_sesion + '-' + data[i].id_grupo +'"></div>';
	        html += '</div>';

		if( i == 0 || ( data[i].id_grupo == data[0].id_grupo )  ){
		    $('#sesionesGrupo1').append(html);
            if (data[i].sn_califico != 0)
                $('#flechaProgresoSesion'+data[i].id_sesion).addClass("active");
        }else{
			$('#sesionesGrupo2').append(html);
            if (data[i].sn_califico != 0)
                $('#flechaProgresoSesion'+data[i].id_sesion+'_2').addClass("active");
        }

        if(id_grupos.indexOf(data[i].id_grupo) == -1)
            id_grupos.push(data[i].id_grupo);

        grupoSeleccionado = data[0].id_grupo;
        comprobarCalificacion( data[i].id_sesion , data[i].id_grupo );
	}
}

function calificarSesion(id_sesion, id_grupo, action = 'calificar')
{
	getFormCalificacion(id_sesion, id_grupo);
	sesionSeleccionada = id_sesion;
	grupoSeleccionado = id_grupo;
    globalAction = action;
    
    if(action == 'editar')
        getCalificacion(id_sesion, id_grupo);
}

function getEstrellas(id_sesion, id_grupo)
{

}

function getFormCalificacion(id_sesion, id_grupo)
{
    if(emprendedor_elegido.id_usuario == undefined){
        bootbox.alert('No tienes emprendedores asignados');
        return;
    }
    
	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'getFormCalificacionMentorDT',
            id_sesion : id_sesion,
			id_grupo : id_grupo,
            id_emprendedor : emprendedor_elegido.id_usuario,
        },
        success : function(Data){
        	var miData = JSON.parse(Data);
            if(miData.preguntas[0].cod_mensaje == 0)
            {
                bootbox.alert(miData.preguntas[0].mensaje);
                return;
            }
            armarFormCalificacion(miData);
            $('#popupCalificacion').show();
        }
    });
}

function comprobarCalificacion(id_sesion, id_grupo)
{
	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'getComprobarCalificacion',
            id_sesion : id_sesion,
			id_grupo : id_grupo,
        },
        success : function(Data){
        	var miData = JSON.parse(Data);
        	if(miData.promedio > 0)
            {
                var nStar = Math.round(miData.promedio);
                for (var i = 1; i < nStar+1; i++) {
                    $('#'+ i +'-miStarSesion-'+ id_sesion + '-' + id_grupo).addClass('checked');
                }
            }
        }
    });
}

function armarFormCalificacion(data)
{
    globalEmprendedores = data.emprendedores;
	var emprendedores = '';
	for (var i = 0; i < data.emprendedores.length; i++) {
		//emprendedores += '<input type="checkbox" class="asistencia" name="asistencia[]" id="asistencia-'+data.emprendedores[i].id_usuario+'" value="'+data.emprendedores[i].id_usuario+'">';
        //emprendedores += '<label for="asistencia-'+data.emprendedores[i].id_usuario+'">'+data.emprendedores[i].nombre+'</label>';
        emprendedores += '<fieldset class="label30">'
            emprendedores += '<label for="Asistió">¿Asistió '+data.emprendedores[i].nombre+'?</label>';
            emprendedores += '<br>';
            
            if(data.emprendedores[i].sn_abandono == -1){
                emprendedores += '<p>El emprendedor abandonó</p>'
                emprendedores += '<input hidden="" type="radio" class="asistencias" name="asistencia'+i+'" id="asistencia-1-'+i+'" value="'+data.emprendedores[i].id_usuario+'" onclick="marcarAsistencia('+data.emprendedores[i].id_usuario+')">';
                emprendedores += '<label hidden="" for="asistencia-1-'+i+'"><i class="fa fa-check"></i></label>';
                emprendedores += '<input hidden="" type="radio" class="asistencias" name="asistencia'+i+'" id="asistencia-2-'+i+'" value="'+data.emprendedores[i].id_usuario+'" onclick="ocultarCalificarEntero('+data.emprendedores[i].id_usuario+')">';
                emprendedores += '<label hidden="" for="asistencia-2-'+i+'"><i class="fa fa-times"></i></label>';
                emprendedores += '<input type="radio" class="asistencias" name="asistencia'+i+'" id="asistencia-3-'+i+'" value="'+data.emprendedores[i].id_usuario+'" onclick="marcarAbandono('+data.emprendedores[i].id_usuario+')">';
                emprendedores += '<label for="asistencia-3-'+i+'">Si el emprendedor retomó, marcá acá</label>';
            }else{
                emprendedores += '<input type="radio" class="asistencias" name="asistencia'+i+'" id="asistencia-1-'+i+'" value="'+data.emprendedores[i].id_usuario+'" onclick="marcarAsistencia('+data.emprendedores[i].id_usuario+')">';
                emprendedores += '<label for="asistencia-1-'+i+'"><i class="fa fa-check"></i></label>';
                emprendedores += '<input type="radio" class="asistencias" name="asistencia'+i+'" id="asistencia-2-'+i+'" value="'+data.emprendedores[i].id_usuario+'" onclick="ocultarCalificarEntero('+data.emprendedores[i].id_usuario+')">';
                emprendedores += '<label for="asistencia-2-'+i+'"><i class="fa fa-times"></i></label>';
                emprendedores += '<input type="radio" class="asistencias" name="asistencia'+i+'" id="asistencia-3-'+i+'" value="'+data.emprendedores[i].id_usuario+'" onclick="marcarAbandono('+data.emprendedores[i].id_usuario+')">';
                emprendedores += '<label for="asistencia-3-'+i+'">Si el emprendedor no continúa, marcá acá</label>';
            }
        emprendedores += '</fieldset>';
	}

	$('#calificarMarcarAsistencia').html(emprendedores);
		
	var preguntas = '';
	for (var i = 0; i < data.preguntas.length; i++) 
	{
		var num = data.preguntas[i].num_pregunta;
		var id_pregunta = data.preguntas[i].id_pregunta;
		$('#pregunta'+num).html(data.preguntas[i].pregunta);

		var respuestas = '';
		for (var j = 0; j < data.preguntas[i].respuestas.length; j++) 
		{
			var resp = data.preguntas[i].respuestas;
			respuestas += '<input type="radio" class="respuestas-pregunta-'+num+'" name="calificacion'+num+'[]" id="calificacion-'+id_pregunta+'-'+resp[j].id_respuesta+'" value="'+resp[j].id_respuesta+'">';
            respuestas += '<label for="calificacion-'+id_pregunta+'-'+resp[j].id_respuesta+'">'+resp[j].respuesta+'</label>';
		}
		$('#respuestas'+num).html(respuestas);
	}
}

var asistencias = [];

function calificar()
{
	var id_sesion = sesionSeleccionada;
	var id_grupo = grupoSeleccionado;
    //var id_emprendedor = emprendedor_elegido.id_usuario;

	//var asistencias = getCheckboxValues('asistencia');
	var resp1       = getCheckboxValues('calificacion1');
	var resp2       = getCheckboxValues('calificacion2');
	var stars       = getRadio('rating');
	var comentario  = $('#calificacion-texto').val();

	if(!validarCalificacion())
		return;

	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'calificarMentorDT',
            id_sesion : id_sesion,
			id_grupo : id_grupo,
			asistencias : asistencias,
			resp1 : resp1,
			resp2 : resp2,
			stars : stars,
			comentario : comentario,
            //id_emprendedor : id_emprendedor,
        },
        success : function(Data){
        	//console.log(Data);
        	var miData = JSON.parse(Data);
        	bootbox.alert(miData[0].mensaje);
            $('#popupCalificacion').hide();
        	if(miData[0].cod_mensaje == -1)
        	{
        		$('#calificarBtn_'+id_sesion+'_'+id_grupo).hide();
                $('#flechaProgresoSesion'+id_sesion).addClass("active");
                //$('#flechaProgresoSesion'+id_sesion+'_2').addClass("active");
        		comprobarCalificacion(id_sesion, id_grupo);
                comprobarEncuestaSatisfaccion();
                $('#calificacion-texto').val('');
                //elegirEmprendedor(emprendedor_elegido.id_usuario, emprendedor_elegido.nombre, null);
                getCalificacionesGrupo();
                if(id_sesion == 6)
                    location.reload();
        	}
        }
    });
}

function validarCalificacion()
{
	var asistencias = getCheckboxValues('asistencia');
	var resp1       = getCheckboxValues('calificacion1');
	var resp2       = getCheckboxValues('calificacion2');
	var stars       = getRadio('rating');
	var comentario  = $('#calificacion-texto').val();

    if($('#cuestionarioCalificacion').css('display') == 'block'){
        if(resp1.length < 1){
            bootbox.alert('Tenés que responder la pregunta 1');
            return false;
        }

        if(resp2.length < 1){
            bootbox.alert('Tenés que responder la pregunta 1');
            return false;
        }

        if(stars.length < 1){
            bootbox.alert('Tenés que calificar la sesión');
            return false;
        }
    }

	if(comentario.length < 1){
		bootbox.alert('Tenés que escribir un comentario');
		return false;
	}

	return true;
}

function marcarAsistencia(id_usuario)
{
    if(jQuery.inArray(id_usuario, asistencias) === -1)
        asistencias.push(id_usuario);

    if($('#cuestionarioCalificacion').css('display') == 'block')
        return;

    $(".cuestionario").fadeToggle();
    $(this).siblings().change(function(){
        $(".cuestionario").fadeOut();
    });
}

function ocultarCalificarEntero(id_usuario)
{
    if(jQuery.inArray(id_usuario, asistencias) !== -1){
        var index = asistencias.indexOf(id_usuario);
        asistencias.splice( index, 1 );
    }

    var names = [];
    for( var i = 0; i < $('.asistencias').length; i++)
    {
        var input = $('.asistencias')[i];
        names[i] = input.getAttribute("name");
    }

    names = unique(names);

    var ocultar = 0;

    for (var i = 0; i < names.length; i++) {
        if(asistencias.indexOf(getRadio(names[i])) != -1)
        {
            return;
        }
    }

    if($('#cuestionarioCalificacion').css('display') == 'none')
        return;

    if(asistencias.length == 0){
        $(".cuestionario").fadeToggle();
        $(this).siblings().change(function(){
            $(".cuestionario").fadeOut();
        });
    }
}

function getCalificacionesGrupo()
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'getCalificacionesGrupo',
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            var cantidadPropias = 0;
            //console.log(miData);
            if(miData[0].cod_mensaje == 0){
                marcarFlechaProgreso(0,1);
                limpiarStars();
                return;
            }

            limpiarStars();

            $('.calificacionesEmprendedores').html('');

            for (var i = 0; i < miData.length; i++) 
            {
                if(miData[i].propio == -1){
                    $('#calificarBtn_'+miData[i].id_sesion+'_'+miData[i].id_grupo).hide();
                    $('#miComentario-'+miData[i].id_sesion+'_'+miData[i].id_grupo).html(miData[i].comentario);
                    for (var j = 1; j <= miData[i].puntuacion; j++) {
                        $('#'+j+'-miStarSesion-'+miData[i].id_sesion+'-'+miData[i].id_grupo).addClass('checked');
                    }
                    cantidadPropias++;
                }else{
                    var html = '<hr>';
                    html +='<p>Comentario '+miData[i].usuario+': <strong>'+miData[i].comentario+'</strong></p>';
                    html +='<small>Calificación</small><br>';
                    html +='<div class="ratingResult">';
                    for (var j = 0; j < 5; j++) {
                        if(j <= miData[i].puntuacion)
                            html += '<span class="fa fa-star checked"></span>';
                        else
                            html += '<span class="fa fa-star"></span>';
                    }
                    html +='</div>';

                    $('#calificacionesEmprendedores-'+miData[i].id_sesion+'-'+miData[i].id_grupo).append(html);
                }
            }

            //marcarFlechaProgreso(cantidadPropias, miData[0].grupo);
        }
    });
}

function popUpCiudad()
{
	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'usuarios',
            call: 'getSede',
        },
        success : function(Data){
        	var miData = JSON.parse(Data);
        	if(miData.cod_mensaje == 0)
        	{
        		getProvincias(1);
                getSedes();
        		$('#popupSede').show();
        		$('.popupSede').show();
        	}
        }
    });
}


function getProvincias(id_pais)
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'localidades',
            call: 'getProvincias',
            id_pais : id_pais,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            var options = '<option value="">Seleccioná una provincia</option>';
            for (var i = 0; i < miData.length; i++) {
                options += '<option value="'+miData[i].id_pais+'_'+miData[i].id_provincia+'">'+miData[i].provincia+'</option>';
            }
            $('#selectProvincia').html(options);
        }
    });
}

function getCiudades()
{
	var v = $('#selectProvincia').val();
    var sp = v.split("_");
    var id_pais = sp[0];
    var id_provincia = sp[1];

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'localidades',
            call: 'getCiudades',
            id_pais : id_pais,
            id_provincia : id_provincia,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            var options = '<option value="">Seleccioná una ciudad</option>';
            for (var i = 0; i < miData.length; i++) {
                options += '<option value="'+miData[i].id_pais+'_'+miData[i].id_provincia+'_'+miData[i].id_ciudad+'">'+miData[i].ciudad+'</option>';
            }
            $('#selectCiudad').html(options);
        }
    });
}

function getSedes()
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'localidades',
            call: 'getSedes',
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            var options = '<option value="">Seleccioná una sede</option>';
            for (var i = 0; i < miData.length; i++) {
                options += '<option value="'+miData[i].id_sede+'">'+miData[i].sede+'</option>';
            }
            $('#selectSede').html(options);
        }
    });
}

function elegirSede()
{
	var v = $('#selectCiudad').val();
    var sp = v.split("_");
    var id_pais = sp[0];
    var id_provincia = sp[1];
    var id_ciudad = sp[1];
    var id_sede = $('#selectSede').val();

    if(id_sede.length < 1)
    {
        alert('Tenés que elegir una sede');
        return;
    }

    if(id_provincia.length < 1)
    {
        alert('Tenés que elegir una provincia');
        return;
    }

    if(id_ciudad.length < 1)
    {
        alert('Tenés que elegir una ciudad');
        return;
    }

	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'usuarios',
            call: 'updSede',
            id_pais : id_pais,
            id_provincia : id_provincia,
            id_ciudad : id_ciudad,
            id_sede : id_sede,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
        	bootbox.alert(miData.mensaje);
            if(miData.cod_mensaje == -1)
                $('#popupSede').hide();
        }
    });
}

function comprobarCertificacion()
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : true,
        data: {
            action: 'sesiones',
            call: 'comprobarCertificacion',
        },
        success : function(Data){
            //console.log(Data);
            var miData = JSON.parse(Data);
            if(miData.cod_mensaje == -1 && encuestaSatisfaccionTerminada){
                $('#popupCertificacion').show();
                $('#nombreMentorCertificacion').text(miData.usuario);
            }
        }
    });
}

function calificarSatisfaccion()
{
    var sstarRelevancia        = getRadio('sstarRelevancia');
    var sstarProfundidad       = getRadio('sstarProfundidad');
    var sstarMetodologia       = getRadio('sstarMetodologia');
    var sstarFacDominio        = getRadio('sstarFacDominio');
    var sstarFacClaridad       = getRadio('sstarFacClaridad');
    var sstarDuracion          = getRadio('sstarDuracion');
    var sstarAmbiente          = getRadio('sstarAmbiente');
    var comentarioSatisfaccion = $('#comentarioSatisfaccion').val();

    if(!validarSatisfaccion())
        return;

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'calificarSatisfaccionMentor',
            sstarRelevancia : sstarRelevancia,
            sstarProfundidad : sstarProfundidad,
            sstarMetodologia : sstarMetodologia,
            sstarFacDominio : sstarFacDominio,
            sstarFacClaridad : sstarFacClaridad,
            sstarDuracion : sstarDuracion,
            sstarAmbiente : sstarAmbiente,
            comentarioSatisfaccion : comentarioSatisfaccion,
        },
        success : function(Data){
            //console.log(Data);
            var miData = JSON.parse(Data);
            $('#popupSatisfaccion').hide();
            bootbox.alert(miData.mensaje);
        }
    });
}

function validarSatisfaccion()
{
    var sstarRelevancia        = getRadio('sstarRelevancia');
    var sstarProfundidad       = getRadio('sstarProfundidad');
    var sstarMetodologia       = getRadio('sstarMetodologia');
    var sstarFacDominio        = getRadio('sstarFacDominio');
    var sstarFacClaridad       = getRadio('sstarFacClaridad');
    var sstarDuracion          = getRadio('sstarDuracion');
    var sstarAmbiente          = getRadio('sstarAmbiente');
    var comentarioSatisfaccion = $('#comentarioSatisfaccion').val();

    if(sstarRelevancia.length < 1){
        bootbox.alert('Tenés que calificar la pregunta 1');
        return false;
    }

    if(sstarProfundidad.length < 1){
        bootbox.alert('Tenés que calificar la pregunta 2');
        return false;
    }

    if(sstarMetodologia.length < 1){
        bootbox.alert('Tenés que calificar la pregunta 3');
        return false;
    }

    if(sstarFacDominio.length < 1){
        bootbox.alert('Tenés que calificar la pregunta 4');
        return false;
    }

    if(sstarFacClaridad.length < 1){
        bootbox.alert('Tenés que calificar la pregunta 5');
        return false;
    }

    if(sstarDuracion.length < 1){
        bootbox.alert('Tenés que calificar la pregunta 6');
        return false;
    }

    if(sstarAmbiente.length < 1){
        bootbox.alert('Tenés que calificar la pregunta 7');
        return false;
    }


    if(comentarioSatisfaccion.length < 1){
        bootbox.alert('Tenés que escribir un comentario en la pregunta 9');
        return false;
    }

    return true;
}

function comprobarEncuestaSatisfaccion()
{
    if(getCookie('satisfaccionResponderMasTarde') == 1)
        return;
    
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'comprobarEncuestaSatisfaccion',
        },
        success : function(Data){
            //console.log(Data);
            var miData = JSON.parse(Data);
            if(miData.cod_mensaje == -1)
                $('#popupHacerSatisfaccion').show();
            else if(miData.mensaje == "Ya hizo la encuesta")
                encuestaSatisfaccionTerminada = true;
        }
    });
}

function satisfaccionResponderMasTarde()
{
    setCookie('satisfaccionResponderMasTarde', 1, 1);
    $('#popupHacerSatisfaccion').hide();
}

function getEmprendedores()
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'getEmprendedoresDeGrupoByIdGrupo',
            id_grupo : grupoSeleccionado,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            html = '';
            for (var i = 0; i < miData.length; i++) {
                if (i == 0) {
                    if(miData[i].sn_abandono == -1)
                        html += '<li class="current" style="opacity:0.4"><a href="#!" class="show'+i+'" onclick="elegirEmprendedor('+miData[i].id_usuario+', \''+miData[i].nombre+'\', '+miData[i].sn_abandono+')">'+miData[i].nombre+'</a></li>';
                    else
                        html += '<li class="current"><a href="#!" class="show'+i+'" onclick="elegirEmprendedor('+miData[i].id_usuario+', \''+miData[i].nombre+'\', '+miData[i].sn_abandono+')">'+miData[i].nombre+'</a></li>';
                    elegirEmprendedor(miData[i].id_usuario, miData[i].nombre, miData[i].sn_abandono);
                    $('#nombre_emprendedor_seleccionado').html(emprendedor_elegido.nombre);
                }else{
                    if(miData[i].sn_abandono == -1)
                        html += '<li style="opacity:0.4"><a href="#!" class="show'+i+'">'+miData[i].nombre+'</a></li>';
                    else
                        html += '<li><a href="#!" class="show'+i+'">'+miData[i].nombre+'</a></li>';
                }
            }
            $('#horizontalTabEmprendedores').html(html);
            $('#horizontalTabEmprendedores2').html(html);
            horizontalTabEmprendedores(miData.length);
        }
    });
}

function comprobarEncuestaMentorValidacion()
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'usuarios',
            call: 'comprobarEncuestaMentorValidacion',
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            if(miData.cod_mensaje == -1)
            {
                getProvincias(1);
                getExpertise();
                getRubros();
                getDatosUsuario();
                $('#popupEncuestaMentor').show();
            }
        }
    });
}

function getProvincias(id_pais)
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'localidades',
            call: 'getProvincias',
            id_pais : id_pais,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            var options = '';
            for (var i = 0; i < miData.length; i++) {
                options += '<option value="'+miData[i].id_provincia+'">'+miData[i].provincia+'</option>';
            }
            $('#popupMentorProvincia').html(options);
        }
    });
}

$('#popupMentorProvincia').change(function(){
    var id_provincia = $('#popupMentorProvincia').val();
    getCiudades(1, id_provincia);
});

$('#popupMentorExpertise').change(function(e){
    var id_expertise = e.target.value;
    var expertise = $("#popupMentorExpertise option:selected").text();

    if(jQuery.inArray(id_expertise, expertises) !== -1 || expertises.length >= 3)
        return;

    expertises.push(id_expertise);

    $('#expertisesElegidas').append('<li><a href="javascript:void(0)">'+expertise+'</a></li>');
});

function getCiudades(id_pais, id_provincia)
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'localidades',
            call: 'getCiudades',
            id_pais : id_pais,
            id_provincia : id_provincia,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            var options = '';
            for (var i = 0; i < miData.length; i++) {
                options += '<option value="'+miData[i].id_ciudad+'">'+miData[i].ciudad+'</option>';
            }
            $('#popupMentorCiudad').html(options);
        }
    });
}

function getRubros()
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'getRubros',
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            var options = '';
            for (var i = 0; i < miData.length; i++) {
                options += '<option value="'+miData[i].id_rubro+'">'+miData[i].rubro+'</option>';
            }
            $('#popupMentorRubro').html(options);
        }
    });
}

function getExpertise()
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'usuarios',
            call: 'getExpertise',
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            var options = '';
            for (var i = 0; i < miData.length; i++) {
                options += '<option value="'+miData[i].id_expertise+'">'+miData[i].expertise+'</option>';
            }
            $('#popupMentorExpertise').html(options);
        }
    });
}

function getDatosUsuario()
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'usuarios',
            call: 'getMisDatos',
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            //console.log(miData);
            $('#popupMentorNombre').val(miData.nombre);
            $('#popupMentorProvincia').val(miData.id_provincia);
            getCiudades(1, miData.id_provincia);
            $('#popupMentorCiudad').val(miData.id_municipio);
            $('#popupMentorEmail').val(miData.email);
            $('#popupMentorLinkedin').val(miData.linkedin);
            $('#popupMentorRubro').val(miData.id_rubro);
            $('#popupMentorExpertise').val(miData.id_expertise);
        }
    });
}

function enviarEncuestaMentor()
{
    var nombre       = $('#popupMentorNombre').val();
    var id_provincia = $('#popupMentorProvincia').val();
    var id_municipio = $('#popupMentorCiudad').val();
    var email        = $('#popupMentorEmail').val();
    var linkedin     = $('#popupMentorLinkedin').val();
    var id_rubro     = $('#popupMentorRubro').val();
    //var id_expertise = $('#popupMentorExpertise').val();
    if(expertises.length <= 0)
        return;

    if(id_rubro == null)
        return;

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'enviarEncuestaMentor',
            nombre : nombre,
            id_provincia : id_provincia,
            id_municipio : id_municipio,
            email : email,
            linkedin : linkedin,
            id_rubro : id_rubro,
            expertises : expertises,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            //console.log(miData);
            if(miData.cod_mensaje == -1){
                bootbox.alert(miData.mensaje);
                $('#popupEncuestaMentor').hide();
            }
        }
    });
}

function horizontalTabEmprendedores(j)
{
    for (var i = 0; i < j; i++) {
        $(".show"+i).click(function() {
            event.preventDefault();
            $(this).parent().addClass("current");
            $(this).parent().siblings().removeClass("current");
            $(".show-content-"+i).fadeIn();
            $(".show-content-"+i).siblings(".col100").hide();
        })
    }
}

function elegirEmprendedor(id_usuario, nombre, sn_abandono)
{
    emprendedor_elegido.id_usuario = id_usuario;
    emprendedor_elegido.nombre = nombre;
    emprendedor_elegido.sn_abandono = sn_abandono;
    $('#nombre_emprendedor_seleccionado').html(nombre);
    
    if(sn_abandono == -1)
        $('#estado_emprendedor').html('El emprendedor abandonó.');
    else
        $('#estado_emprendedor').html('');
    
    getCalificacionEmprendedor(id_usuario);
}

function marcarFlechaProgreso(num_sesion, grupo)
{
    $('.breadcrumb li').removeClass('active');

    for (var i = 0; i <= num_sesion; i++) 
    {
        if( grupo == 1 )
            $('#flechaProgresoSesion'+i).addClass("active");
        else
            $('#flechaProgresoSesion'+i+'_2').addClass("active");
    }
}

function getCalificacionEmprendedor(id_emprendedor)
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'getCalificacionesEmprendedor',
            id_emprendedor : emprendedor_elegido.id_usuario,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            var cantidadPropias = 0;
            //console.log(miData);
            if(miData[0].cod_mensaje == 0){
                marcarFlechaProgreso(0,1);
                limpiarStars();
                return;
            }

            limpiarStars();

            for (var i = 0; i < miData.length; i++) 
            {
                if(miData[i].propio == -1){
                    $('#calificarBtn_'+miData[i].id_sesion+'_'+miData[i].id_grupo).hide();
                    $('#miComentario-'+miData[i].id_sesion+'_'+miData[i].id_grupo).html(miData[i].comentario);
                    for (var j = 1; j <= miData[i].puntuacion; j++) {
                        $('#'+j+'-miStarSesion-'+miData[i].id_sesion+'-'+miData[i].id_grupo).addClass('checked');
                    }
                    cantidadPropias++;
                }else{
                    $('#comentarioMentor-'+miData[i].id_sesion+'_'+miData[i].id_grupo).html(miData[i].comentario);
                    for (var j = 1; j <= miData[i].puntuacion; j++) {
                        $('#'+j+'-starSesionEmprendedor-'+miData[i].id_sesion+'-'+miData[i].id_grupo).addClass('checked');
                    }
                }
            }

            marcarFlechaProgreso(cantidadPropias, miData[0].grupo);
        }
    });
}

function marcarAbandono(id_emprendedor)
{
    //var id_emprendedor = emprendedor_elegido.id_usuario;
    for (var i = 0; i < globalEmprendedores.length; i++) {
        if(globalEmprendedores[i].id_usuario == id_emprendedor){
            var sn_abandono = globalEmprendedores[i].sn_abandono;
            break;
        }
    }

    if(sn_abandono == -1)
        var texto = '<h3>¿Está seguro?</h3> <br> Este estado indica que el emprendedor <strong>volvió a participar</strong> en las sesiones de Mentoría.';
    else
        var texto = '<h3>¿Está seguro?</h3> <br> Este estado indica que el emprendedor <strong>no participa más del programa y no volverá</strong> a las sesiones de Mentoría.';

    ocultarCalificarEntero(id_emprendedor);

    bootbox.confirm(texto, function(result){
        if(result)
        {
            $.ajax({
                type: 'POST',
                url: 'ajaxRedirect.php',
                datatype: 'html',
                async : false,
                data: {
                    action: 'usuarios',
                    call: 'updAbandono',
                    id_emprendedor : id_emprendedor,
                },
                success : function(Data){
                    var miData = JSON.parse(Data);
                    if(bootbox.alert(miData.mensaje))
                        location.reload();
                }
            });
        }
    });    
}

function limpiarStars()
{
    for (var numSesion = 1; numSesion <= 6; numSesion++) {
        for (var i = 1; i <= 5; i++) {
            $('#'+i+'-miStarSesion-'+numSesion+'-'+grupoSeleccionado).removeClass('checked');
        }

        for (var i = 1; i <= 5; i++) {
            $('#'+i+'-starSesionEmprendedor-'+numSesion+'-'+grupoSeleccionado).removeClass('checked');
        }

        $('#miComentario-'+numSesion+'_'+grupoSeleccionado).html('');
        $('#comentarioMentor-'+numSesion+'_'+grupoSeleccionado).html('');

        var button  = '<a href="#popupCalificacion" class="calificarBtn" id="calificarBtn_'+ numSesion + '_' + grupoSeleccionado +'">';
            button +=     '<button class="sesionCalificar" onclick="calificarSesion('+ numSesion + ',' + grupoSeleccionado +')">Calificar Sesión</button>';
            button += '</a>';

        $('#div_calificar_'+ numSesion + '_' + grupoSeleccionado).html(button);
    }
}

function getCalificacion(id_sesion, id_grupo)
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'getCalificacion',
            id_sesion : id_sesion,
            id_grupo : id_grupo,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            console.log('Calificacion');
            console.log(miData);

            for (var i = 0; i < miData.length; i++) {
                for (var j = 0; j < $('.asistencias').length; j++) {
                    if($('.asistencias')[j].value == miData[i].id_emprendedor ){
                        var id_radio_asistencia = $('.asistencias')[j].id;
                        var id_split = id_radio_asistencia.split('-');
                        //el primer numero es el numero de respuesta, el segundo numero se refiere al numero de usuario
                        if(miData[i].asistencia == -1){
                            document.getElementById(id_split[0]+'-'+1+'-'+id_split[2]).click();
                        }else{
                            document.getElementById(id_split[0]+'-'+2+'-'+id_split[2]).click();
                        }
                    }
                }
            }

            if(Number.isInteger(miData[0].puntuacion))
                document.getElementById('star'+miData[0].puntuacion).click()
            else
                document.getElementById('star'+parseInt(miData[0].puntuacion)+'half').click()

            for (var i = 0; i < miData[0].respuestas.length; i++) {
                document.getElementById('calificacion-'+miData[0].respuestas[i].id_pregunta+'-'+miData[0].respuestas[i].id_respuesta).click();
            }
            $('#calificacion-texto').text(miData[0].txt_comentario);

            //$('#cuestionarioCalificacion').show();
        }
    });
}

function comprobarFotoPerfil()
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'usuarios',
            call: 'getMisDatos',
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            if(miData.imagen == null)
                bootbox.alert('Haz click para <a href="miperfil.php" >cargar tu foto de perfil</a>');
        }
    });
}

function getEmprendedoresAptosFase2()
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'getEmprendedoresAptosFase2',
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            var usuarios = '';
            for (var i = 0; i < miData.length; i++) {
                usuarios += '<input type="checkbox" class="asistencias" name="checkboxEmprendedoresFase2[]" id="'+miData[i].id_usuario+'" value="'+miData[i].id_usuario+'">';
                usuarios += '<label for="">'+miData[i].nombre+'</label>';
                usuarios += '<br>';
            }
            $('#emprendedoresParaFase2').html(usuarios);
        }
    });
}

function popUpFase2()
{
    $('#popupFase2').show();
    getEmprendedoresAptosFase2();
}

function elegirEmprendedoresFase2()
{
    var emprendedores = getCheckboxValues('checkboxEmprendedoresFase2').split(",");

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'insEmprendedoresFase2',
            emprendedores : emprendedores,
        },
        success : function(Data){debugger
            var miData = JSON.parse(Data);
            $('#popupFase2').hide();
            var msj = '';
            for (var i = 0; i < miData.length; i++) {
                msj += miData[i].mensaje;
                msj += '<br>';
            }
            bootbox.alert(msj);
        }
    });
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function deleteCookie( name ) {
    document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

function getCheckboxValues(name)
{
	var checkboxes = document.getElementsByName(name+'[]');
	var vals = "";
	for (var i=0, n=checkboxes.length;i<n;i++) 
	{
	    if (checkboxes[i].checked) 
	    {
	        vals += ","+checkboxes[i].value;
	    }
	}
	if (vals) vals = vals.substring(1);
	return vals;
}

function getRadio(name)
{
	var radios = document.getElementsByName(name);
	for (var i = 0, length = radios.length; i < length; i++)
	{
		if (radios[i].checked)
		{
		  	return(radios[i].value);
		  	break;
		}
	}
}

function unique(list) {
  var result = [];
  $.each(list, function(i, e) {
    if ($.inArray(e, result) == -1) result.push(e);
  });
  return result;
}