function cambiarPass()
{
	var password = $('#input_password').text();
	
	if(password.length < 1 || password == '••••••••••••')
		return;

	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'usuarios',
            call: 'updPassword',
            password : password,
        },
        success : function(Data){debugger
        	var miData = JSON.parse(Data);
        	bootbox.alert(miData.mensaje);
        }
    });
}

function cargarFotoDePerfil()
{
    var f = $(this);
    var formData = new FormData(document.getElementById("fotoDePerfil"));
    formData.append("action", "updFotoDePerfil");

    $.ajax({
        xhr: function() {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function(evt) {
              if (evt.lengthComputable) {
                var percentComplete = evt.loaded / evt.total;
                percentComplete = parseInt(percentComplete * 100);
                console.log('Porcentaje subido : '+percentComplete);
              }
            }, false);
            return xhr;
        },
        type: 'POST',
        url: 'ajaxRedirect.php',
        type: "POST",
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success : function(Data){debugger
            var miData = JSON.parse(Data);
            bootbox.alert(miData.mensaje);
            $('#imagen-perfil').html('<img src="'+miData.imagen+'" height="100" width="100">');
        }
    });
}