<!doctype html>
<html class="no-js" lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>MAR - Home</title>
		<meta name="description" content="">
		<meta name="author" content="Amedia">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
		<meta name="theme-color" content="#395597">

		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/icons/apple-touch-icon-114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/icons/apple-touch-icon-72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" href="img/icons/apple-touch-icon-57-precomposed.png">
		<link rel="shortcut icon" href="img/icons/favicon.png">

		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,900" rel="stylesheet">

		<link rel="stylesheet" href="css/normalize.css">
		<link rel="stylesheet" href="css/icons.css">
		<link rel="stylesheet" href="css/main.css">
		<script src="js/vendor/modernizr-2.8.3.min.js"></script>
	</head>
	<body>
		<!--[if lt IE 8]>
			<p class="browserupgrade">Estás usando un navegador <strong>desactualizado</strong>. Por favor, <a href="http://browsehappy.com/">actualizá tu navegador</a> para una mejor experiencia.</p>
		<![endif]-->

		<header>
			<ul class="breadcrumbs">
				<li><a href="entrenamiento.html">Entrenamiento</a></li>
				<li><a href="modulo.html">Módulo</a></li>
			</ul>

			<div class="noti-top">  <!-- CAMPANITA DE NOTIFICACIONES -->
				<a href="javascript:void(0);" data-icon="x">
					<span class="noti-count">3</span>
				</a>
			</div>

			<div class="user-top">  <!-- NOMBRE DE USUARIO Y MENÚ DESPLEGABLE -->
				<a href="javascript:void(0);" class="dropdown">
					<div class="user-pic" style="background-image: url('img/user-pic.jpg');"></div>
					<h4>Juan Carlos Pérez</h4>
				</a>
				<ul>
					<li><a href="#" data-icon="&#xe056;">Mi perfil</a></li>
					<li><a href="login.html" data-icon="&#xe036;">Cerrar sesión</a></li>
				</ul>
			</div>
		</header>

		<aside>  <!-- MENÚ LATERAL -->
			<a href="index.html" class="logo"><img src="img/logo-mar.png" alt="MAR - Mentores argentinos en red"></a>
			<nav>
				<ul>
					<li><a href="home.html" data-icon="&#xe006;">Inicio</a></li>
					<li class="current"><a href="#" data-icon="\">Entrenamiento</a></li>
					<li><a href="biblioteca.html" data-icon=",">Biblioteca</a></li>
					<li><a href="#" data-icon="M">Agenda de novedades</a></li>
					<li><a href="#" data-icon="&#xe001;">Reportes</a></li>
					<li><a href="javascript:void(0);" data-icon="~" class="dropdown">Soporte</a>
						<ul>
							<li><a href="#" data-icon="&#xe057;">Ayuda</a></li>
							<li><a href="#" data-icon="&#xe03a;">FAQ</a></li>
						</ul>
					</li>
					<li><a href="#" data-icon="&#xe008;">Inbox</a></li>
				</ul>
			</nav>
		</aside>

		<div class="notifications">  <!-- NOTIFICACIONES QUE SE ABREN -->
			<h4>Notificaciones</h4>
			<ul>
				<li><a href="#">
					<span class="noti-text">Nombre de la nota. Esta es una notificación leída.</span>
					<span class="noti-date">07/06/2017, 11:35hs</span>
				</a></li>
				<li class="unread"><a href="#">
					<span class="noti-text">Nombre de la nota. Esta es una notificación no leída.</span>
					<span class="noti-date">07/06/2017, 11:35hs</span>
				</a></li>
				<li class="unread"><a href="#">
					<span class="noti-text">Nombre de la nota. Esta es una notificación no leída.</span>
					<span class="noti-date">07/06/2017, 11:35hs</span>
				</a></li>
				<li><a href="#">
					<span class="noti-text">Nombre de la nota. Esta es una notificación leída.</span>
					<span class="noti-date">07/06/2017, 11:35hs</span>
				</a></li>
				<li><a href="#">
					<span class="noti-text">Nombre de la nota. Esta es una notificación leída.</span>
					<span class="noti-date">07/06/2017, 11:35hs</span>
				</a></li>
				<li class="unread"><a href="#">
					<span class="noti-text">Nombre de la nota. Esta es una notificación no leída.</span>
					<span class="noti-date">07/06/2017, 11:35hs</span>
				</a></li>
			</ul>
		</div>

		<div class="content">  <!-- TODO EL CONTENIDO (LO QUE NO ES MENÚ NI HEADER) -->
			<div class="wrapper">
				<div class="col100">
				<ul class="cursos">
					<li class="curso-featured">
						<div class="module module-blue">
							<h2>Curso destacado</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
							<div class="curso-footer clearfix">
								<ul class="curso-status clearfix">
									<li data-icon="&#xe007;">
										<span>21:36</span>
										<em>Duración <br>total</em>
									</li>
									<li data-icon="&#xe013;">
										<span>2</span>
										<em>Lecciones <br>completadas</em>
									</li>
									<li data-icon="&#xe014;">
										<span>4</span>
										<em>Lecciones <br>restantes</em>
									</li>
									<li data-icon="&#xe045;">
										<span>200/300</span>
										<em>Puntos <br>obtenidos</em>
									</li>
								</ul>
								<a href="leccion.html">Acceder</a>
							</div>
						</div>
					</li>

					<li>
						<div class="module">
							<h2>Curso destacado</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor, consectetur adipiscing elit, sed do eiusmod tempor.</p>
							<div class="curso-footer">
								<span data-icon="&#xe007;">21:36</span>
								<span data-icon="&#xe013;">3</span>
								<span data-icon="&#xe014;">4</span>
								<span data-icon="&#xe045;">200/300</span>
								<a href="leccion.html">Acceder</a>
							</div>
						</div>
					</li>

					<li>
						<div class="module">
							<h2>Curso destacado</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
							<div class="curso-footer">
								<span data-icon="&#xe007;">21:36</span>
								<span data-icon="&#xe013;">3</span>
								<span data-icon="&#xe014;">4</span>
								<span data-icon="&#xe045;">200/300</span>
								<a href="leccion.html">Acceder</a>
							</div>
						</div>
					</li>

					<li>
						<div class="module">
							<h2>Curso destacado</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor, consectetur adipiscing elit, sed do eiusmod tempor.</p>
							<div class="curso-footer">
								<span data-icon="&#xe007;">21:36</span>
								<span data-icon="&#xe013;">3</span>
								<span data-icon="&#xe014;">4</span>
								<span data-icon="&#xe045;">200/300</span>
								<a href="leccion.html">Acceder</a>
							</div>
						</div>
					</li>

					<li>
						<div class="module">
							<h2>Curso destacado</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
							<div class="curso-footer">
								<span data-icon="&#xe007;">21:36</span>
								<span data-icon="&#xe013;">3</span>
								<span data-icon="&#xe014;">4</span>
								<span data-icon="&#xe045;">200/300</span>
								<a href="leccion.html">Acceder</a>
							</div>
						</div>
					</li>

					<li>
						<div class="module">
							<h2>Curso destacado</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
							<div class="curso-footer">
								<span data-icon="&#xe007;">21:36</span>
								<span data-icon="&#xe013;">3</span>
								<span data-icon="&#xe014;">4</span>
								<span data-icon="&#xe045;">200/300</span>
								<a href="leccion.html">Acceder</a>
							</div>
						</div>
					</li>

					<li>
						<div class="module">
							<h2>Curso destacado</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor, consectetur adipiscing elit, sed do eiusmod tempor.</p>
							<div class="curso-footer">
								<span data-icon="&#xe007;">21:36</span>
								<span data-icon="&#xe013;">3</span>
								<span data-icon="&#xe014;">4</span>
								<span data-icon="&#xe045;">200/300</span>
								<a href="leccion.html">Acceder</a>
							</div>
						</div>
					</li>

					<li>
						<div class="module">
							<h2>Curso destacado</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
							<div class="curso-footer">
								<span data-icon="&#xe007;">21:36</span>
								<span data-icon="&#xe013;">3</span>
								<span data-icon="&#xe014;">4</span>
								<span data-icon="&#xe045;">200/300</span>
								<a href="leccion.html">Acceder</a>
							</div>
						</div>
					</li>
				</ul>
				</div>
			</div>
		</div>

		<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
		<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
		<script src="js/plugins.js"></script>
		<script src="js/imagesloaded.pkgd.min.js"></script>
		<script src="js/masonry.pkgd.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
    	<script src="js/bootbox.min.js"></script>
		<script src="js/main.js"></script>
	</body>
</html>
