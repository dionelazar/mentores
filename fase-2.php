<?php
	require_once('class/class.helpers.php');

	if(!isset($_COOKIE['tipo_usuario']))
		header('location:login.php');

	$tipo_usuario = Encriptacion::desencriptar($_COOKIE['tipo_usuario']);

	if(!is_int(intval($tipo_usuario)))
		header('location:login.php');

	switch ($tipo_usuario) {
		case 2:
			require __DIR__.'/share/header-mentorDT.php';  
			break;

		case 3:
			require __DIR__.'/share/header-mentor-con.php';  
			break;

		default:
			header('location:login.php');
			break;
	}
?>
	<div class="content home">
		<div class="col100">
			<div class="module library">
                <h1>Emprendedores fase 2</h1>

                <table>
            		<thead>
            			<th>Emprendedor</th>
            			<th>DNI emprendedor</th>
            			<th>Estado</th>
            		</thead>
            		<tbody id="emprendedores">

            		</tbody>
            	</table>
		</div>
	</div>

    <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/masonry.pkgd.min.js"></script>
    <script src="js/select2.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="js/main.js"></script>
    <script src="scripts/header-mentorDT.js"></script>
    <script src="scripts/fase-2.js"></script>
    </body>
</html>