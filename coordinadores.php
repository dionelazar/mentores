<?php 
	 require_once( __DIR__.'/share/header-admin.php' );
?>
	<div class="content">
		<div class="wrapper">
			<h1>Coordinadores</h1>
			<hr>

			<div class="col100">
                <div class="module library">
                    <div class="right">
                        <input type="button" name="abrir-importar-excel" class="btn" id="abrir-importar-excel" value="Crear usuario" onclick="javascript:$('#popUpCrearUsuario').show();" />
                    </div>
                    <table>
                        <thead>
                            <tr>
                                <th data-icon="">
                                    <br>Nombre</th>
                                <th data-icon="">
                                    <br>DNI</th>
                                <th data-icon="&#xe05d;">
                                    <br>Sedes</th>
                                <th data-icon="&#xe046;">
                                    <br>Última Actividad</th>
                                <!--th data-icon="p">
                                    <br>Sesiones</th-->
                                <!--th data-icon="&#xe015;">
                                    <br>E-mail</th-->
                                <th><i class="fa fa-key" aria-hidden="true"></i>
                                    <br>Constraseña</th>
                                <th data-icon="*">
                                    <br>Editar</th>
                                <th data-icon="m">
                                    <br>Estado</th>
                            </tr>
                        </thead>
                        <tbody id="listaUsuarios">
                            
                        </tbody>
                    </table>
                </div>
            </div>

			<div id="popUpCrearUsuario" class="absolute-center" hidden="">
		        <div id="overlayCalificacion"></div>
		        <div class="popupReportes">
		            <a href="#" id="closeBtn" class="closePopUp">
		                <i class="close-icon" data-icon="9"></i>
		            </a>
		            <div class="container">
		                <div class="row">
		                    <div class="col-xs-12">
		                        <h2>Crear usuario coordinador</h2>
		                        <hr>
		                    </div>
		                    <form id="formCrearUsuario" action="javascript:void(0)">
		                        <div class="form-group">
		                            <label for="">Nombre y apellido</label>
		                            <input class="form-control" type="text" name="crearNombre" id="crearNombre" value="">    
		                        </div>
		                        <div class="form-group">
		                            <label for="">Email</label>
		                            <input class="form-control" type="email" name="crearEmail" id="crearEmail" value="">    
		                        </div>
		                        <div class="form-group">
		                            <label for="">DNI</label>
		                            <input class="form-control" type="text" name="crearDni" id="crearDni" value="">    
		                        </div>
		                        <div class="form-group">
		                            <label for="">Provincia</label>
		                            <select class="form-control" id="crearProvincia" name="crearProvincia" onchange="getCiudades(1, this.options[this.selectedIndex].value)">
		                                
		                            </select>
		                        </div>
		                        <div class="form-group">
		                            <label for="">Ciudad</label>
		                            <select class="form-control" id="crearCiudad" name="crearCiudad">
		                                
		                            </select>
		                        </div>
		                        <div class="form-group">
		                            <label for="">Sede</label>
		                            <select class="form-control" id="crearSede" name="crearSede">
		                                
		                            </select>
		                            <strong><small id="crearSedesElegidas"></small></strong>
		                        </div>
		                        <div class="form-group">
		                            <label for="">Contraseña</label>
		                            <input class="form-control" type="password" name="crearPassword" id="crearPassword" value="">    
		                        </div>
		                        <input type="hidden" id="crearIdUsuario" value="">
		                        <div class="form-group">
		                            <input class="btn btn-primary" type="button" name="crearBtn" id="crearBtn" value="Crear usuario" onclick="crearUsuario()">    
		                        </div>
		                    </form>
		                </div>
		            </div>
		        </div>
		    </div>

		    <div id="popUpEditarUsuario" class="absolute-center" hidden="">
		        <div id="overlayCalificacion"></div>
		        <div class="popupReportes">
		            <a href="#" id="closeBtn" class="closePopUp">
		                <i class="close-icon" data-icon="9"></i>
		            </a>
		            <div class="container">
		                <div class="row">
		                    <div class="col-xs-12">
		                        <h2>Editar usuario administrador</h2>
		                        <hr>
		                    </div>
		                    <form id="formEditarUsuario" action="javascript:void(0)">
		                        <div class="form-group">
		                            <label for="">Nombre y apellido</label>
		                            <input class="form-control" type="text" name="editarNombre" id="editarNombre" value="">    
		                        </div>
		                        <div class="form-group">
		                            <label for="">Email</label>
		                            <input class="form-control" type="email" name="editarEmail" id="editarEmail" value="">    
		                        </div>
		                        <div class="form-group">
		                            <label for="">DNI</label>
		                            <input class="form-control" type="text" name="editarDni" id="editarDni" value="">    
		                        </div>
		                        <div class="form-group">
		                            <label for="">Sede</label>
		                            <select class="form-control" id="editarSede" name="editarSede">
		                                
		                            </select>
		                            <small id=""></small>
		                        </div>
		                        <div class="form-group">
		                            <label for="">Provincia</label>
		                            <select class="form-control" id="editarProvincia" name="editarProvincia" onchange="getCiudades(1, this.options[this.selectedIndex].value)">
		                                
		                            </select>
		                        </div>
		                        <div class="form-group">
		                            <label for="">Ciudad</label>
		                            <select class="form-control" id="editarCiudad" name="editarCiudad">
		                                
		                            </select>
		                        </div>
		                        <div class="form-group">
		                            <label for="">Contraseña</label>
		                            <input class="form-control" type="password" name="editarPassword" id="editarPassword" value="">    
		                        </div>
		                        <input type="hidden" id="editarIdUsuario" value="">
		                        <div class="form-group">
		                            <input class="btn btn-primary" type="button" name="editarBtn" id="editarBtn" value="Editar" onclick="editarUsuario()">    
		                        </div>
		                    </form>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</div>

    <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script>
    window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')
    </script>
    <script src="js/plugins.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.js"></script>
    <script src="js/masonry.pkgd.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="js/main.js"></script>
    <script src="scripts/header.js"></script>
    <script src="scripts/coordinadores.js"></script>
</body>

</html>