ALTER PROC sp_get_mentorias_activas_con
(
     @id_usuario INT
    ,@id_sede INT
)
AS BEGIN
SET NOCOUNT ON
    DECLARE @id_tipo_usuario INT
    SELECT @id_tipo_usuario = id_tipo_usuario FROM usuarios WHERE id_usuario = @id_usuario

    IF @id_tipo_usuario = 1 BEGIN
        IF @id_sede = 0 BEGIN
            SELECT   m.txt_nombre 'mentor'
                    ,m.txt_doc 'dni_mentor'
                    ,s.txt_desc 'sede'
                    ,u.txt_nombre 'emprendedor'
                    ,u.txt_doc 'dni_emprendedor'
                    ,g.txt_desc 'grupo'
                    ,u.id_tipo_usuario 'id_tipo_usuario'
                    --,( SELECT SUM(puntuacion) / ISNULL((SELECT COUNT(*) FROM calificaciones WHERE id_usuario = u.id_usuario), 1) FROM calificaciones WHERE id_usuario = u.id_usuario) 'promedio_calificacion'
                    ,( SELECT SUM(puntuacion) / ISNULL((SELECT COUNT(*) FROM calificaciones WHERE id_usuario = m.id_usuario), 1) FROM calificaciones WHERE id_usuario = m.id_usuario) 'promedio_calificacion'
            FROM usuarios u
            JOIN grupos g ON u.id_grupo = g.id_grupo
            JOIN usuarios m ON g.id_mentor  = m.id_usuario
            LEFT JOIN sedes s ON u.id_sede = s.id_sede
            WHERE u.id_tipo_usuario = 5
              AND u.sn_activo = -1
              AND u.id_grupo IS NOT NULL
            ORDER BY m.txt_nombre ASC
        END ELSE BEGIN
            SELECT   m.txt_nombre 'mentor'
                    ,m.txt_doc 'dni_mentor'
                    ,s.txt_desc 'sede'
                    ,u.txt_nombre 'emprendedor'
                    ,u.txt_doc 'dni_emprendedor'
                    ,g.txt_desc 'grupo'
                    ,u.id_tipo_usuario 'id_tipo_usuario'
                    ,( SELECT SUM(puntuacion) / ISNULL((SELECT COUNT(*) FROM calificaciones WHERE id_usuario = m.id_usuario), 1) FROM calificaciones WHERE id_usuario = m.id_usuario) 'promedio_calificacion'
            FROM usuarios u
            JOIN grupos g ON u.id_grupo = g.id_grupo
            JOIN usuarios m ON g.id_mentor  = m.id_usuario
            LEFT JOIN sedes s ON u.id_sede = s.id_sede
            WHERE u.id_tipo_usuario = 5
              AND u.sn_activo = -1
              AND u.id_grupo IS NOT NULL
              AND m.id_sede = @id_sede
            ORDER BY m.txt_nombre ASC
        END
    END ELSE IF @id_tipo_usuario = 7 BEGIN
        IF @id_sede = 0 BEGIN
            SELECT   m.txt_nombre 'mentor'
                    ,m.txt_doc 'dni_mentor'
                    ,s.txt_desc 'sede'
                    ,u.txt_nombre 'emprendedor'
                    ,u.txt_doc 'dni_emprendedor'
                    ,g.txt_desc 'grupo'
                    ,u.id_tipo_usuario 'id_tipo_usuario'
                    ,( SELECT SUM(puntuacion) / ISNULL((SELECT COUNT(*) FROM calificaciones WHERE id_usuario = m.id_usuario), 1) FROM calificaciones WHERE id_usuario = m.id_usuario) 'promedio_calificacion'
            FROM usuarios u
            JOIN grupos g ON u.id_grupo = g.id_grupo
            JOIN usuarios m ON g.id_mentor  = m.id_usuario
            LEFT JOIN sedes s ON u.id_sede = s.id_sede
            WHERE u.id_tipo_usuario = 5
              AND u.sn_activo = -1
              AND u.id_grupo IS NOT NULL
              AND m.id_sede IN(SELECT id_sede FROM sedes_coordinador WHERE id_usuario = @id_usuario)
            ORDER BY m.txt_nombre ASC
        END ELSE BEGIN
            SELECT   m.txt_nombre 'mentor'
                    ,m.txt_doc 'dni_mentor'
                    ,s.txt_desc 'sede'
                    ,u.txt_nombre 'emprendedor'
                    ,u.txt_doc 'dni_emprendedor'
                    ,g.txt_desc 'grupo'
                    ,u.id_tipo_usuario 'id_tipo_usuario'
                    ,( SELECT SUM(puntuacion) / ISNULL((SELECT COUNT(*) FROM calificaciones WHERE id_usuario = m.id_usuario), 1) FROM calificaciones WHERE id_usuario = m.id_usuario) 'promedio_calificacion'
            FROM usuarios u
            JOIN grupos g ON u.id_grupo = g.id_grupo
            JOIN usuarios m ON g.id_mentor  = m.id_usuario
            LEFT JOIN sedes s ON u.id_sede = s.id_sede
            WHERE u.id_tipo_usuario = 5
              AND u.sn_activo = -1
              AND u.id_grupo IS NOT NULL
              AND m.id_sede = @id_sede
            ORDER BY m.txt_nombre ASC
        END
    END ELSE BEGIN
        SELECT 0 'cod_usuario'
              ,'Usuario sin permisos' 'mensaje'
    END
END



GO

ALTER PROC [dbo].[sp_get_mentorias_activas]
(
    @id_usuario INT
    ,@id_sede INT
)
AS BEGIN
SET NOCOUNT ON
    DECLARE @id_tipo_usuario INT
    SELECT @id_tipo_usuario = id_tipo_usuario FROM usuarios WHERE id_usuario = @id_usuario

    IF @id_tipo_usuario = 1 BEGIN
        IF @id_sede = 0 BEGIN
            SELECT u.txt_nombre 'mentor'
                  ,u.txt_doc 'dni_mentor'
                  ,s.txt_desc 'sede'
                  ,( SELECT  STUFF(( SELECT ',' + txt_nombre FROM usuarios WHERE g.id_grupo = id_grupo AND id_tipo_usuario in (4,5) FOR XML PATH('') ), 1, 1, '') ) 'emprendedores'
                  ,( SELECT  STUFF(( SELECT ',' + txt_doc FROM usuarios WHERE g.id_grupo = id_grupo AND id_tipo_usuario in (4,5) FOR XML PATH('') ), 1, 1, '') ) 'dni_emprendedores'
                  ,g.txt_desc 'grupo'
                  ,u.id_tipo_usuario 'id_tipo_usuario'
                  ,( SELECT SUM(puntuacion) / ISNULL((SELECT COUNT(*) FROM calificaciones WHERE id_grupo = g.id_grupo), 1) FROM calificaciones WHERE id_grupo = g.id_grupo) 'promedio_calificacion'
            FROM grupos g
            JOIN usuarios u ON u.id_usuario = g.id_mentor
            LEFT JOIN sedes s ON u.id_sede = s.id_sede
            WHERE ( SELECT  STUFF(( SELECT ',' + txt_nombre FROM usuarios WHERE g.id_grupo = id_grupo AND id_tipo_usuario in (4,5) FOR XML PATH('') ), 1, 1, '') ) IS NOT NULL
              AND u.id_tipo_usuario = 2
        END ELSE BEGIN
            SELECT u.txt_nombre 'mentor'
                  ,u.txt_doc 'dni_mentor'
                  ,s.txt_desc 'sede'
                  ,( SELECT  STUFF(( SELECT ',' + txt_nombre FROM usuarios WHERE g.id_grupo = id_grupo AND id_tipo_usuario in (4,5) FOR XML PATH('') ), 1, 1, '') ) 'emprendedores'
                  ,( SELECT  STUFF(( SELECT ',' + txt_doc FROM usuarios WHERE g.id_grupo = id_grupo AND id_tipo_usuario in (4,5) FOR XML PATH('') ), 1, 1, '') ) 'dni_emprendedores'
                  ,g.txt_desc 'grupo'
                  ,u.id_tipo_usuario 'id_tipo_usuario'
                  ,( SELECT SUM(puntuacion) / ISNULL((SELECT COUNT(*) FROM calificaciones WHERE id_grupo = g.id_grupo), 1) FROM calificaciones WHERE id_grupo = g.id_grupo) 'promedio_calificacion'
            FROM grupos g
            JOIN usuarios u ON u.id_usuario = g.id_mentor
            LEFT JOIN sedes s ON u.id_sede = s.id_sede
            WHERE ( SELECT  STUFF(( SELECT ',' + txt_nombre FROM usuarios WHERE g.id_grupo = id_grupo AND id_tipo_usuario in (4,5) FOR XML PATH('') ), 1, 1, '') ) IS NOT NULL
               AND u.id_sede = @id_sede
               AND u.id_tipo_usuario = 2
        END
    END ELSE IF @id_tipo_usuario = 7 BEGIN
        IF @id_sede = 0 BEGIN
            SELECT u.txt_nombre 'mentor'
                  ,u.txt_doc 'dni_mentor'
                  ,s.txt_desc 'sede'
                  ,( SELECT  STUFF(( SELECT ',' + txt_nombre FROM usuarios WHERE g.id_grupo = id_grupo AND id_tipo_usuario in (4,5) FOR XML PATH('') ), 1, 1, '') ) 'emprendedores'
                  ,( SELECT  STUFF(( SELECT ',' + txt_doc FROM usuarios WHERE g.id_grupo = id_grupo AND id_tipo_usuario in (4,5) FOR XML PATH('') ), 1, 1, '') ) 'dni_emprendedores'
                  ,g.txt_desc 'grupo'
                  ,u.id_tipo_usuario 'id_tipo_usuario'
                  ,( SELECT SUM(puntuacion) / ISNULL((SELECT COUNT(*) FROM calificaciones WHERE id_grupo = g.id_grupo), 1) FROM calificaciones WHERE id_grupo = g.id_grupo) 'promedio_calificacion'
            FROM grupos g
            JOIN usuarios u ON u.id_usuario = g.id_mentor
            LEFT JOIN sedes s ON u.id_sede = s.id_sede
            WHERE ( SELECT  STUFF(( SELECT ',' + txt_nombre FROM usuarios WHERE g.id_grupo = id_grupo AND id_tipo_usuario in (4,5) FOR XML PATH('') ), 1, 1, '') ) IS NOT NULL
                AND u.id_sede IN(SELECT id_sede FROM sedes_coordinador WHERE id_usuario = @id_usuario)
                AND u.id_tipo_usuario = 2
        END ELSE BEGIN
            SELECT u.txt_nombre 'mentor'
                  ,u.txt_doc 'dni_mentor'
                  ,s.txt_desc 'sede'
                  ,( SELECT  STUFF(( SELECT ',' + txt_nombre FROM usuarios WHERE g.id_grupo = id_grupo AND id_tipo_usuario in (4,5) FOR XML PATH('') ), 1, 1, '') ) 'emprendedores'
                  ,( SELECT  STUFF(( SELECT ',' + txt_doc FROM usuarios WHERE g.id_grupo = id_grupo AND id_tipo_usuario in (4,5) FOR XML PATH('') ), 1, 1, '') ) 'dni_emprendedores'
                  ,g.txt_desc 'grupo'
                  ,u.id_tipo_usuario 'id_tipo_usuario'
                  ,( SELECT SUM(puntuacion) / ISNULL((SELECT COUNT(*) FROM calificaciones WHERE id_grupo = g.id_grupo), 1) FROM calificaciones WHERE id_grupo = g.id_grupo) 'promedio_calificacion'
            FROM grupos g
            JOIN usuarios u ON u.id_usuario = g.id_mentor
            LEFT JOIN sedes s ON u.id_sede = s.id_sede
            WHERE ( SELECT  STUFF(( SELECT ',' + txt_nombre FROM usuarios WHERE g.id_grupo = id_grupo AND id_tipo_usuario in (4,5) FOR XML PATH('') ), 1, 1, '') ) IS NOT NULL
               AND u.id_sede = @id_sede
               AND u.id_tipo_usuario = 2
        END
    END ELSE BEGIN
        SELECT 0 'cod_usuario'
              ,'Usuario sin permisos' 'mensaje'
    END
END

GO


ALTER PROC [dbo].[sp_get_form]
(
   @id_usuario INT
  ,@id_sesion INT
  ,@id_grupo INT
  ,@id_emprendedor INT
)
AS BEGIN
SET NOCOUNT ON
  DECLARE @id_tipo_usuario INT

  SELECT @id_tipo_usuario = id_tipo_usuario FROM usuarios WHERE id_usuario = @id_usuario
  IF       @id_sesion = 1 --NOT EXISTS(SELECT 1 FROM calificaciones WHERE id_grupo = @id_grupo AND id_sesion = @id_sesion) 
      OR @id_sesion = (SELECT MAX(id_sesion) + 1 FROM calificaciones WHERE id_grupo = @id_grupo AND id_usuario = @id_usuario AND id_emprendedor = @id_emprendedor) BEGIN
    IF @id_tipo_usuario = 2 BEGIN -- mentor dt
      IF @id_sesion IN (1,2,3) BEGIN
        SELECT p.id_pregunta
            ,p.num_pregunta
            ,p.txt_desc 'pregunta'
            ,-1 'cod_mensaje'
        FROM preguntas p
        WHERE id_pregunta in (7, 8)
      END ELSE IF @id_sesion IN (4,5,6) BEGIN
        SELECT p.id_pregunta
            ,p.num_pregunta
            ,p.txt_desc 'pregunta'
            ,-1 'cod_mensaje'
        FROM preguntas p
        WHERE id_pregunta in (9, 10)
      END

    END ELSE IF @id_tipo_usuario = 3 BEGIN -- mentor con
      SELECT p.id_pregunta
          ,p.num_pregunta
          ,p.txt_desc 'pregunta'
          ,-1 'cod_mensaje'
      FROM preguntas p
      WHERE id_pregunta in (11, 12)

    END ELSE IF @id_tipo_usuario = 4 BEGIN -- DT
      IF @id_sesion IN (1,2,3) BEGIN
        SELECT p.id_pregunta
            ,p.num_pregunta
            ,p.txt_desc 'pregunta'
            ,-1 'cod_mensaje'
        FROM preguntas p
        WHERE id_pregunta in (1, 2)
      END ELSE IF @id_sesion IN (4,5,6) BEGIN
        SELECT p.id_pregunta
            ,p.num_pregunta
            ,p.txt_desc 'pregunta'
            ,-1 'cod_mensaje'
        FROM preguntas p
        WHERE id_pregunta in (3, 4)
      END
    END ELSE IF @id_tipo_usuario = 5 BEGIN -- con
      SELECT p.id_pregunta
          ,p.num_pregunta
          ,p.txt_desc 'pregunta'
          ,-1 'cod_mensaje'
      FROM preguntas p
      WHERE id_pregunta in (5, 6)
    END
  END ELSE BEGIN
    IF @id_tipo_usuario = 2 AND 
       ( @id_sesion = 1
       OR @id_sesion = (SELECT MAX(id_sesion) + 1 FROM calificaciones WHERE id_grupo = @id_grupo AND id_usuario = @id_usuario) ) BEGIN -- mentor dt
      IF @id_sesion IN (1,2,3) BEGIN
        SELECT p.id_pregunta
            ,p.num_pregunta
            ,p.txt_desc 'pregunta'
            ,-1 'cod_mensaje'
        FROM preguntas p
        WHERE id_pregunta in (7, 8)
      END ELSE IF @id_sesion IN (4,5,6) BEGIN
        SELECT p.id_pregunta
            ,p.num_pregunta
            ,p.txt_desc 'pregunta'
            ,-1 'cod_mensaje'
        FROM preguntas p
        WHERE id_pregunta in (9, 10)
      END
    END ELSE BEGIN
      IF @id_emprendedor = 0 AND @id_tipo_usuario != 2 BEGIN
        SELECT  0  'cod_mensaje'
            ,'No puedes calificar porque no tienes ningún emprendedor en tu grupo' 'mensaje'
      END ELSE BEGIN
        SELECT  0  'cod_mensaje'
            ,'Tienes que calificar las sesiones anteriores para avanzar' 'mensaje'
      END
    END
  END
END

GO

CREATE PROC sp_get_calificaciones_grupo
(
   @id_usuario INT
)
AS BEGIN
SET NOCOUNT ON
  DECLARE @id_grupo INT
  SELECT @id_grupo = id_grupo FROM usuarios WHERE id_usuario = @id_usuario

  IF EXISTS(SELECT 1 FROM calificaciones WHERE id_grupo = @id_grupo) BEGIN
    SELECT   '' 'mensaje'
        ,-1 'cod_mensaje'
        ,c.id_sesion 'id_sesion'
        ,c.id_grupo 'id_grupo'
        ,c.id_calificacion 'id_calificacion'
        ,c.txt_comentario 'comentario'
        ,c.puntuacion 'puntuacion'
        ,g.txt_desc 'grupo'
        ,c.id_usuario 'id_usuario'
        ,u.txt_nombre 'usuario'
        ,c.id_emprendedor 'id_emprendedor'
    FROM calificaciones c
    JOIN grupos g ON g.id_grupo = c.id_grupo
    JOIN usuarios u ON c.id_usuario = u.id_usuario
    WHERE c.id_grupo = @id_grupo
    ORDER BY id_sesion
  END ELSE BEGIN
    SELECT   'No hay calificaciones' 'mensaje'
        ,0 'cod_mensaje'
  END
END


GO

