<?php 
    require __DIR__.'/share/header-admin.php';
?>
    <div class="content">
        <!-- TODO EL CONTENIDO (LO QUE NO ES MENÚ NI HEADER) -->
        <div class="wrapper">
            <div class="col100">
                <div class="module search-bar">
                    <!-- BARRA DE BÚSQUEDA -->
                    <div class="inner-cols clearfix">
                        <div class="col50">
                            <form action="javascript:void(0)" name="search" id="search">
                                <input type="text" placeholder="Buscar usuarios" id="inputSearch">
                                <input type="submit" value="&#xe041;">
                            </form>
                        </div>
                        <div class="col50">
                            <ul class="library-filters">
                                <li data-icon="&#xe052;" class="pad25top">Filtrar</li>
                                <li class="filtros current"><a style="padding-top: 20px;" href="javascript:void(0)" onclick="filtrarUsuarios(1, this)">Todos</a></li>
                                <li class="filtros"><a href="javascript:void(0)" onclick="filtrarUsuarios(2, this)"><strong>Mentor</strong><br>D. Temprano</a></li>
                                <li class="filtros"><a href="javascript:void(0)" onclick="filtrarUsuarios(3, this)"><strong>Mentor</strong><br> Consolidación</a></li>
                                <li class="filtros"><a href="javascript:void(0)" onclick="filtrarUsuarios(4, this)"><strong>Emprendedor</strong> <br>D. Temprano</a></li>
                                <li class="filtros"><a href="javascript:void(0)" onclick="filtrarUsuarios(5, this)"><strong>Emprendedor</strong><br> Consolidación</a></li>
                            </ul>
                            <select id="filtrarPorSede" onchange="getUsuariosPorSede()">
                                <option value="">Filtrar por sede</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col100">
                <div class="module library">
                    <div class="right">
                        <input type="button" name="abrir-importar-excel" class="btn" id="abrir-importar-excel" value="Importar" />
                    </div>
                    <div id="divExcelListaUsuarios"></div>
                    <table>
                        <thead>
                            <tr>
                                <th data-icon="">
                                    <br>Nombre</th>
                                <th data-icon="">
                                    <br>DNI</th>
                                <th data-icon="p">
                                    <br>Sede</th>
                                <!--th data-icon="&#xe015;">
                                    <br>E-mail</th-->
                                <th data-icon="&#xe05d;">
                                    <br>Cargo</th>
                                <th data-icon="&#xe046;">
                                    <br>Última Actividad</th>
                                <th><i class="fa fa-key" aria-hidden="true"></i>
                                    <br>Contraseña</th>
                                <th data-icon="*">
                                    <br>Editar</th>
                                <th data-icon="m">
                                    <br>Estado</th>
                            </tr>
                        </thead>
                        <tbody id="listaUsuarios">
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div id="popupImportarUsuario" class="absolute-center" hidden="">
        <div id="overlayCalificacion"></div>
        <div class="popupReportes">
            <a href="#" id="closeBtn" class="closePopUp">
                <i class="close-icon" data-icon="9"></i>
            </a>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h2>Importar Usuario</h2>
                        <hr>
                        <h3 style="color: #d62902">Para importar usuarios debe descargar las planillas de ejemplo y completar todos los campos de cada usuario, respetando el formato de cada celda.</h3>
                        <small>El formato tiene que ser .xls</small>
                        <h4>Templates:</h4>
                        <h5><a href="assets/Ejemplo-importador-de-mentores.xls" download="download">Ejemplo mentores</a></h5>
                        <h5><a href="assets/Ejemplo-importador-de-emprendedores.xls" download="download">Ejemplo emprendedores</a></h5>
                        <h5><a href="assets/Ejemplo-importador-de-relacionamiento.xls" download="download">Ejemplo relacionamiento</a></h5>
                        <h5><a href="assets/roles.xls" download="download">Roles</a></h5>
                    </div>
                    <form enctype="multipart/form-data" id="formImportadorExcel" action="javascript:void(0)">
                        <div class="col-xs-12">
                            <p>Seleccione su hoja de excel de usuarios</p>
                            <input name="myFileExcel" id="myFileExcel" type="file">
                        </div>
                        <div class="col-xs-12" style="text-align: center;">
                            <input type="button" name="importarRel" class="btn" id="importarRel" value="Importar Relacionamiento" onclick="importarRelacionamiento()" />
                            <input type="button" name="importarMentores" class="btn" id="importarMentores" value="Importar Mentores" />
                            <input type="button" name="importarEmprendedores" class="btn" id="importarEmprendedores" value="Importar Emprendedores" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="popUpEditarUsuario" class="absolute-center" hidden="">
        <div id="overlayCalificacion"></div>
        <div class="popupReportes">
            <a href="#" id="closeBtn" class="closePopUp">
                <i class="close-icon" data-icon="9"></i>
            </a>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h2>Editar usuario</h2>
                        <hr>
                    </div>
                    <form id="formEditarUsuario" action="javascript:void(0)">
                        <div class="form-group">
                            <label for="">Nombre y apellido</label>
                            <input class="form-control" type="text" name="editarNombre" id="editarNombre" value="">    
                        </div>
                        <div class="form-group">
                            <label for="">Email</label>
                            <input class="form-control" type="email" name="editarEmail" id="editarEmail" value="">    
                        </div>
                        <div class="form-group">
                            <label for="">DNI</label>
                            <input class="form-control" type="text" name="editarDni" id="editarDni" value="">    
                        </div>
                        <div class="form-group">
                            <label for="">Rol</label>
                            <select class="form-control" id="editarRol" name="editarRol" disabled>
                                
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Provincia</label>
                            <select class="form-control" id="editarProvincia" name="editarProvincia" onchange="getCiudades(1, this.options[this.selectedIndex].value)">
                                
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Ciudad</label>
                            <select class="form-control" id="editarCiudad" name="editarCiudad">
                                
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Sede</label>
                            <select class="form-control" id="editarSede" name="editarSede">
                                
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Contraseña</label>
                            <input class="form-control" type="password" name="editarPassword" id="editarPassword" value="">    
                        </div>
                        <input type="hidden" id="editarIdUsuario" value="">
                        <div class="form-group">
                            <input class="btn btn-primary" type="button" name="editarBtn" id="editarBtn" value="Editar" onclick="editarUsuario()">    
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="popUpDetallesUsuario" class="absolute-center" hidden="">
        <div id="overlayCalificacion"></div>
        <div class="popupReportes">
            <a href="#" id="closeBtn" class="closePopUp" onclick="$('#popUpDetallesUsuario').hide()">
                <i class="close-icon" data-icon="9"></i>
            </a>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h2>Detalles usuario</h2>
                        <hr>
                    </div>
                    <div class="col-xs-12">
                        <div class="tab-content">
                            <div class="module library reportes">
                                <table>
                                    <thead>
                                        <th>Pregunta</th>
                                        <th>Respuesta</th>
                                    </thead>
                                    <tbody id="bodyDetallesUsuario">
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div id="divExcelDetalleEmprendedor"></div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script>
    window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')
    </script>
    <script src="js/plugins.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.js"></script>
    <script src="js/masonry.pkgd.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="js/main.js"></script>
    <script src="scripts/header.js"></script>
    <script src="scripts/gestion-usuarios.js"></script>
</body>

</html>