$(document).ready(function(){
	marcarAside();
})

function logout()
{
	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'usuarios',
            call: 'logout',
        },
        success : function(Data){
        	location.replace('index.php');
        }
    });
}

function marcarAside()
{
    var aside = $('#asideMenu li a');
    var url = window.location.pathname;
    var urlArray = url.split("/");
    for (var i = 0; i < aside.length; i++) 
    {
        var href = aside[i].getAttribute("href");
        if(href == urlArray[urlArray.length-1])
        {
            $('#asideMenu li')[i].className = "current";
            return;
        }
    }

    if(urlArray[urlArray.length-1] == "listado" || urlArray[urlArray.length-1].includes("solicitud"))
    {
        $('#asideMenu li')[1].className = "current";
        return;
    }
}