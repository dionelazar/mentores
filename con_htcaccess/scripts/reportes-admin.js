$(document).ready(function(){
	getGrupos();
	inicializarAcordion();
    getPaises();
    getEstadisticasNumeros();
});

$('#filtrosPais').change(function(){
    filtrarPorPais();
    getProvincias();
});

$('#filtrosProvincia').change(function(){
    filtrarPorProvincia();
    getCiudades();
});

$('#filtrosCiudad').change(function(){
    filtrarPorCiudad();
});

$('#filtrosMentoria').change(function(){
    filtrarPorMentoria();
});


function getGrupos()
{
	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'getGruposReporte',
        },
        success : function(Data){
        	var miData = JSON.parse(Data);
        	armarListaGrupos(miData);
        }
    });
}

function getCalificacionesEmprendedores(id_grupo)
{
	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'getCalificacionesEmprendedores',
            id_grupo : id_grupo,
        },
        success : function(Data){
        	var miData = JSON.parse(Data);
        	armarListaEmprendedoresGrupo(miData, id_grupo);
        }
    });
}

function armarListaGrupos(data)
{
    $('#desarrolloTempranoGrupos').html('');
    $('#consolidacionGrupos').html('');

	for (var i = 0; i < data.length; i++) 
	{
		var html = '';
		html += '<div class="timeline-item timeline-closed" name="'+data[i].grupo+'">';
        html += '    <div class="timeline-closed">';
        html += '        <h2>'+data[i].mentor+'</h2>';
        html += '        <h3><strong>Puntuación del grupo:</strong></h3>';
        html += '        <div class="ratingResult">';
        for (var j = 1; j < 6; j++) 
        {
        	if(data[i].promedio >= j-0.4)
        		html += '<span class="fa fa-star checked"></span>';
        	else
        		html += '<span class="fa fa-star"></span>';
        }
        html += '        </div>';
        html += '    </div>';
        html += '    <a class="verCalificacionBtn">';
        html += '        <button class="verCalificacion" data-icon="&#xe057;" onclick="getCalificacionesEmprendedores('+data[i].id_grupo+')"></button>';
        html += '    </a>';
        html += '    <div class="timeline-open" id="calificacionesEmprendedores-'+data[i].id_grupo+'"></div>';
        html += '</div>';

        if(data[i].id_tipo_grupo == 2)
        	$('#desarrolloTempranoGrupos').append(html);
        else
			$('#consolidacionGrupos').append(html);
	}
}

function armarListaEmprendedoresGrupo(data, id_grupo)
{
	var	html  = '<hr>';
		html += '<ul>';
	for (var i = 0; i < data.length; i++) 
	{
		html += '    <li>';
		html += '        <i data-icon="&#xe056;"></i>';
		html += '        <h4><strong>'+data[i].nombre+'</strong></h4>';
		html += '        <div class="ratingResult">';
		for (var j = 1; j < 6; j++) 
        {
        	if(data[i].promedio >= j-0.4)
        		html += '<span class="fa fa-star checked"></span>';
        	else
        		html += '<span class="fa fa-star"></span>';
        }
		html += '        </div>';
		html += '        <p>"'+data[i].comentario+'"</p>';
		html += '    </li>';
	}
		html += '</ul>';
	
	$('#calificacionesEmprendedores-'+id_grupo).html(html);

}

function getPaises()
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'localidades',
            call: 'getPaises',
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            var options = '<option value="">Filtrar por país</option>';
            for (var i = 0; i < miData.length; i++) {
                options += '<option value="'+miData[i].id_pais+'">'+miData[i].pais+'</option>';
            }
            $('#filtrosPais').html(options);
        }
    });
}

function getProvincias()
{
    var id_pais = $('#filtrosPais').val();

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'localidades',
            call: 'getProvincias',
            id_pais : id_pais,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            var options = '<option value="">Filtrar por provincia</option>';
            for (var i = 0; i < miData.length; i++) {
                options += '<option value="'+miData[i].id_pais+'_'+miData[i].id_provincia+'">'+miData[i].provincia+'</option>';
            }
            $('#filtrosProvincia').html(options);
        }
    });
}

function getCiudades()
{
    var v = $('#filtrosProvincia').val();
    var sp = v.split("_");
    var id_pais = sp[0];
    var id_provincia = sp[1];

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'localidades',
            call: 'getCiudades',
            id_pais : id_pais,
            id_provincia : id_provincia,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            var options = '<option value="">Filtrar por ciudad</option>';
            for (var i = 0; i < miData.length; i++) {
                options += '<option value="'+miData[i].id_pais+'_'+miData[i].id_provincia+'_'+miData[i].id_ciudad+'">'+miData[i].ciudad+'</option>';
            }
            $('#filtrosCiudad').html(options);
        }
    });
}

function filtrarPorPais()
{
    var id_pais = $('#filtrosPais').val();

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'getGruposPorPais',
            id_pais : id_pais,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarListaGrupos(miData);
        }
    });
}

function filtrarPorProvincia()
{
    var v = $('#filtrosProvincia').val();
    var sp = v.split("_");

    var id_pais = sp[0];
    var id_provincia = sp[1];

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'getGruposPorProvincia',
            id_pais : id_pais,
            id_provincia : id_provincia,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarListaGrupos(miData);
        }
    });
}

function filtrarPorCiudad()
{
    var v = $('#filtrosCiudad').val();
    var sp = v.split("_");

    var id_pais = sp[0];
    var id_provincia = sp[1];
    var id_ciudad = sp[2];

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'getGruposPorCiudad',
            id_pais : id_pais,
            id_provincia : id_provincia,
            id_ciudad : id_ciudad,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarListaGrupos(miData);
        }
    });
}

function filtrarPorMentoria()
{
    var id_mentoria = $('#filtrosMentoria').val();

    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'getGruposPorMentoria',
            id_mentoria : id_mentoria,
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarListaGrupos(miData);
        }
    });
}

function getEstadisticasNumeros()
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'reportes',
            call: 'getEstadisticasNumeros',
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarEstadisticasNumeros(miData);
        }
    });
}

function armarEstadisticasNumeros(data)
{
    $('#num_mentores_formacion').html(data.mentores_formacion);
    $('#num_mentores_formados').html(data.mentores_formados);
    $('#num_mentorias_activas').html(data.mentorias_activas);
    $('#num_emprendedores_en_proceso').html(data.emprendedores_en_proceso);
    $('#num_emprendedores_egresados_1').html(data.emprendedores_egresados);
    $('#num_emprendedores_egresados_2').html();
    $('#num_alcance_territorial').html(data.alcance_territorial);
    $('#num_atencion_por_participante').html(data.atencion_por_participante+'<small>%</small>');
    $('#num_impacto_grado_satisfaccion').html(data.impacto_grado_satisfaccion+'<span class="fa fa-star checked"></span>');
    $('#num_genero').html('<span style="color:#FF6B1F;">'+data.generoF+'%</span>/<span style="color: #0074AD">'+data.generoM+'%</span>');
    $('#num_diversidad_productiva').html(data.diversidad_productiva+'<small>%</small>');
}

function getReporteMentoresEnFormacion()
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'reportes',
            call: 'getMentoresEnFormacion',
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarReporteMentoresEnFormacion(miData);
        }
    });
}

function armarReporteMentoresEnFormacion(data)
{
    $('#mentoresEnFormacionDT').html('');
    $('#mentoresEnFormacionCON').html('');
    var cant_mentores_formacion_dt = 0
    var cant_mentores_formacion_con = 0
    var total = 0;

    for (var i = 0; i < data.length; i++) 
    {
        html  = '<tr>';
        html += '    <td data-label="Nombre: "><a href="javascript:void(0)">'+data[i].nombre+'</a></td>';
        html += '    <td data-label="Ciudad: ">'+data[i].ciudad+', '+data[i].provincia+'</td>';
        html += '    <td data-label="E-mail: " style="white-space: nowrap;">'+data[i].email+'com</td>';
        html += '</tr>';

        if(data[i].id_tipo_usuario == 2){
            $('#mentoresEnFormacionDT').append(html);
            cant_mentores_formacion_dt++;
        }else{
            $('#mentoresEnFormacionCON').append(html);
            cant_mentores_formacion_con++;
        }
        total++;
    }
    $('#cant_mentores_formacion_dt').html(cant_mentores_formacion_dt);
    $('#cant_mentores_formacion_con').html(cant_mentores_formacion_con);
}

function getReporteMentoresFormados()
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'reportes',
            call: 'getMentoresFormados',
        },
        success : function(Data){debugger
            var miData = JSON.parse(Data);
            armarReporteMentoresFormados(miData);
        }
    });
}

function armarReporteMentoresFormados(data)
{
    $('#mentoresFormadosDT').html('');
    $('#mentoresFormadosCON').html('');
    var cant_mentores_formados_dt = 0
    var cant_mentores_formados_con = 0
    var total = 0;

    for (var i = 0; i < data.length; i++) 
    {
        html  = '<tr>';
        html += '    <td data-label="Nombre: "><a href="javascript:void(0)">'+data[i].nombre+'</a></td>';
        html += '    <td data-label="Ciudad: ">'+data[i].ciudad+', '+data[i].provincia+'</td>';
        html += '    <td data-label="E-mail: " style="white-space: nowrap;">'+data[i].email+'com</td>';
        html += '</tr>';

        if(data[i].id_tipo_usuario == 2){
            $('#mentoresFormadosDT').append(html);
            cant_mentores_formados_dt++;
        }else{
            $('#mentoresFormadosCON').append(html);
            cant_mentores_formados_con++;
        }
        total++;
    }
    $('#cant_mentores_formados_dt').html(cant_mentores_formados_dt);
    $('#cant_mentores_formados_con').html(cant_mentores_formados_con);
}

function getReporteGenero()
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'reportes',
            call: 'getReporteGenero',
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarReporteGenero(miData);
        }
    });
}

function armarReporteGenero(data)
{
    $('#mentoresM').html(data.mentoresM+'%');
    $('#mentoresF').html(data.mentoresF+'%');
    $('#emprendedoresM').html(data.emprendedoresM+'%');
    $('#emprendedoresF').html(data.emprendedoresF+'%');
    $('#mentoresConM').html(data.mentoresConM+'%');
    $('#mentoresConF').html(data.mentoresConF+'%');
    $('#emprendedoresConM').html(data.emprendedoresConM+'%');
    $('#emprendedoresConF').html(data.emprendedoresConF+'%');
    $('#mentoresDtM').html(data.mentoresDtM+'%');
    $('#mentoresDtF').html(data.mentoresDtF+'%');
    $('#emprendedoresDtM').html(data.emprendedoresDtM+'%');
    $('#emprendedoresDtF').html(data.emprendedoresDtF+'%');
}

function getMentoriasActivas()
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'reportes',
            call: 'getMentoriasActivas',
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarMentoriasActivas(miData);
        }
    });    
}

function armarMentoriasActivas(data)
{
    $('#mentoriasActivasDT').html('');
    $('#mentoriasActivasCON').html('');
    var total = 0;
    var cant_DT = 0;
    var cant_CON = 0;

    for (var i = 0; i < data.length; i++) 
    {
        var emprendedores = data[i].emprendedores.split(",,,")
        emprendedores[0] = emprendedores[0] == undefined ? "" : emprendedores[0];
        emprendedores[1] = emprendedores[1] == undefined ? "" : emprendedores[1];
        emprendedores[2] = emprendedores[2] == undefined ? "" : emprendedores[2]

        if(emprendedores[0].split(",,").length == 2)
        {
            var e = emprendedores[0].split(",,")
            emprendedores[0] = e[1];
        }
        html  = '<tr>';
        if(data[i].id_tipo_usuario == 2)
            html += '<td data-label="Nombre Mentor: ">'+data[i].grupo +'</td>';
        html += '    <td data-label="Nombre Mentor: ">'+data[i].mentor +'</td>';
        html += '    <td data-label="Nombre Emprendedor 1: ">'+emprendedores[0]+'</td>';
        html += '    <td data-label="Nombre Emprendedor 2: ">'+emprendedores[1]+'</td>';
        html += '    <td data-label="Nombre Emprendedor 3: ">'+emprendedores[2]+'</td>';
        html += '</tr>';

        if(data[i].id_tipo_usuario == 2){
            $('#mentoriasActivasDT').append(html);
            cant_DT++;
        }else{
            $('#mentoriasActivasCON').append(html);
            cant_CON++;
        }
        total++;
    }
    
    $('#cant_grupos_creados_mentorias_dt').html(cant_DT)
    $('#cant_grupos_creados_mentorias_con').html(cant_CON)
    $('#cant_grupos_creados_mentorias').html(total)
}

function getEmprendedoresEgresados()
{
    $.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'reportes',
            call: 'getEmprendedoresEgresados',
        },
        success : function(Data){
            var miData = JSON.parse(Data);
            armarEmprendedoresEgresados(miData);
        }
    });    
}

function armarEmprendedoresEgresados(data)
{
    $('#table_egresados_dt').html('');
    $('#table_egresados_con').html('');
    var total = 0;
    var cant_DT = 0;
    var cant_CON = 0;

    for (var i = 0; i < data.length; i++) 
    {
        html  = '<tr>';
        html += '    <td data-label="Nombre: "><a href="javascript:void(0)">'+data[i].nombre+'</a></td>';
        html += '    <td data-label="Sede: ">'+data[i].ciudad+', '+data[i].provincia+'</td>';
        html += '    <td data-label="E-mail: " style="white-space: nowrap;">'+data[i].email+'</td>';
        html += '</tr>';

        if(data[i].id_tipo_usuario == 4){
            $('#table_egresados_dt').html(html);
            cant_DT++;
        }else{
            $('#table_egresados_con').html(html);
            cant_CON++;
        }
        total++;
    }

    $('#cant_emprendedores_egresados_dt').html(cant_DT);
    $('#cant_emprendedores_egresados_con').html(cant_CON);
}

//falta:
//alcance territorial
//satisfacción (hay qeu hacer la encuesta!!)