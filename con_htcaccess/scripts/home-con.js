$(document).ready(function(){
	getSesiones();
});

var sesionSeleccionada = 0;
var grupoSeleccionado  = 0;

function getSesiones()
{
	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'getSesionesCON',
        },
        success : function(Data){
        	var miData = JSON.parse(Data);
        	armarSesiones(miData);
        }
    });
}

function armarSesiones(data)
{
	for (var i = 0; i < data.length; i++) 
	{
		var html  = '<div class="timeline-item activeTimeline" sesion="'+ data[i].desc +'">';
	        //html += 	'<h3>Grupo: <strong>Nombre del grupo</strong></h3>';
	        html += '	<h3>Mentor: <strong>'+data[i].mentor+'</strong></h3>'
            html += '   <p>Comentarios del Mentor: <strong>'+data[i].comentario+'</strong></p>';
            if (data[i].sn_califico == 0 ) 
            {
            	html += '   <a href="#popupCalificacion" class="calificarBtn">';
	            html += '   	<button class="sesionCalificar" onclick="calificarSesion('+ data[i].id_sesion + ',' + data[i].id_grupo +')">Calificar Sesión</button>';
	            html += '   </a>';
            }
	        html += 	'<div class="ratingResult">';
            html += 		'<span class="fa fa-star" id="1-starSesion-'+ data[i].id_sesion + '-' + data[i].id_grupo +'"></span>';
            html += 		'<span class="fa fa-star" id="2-starSesion-'+ data[i].id_sesion + '-' + data[i].id_grupo +'"></span>';
            html += 		'<span class="fa fa-star" id="3-starSesion-'+ data[i].id_sesion + '-' + data[i].id_grupo +'"></span>';
            html += 		'<span class="fa fa-star" id="4-starSesion-'+ data[i].id_sesion + '-' + data[i].id_grupo +'"></span>';
            html += 		'<span class="fa fa-star" id="5-starSesion-'+ data[i].id_sesion + '-' + data[i].id_grupo +'"></span>';
            html += 	'</div>';
	        html += '</div>';

			$('#sesiones').append(html);

        comprobarCalificacion( data[i].id_sesion , data[i].id_grupo );
	}
}

function calificarSesion(id_sesion, id_grupo)
{
	getFormCalificacion(id_sesion, id_grupo);
	$('#popupCalificacion').show();
	sesionSeleccionada = id_sesion;
	grupoSeleccionado = id_grupo;
}

function getEstrellas(id_sesion, id_grupo)
{

}

function getFormCalificacion(id_sesion, id_grupo)
{
	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'getFormCalificacionCON',
            id_sesion : id_sesion,
			id_grupo : id_grupo,
        },
        success : function(Data){debugger
        	var miData = JSON.parse(Data);
        	armarFormCalificacion(miData);
        }
    });
}

function comprobarCalificacion(id_sesion, id_grupo)
{
	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'getComprobarCalificacion',
            id_sesion : id_sesion,
			id_grupo : id_grupo,
        },
        success : function(Data){
        	var miData = JSON.parse(Data);
        	if(miData.promedio > 0)
            {
                var nStar = Math.round(miData.promedio);
                for (var i = 1; i < nStar+1; i++) {
                    $('#'+ i +'-starSesion-'+ id_sesion + '-' + id_grupo).addClass('checked');
                }
            }
        }
    });
}

function armarFormCalificacion(data)
{
	var preguntas = '';
	for (var i = 0; i < data.preguntas.length; i++) 
	{
		var num = data.preguntas[i].num_pregunta;
		var id_pregunta = data.preguntas[i].id_pregunta;
		$('#pregunta'+num).html(data.preguntas[i].pregunta);

		var respuestas = '';
		for (var j = 0; j < data.preguntas[i].respuestas.length; j++) 
		{
			var resp = data.preguntas[i].respuestas;
			respuestas += '<input type="checkbox" class="respuestas-pregunta-'+num+'" name="calificacion'+num+'[]" id="calificacion-'+id_pregunta+'-'+resp[j].id_respuesta+'" value="'+resp[j].id_respuesta+'">';
            respuestas += '<label for="calificacion-'+id_pregunta+'-'+resp[j].id_respuesta+'">'+resp[j].respuesta+'</label>';
		}
		$('#respuestas'+num).html(respuestas);
	}
}

function calificar()
{
	var id_sesion = sesionSeleccionada;
	var id_grupo = grupoSeleccionado;

	var asistencias = getCheckboxValues('asistencia');
	var resp1       = getCheckboxValues('calificacion1');
	var resp2       = getCheckboxValues('calificacion2');
	var stars       = getRadio('rating');
	var comentario  = $('#calificacion-texto').val();

	if(!validarCalificacion())
		return;

	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'sesiones',
            call: 'calificar',
            id_sesion : id_sesion,
			id_grupo : id_grupo,
			asistencias : asistencias,
			resp1 : resp1,
			resp2 : resp2,
			stars : stars,
			comentario : comentario,
        },
        success : function(Data){
        	console.log(Data);
        	var miData = JSON.parse(Data);
        	alert(miData.mensaje);
        }
    });
}

function validarCalificacion()
{
	var asistencias = getCheckboxValues('asistencia');
	var resp1       = getCheckboxValues('calificacion1');
	var resp2       = getCheckboxValues('calificacion2');
	var stars       = getRadio('rating');
	var comentario  = $('#calificacion-texto').val();

	if(resp1.length < 1){
		alert('Tenés que responder la pregunta 1');
		return false;
	}

	if(resp2.length < 1){
		alert('Tenés que responder la pregunta 1');
		return false;
	}

	if(stars.length < 1){
		alert('Tenés que calificar la sesión');
		return false;
	}

	if(comentario.length < 1){
		alert('Tenés que escribir un comentario');
		return false;
	}

	return true;
}

function getCheckboxValues(name)
{
	var checkboxes = document.getElementsByName(name+'[]');
	var vals = "";
	for (var i=0, n=checkboxes.length;i<n;i++) 
	{
	    if (checkboxes[i].checked) 
	    {
	        vals += ","+checkboxes[i].value;
	    }
	}
	if (vals) vals = vals.substring(1);
	return vals;
}

function getRadio(name)
{
	var radios = document.getElementsByName(name);
	for (var i = 0, length = radios.length; i < length; i++)
	{
		if (radios[i].checked)
		{
		  	return(radios[i].value);
		  	break;
		}
	}
}