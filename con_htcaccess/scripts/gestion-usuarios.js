$(document).ready(function(){
	getUsuarios();
});

$('#abrir-importar-excel').click(function(){
    $('#popupImportarUsuario').show();
});

$('#importarMentores').click(function(){
    importarMentores()
});

function getUsuarios()
{
	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'usuarios',
            call: 'getUsuarios',
        },
        success : function(Data){
        	var miData = JSON.parse(Data);
        	armarListaUsuarios(miData);
        }
    });
}

function filtrarUsuarios(id_tipo_usuario, button)
{
	var tipo_usuario = ['', 'Todos', 'Mentor Desarrollo Temprano', 'Mentor Consolidacion', 'Emprendedor Desarrollo temprano', 'Emprendedor Consolidación'];
	var lista = $('#listaUsuarios tr');
	$('.filtros').removeClass("current");
	button.parentElement.classList.add("current");
	
	if(id_tipo_usuario == 1)
	{
		lista.each(function( i ) {
		  	lista[i].style.visibility = "visible";
		});
	}else{
		lista.each(function( i ) {
		  	$("td", this).each(function( j ) {
		  		if(j == 1 )
		  		{
		  			if( $(this).text() == tipo_usuario[id_tipo_usuario] )
		  				lista[i].style.visibility = "visible";
		  			else
		  				lista[i].style.visibility = "hidden";
		  		}
		    	console.log("".concat("row: ", i, ", col: ", j, ", value: ", $(this).text()));
		  	});
		});
	}
}

function armarListaUsuarios(data)
{
	var html = '';

	for (var i = 0; i < data.length; i++) 
	{
		html += '<tr>';
        html += '    <td data-label="Nombre: "><a href="javascript:void(0)">'+data[i].nombre+'</a></td>';
        html += '    <td data-label="Cargo: "><a href="javascript:void(0)">'+data[i].cargo+'</a></td>';
        html += '    <td data-label="Última actividad: ">'+data[i].ult_login+'</td>';
        html += '    <td data-label="Sesiones: ">'+data[i].sesiones+'</td>';
        html += '    <td data-label="E-mail: " style="white-space: nowrap;">'+data[i].email+'</td>';
        html += '    <td data-label="Contraseña: ">'+data[i].password+'</td>';
        html += '    <td data-label="Estado: ">';
        html += '        <label class="switch">';
        if(data[i].estado == -1)
        	html += '        <input type="checkbox" onclick="bloquearUsuario('+data[i].id_usuario+')">';
    	else
        	html += '        <input type="checkbox" checked onclick="desbloquearUsuario('+data[i].id_usuario+')">';
        html += '            <span class="slider round"></span>';
        html += '        </label>';
        html += '    </td>';
        html += '</tr>';
	}

	$('#listaUsuarios').html(html);
}

function importarMentores()
{
	var f = $(this);
    var formData = new FormData(document.getElementById("formImportadorExcel"));
    formData.append("action", "importarMentoresExcel");

    $.ajax({
        xhr: function() {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function(evt) {
              if (evt.lengthComputable) {
                var percentComplete = evt.loaded / evt.total;
                percentComplete = parseInt(percentComplete * 100);
                console.log('Porcentaje subido : '+percentComplete);
              }
            }, false);
            return xhr;
          },
        url: "ajaxRedirect.php",
        type: "post",
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success : function(Data){   
            debugger
            if (!isJson(Data)) {
            	alert(Data);
            	return;
            }
            var data = JSON.parse(Data);
            var mensaje = '';
            for (var i = 0; i < data.length; i++) {
            	if(data[i].cod_mensaje == 0)
            	{
            		mensaje += data[i].mensaje + ' (línea '+data[i].linea+ ') <br> ' ;
            	}
            }
            if(mensaje == '')
            	alert('Todos los usuarios se cargaron correctamente!');
            else
            	alert(mensaje);
        }
    });
}

function bloquearUsuario(id_usuario)
{
	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'usuarios',
            call: 'bloquearUsuario',
            id_usuario: id_usuario,
        },
        success : function(Data){
        	var miData = JSON.parse(Data);
        	console.log(miData);
        }
    });
}

function desbloquearUsuario(id_usuario)
{
	$.ajax({
        type: 'POST',
        url: 'ajaxRedirect.php',
        datatype: 'html',
        async : false,
        data: {
            action: 'usuarios',
            call: 'desbloquearUsuario',
            id_usuario: id_usuario,
        },
        success : function(Data){
        	var miData = JSON.parse(Data);
        	console.log(miData);
        }
    });
}

function importarRelacionamiento()
{
    
}

function isJson(str) 
{
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}