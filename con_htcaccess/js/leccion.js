$(document).ready(function() {
	var enterQuiz = function(){
		$('.video-holder').fadeOut(700, function(){
			$("#js-quiz").fadeIn(700);
			$("#js-quiz").slickQuiz({
				// Así se pasarían dinámicamente las preguntas precargadas por AJAX...
				// json: url,
				preventUnanswered: true,
				disableRanking: true,
				displayQuestionNumber: false,
				displayQuestionCount: false,
				preventUnansweredText: 'Debes seleccionar una respuesta.',
				checkAnswerText:  'Verificar respuesta',
				nextQuestionText: 'Siguiente',
				completeQuizText: 'Finalizar',
				events: {
					onStartQuiz: startQuiz,
					onCompleteQuiz: finishQuiz
				}
			});
		});
	}

	// Se esconden y se muestran algunas cosas cuando
	// arranca el quiz con el botón "¡Estoy listo!"
	var startQuiz = function(){
		$('.js-quiz-tips, .js-quiz-buttons').fadeOut(300);

		$('.js-quiz-header').slideUp(600, function(){
			$('.js-quiz-timer').fadeIn(600);
		});

		var scrollPoint = $("#js-quiz").offset().top;
		$('body,html').scrollTop(scrollPoint);

		// Agrega clase a .answer para stylear el choice de imagenes
		$('.answers').each(function() {
			var liAnswer = $(this).children().children('label').children('img');
			var liAnswerVid = $(this).children().children('label').children('video');
			var answerList = $(this);

			if (liAnswer.hasClass('img-responsive')) {
				answerList.addClass('answers-img');
			};

			if (liAnswerVid.hasClass('video')) {
				answerList.addClass('answers-video');
			};
		});

		$('.questions>li>[class=answers]').addClass('answers-text');

		// Toggle de clases del choice de imagenes
		$('.answers-img li input[type="radio"], .answers-video li input[type="radio"], .answers-text li input[type="radio"]').each(function(){
			$(this).change(function(){
				if ($("label[for='"+$(this).attr("id")+"']").not('.checked-answer')) {
					$("label[for='"+$(this).attr("id")+"']").addClass('checked-answer');         
				}if ($(this).parent().siblings().children().hasClass('checked-answer')){
					$(this)
					.parent()
					.siblings()
					.children()
					.removeClass('checked-answer');
				}
			});
		});
	}

	var finishQuiz = function(results){
		$('.js-quiz-timer').fadeOut(600);
		// Si todas las preguntas son correctas...
		if (results.questionCount == results.score) {
			$('.js-quiz-results-copy').text('¡Felicitaciones! Respondiste correctamente todas las preguntas y podés continuar al próximo video.');
			$('.js-quiz-results-btn').text('Ir al próximo video');
		} else {
			setTimeout(function(){
				$('.js-quiz-container').addClass('quiz__container--error');
			}, 500);
			$('.js-quiz-results-copy').text('Lamentablemente no respondiste correctamente alguna de las preguntas, vas a tener que ver nuevamente el video.');
			$('.js-quiz-results-btn')
			.text('Volver al video')
			.css('color', '#f22065');
		}
	}

	// Video player: Projekktor
	
	var projekktorTemplate = '\
	<ul class="left">\
	<li><div %{play}></div><div %{pause}></div></li>\
	<li><div %{title}></div></li>\
	</ul>\
	<ul class="right">\
	<li><div %{fsexit}></div><div %{fsenter}></div></li>\
	<li><div class="ppvslider-container"><i class="ppvslider-icon"></i><div %{vslider}><div %{vmarker}></div><div %{vknob}></div></div></div></li>\
	<li><div %{timeleft}>%{min_elp}:%{sec_elp}<span class="duration">%{min_dur}:%{sec_dur}</span></div></li>\
	</ul>\
	<ul class="bottom">\
	<li><div %{scrubber}><div %{loaded}></div><div %{playhead}></div><div %{scrubberdrag}></div></div></li>\
	</ul>\
	';

	$('.video-overlay').click(function() {
		$(this).fadeOut(500).next('.projekktor').find('.ppstart').click();
	});

	var videoPlayer = projekktor('.projekktor', {
		volume: 1,
		width: 0,
		height: null,
		ratio: 16/9,
		playerFlashMP4: 'js/StrobeMediaPlayback.swf',
		controlsTemplate: projekktorTemplate,
		fadeDelay: 0
	});

	videoPlayer.addListener('state', function(){
		if (videoPlayer.getState() == 'COMPLETED') {
			enterQuiz();
		}
	});
});