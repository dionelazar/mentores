/* DESPLIEGA Y CIERRA DROPDOWN */
$('.dropdown').click(function() {
	$(this).toggleClass('dropdown-expanded').next('ul').slideToggle(250);
	$(this).parent('.user-top').toggleClass('user-top-expanded');
});

$(document).click(function() { 
	if(!$(event.target).closest('.user-top').length) {
		$('.user-top').removeClass('user-top-expanded').find('.dropdown').removeClass('dropdown-expanded');
		$('.user-top .dropdown + ul').slideUp(250);
	}
});

/* DESPLIEGA Y CIERRA NOTIFICACIONES */
$('.noti-top a').click(function() {
	$(this).toggleClass('noti-top-active');
	$('.notifications').toggleClass('notifications-expanded');
});

$(document).click(function() { 
	if(!$(event.target).closest('.notifications, .noti-top').length) {
		$('.noti-top a').removeClass('noti-top-active');
		$('.notifications').removeClass('notifications-expanded');
	}
});

/* DESPLIEGA Y CIERRA MENÚ RESPONSIVE */
$('.burger').click(function() {
	$('aside').addClass('aside-expanded');
});

$(document).click(function() { 
	if(!$(event.target).closest('aside, .burger').length) {
		$('aside').removeClass('aside-expanded');
	}
});

/* EFECTO TIPO MATERIAL AL CLICKEAR LINKS */
(function (window, $) {
	$(function() {
		$('a.button, nav a, .video-play, .ultimas-novedades a, .user-top a, .noti-top a, .lecciones a, .notifications a, .ppstart').on('click', function (event) {
			var $div = $('<div/>'),
			btnOffset = $(this).offset(),
			xPos = event.pageX - btnOffset.left,
			yPos = event.pageY - btnOffset.top;

			$div.addClass('ripple-effect');
			var $ripple = $(".ripple-effect");

			$ripple.css("height", $(this).height());
			$ripple.css("width", $(this).height());
			$div
			.css({
				top: yPos - ($ripple.height()/2),
				left: xPos - ($ripple.width()/2),
				background: $(this).data("ripple-color")
			}) 
			.appendTo($(this));

			window.setTimeout(function(){
				$div.remove();
			}, 1000);
		});
	});
})(window, jQuery);


/* FORM on click show */

var situacion1 = $(".situacion-laboral-1");
var situacion2 = $(".situacion-laboral-2");
var situacion3 = $(".situacion-laboral-3");
var situacion4 = $(".situacion-laboral-4");
var situacion5 = $(".situacion-laboral-5");

$('#situacion-laboral-1').click(function() {
	situacion1.toggle()
	situacion2.toggle(false)
	situacion3.toggle(false)
	situacion4.toggle(false)
	situacion5.toggle(false)
});
$('#situacion-laboral-2').click(function() {
	situacion1.toggle(false)
	situacion2.toggle()
	situacion3.toggle(false)
	situacion4.toggle(false)
	situacion5.toggle(false)
});
$('#situacion-laboral-3').click(function() {
	situacion1.toggle(false)
	situacion2.toggle(false)
	situacion3.toggle()
	situacion4.toggle(false)
	situacion5.toggle(false)
});
$('#situacion-laboral-4').click(function() {
	situacion1.toggle(false)
	situacion2.toggle(false)
	situacion3.toggle(false)
	situacion4.toggle()
	situacion5.toggle(false)
});
$('#situacion-laboral-5').click(function() {
	situacion1.toggle(false)
	situacion2.toggle(false)
	situacion3.toggle(false)
	situacion4.toggle(false)
	situacion5.toggle()
});


//jQuery time

var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

$(".next").click(function(){
	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parent();
	next_fs = $(this).parent().next();
	
	//activate next step on progressbar using the index of next_fs
	$("#progressbar li").eq($(".fieldset").index(next_fs)).addClass("active");
	
	//show the next fieldset
	next_fs.show(); 
	current_fs.css({'transform': 'scale('+scale+')','position':'absolute'});
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//2. bring next_fs from the right(50%)
			left = (now * 50)+"%";
			//3. increase opacity of next_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({
        'position': 'absolute'
      });
			next_fs.css({'left': left, 'opacity': opacity});
		}, 
		duration: 400, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
	});
});

$(".previous").click(function(){
	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parent();
	previous_fs = $(this).parent().prev();
	
	//de-activate current step on progressbar
	$("#progressbar li").eq($(".fieldset").index(current_fs)).removeClass("active");
	
	//show the previous fieldset
	previous_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//2. take current_fs to the right(50%) - from 0%
			left = ((1-now) * 50)+"%";
			//3. increase opacity of previous_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({'left': left});
			previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
		}, 
		duration: 400, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
	});
});
// HOME 

var sesionA = $(".sesionA");
var sesionB = $(".sesionB");
var sesionC = $(".sesionC");
var sesionD = $(".sesionD");

$('#homeTab1').click(function() {
	sesionA.addClass("show");
	sesionB.removeClass("show");
	sesionC.removeClass("show");
	sesionD.removeClass("show");
	$('#homeTab1').addClass("current");
	$('#homeTab2').removeClass("current");
	$('#homeTab3').removeClass("current");
	$('#homeTab4').removeClass("current");
});
$('#homeTab2').click(function() {
	sesionA.removeClass("show");
	sesionB.addClass("show");
	sesionC.removeClass("show");
	sesionD.removeClass("show");
	$('#homeTab1').removeClass("current");
	$('#homeTab2').addClass("current");
	$('#homeTab3').removeClass("current");
	$('#homeTab4').removeClass("current");
});
$('#homeTab3').click(function() {
	sesionA.removeClass("show");
	sesionB.removeClass("show");
	sesionC.addClass("show");
	sesionD.removeClass("show");
	$('#homeTab1').removeClass("current");
	$('#homeTab2').removeClass("current");
	$('#homeTab3').addClass("current");
	$('#homeTab4').removeClass("current");
});
$('#homeTab4').click(function() {
	sesionA.removeClass("show");
	sesionB.removeClass("show");
	sesionC.removeClass("show");
	sesionD.addClass("show");
	$('#homeTab1').removeClass("current");
	$('#homeTab2').removeClass("current");
	$('#homeTab3').removeClass("current");
	$('#homeTab4').addClass("current");
});

///////

/* POP UP CALIFICACION */

    var overlay = $("overlayCalificacion");
    var BtnCalificar = $(".calificarBtn");
    var BtnCertificado = $(".btnCertificado");

    BtnCalificar.click(function() {
    $("#popupCalificacion").fadeIn(250);
    });
    $(".closePopUp").click(function() {
    $("#popupCalificacion").fadeOut(250);
    });
    BtnCertificado.click(function() {
    $("#popupCertificacion").fadeIn(250);
    });
    $(".closePopUp").click(function() {
    $("#popupCertificacion").fadeOut(250);
    });
    $(".closePopUpSede").click(function() {
    $("#popupSede").fadeOut(250);
    });
    $(".closePopUp").click(function() {
    $("#popupSatisfaccion").fadeOut(250);
    });
    $(".closePopUp").click(function() {
    $("#popupEvaluacion").fadeOut(250);
    });
    $(".closePopUp").click(function() {
    $("#popupImportarUsuario").fadeOut(250);
    });



 /////


/* POP UP Reportes */

    $(".closePopUp").click(function() {
    	event.preventDefault();
    $("#popupEgresados").fadeOut(250);
    $("#popupEgresados2").fadeOut(250);
    $("#popupMentores").fadeOut(250);
    $("#popupMentorias").fadeOut(250);
    $("#popupEmprendedoresSesion1").fadeOut(250);
    $("#popupGenero").fadeOut(250);
    $("#popupRubros").fadeOut(250);
    $("#popupTerritoriales").fadeOut(250);
    $("#popupSatisfaccionEmpr").fadeOut(250);
    $("#popupProcesoMentoria").fadeOut(250);
    $("#popupMentoresEnFormacion").fadeOut(250);
	$("#popupAtencionParticipante").fadeOut(250);
    
    });

    

 ///// /////

 /* Abre popups de reportes */


$(".mentoresFormados").click(function() {
    	event.preventDefault();
    $("#popupMentores").fadeIn(250);
});
$(".mentoriasFuncionando").click(function() {
    	event.preventDefault();
    $("#popupMentorias").fadeIn(250);
});
$(".emprEgresados1").click(function() {
    	event.preventDefault();
    $("#popupEgresados").fadeIn(250);
});
$(".emprEgresados2").click(function() {
    	event.preventDefault();
    $("#popupEgresados2").fadeIn(250);
});
$(".representacionGenero").click(function() {
    	event.preventDefault();
    $("#popupGenero").fadeIn(250);
});
$(".alcanceTerritorial").click(function() {
    	event.preventDefault();
    $("#popupTerritoriales").fadeIn(250);
});
$(".diversidadProductiva").click(function() {
    	event.preventDefault();
    $("#popupRubros").fadeIn(250);
});
$(".emprSatisfaccion").click(function() {
    	event.preventDefault();
    $("#popupSatisfaccionEmpr").fadeIn(250);
});
$(".mentoresFormacion").click(function() {
    	event.preventDefault();
    $("#popupMentoresEnFormacion").fadeIn(250);
});
$(".participanteSatisfaccion").click(function() {
    	event.preventDefault();
    $("#popupAtencionParticipante").fadeIn(250);
});
$(".emprProcesoMentoria").click(function() {
    	event.preventDefault();
    $("#popupProcesoMentoria").fadeIn(250);
});
$(".emprProcesoMentoria").click(function() {
    	event.preventDefault();
    $("#popupImportarUsuario").fadeIn(250);
});

//$('.collapse').collapse();

 ///// ///// ///// /////

 /* ACORDEON REPORTE */

	function inicializarAcordion() {

		$('.timeline-open').hide();

		$('.verCalificacionBtn').click(function () {
			var section = $(this).parent();
			if (section.hasClass('active')) {
				section.removeClass('active');
				section.find('.timeline-open').slideUp();
				section.find('.verCalificacion').css("background-color", "rgba(0, 188, 241, 0.5)");
				section.find('.expandir-emprendedor').removeClass("current");
			} else {
				section.addClass('active');
				section.find('.timeline-open').slideDown();
				cerrarHermanos(section);
				section.find('.verCalificacion').css("background-color", "rgba(227,86,49,1)");
				section.find('.expandir-emprendedor').addClass("current");
			}
		})
	}
	function cerrarHermanos(section) {
		section.siblings().removeClass('active');
		section.siblings().find('.timeline-open').slideUp();
	}

	inicializarAcordion();

//////

$(".submit").click(function(){
	return false;
})


$( document ).ready(function() {


	$(".carrousel").slick({
	infinite: true,
	slidesToShow: 4,
	dots: false,
	autoplay: true,
	autoplaySpeed: 3000,
	speed: 1000,
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3,
	        infinite: true,
	        autoplay: true
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2,
	        infinite: true,
	        autoplay: true
	      }
	    },
	    {
	      breakpoint: 360,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1,
	        infinite: true,
	        autoplay: true
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});
});

// ESTADISTICAS 

var listChart1 = $(".listChart1");
var listChart2 = $(".listChart2");

$('#listChart1').click(function() {
	$(window).trigger('resize');
	listChart1.toggleClass("show");
	listChart2.toggleClass("show");
	$('#listChart2').toggleClass("current");
	$('#listChart1').toggleClass("current");
	$(window).trigger('resize');
});
$('#listChart2').click(function() {
	
	listChart1.toggleClass("show");
	listChart2.toggleClass("show");
	$('#listChart1').toggleClass("current");
	$('#listChart2').toggleClass("current");
	$(window).trigger('resize');
});


$('[data-slidetoggle]').click(function() {
	var slide = $(this).attr('data-slidetoggle');
	$('.slided:not(#' + slide + ')').slideUp(500).removeClass('slided');
	$('#' + slide).slideToggle(500, function() {
		$(this).addClass('slided');
		var scrollTop = $(this).offset().top;
		var headHeight = $('header').height();
		var scrollTo = scrollTop + headHeight + 20;
		$('html, body').animate({scrollTop: scrollTo}, 700);
	});
});

$('[data-slidebasic]').click(function() {
	var slide = $(this).attr('data-slidebasic');
	$('#' + slide).slideToggle(500);
});

$('[data-slideup]').click(function() {
	var slide = $(this).attr('data-slideup');
	$('#' + slide).removeClass('slided').slideUp(500);
});

$('[data-slideupall]').click(function() {
	$('.panel-desplegable').removeClass('slided').slideUp(500);
});

$('.ocultar-estadisticas').click(function() {
	$(this).find('span').toggle();
});

/* TABS POPUP REPORTES */
$('#myTabs a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})

/******************************/