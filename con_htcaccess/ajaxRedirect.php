<?php 
	if(isset($_POST['action']))
	{
		switch ($_POST['action']) //action seria la class , despues iria el metodo (call)
		{
			case 'importarMentoresExcel':
				if( $_FILES['myFileExcel']['size'] <= 0 )
					die();

				require 'class/class.usuarios.php';
				$result = Usuarios::insMentoresExcel($_FILES['myFileExcel']);
				break;

			default:
				$class  = ucwords($_POST['action']);
				$method = $_POST['call'];
				require_once 'class/class.'.$_POST['action'].'.php';
				$result = $class::$method($_POST);
				if(is_array($result))
					echo json_encode($result);
				else
					echo $result;
				break;
		}
	}
?>