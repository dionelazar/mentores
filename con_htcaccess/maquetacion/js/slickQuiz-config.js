var quizJSON = {
    "info": {
        "name":    "Samsung Galaxy S6 - Caracteristicas Generales",
        "main":    "",
        "results": "&nbsp;"
    },
    "questions": [
        {
            "q": "¿Cuál es la correcta?",
            "a": [
                {"option": "<img class='img-responsive' src='http://placehold.it/300x100/ffffff/395597/&text=OPCI%C3%93N%201' alt=''/>", "correct": false},
                {"option": "<img class='img-responsive' src='http://placehold.it/300x100/ffffff/395597/&text=OPCI%C3%93N%202' alt=''/>", "correct": false},
                {"option": "<img class='img-responsive' src='http://placehold.it/300x100/ffffff/395597/&text=OPCI%C3%93N%202' alt=''/>", "correct": true},
            ],
            "correct": '<p>Su alta cantidad de RAM le otorga excelentes capacidades de multitasking.</p>',
            "incorrect": '<p>El equipo tiene 3 GB de RAM, lo cual le otorga excelentes capacidades de multitasking.</p>'
        },
        {
            "q": "¿Cuál es la correcta?",
            "a": [
                {"option": "<video controls class='video' poster'img/video-bg.jpg'><source type='video/mp4' src='https://www.sidepro.com.ar/PortalMovistar/img/Videos/submodulo5.mp4'></video>", "correct": false},
                {"option": "<video controls class='video' poster'img/video-bg.jpg'><source type='video/mp4' src='https://www.sidepro.com.ar/PortalMovistar/img/Videos/submodulo5.mp4'></video>", "correct": false},
                {"option": "<video controls class='video' poster'img/video-bg.jpg'><source type='video/mp4' src='https://www.sidepro.com.ar/PortalMovistar/img/Videos/submodulo5.mp4'></video>", "correct": true},
            ],
            "correct": '<p>Su alta cantidad de RAM le otorga excelentes capacidades de multitasking.</p>',
            "incorrect": '<p>El equipo tiene 3 GB de RAM, lo cual le otorga excelentes capacidades de multitasking.</p>'
        },
        {
            "q": "¿Qué resolución tiene la pantalla del equipo?",
            "a": [
                {"option": "720p (1280x720)",   "correct": false},
                {"option": "1080p (1920x1080)", "correct": false},
                {"option": "QHD (2560x1440)",   "correct": true},
                {"option": "4K (3840x2160)",    "correct": false}
            ],
            "correct": "<p>Por el momento, muy pocos equipos llegan a tener resolución 4K.</p>",
            "incorrect": "<p>La pantalla tiene resolución QHD. Por el momento, muy pocos equipos llegan a tener resolución 4K.</p>"
        }
    ]
};