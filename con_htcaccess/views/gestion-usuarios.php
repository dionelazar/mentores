<?php 
    require __DIR__.'/share/header-admin.php';
?>
    <div class="content">
        <!-- TODO EL CONTENIDO (LO QUE NO ES MENÚ NI HEADER) -->
        <div class="wrapper">
            <div class="col100">
                <div class="module search-bar">
                    <!-- BARRA DE BÚSQUEDA -->
                    <div class="inner-cols clearfix">
                        <div class="col50">
                            <form action="#" name="search" id="search">
                                <input type="text" placeholder="Buscar módulos, cursos o lecciones...">
                                <input type="submit" value="&#xe041;">
                            </form>
                        </div>
                        <div class="col50">
                            <ul class="library-filters">
                                <li data-icon="&#xe052;" class="pad25top">Filtrar</li>
                                <li class="filtros current"><a style="padding-top: 20px;" href="javascript:void(0)" onclick="filtrarUsuarios(1, this)">Todos</a></li>
                                <li class="filtros"><a href="javascript:void(0)" onclick="filtrarUsuarios(2, this)"><strong>Mentor</strong><br>D. Temprano</a></li>
                                <li class="filtros"><a href="javascript:void(0)" onclick="filtrarUsuarios(3, this)"><strong>Mentor</strong><br> Consolidación</a></li>
                                <li class="filtros"><a href="javascript:void(0)" onclick="filtrarUsuarios(4, this)"><strong>Emprendedor</strong> <br>D. Temprano</a></li>
                                <li class="filtros"><a href="javascript:void(0)" onclick="filtrarUsuarios(5, this)"><strong>Emprendedor</strong><br> Consolidación</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col100">
                <div class="module library">
                    <div class="right">
                        <input type="button" name="abrir-importar-excel" class="btn" id="abrir-importar-excel" value="Importar" />
                    </div>
                    <table>
                        <thead>
                            <tr>
                                <th data-icon="">
                                    <br>Nombre</th>
                                <th data-icon="&#xe05d;">
                                    <br>Cargo</th>
                                <th data-icon="&#xe046;">
                                    <br>Última Actividad</th>
                                <th data-icon="p">
                                    <br>Sesiones</th>
                                <th data-icon="&#xe015;">
                                    <br>E-mail</th>
                                <th><i class="fa fa-key" aria-hidden="true"></i>
                                    <br>Constraseña</th>
                                <th data-icon="m">
                                    <br>Estado</th>
                            </tr>
                        </thead>
                        <tbody id="listaUsuarios">
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div id="popupImportarUsuario" class="absolute-center" hidden="">
        <div id="overlayCalificacion"></div>
        <div class="popupReportes">
            <a href="#" id="closeBtn" class="closePopUp">
                <i class="close-icon" data-icon="9"></i>
            </a>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h2>Importar Usuario</h2>
                        <hr>
                        <h3 style="color: #d62902"><strong>Recuerde, al importar usuarios debe descargarse la planilla de excel modelo <a href="assets/Ejemplo-importador-de-mentores.xls" download>(link)</a> y completar todos los campos de cada usuario, sólo el email es opcional de completar. El formato debe ser .xls y se debe respetar completamente el formato del <a href="assets/Ejemplo-importador-de-mentores.xls" download>ejemplo</a>. Muchas gracias.</strong></h3>
                    </div>
                    <form enctype="multipart/form-data" id="formImportadorExcel" action="javascript:void(0)">
                        <div class="col-xs-12">
                            <p>Seleccione su hoja de excel de usuarios</p>
                            <input name="myFileExcel" id="myFileExcel" type="file">
                        </div>
                        <div class="col-xs-12" style="text-align: center;">
                            <input type="button" name="importarRel" class="btn" id="importarRel" value="Importar Relacionamiento" onclick="importarRelacionamiento()" />
                            <input type="button" name="importarMentores" class="btn" id="importarMentores" value="Importar Mentores" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script>
    window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')
    </script>
    <script src="js/plugins.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.js"></script>
    <script src="js/masonry.pkgd.min.js"></script>
    <script src="js/main.js"></script>
    <script src="scripts/header.js"></script>
    <script src="scripts/gestion-usuarios.js"></script>
</body>

</html>