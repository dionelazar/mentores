<?php 
    require __DIR__.'/share/header-admin.php';
?>
    <div class="content home ministerio" >
        <!-- TODO EL CONTENIDO (LO QUE NO ES MENÚ NI HEADER) -->
        <div class="wrapper">
            <div class="col100" id="referencias">
                <a href="reportes-admin" class="volver"><i data-icon="l"></i> Volver</a>
                <h1><strong>Referencias Reportes</strong></h1>
                <hr>
                <br>
                <h3><span style="font-size: 14px;font-weight: 600;">1-</span> <strong> Cantidad de mentores formados</strong></h3>
                <p>Al finalizar el proceso de formación, el mentor recibe un certificado que incluirá también el “código/usuario” para poder ingresar a la APP. Teniendo en cuenta esto, todo mentor que se este ingresado en la APP se contabiliza como “mentor formado.</p>
                <h3><span style="font-size: 14px;font-weight: 600;">2-</span><strong> Cantidad de procesos de mentoría en funcionamiento”</strong></h3>
                <p>El “proceso de mentoría” equivale a un “grupo de mentoría”, es decir que, cada grupo que formemos en la APP deberá figurar como “proceso de mentoría en funcionamiento” a partir de que se lleve a cabo la primera sesión.</p>
                <h3><span style="font-size: 14px;font-weight: 600;">3-</span><strong> Cantidad de emprendedores en proceso de mentoría</strong></h3>
                <p>Comprende a los emprendedores que están participando activamente en su proceso de mentoría. Acordamos con el ministerio que este indicador se acreditará mediante una “declaración del mentor” en la que declara los emprendedores que está mentoreando. Sugiero en este caso, que además de mostrarlo en el tablero, se agregue la posibilidad de descargar un PDF “declaración de emprendedores en proceso de mentoría”. Esta declaración debe incluir a los emprendedores que:</p>
                <ol>
                    <li>Hayan sido asignado a un mentor, conformando un grupo de mentoría.</li>
                    <li>Dicho grupo se encuentre activo (que ya se haya realizado la primera sesión, refiere a indicador.</li>
                    <li>Que cumpla con el requisito de asistencia mínima (80% de asistencia, refiere a indicador.)</li>
                </ol>
                <h3><span style="font-size: 14px;font-weight: 600;">4-</span><strong> Cantidad de emprendedores egresados del proceso de mentoría</strong></h3>
                <p>Contabiliza los emprendedores que han finalizado la FASE 1. Los emprendedores que según la tabla de asistencia hayan concurrido por lo menos al 80% de las sesiones de la FASE 1 se contabilizarán como “emprendedores egresados.</p>
                <h3><span style="font-size: 14px;font-weight: 600;">5-</span><strong> Alcance territorial del programa a nivel provincial y municipal</strong></h3>
                <p>El programa tiene sedes asignadas. Se las “provincias” representadas, vinculando el campo provincia del formulario con los “emprendedores en proceso de mentoría, indicador 4”. De esta manera contabilizaríamos e identificaríamos las sedes “activas”, es decir las que tienen emprendedores. </p>
                <h3><span style="font-size: 14px;font-weight: 600;">6- </span><strong>Cantidad de encuentros que cada grupo de mentoría ha completado</strong></h3>
                <p>Cantidad de sesiones en las que el mentor completo la lista de asistencia. El indicador también incluye el porcentaje de grupos que hay en cada etapa, por lo tanto, se calcula también cuantos grupos han completado una sesión, dos sesiones, tres sesiones, etc.</p>
                <h3><span style="font-size: 14px;font-weight: 600;">7- </span><strong>Representación de género</strong></h3>
                <p>Porcentaje hombre/mujer en cada caso.</p>
                <h3><span style="font-size: 14px;font-weight: 600;">8- </span><strong>Diversidad productiva</strong></h3>
                <p>Porcentaje de cada rubro sobre el total de emprendedores.</p>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/masonry.pkgd.min.js"></script>
    <script src="js/select2.js"></script>
    <script src="js/main.js"></script>
</body>

</html>