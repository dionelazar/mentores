<?php 
	if(isset($_COOKIE['tipo_usuario']))
	{
		require_once '../class/class.helpers.php';

		$tipo_usuario = Encriptacion::desencriptar($_COOKIE['tipo_usuario']);

		switch ($tipo_usuario) 
		{
			case 1:
				header('location:gestion-usuarios');
				break;
			
			case 2:
				header('location:home-mentorDT');
				break;

			case 3:
				header('location:home-mentorCON');
				break;

			case 4:
				header('location:home-DT');
				break;

			case 5:
				header('location:home-CON');
				break;
		}
	}else{
		header('location:login');
	}
?>