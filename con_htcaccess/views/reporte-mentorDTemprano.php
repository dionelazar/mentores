<?php require __DIR__.'/share/header-mentorDT.php' ?>
            <!-- TODO EL CONTENIDO (LO QUE NO ES MENÚ NI HEADER) -->
        <div class="content reporte">
            <div class="wrapper">
                <div class="col100">
                    <div class="module search-bar">
                        <!-- BARRA DE BÚSQUEDA -->
                        <div class="inner-cols clearfix">
                            <div class="col100">
                                <form action="#" name="search" id="search">
                                    <input type="text" placeholder="Buscar sesiones, mentores, grupos o usuarios">
                                    <input type="submit" value="&#xe041;">
                                </form>
                            </div>
                        </div>
                    </div>
                    <ul class="tabs tabs-2 clearfix">
                        <li id="homeTab1" class="current"><a href="#0">Emprendedor 1</a></li>
                        <li id="homeTab2"><a href="#0">Emprendedor 2</a></li>
                    </ul>
                    <div class="container reporte">
                        <div class="sesionA show">
                            <div class="timeline-item timeline-closed" sesion='1ª Sesión'>
                                <div class="timeline-closed">
                                    <h3><strong>Puntuación del grupo:</strong></h3>
                                    <div class="ratingResult">
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                    </div>
                                </div>
                                <a class="verCalificacionBtn">
                                    <button class="verCalificacion" data-icon="&#xe057;"></button>
                                </a>
                                <div class="timeline-open">
                                    <hr>
                                    <ul>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="timeline-item timeline-closed" sesion='2ª Sesión'>
                                <div class="timeline-closed">
                                    <h3><strong>Puntuación del grupo:</strong></h3>
                                    <div class="ratingResult">
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                    </div>
                                </div>
                                <a class="verCalificacionBtn">
                                    <button class="verCalificacion" data-icon="&#xe057;"></button>
                                </a>
                                <div class="timeline-open">
                                    <hr>
                                    <ul>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="timeline-item timeline-closed" sesion='3ª Sesión'>
                                <div class="timeline-closed">
                                    <h3><strong>Puntuación del grupo:</strong></h3>
                                    <div class="ratingResult">
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                    </div>
                                </div>
                                <a class="verCalificacionBtn">
                                    <button class="verCalificacion" data-icon="&#xe057;"></button>
                                </a>
                                <div class="timeline-open">
                                    <hr>
                                    <ul>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="timeline-item timeline-closed" sesion='4ª Sesión'>
                                <div class="timeline-closed">
                                    <h3><strong>Puntuación del grupo:</strong></h3>
                                    <div class="ratingResult">
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                    </div>
                                </div>
                                <a class="verCalificacionBtn">
                                    <button class="verCalificacion" data-icon="&#xe057;"></button>
                                </a>
                                <div class="timeline-open">
                                    <hr>
                                    <ul>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="timeline-item timeline-closed" sesion='5ª Sesión'>
                                <div class="timeline-closed">
                                    <h3><strong>Puntuación del grupo:</strong></h3>
                                    <div class="ratingResult">
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                    </div>
                                </div>
                                <a class="verCalificacionBtn">
                                    <button class="verCalificacion" data-icon="&#xe057;"></button>
                                </a>
                                <div class="timeline-open">
                                    <hr>
                                    <ul>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="timeline-item timeline-closed" sesion='6ª Sesión'>
                                <div class="timeline-closed">
                                    <h3><strong>Puntuación del grupo:</strong></h3>
                                    <div class="ratingResult">
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                    </div>
                                </div>
                                <a class="verCalificacionBtn">
                                    <button class="verCalificacion" data-icon="&#xe057;"></button>
                                </a>
                                <div class="timeline-open">
                                    <hr>
                                    <ul>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="sesionB">
                            <div class="timeline-item timeline-closed" sesion='1ª Sesión'>
                                <div class="timeline-closed">
                                    <h3><strong>Puntuación del grupo:</strong></h3>
                                    <div class="ratingResult">
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                    </div>
                                </div>
                                <a class="verCalificacionBtn">
                                    <button class="verCalificacion" data-icon="&#xe057;"></button>
                                </a>
                                <div class="timeline-open">
                                    <hr>
                                    <ul>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="timeline-item timeline-closed" sesion='2ª Sesión'>
                                <div class="timeline-closed">
                                    <h3><strong>Puntuación del grupo:</strong></h3>
                                    <div class="ratingResult">
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                    </div>
                                </div>
                                <a class="verCalificacionBtn">
                                    <button class="verCalificacion" data-icon="&#xe057;"></button>
                                </a>
                                <div class="timeline-open">
                                    <hr>
                                    <ul>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="timeline-item timeline-closed" sesion='3ª Sesión'>
                                <div class="timeline-closed">
                                    <h3><strong>Puntuación del grupo:</strong></h3>
                                    <div class="ratingResult">
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                    </div>
                                </div>
                                <a class="verCalificacionBtn">
                                    <button class="verCalificacion" data-icon="&#xe057;"></button>
                                </a>
                                <div class="timeline-open">
                                    <hr>
                                    <ul>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="timeline-item timeline-closed" sesion='4ª Sesión'>
                                <div class="timeline-closed">
                                    <h3><strong>Puntuación del grupo:</strong></h3>
                                    <div class="ratingResult">
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                    </div>
                                </div>
                                <a class="verCalificacionBtn">
                                    <button class="verCalificacion" data-icon="&#xe057;"></button>
                                </a>
                                <div class="timeline-open">
                                    <hr>
                                    <ul>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="timeline-item timeline-closed" sesion='5ª Sesión'>
                                <div class="timeline-closed">
                                    <h3><strong>Puntuación del grupo:</strong></h3>
                                    <div class="ratingResult">
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                    </div>
                                </div>
                                <a class="verCalificacionBtn">
                                    <button class="verCalificacion" data-icon="&#xe057;"></button>
                                </a>
                                <div class="timeline-open">
                                    <hr>
                                    <ul>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="timeline-item timeline-closed" sesion='6ª Sesión'>
                                <div class="timeline-closed">
                                    <h3><strong>Puntuación del grupo:</strong></h3>
                                    <div class="ratingResult">
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                    </div>
                                </div>
                                <a class="verCalificacionBtn">
                                    <button class="verCalificacion" data-icon="&#xe057;"></button>
                                </a>
                                <div class="timeline-open">
                                    <hr>
                                    <ul>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="http://via.placeholder.com/150x150" alt="">
                                            <div class="content-timeline">
                                                <i data-icon="&#xe056;"></i>
                                                <h4><strong>Nombre del Emprendedor:</strong></h4>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere aliquid distinctio quaerat sequi iure optio non doloribus fugiat eius, laborum ducimus hic, voluptates, natus repudiandae adipisci quasi nulla pariatur.
                                                "</p>
                                            </div>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col100">
                <button class="descargarFotos"><i data-icon=":"></i>Descargar todas las Selfies</button>
            </div>
            <div id="popupCalificacion" class="absolute-center">
                <div id="overlayCalificacion"></div>
                <div class="popupCalificacion">
                    <a href="#" id="closeBtn">
                        <i class="close-icon" data-icon="9"></i>
                    </a>
                    <label for="rating">Calificá esta sesión</label>
                    <fieldset class="rating">
                        <input type="radio" id="star5" name="rating" value="5" />
                        <label class="full" for="star5" title="Awesome - 5 stars"></label>
                        <input type="radio" id="star4half" name="rating" value="4 and a half" />
                        <label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
                        <input type="radio" id="star4" name="rating" value="4" />
                        <label class="full" for="star4" title="Pretty good - 4 stars"></label>
                        <input type="radio" id="star3half" name="rating" value="3 and a half" />
                        <label class="half" for="star3half" title="Meh - 3.5 stars"></label>
                        <input type="radio" id="star3" name="rating" value="3" />
                        <label class="full" for="star3" title="Meh - 3 stars"></label>
                        <input type="radio" id="star2half" name="rating" value="2 and a half" />
                        <label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
                        <input type="radio" id="star2" name="rating" value="2" />
                        <label class="full" for="star2" title="Kinda bad - 2 stars"></label>
                        <input type="radio" id="star1half" name="rating" value="1 and a half" />
                        <label class="half" for="star1half" title="Meh - 1.5 stars"></label>
                        <input type="radio" id="star1" name="rating" value="1" />
                        <label class="full" for="star1" title="Sucks big time - 1 star"></label>
                        <input type="radio" id="starhalf" name="rating" value="half" />
                        <label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
                    </fieldset>
                    <fieldset class="label50">
                        <label for="calificacion1">En tu emprendimiento, ¿cuál/es de las siguientes funciones desempeñás?
                        <br><em><small>Podés seleccionar varias opciones</small></em></label>
                        <br>
                        <input type="checkbox" name="calificacion1" id="calificacion1-1" value="Decisión de la estrategia integral">
                        <label for="calificacion1-1">Decisión de la estrategia integral</label>
                        <input type="checkbox" name="calificacion1" id="calificacion1-2" value="Abastecimiento de insumos">
                        <label for="calificacion1-2">Abastecimiento de insumos</label>
                        <input type="checkbox" name="calificacion1" id="calificacion1-4" value="Ventas/Marketing">
                        <label for="calificacion1-4">Ventas/Marketing</label>
                        <input type="checkbox" name="calificacion1" id="calificacion1-5" value="Distribución/Lógica">
                        <label for="calificacion1-5">Distribución/Lógica</label>
                    </fieldset>
                    <fieldset class="label50">
                        <label for="calificacion2">En tu emprendimiento, ¿cuál/es de las siguientes funciones desempeñás?
                        <br><em><small>Podés seleccionar varias opciones</small></em></label>
                        <br>
                        <input type="checkbox" name="calificacion2" id="calificacion2-1" value="Decisión de la estrategia integral">
                        <label for="calificacion2-1">Decisión de la estrategia integral</label>
                        <input type="checkbox" name="calificacion2" id="calificacion2-2" value="Abastecimiento de insumos">
                        <label for="calificacion2-2">Abastecimiento de insumos</label>
                        <input type="checkbox" name="calificacion2" id="calificacion2-3" value="Ventas/Marketing">
                        <label for="calificacion2-3">Ventas/Marketing</label>
                        <input type="checkbox" name="calificacion2" id="calificacion2-4" value="Distribución/Lógica">
                        <label for="calificacion2-4">Distribución/Lógica</label>
                    </fieldset>
                    <input type="submit" name="submit-calificacion" id="submit-calificacion" value="Calificar">
                </div>
            </div>
            <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
            <script>
            window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')
            </script>
            <script src="js/plugins.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.js"></script>
            <script src="js/imagesloaded.pkgd.min.js"></script>
            <script src="js/masonry.pkgd.min.js"></script>
            <script src="js/main.js"></script>
            <script src="scripts/header-mentorDT.js"></script>
        </body>
    </html>