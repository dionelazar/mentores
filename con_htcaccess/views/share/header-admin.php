<?php 
	require_once __DIR__.'/../../class/class.helpers.php';
	$datosUser = Cookies::getDatosUser();
?>
<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Reporte-Ministerio</title>
        <meta name="description" content="">
        <meta name="author" content="Amedia">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
        <meta name="theme-color" content="#395597">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/icons/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/icons/apple-touch-icon-72-precomposed.png">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="apple-touch-icon-precomposed" href="img/icons/apple-touch-icon-57-precomposed.png">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,900" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/icons.css">
        <link rel="stylesheet" href="css/slick.css">
        <link rel="stylesheet" href="css/chartist.min.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
        <p class="browserupgrade">Estás usando un navegador <strong>desactualizado</strong>. Por favor, <a href="http://browsehappy.com/">actualizá tu navegador</a> para una mejor experiencia.</p>
        <![endif]-->
        <header>
            <span class="burger" data-icon="&#xe020;"></span>
            <!--<span class="icon-cross" data-icon="&#xe039;"></span>-->
            <a href="home.html" class="logo-mobile"><img src="img/logoMinisteriowhite.png" alt="MAR - Mentores argentinos en red"></a>
            <div class="noti-top">
                <!-- CAMPANITA DE NOTIFICACIONES -->
                <a href="javascript:void(0);" data-icon="x">
                    <span class="noti-count">3</span>
                </a>
            </div>
            <div class="user-top">
                <!-- NOMBRE DE USUARIO Y MENÚ DESPLEGABLE -->
                <a href="javascript:void(0);" class="dropdown">
                    <h4><?php echo $datosUser->nombre; ?></h4>
                </a>
                <ul>
                    <li><a href="#" data-icon="&#xe056;">Mi perfil</a></li>
                    <li><a href="javascript:void(0)" onclick="logout()" data-icon="&#xe036;">Cerrar sesión</a></li>
                </ul>
            </div>
        </header>
        <aside>
            <!-- MENÚ LATERAL -->
            <a href="home-mentorDT" class="logo"><img src="img/logoMinisterioCentroW.png" alt="MAR"></a>
            <nav>
                <ul id="asideMenu">
                    <li><a href="gestion-usuarios" data-icon="&#xe056;">Gestión de usuarios</a></li>
                    <li><a href="reportes-admin" data-icon="M">Reportes</a></li>
                </ul>
            </nav>
        </aside>
        <div class="notifications">
            <!-- NOTIFICACIONES QUE SE ABREN -->
            <h4>Notificaciones</h4>
            <ul>
                <li><a href="#">
                    <span class="noti-text">Nombre de la nota. Esta es una notificación leída.</span>
                    <span class="noti-date">07/06/2017, 11:35hs</span>
                </a></li>
                <li class="unread"><a href="#">
                    <span class="noti-text">Nombre de la nota. Esta es una notificación no leída.</span>
                    <span class="noti-date">07/06/2017, 11:35hs</span>
                </a></li>
            </ul>
        </div>