<?php 
    require __DIR__.'/share/header-con.php';
?>
    <div class="content home">
        <!-- TODO EL CONTENIDO (LO QUE NO ES MENÚ NI HEADER) -->
        <div class="wrapper">
            <div class="col100">
                <div class="module search-bar">
                    <!-- BARRA DE BÚSQUEDA -->
                    <div class="inner-cols clearfix">
                        <div class="col100">
                            <form action="#" name="search" id="search">
                                <input type="text" placeholder="Buscar sesiones, mentores, grupos o usuarios">
                                <input type="submit" value="&#xe041;">
                            </form>
                        </div>
                    </div>
                </div>
                <ul class="tabs tabs-2 oneTab clearfix">
                    <li class="current">
                        <a href="#0">
                            <h2>Sesiones</strong></h2>
                        </a>
                    </li>
                </ul>
                <div class="container home">
                    <div class="sesionA show">
                         <div class="col-100 pad10">
                            <div class="col-100 pad10">
                                <div class="feature-panel">
                                      <ul class="breadcrumb sesion3">
                                        <li class="active">
                                          <a class="visibleDesktop" href="#">1ª Sesión</a>
                                          <a class="visibleSmInline" href="#">1ª</a>
                                        </li>
                                        <li class="active">
                                          <a class="visibleDesktop" href="#">2ª Sesión</a>
                                          <a class="visibleSmInline" href="#">2ª</a>
                                        </li>
                                        <li class="active">
                                          <a class="visibleDesktop" href="#">3ª Sesión</a>
                                          <a class="visibleSmInline" href="#">3ª</a>
                                        </li>
                                      </ul>
                                    </div>
                                </div>  
                        <div id="sesiones">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="popupCalificacion" class="absolute-center">
        <div id="overlayCalificacion"></div>
        <div class="popupCalificacion">
            <a href="#" id="closeBtn" class="closePopUp">
                <i class="close-icon" data-icon="9"></i>
            </a>
            <div class="label100">
            <label for="rating">Calificá esta sesión</label>
            </div>
            <fieldset class="rating">
                <input type="radio" id="star5" name="rating" value="5" />
                <label class="full" for="star5" title="Awesome - 5 stars"></label>
                <input type="radio" id="star4half" name="rating" value="4.5" />
                <label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
                <input type="radio" id="star4" name="rating" value="4" />
                <label class="full" for="star4" title="Pretty good - 4 stars"></label>
                <input type="radio" id="star3half" name="rating" value="3.5" />
                <label class="half" for="star3half" title="Meh - 3.5 stars"></label>
                <input type="radio" id="star3" name="rating" value="3" />
                <label class="full" for="star3" title="Meh - 3 stars"></label>
                <input type="radio" id="star2half" name="rating" value="2.5" />
                <label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
                <input type="radio" id="star2" name="rating" value="2" />
                <label class="full" for="star2" title="Kinda bad - 2 stars"></label>
                <input type="radio" id="star1half" name="rating" value="1.5" />
                <label class="half" for="star1half" title="Meh - 1.5 stars"></label>
                <input type="radio" id="star1" name="rating" value="1" />
                <label class="full" for="star1" title="Sucks big time - 1 star"></label>
                <input type="radio" id="starhalf" name="rating" value="0.5" />
                <label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
            </fieldset>
            <fieldset class="label100">
                <label for="calificacion1" id="pregunta1">¿Pregunta #1?
                <br><em><small>Podés seleccionar varias opciones</small></em></label>
                <div id="respuestas1"></div>
            </fieldset>
            <fieldset class="label100">
                <label for="calificacion2" id="pregunta2">¿Pregunta #2?
                <br><em><small>Podés seleccionar varias opciones</small></em></label>
                <div id="respuestas2"></div>
            </fieldset>
            <fieldset class="label100">
                <label for="calificacion2">Describí brevemente como te pareció la sesión</label>
                <textarea class="label100" name="calificacion-texto" id="calificacion-texto"></textarea>
            </fieldset>
            <button class="cameraButton"><i data-icon="O"></i>Sacar una Selfie</button>
            <input type="button" class="closePopUp label100" name="submit-calificacion" id="submit-calificacion" value="Calificar" onclick="calificar()">
        </div>
    </div>
        <!--div id="popupSatisfaccion" class="absolute-center">
            <div id="overlayCalificacion"></div>
            <div class="popupCalificacion popupSatisfaccion">
                <a href="#" id="closeBtn" class="closePopUp">
                    <i class="close-icon" data-icon="9"></i>
                </a>
                <h3>Por favor, te pedimos que respondas las siguientes preguntas sobre el programa de mentoría, utilizando una escala de valoración entre 1 (baja) y 5 (alta).</h3>
                <div class="label100">
                    <label for="rating1">1. Estructura y contenidos de cada reunión.*</label>
                </div>
                <fieldset class="rating">
                    <input type="radio" id="rating1-1" name="rating1" value="5" />
                    <label class="full" for="rating1-1" title="Awesome - 5 stars"></label>
                    <input type="radio" id="rating1-2" name="rating1" value="4 and a half" />
                    <label class="half" for="rating1-2" title="Pretty good - 4.5 stars"></label>
                    <input type="radio" id="rating1-3" name="rating1" value="4" />
                    <label class="full" for="rating1-3" title="Pretty good - 4 stars"></label>
                    <input type="radio" id="rating1-4" name="rating1" value="3 and a half" />
                    <label class="half" for="rating1-4" title="Meh - 3.5 stars"></label>
                    <input type="radio" id="rating1-5" name="rating1" value="3" />
                    <label class="full" for="rating1-5" title="Meh - 3 stars"></label>
                    <input type="radio" id="rating1-6" name="rating1" value="2 and a half" />
                    <label class="half" for="rating1-6" title="Kinda bad - 2.5 stars"></label>
                    <input type="radio" id="rating1-7" name="rating1" value="2" />
                    <label class="full" for="rating1-7" title="Kinda bad - 2 stars"></label>
                    <input type="radio" id="rating1-8" name="rating1" value="1 and a half" />
                    <label class="half" for="rating1-8" title="Meh - 1.5 stars"></label>
                    <input type="radio" id="rating1-9" name="rating1" value="1" />
                    <label class="full" for="rating1-9" title="Sucks big time - 1 star"></label>
                    <input type="radio" id="rating1-10" name="rating1" value="half" />
                    <label class="half" for="rating1-10" title="Sucks big time - 0.5 stars"></label>
                </fieldset>
                <div class="label100">
                    <label for="rating2">2. Utilidad de los temas trabajados.*</label>
                </div><br>
                <fieldset class="rating">
                    <input type="radio" id="rating2-1" name="rating2" value="5" />
                    <label class="full" for="rating2-1" title="Awesome - 5 stars"></label>
                    <input type="radio" id="rating2-2" name="rating2" value="4 and a half" />
                    <label class="half" for="rating2-2" title="Pretty good - 4.5 stars"></label>
                    <input type="radio" id="rating2-3" name="rating2" value="4" />
                    <label class="full" for="rating2-3" title="Pretty good - 4 stars"></label>
                    <input type="radio" id="rating2-4" name="rating2" value="3 and a half" />
                    <label class="half" for="rating2-4" title="Meh - 3.5 stars"></label>
                    <input type="radio" id="rating2-5" name="rating2" value="3" />
                    <label class="full" for="rating2-5" title="Meh - 3 stars"></label>
                    <input type="radio" id="rating2-6" name="rating2" value="2 and a half" />
                    <label class="half" for="rating2-6" title="Kinda bad - 2.5 stars"></label>
                    <input type="radio" id="rating2-7" name="rating2" value="2" />
                    <label class="full" for="rating2-7" title="Kinda bad - 2 stars"></label>
                    <input type="radio" id="rating2-8" name="rating2" value="1 and a half" />
                    <label class="half" for="rating2-8" title="Meh - 1.5 stars"></label>
                    <input type="radio" id="rating2-9" name="rating2" value="1" />
                    <label class="full" for="rating2-9" title="Sucks big time - 1 star"></label>
                    <input type="radio" id="rating2-10" name="rating2" value="half" />
                    <label class="half" for="rating2-10" title="Sucks big time - 0.5 stars"></label>
                </fieldset><br>
                <div class="label100">
                    <label for="rating3">3. Cumplimiento de los objetivos del plan de trabajo.*</label>
                </div><br>
                <fieldset class="rating">
                    <input type="radio" id="rating3-1" name="rating3" value="5" />
                    <label class="full" for="rating3-1" title="Awesome - 5 stars"></label>
                    <input type="radio" id="rating3-2" name="rating3" value="4 and a half" />
                    <label class="half" for="rating3-2" title="Pretty good - 4.5 stars"></label>
                    <input type="radio" id="rating3-3" name="rating3" value="4" />
                    <label class="full" for="rating3-3" title="Pretty good - 4 stars"></label>
                    <input type="radio" id="rating3-4" name="rating3" value="3 and a half" />
                    <label class="half" for="rating3-4" title="Meh - 3.5 stars"></label>
                    <input type="radio" id="rating3-5" name="rating3" value="3" />
                    <label class="full" for="rating3-5" title="Meh - 3 stars"></label>
                    <input type="radio" id="rating3-6" name="rating3" value="2 and a half" />
                    <label class="half" for="rating3-6" title="Kinda bad - 2.5 stars"></label>
                    <input type="radio" id="rating3-7" name="rating3" value="2" />
                    <label class="full" for="rating3-7" title="Kinda bad - 2 stars"></label>
                    <input type="radio" id="rating3-8" name="rating3" value="1 and a half" />
                    <label class="half" for="rating3-8" title="Meh - 1.5 stars"></label>
                    <input type="radio" id="rating3-9" name="rating3" value="1" />
                    <label class="full" for="rating3-9" title="Sucks big time - 1 star"></label>
                    <input type="radio" id="rating3-10" name="rating3" value="half" />
                    <label class="half" for="rating3-10" title="Sucks big time - 0.5 stars"></label>
                </fieldset><br>
                <div class="label100">
                    <label for="rating4">4. Valor de la interacción con otros emprendedores.*</label>
                </div><br>
                <fieldset class="rating">
                    <input type="radio" id="rating4-1" name="rating4" value="5" />
                    <label class="full" for="rating4-1" title="Awesome - 5 stars"></label>
                    <input type="radio" id="rating4-2" name="rating4" value="4 and a half" />
                    <label class="half" for="rating4-2" title="Pretty good - 4.5 stars"></label>
                    <input type="radio" id="rating4-3" name="rating4" value="4" />
                    <label class="full" for="rating4-3" title="Pretty good - 4 stars"></label>
                    <input type="radio" id="rating4-4" name="rating4" value="3 and a half" />
                    <label class="half" for="rating4-4" title="Meh - 3.5 stars"></label>
                    <input type="radio" id="rating4-5" name="rating4" value="3" />
                    <label class="full" for="rating4-5" title="Meh - 3 stars"></label>
                    <input type="radio" id="rating4-6" name="rating4" value="2 and a half" />
                    <label class="half" for="rating4-6" title="Kinda bad - 2.5 stars"></label>
                    <input type="radio" id="rating4-7" name="rating4" value="2" />
                    <label class="full" for="rating4-7" title="Kinda bad - 2 stars"></label>
                    <input type="radio" id="rating4-8" name="rating4" value="1 and a half" />
                    <label class="half" for="rating4-8" title="Meh - 1.5 stars"></label>
                    <input type="radio" id="rating4-9" name="rating4" value="1" />
                    <label class="full" for="rating4-9" title="Sucks big time - 1 star"></label>
                    <input type="radio" id="rating4-10" name="rating4" value="half" />
                    <label class="half" for="rating4-10" title="Sucks big time - 0.5 stars"></label>
                </fieldset><br>
                <div class="label100">
                    <label for="rating5">5. Desempeño del mentor.*</label>
                </div><br>
                <fieldset class="rating">
                    <input type="radio" id="rating5-1" name="rating5" value="5" />
                    <label class="full" for="rating5-1" title="Awesome - 5 stars"></label>
                    <input type="radio" id="rating5-2" name="rating5" value="4 and a half" />
                    <label class="half" for="rating5-2" title="Pretty good - 4.5 stars"></label>
                    <input type="radio" id="rating5-3" name="rating5" value="4" />
                    <label class="full" for="rating5-3" title="Pretty good - 4 stars"></label>
                    <input type="radio" id="rating5-4" name="rating5" value="3 and a half" />
                    <label class="half" for="rating5-4" title="Meh - 3.5 stars"></label>
                    <input type="radio" id="rating5-5" name="rating5" value="3" />
                    <label class="full" for="rating5-5" title="Meh - 3 stars"></label>
                    <input type="radio" id="rating5-6" name="rating5" value="2 and a half" />
                    <label class="half" for="rating5-6" title="Kinda bad - 2.5 stars"></label>
                    <input type="radio" id="rating5-7" name="rating5" value="2" />
                    <label class="full" for="rating5-7" title="Kinda bad - 2 stars"></label>
                    <input type="radio" id="rating5-8" name="rating5" value="1 and a half" />
                    <label class="half" for="rating5-8" title="Meh - 1.5 stars"></label>
                    <input type="radio" id="rating5-9" name="rating5" value="1" />
                    <label class="full" for="rating5-9" title="Sucks big time - 1 star"></label>
                    <input type="radio" id="rating5-10" name="rating5" value="half" />
                    <label class="half" for="rating5-10" title="Sucks big time - 0.5 stars"></label>
                </fieldset><br>
                <div class="label100">
                    <label for="rating6">6. Material de apoyo.*</label>
                </div><br>
                <fieldset class="rating">
                    <input type="radio" id="rating6-1" name="rating6" value="5" />
                    <label class="full" for="rating6-1" title="Awesome - 5 stars"></label>
                    <input type="radio" id="rating6-2" name="rating6" value="4 and a half" />
                    <label class="half" for="rating6-2" title="Pretty good - 4.5 stars"></label>
                    <input type="radio" id="rating6-3" name="ratirating6ng1" value="4" />
                    <label class="full" for="rating6-3" title="Pretty good - 4 stars"></label>
                    <input type="radio" id="rating6-4" name="rating6" value="3 and a half" />
                    <label class="half" for="rating6-4" title="Meh - 3.5 stars"></label>
                    <input type="radio" id="rating6-5" name="rating6" value="3" />
                    <label class="full" for="rating6-5" title="Meh - 3 stars"></label>
                    <input type="radio" id="rating6-6" name="rating6" value="2 and a half" />
                    <label class="half" for="rating6-6" title="Kinda bad - 2.5 stars"></label>
                    <input type="radio" id="rating6-7" name="rating6" value="2" />
                    <label class="full" for="rating6-7" title="Kinda bad - 2 stars"></label>
                    <input type="radio" id="rating6-8" name="rating6" value="1 and a half" />
                    <label class="half" for="rating6-8" title="Meh - 1.5 stars"></label>
                    <input type="radio" id="rating6-9" name="rating6" value="1" />
                    <label class="full" for="rating6-9" title="Sucks big time - 1 star"></label>
                    <input type="radio" id="rating6-10" name="rating6" value="half" />
                    <label class="half" for="rating6-10" title="Sucks big time - 0.5 stars"></label>
                </fieldset><br>
                <div class="calificacion">
                <div class="label100">
                    <label for="radio1">7. ¿Cuál es tu valoración general del programa?</label>
                </div><br>
                    <fieldset class="labelWCheck">
                        <input type="radio" name="puntaje-valoracion" id="puntaje-valoracion-1" value="1">
                        <label for="puntaje-valoracion-1">1</label>
                        <input type="radio" name="puntaje-valoracion" id="puntaje-valoracion-2" value="2">
                        <label for="puntaje-valoracion-2">2</label>
                        <input type="radio" name="puntaje-valoracion" id="puntaje-valoracion-3" value="3">
                        <label for="puntaje-valoracion-3">3</label>
                        <input type="radio" name="puntaje-valoracion" id="puntaje-valoracion-4" value="4">
                        <label for="puntaje-valoracion-4">4</label>
                        <input type="radio" name="puntaje-valoracion" id="puntaje-valoracion-5" value="5">
                        <label for="puntaje-valoracion-5">5</label>
                        <input type="radio" name="puntaje-valoracion" id="puntaje-valoracion-6" value="1">
                        <label for="puntaje-valoracion-6">6</label>
                        <input type="radio" name="puntaje-valoracion" id="puntaje-valoracion-7" value="2">
                        <label for="puntaje-valoracion-7">7</label>
                        <input type="radio" name="puntaje-valoracion" id="puntaje-valoracion-8" value="3">
                        <label for="puntaje-valoracion-8">8</label>
                        <input type="radio" name="puntaje-valoracion" id="puntaje-valoracion-9" value="4">
                        <label for="puntaje-valoracion-9">9</label>
                        <input type="radio" name="puntaje-valoracion" id="puntaje-valoracion-10" value="5">
                        <label for="puntaje-valoracion-10">10</label>
                    </fieldset>
                </div>
                <fieldset class="label100">
                    <label for="calificacion3">8. Por favor, ¿podrías darnos más detalles sobre tu valoración del programa?*</label>
                    <textarea class="label100" name="calificacion3" id="calificacion3"></textarea>
                </fieldset>
                 <fieldset class="label100">
                    <label for="calificacion4">9. ¿Tenés algún comentario, sugerencia o aspecto de mejora para futuras ediciones?*</label>
                    <textarea class="label100" name="calificacion4" id="calificacion4"></textarea>
                </fieldset>
                <input type="submit" class="closePopUp label100" name="submit-calificacion" id="submit-calificacion" value="Calificar">
            </div>
        </div-->
    </div>
    <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/masonry.pkgd.min.js"></script>
    <script src="js/main.js"></script>
    <script src="scripts/header.js"></script>
    <script src="scripts/home-con.js"></script>
</body>

</html>