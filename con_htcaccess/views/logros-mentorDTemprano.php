<?php require __DIR__.'/share/header-mentorDT.php' ?>
        <!-- TODO EL CONTENIDO (LO QUE NO ES MENÚ NI HEADER) -->
    <div class="content logros">
        <div class="wrapper">
            <div class="col100">
                <ul class="tabs tabs-2 oneTab clearfix">
                    <li class="current">
                        <a href="#0">
                            <h2>Logros</h2>
                        </a>
                    </li>
                </ul>
                <div class="container logros">
                    <div class="module library logros">
                        <table>
                            <tbody>
                                <tr>
                                    <td class="tdLogros">
                                        <img src="img/award1.png" alt="">
                                        <h2>Logro 1</h2>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores eius non minus possimus, at modi dignissimos, nam, consequuntur quod dolorum earum voluptatibus. Error sapiente vitae quasi quis! Cupiditate, suscipit, aut.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores eius non minus possimus, at modi dignissimos, nam, consequuntur quod dolorum earum voluptatibus. Error sapiente vitae quasi quis! Cupiditate, suscipit, aut.</p>
                                        <div class="progress-bar">
                                            <div class="progress-completed" style="width:100%;"></div><span>100%</span></div>
                                    </td>
                                    <td class="tdLogros">
                                        <img src="img/award2.png" alt="">
                                        <h2>Logro 2</h2>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores eius non minus possimus, at modi dignissimos, nam, consequuntur quod dolorum earum voluptatibus. Error sapiente vitae quasi quis! Cupiditate, suscipit, aut.</p>
                                        <div class="progress-bar">
                                            <div class="progress-completed" style="width:100%;"></div><span>100%</span></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdLogros locked">
                                        <img src="img/awardLock.png" alt="">
                                        <h2>Logro 3</h2>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores eius non minus possimus, at modi dignissimos, nam, consequuntur quod dolorum earum voluptatibus. Error sapiente vitae quasi quis! Cupiditate, suscipit, aut.</p>
                                        <div class="progress-bar">
                                            <div class="progress-completed" style="width:75%;"></div><span>75%</span></div>
                                    </td>
                                    <td class="tdLogros locked">
                                        <img src="img/awardLock.png" alt="">
                                        <h2>Logro 4</h2>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores eius non minus possimus, at modi dignissimos, nam, consequuntur quod dolorum earum voluptatibus. Error sapiente vitae quasi quis! Cupiditate, suscipit, aut.</p>
                                        <div class="progress-bar">
                                            <div class="progress-completed" style="width:50%;"></div><span>50%</span></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdLogros locked">
                                        <img src="img/awardLock.png" alt="">
                                        <h2>Logro 5</h2>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores eius non minus possimus, at modi dignissimos, nam, consequuntur quod dolorum earum voluptatibus. Error sapiente vitae quasi quis! Cupiditate, suscipit, aut.</p>
                                        <div class="progress-bar">
                                            <div class="progress-completed" style="width:40%;"></div><span>40%</span></div>
                                    </td>
                                    <td class="tdLogros locked">
                                        <img src="img/awardLock.png" alt="">
                                        <h2>Logro 6</h2>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores eius non minus possimus, at modi dignissimos, nam, consequuntur quod dolorum earum voluptatibus. Error sapiente vitae quasi quis! Cupiditate, suscipit, aut.</p>
                                        <div class="progress-bar">
                                            <div class="progress-completed" style="width:5%;"></div><span>5%</span></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdLogros locked">
                                        <img src="img/awardLock.png" alt="">
                                        <h2>Logro 7</h2>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores eius non minus possimus, at modi dignissimos, nam, consequuntur quod dolorum earum voluptatibus. Error sapiente vitae quasi quis! Cupiditate, suscipit, aut.</p>
                                        <div class="progress-bar">
                                            <div class="progress-completed" style="width:20%;"></div><span>20%</span></div>
                                    </td>
                                    <td class="tdLogros locked">
                                        <img src="img/awardLock.png" alt="">
                                        <h2>Logro 8</h2>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores eius non minus possimus, at modi dignissimos, nam, consequuntur quod dolorum earum voluptatibus. Error sapiente vitae quasi quis! Cupiditate, suscipit, aut.</p>
                                        <div class="progress-bar">
                                            <div class="progress-completed" style="width:20%;"></div>
                                            <span>20%</span>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--<div class="table">
							<div class="col50 trophy trophy1">
								<div class="achievementUnlocked">
									<img src="img/award1.png" alt="">
								</div>
								<h2>Logro 1</h2>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores eius non minus possimus, at modi dignissimos, nam, consequuntur quod dolorum earum voluptatibus. Error sapiente vitae quasi quis! Cupiditate, suscipit, aut.</p>
							</div>
							<div class="col50 trophy trophy2">
								<div class="achievementUnlocked">
									<img src="img/award1.png" alt="">
								</div>
								<h2>Logro 2</h2>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores eius non minus possimus, at modi dignissimos, nam, consequuntur quod dolorum earum voluptatibus. Error sapiente vitae quasi quis! Cupiditate, suscipit, aut.</p>
							</div>
							<div class="overlayAward">
							<div class="col50 trophy trophy3">
								<div class="achievementLocked">
									<img src="img/awardLock.png" alt="">
								</div>
							</div>
							</div>
							<div class="col50 trophy trophy4">
								<div class="achievementLocked">

									<img src="img/awardLock.png" alt="">
								</div>
							</div>
							<div class="col50 trophy trophy5">
								<div class="achievementLocked">
									<img src="img/awardLock.png" alt="">
								</div>
							</div>
							<div class="col50 trophy trophy6">
								<div class="achievementLocked">
									<img src="img/awardLock.png" alt="">
								</div>
							</div>
		            	</div>-->
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script>
    window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')
    </script>
    <script src="js/plugins.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/masonry.pkgd.min.js"></script>
    <script src="js/main.js"></script>
    <script src="scripts/header-mentorDT.js"></script>
</body>

</html>