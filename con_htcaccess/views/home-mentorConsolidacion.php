<?php 
    require __DIR__.'/share/header-mentor-con.php';
?>
    <div class="content home">
        <!-- TODO EL CONTENIDO (LO QUE NO ES MENÚ NI HEADER) -->
        <div class="wrapper">
            <div class="col100">
                <div class="module search-bar">
                    <!-- BARRA DE BÚSQUEDA -->
                    <div class="inner-cols clearfix">
                        <div class="col100">
                            <form action="#" name="search" id="search">
                                <input type="text" placeholder="Buscar sesiones, mentores, grupos o usuarios">
                                <input type="submit" value="&#xe041;">
                            </form>
                        </div>
                    </div>
                </div>
                <ul class="tabs tabs-2 oneTab clearfix">
                    <li class="current">
                        <a href="#0">
                            <h2>Grupo <strong></strong></h2>
                        </a>
                    </li>
                </ul>
                <div class="container home">
                    <div class="sesionA show">
                        <div class="col-100 pad10">
                            <div class="feature-panel">
                                <ul class="breadcrumb sesiones9">
                                    <li class="active">
                                      <a class="visibleDesktop" href="#">1ª Sesión</a>
                                      <a class="visibleSmInline" href="#">1ª</a>
                                    </li>
                                    <li class="active">
                                      <a class="visibleDesktop" href="#">2ª Sesión</a>
                                      <a class="visibleSmInline" href="#">2ª</a>
                                    </li>
                                    <li class="active">
                                      <a class="visibleDesktop" href="#">3ª Sesión</a>
                                      <a class="visibleSmInline" href="#">3ª</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div id="sesionesGrupo1">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div id="popupCalificacion" class="absolute-center">
    <div id="overlayCalificacion"></div>
    <div class="popupCalificacion">
        <a href="#" id="closeBtn" class="closePopUp">
            <i class="close-icon" data-icon="9"></i>
        </a>
        <div class="label100">
            <div class="titleForm">Marcar Asistencia</div>
            <fieldset class="label100">
                <div id="calificarMarcarAsistencia"></div>
            </fieldset>
        </div>
        <div class="label100">
            <label for="rating">Calificá esta sesión</label>
        </div>
        <fieldset class="rating">
            <input type="radio" id="star5" name="rating" value="5" />
            <label class="full" for="star5" title="Awesome - 5 stars"></label>
            <input type="radio" id="star4half" name="rating" value="4.5" />
            <label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
            <input type="radio" id="star4" name="rating" value="4" />
            <label class="full" for="star4" title="Pretty good - 4 stars"></label>
            <input type="radio" id="star3half" name="rating" value="3.5" />
            <label class="half" for="star3half" title="Meh - 3.5 stars"></label>
            <input type="radio" id="star3" name="rating" value="3" />
            <label class="full" for="star3" title="Meh - 3 stars"></label>
            <input type="radio" id="star2half" name="rating" value="2.5" />
            <label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
            <input type="radio" id="star2" name="rating" value="2" />
            <label class="full" for="star2" title="Kinda bad - 2 stars"></label>
            <input type="radio" id="star1half" name="rating" value="1.5" />
            <label class="half" for="star1half" title="Meh - 1.5 stars"></label>
            <input type="radio" id="star1" name="rating" value="1" />
            <label class="full" for="star1" title="Sucks big time - 1 star"></label>
            <input type="radio" id="starhalf" name="rating" value="0.5" />
            <label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
        </fieldset>
        <fieldset class="label100">
            <label for="calificacion1"><div id="pregunta1"></div>
            <br><em><small>Podés seleccionar varias opciones</small></em></label>
            <div id="respuestas1"></div>

        </fieldset>
        <fieldset class="label100">
            <label for="calificacion2"><div id="pregunta2"></div>
            <br><em><small>Podés seleccionar varias opciones</small></em></label>
            <div id="respuestas2"></div>

        </fieldset>
        <fieldset class="label100">
            <label for="calificacion2">Describí brevemente como te pareció la sesión</label>
            <textarea class="label100" name="calificacion-texto" id="calificacion-texto"></textarea>
        </fieldset>
        <button class="cameraButton"><i data-icon="O"></i>Sacar una Selfie</button>
        <input type="submit" class="closePopUp label100" name="submit-calificacion" id="submit-calificacion" value="Calificar" onclick="calificar()">
    </div>
</div>

    <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/masonry.pkgd.min.js"></script>
    <script src="js/select2.js"></script>
    <script src="js/featherlight.js"></script>
    <script src="js/main.js"></script>
    <script src="scripts/header.js"></script>
    <script src="scripts/home-mentor-con.js"></script>
</body>

</html>