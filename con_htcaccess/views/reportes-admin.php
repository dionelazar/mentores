<?php 
    require __DIR__.'/share/header-admin.php';
?>
        <!-- CHART -->
        <div class="content reporte ministerio">
            <!-- TODO EL CONTENIDO (LO QUE NO ES MENÚ NI HEADER) -->
            <div class="wrapper">
                <div class="col100">
                    <ul class="tabs tabs-2 oneTab clearfix">
                        <li id="listChart1" class="current"><a>Indicadores Clave</a></li>
                    </ul>
                    <div class="listChart1 show">
                        <div class="scroll-wrap">
                            <section id="estadisticas" class="clearfix">
                                <div class="col100">
                                    <section id="filtros" class="clearfix">
                                        
                                        <!--<a class="filtrarReporte"> <i data-icon="&#xe040;"></i></a>-->
                                        <a href="javascript:void(0);" data-icon="&#xe052;" data-slidebasic="filtros-opciones" class="ocultar-estadisticas mostrar-filtros"><span>Ocultar</span><span style="display:none;">Mostrar</span> filtros</a>
                                        <div class="clearfix"></div>
                                        <div class="col100">
                                            <ul id="filtros-opciones" >
                                                <li>
                                                    <select name="filtrosPais" id="filtrosPais" class="selectize">
                                                        
                                                    </select>
                                                </li>
                                                <li>
                                                    <select name="filtrosProvincia" id="filtrosProvincia" class="selectize">
                                                        <option value="">Filtrar por provincia</option>
                                                    </select>
                                                </li>
                                                <li>
                                                    <select name="filtrosCiudad" id="filtrosCiudad" class="selectize">
                                                        <option value="">Filtrar por ciudad</option>
                                                    </select>
                                                </li>
                                                <!--li>
                                                    <select name="filtrosMentoria" id="filtrosMentoria" class="selectize">
                                                        <option value="">Filtrar por tipo de mentoria</option>
                                                        <option value="2">Desarrollo Temprano</option>
                                                        <option value="3">Consolidación</option>
                                                    </select>
                                                </li-->
                                                <li>
                                                    <button class="filtros-reset pull-right" data-icon="9" onclick="getGrupos()">Resetear filtros</button>
                                                </li>
                                            </ul>
                                        </div>
                                    </section>
                                    <ul class="estadisticas estadisticas-big clearfix carrousel">
                                        <li>
                                            <a class="panel panel-verde mentoresFormacion" href="#" data-icon="&#xe056;" onclick="getReporteMentoresEnFormacion()">
                                                <strong id="num_mentores_formacion"></strong>
                                                <span class="desc">Mentores en formación</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="panel panel-verde mentoresFormados" href="#popupMentores"  data-icon="&#xe056;" onclick="getReporteMentoresFormados()">
                                                <strong id="num_mentores_formados"></strong>
                                                <span class="desc">Mentores formados</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="panel panel-verde mentoriasFuncionando" href="#popupMentorias"  data-icon="," onclick="getMentoriasActivas()">
                                                <strong id="num_mentorias_activas"></strong>
                                                <span class="desc">Procesos de mentoría en funcionamiento</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="panel panel-rojo emprEnMentoria" href="#"  data-icon="&#xe056;">
                                                <i class="icon-thumb-up"></i>
                                                <strong id="num_emprendedores_en_proceso"></strong>
                                                <span class="desc">Emprendedores en proceso de mentoría</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="panel panel-rojo emprEgresados1" href="#popupEmprendedoresSesion1"  data-icon="&#xe056;" onclick="getEmprendedoresEgresados()">
                                                <strong id="num_emprendedores_egresados_1"></strong>
                                                <span class="desc">Emprendedores egresados<br>-Fase 1-</span>
                                            </a>
                                        </li>
                                        <!--li>
                                            <a class="panel panel-rojo emprEgresados2" href="#popupEmprendedoresSesion2"  data-icon="&#xe056;">
                                                <strong id="num_emprendedores_egresados_2"></strong>
                                                <span class="desc">Emprendedores egresados<br>-Fase 2-</span>
                                            </a>
                                        </li-->
                                        <li>
                                            <a class="panel alcanceTerritorial" href="#popupTerritoriales"  data-icon="^">
                                                <strong id="num_alcance_territorial"></strong>
                                                <span class="desc">Alcance territorial</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="panel atencionParticipante" href="#"  data-icon="^">
                                                <strong id="num_atencion_por_participante"></strong>
                                                <span class="desc">Proceso de atención por participante ?</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="panel emprSatisfaccion"  href="#" data-icon="&#xe001;">
                                                <strong id="num_impacto_grado_satisfaccion"></strong>
                                                <span class="desc">Impacto-grado de satisfacción del emprendedor</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="panel representacionGenero" href="#popupGenero"  data-icon="&#xe056;" onclick="getReporteGenero()">
                                                <strong id="num_genero"></strong>
                                                <span class="desc">Representación de género</span>
                                            </a>
                                        </li>
                                        <!--li>
                                            <a class="panel diversidadProductiva" href="#"  data-icon="&#xe046;">
                                                <strong id="num_diversidad_productiva"></strong>
                                                <span class="desc">Diversidad productiva</span>
                                            </a>
                                        </li-->
                                    </ul>
                                    <div class="referencias">
                                        <a class="left refes" href="referencias"><i data-icon="&#xe035;"></i>Referencias</a>
                                        <ul class="referencias">
                                            <li class="ref-verde">Mentorias</li>
                                            <li class="ref-azul">General</li>
                                            <li class="ref-rojo">Emprendedores</li>
                                        </ul>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                    <!--
                    <div class="charts">
                        <h3>Mentores</h3>
                        <div class="ct-chart ct-minor-seventh" id="chart1"></div>
                        <ul>
                            <li><strong>Cantidad de mentores en formación: 56</strong><br><small>número de mentores participando del espacio de formación previo al inicio de la mentoría.</small> </li>
                            <li><strong>Cantidad de mentores formados: 15</strong><br><small> número de mentores que completaron el espacio de formación previo al inicio de la mentoría.</small> </li>
                        </ul>
                        <div class="ct-slice-pie ct-minor-seventh" id="chart2"></div>
                    </div> -->
                    
                    <ul class="tabs tabs-2 clearfix">
                        <li id="homeTab1" class="current"><a href="#0">Desarrollo Temprano</a></li>
                        <li id="homeTab2"><a href="#0">Consolidación</a></li>
                    </ul>
                    <div class="module search-bar">
                        <!-- BARRA DE BÚSQUEDA -->
                        <div class="inner-cols clearfix">
                            <div class="col100">
                                <form action="#" name="search" id="search">
                                    <input type="text" placeholder="Buscar nombres, mentores, grupos o usuarios">
                                    <input type="submit" value="&#xe041;">
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class=" reporte ministerio">
                        <div class="nameA sesionA show" id="desarrolloTempranoGrupos">
                            
                        </div>
                        <div class="nameB sesionB" id="consolidacionGrupos">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="popupCalificacion" class="absolute-center">
            <div id="overlayCalificacion"></div>
            <div class="popupCalificacion">
                <a href="#" id="closeBtn">
                    <i class="close-icon" data-icon="9"></i>
                </a>
                <label for="rating">Calificá esta sesión</label>
                <fieldset class="rating">
                    <input type="radio" id="star5" name="rating" value="5" />
                    <label class="full" for="star5" title="Awesome - 5 stars"></label>
                    <input type="radio" id="star4half" name="rating" value="4 and a half" />
                    <label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
                    <input type="radio" id="star4" name="rating" value="4" />
                    <label class="full" for="star4" title="Pretty good - 4 stars"></label>
                    <input type="radio" id="star3half" name="rating" value="3 and a half" />
                    <label class="half" for="star3half" title="Meh - 3.5 stars"></label>
                    <input type="radio" id="star3" name="rating" value="3" />
                    <label class="full" for="star3" title="Meh - 3 stars"></label>
                    <input type="radio" id="star2half" name="rating" value="2 and a half" />
                    <label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
                    <input type="radio" id="star2" name="rating" value="2" />
                    <label class="full" for="star2" title="Kinda bad - 2 stars"></label>
                    <input type="radio" id="star1half" name="rating" value="1 and a half" />
                    <label class="half" for="star1half" title="Meh - 1.5 stars"></label>
                    <input type="radio" id="star1" name="rating" value="1" />
                    <label class="full" for="star1" title="Sucks big time - 1 star"></label>
                    <input type="radio" id="starhalf" name="rating" value="half" />
                    <label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
                </fieldset>
                <fieldset class="label50">
                    <label for="calificacion1">En tu emprendimiento, ¿cuál/es de las siguientes funciones desempeñás?
                    <br><em><small>Podés seleccionar varias opciones</small></em></label>
                    <br>
                    <input type="checkbox" name="calificacion1" id="calificacion1-1" value="Decisión de la estrategia integral">
                    <label for="calificacion1-1">Decisión de la estrategia integral</label>
                    <input type="checkbox" name="calificacion1" id="calificacion1-2" value="Abastecimiento de insumos">
                    <label for="calificacion1-2">Abastecimiento de insumos</label>
                    <input type="checkbox" name="calificacion1" id="calificacion1-4" value="Ventas/Marketing">
                    <label for="calificacion1-4">Ventas/Marketing</label>
                    <input type="checkbox" name="calificacion1" id="calificacion1-5" value="Distribución/Lógica">
                    <label for="calificacion1-5">Distribución/Lógica</label>
                </fieldset>
                <fieldset class="label50">
                    <label for="calificacion2">En tu emprendimiento, ¿cuál/es de las siguientes funciones desempeñás?
                    <br><em><small>Podés seleccionar varias opciones</small></em></label>
                    <br>
                    <input type="checkbox" name="calificacion2" id="calificacion2-1" value="Decisión de la estrategia integral">
                    <label for="calificacion2-1">Decisión de la estrategia integral</label>
                    <input type="checkbox" name="calificacion2" id="calificacion2-2" value="Abastecimiento de insumos">
                    <label for="calificacion2-2">Abastecimiento de insumos</label>
                    <input type="checkbox" name="calificacion2" id="calificacion2-3" value="Ventas/Marketing">
                    <label for="calificacion2-3">Ventas/Marketing</label>
                    <input type="checkbox" name="calificacion2" id="calificacion2-4" value="Distribución/Lógica">
                    <label for="calificacion2-4">Distribución/Lógica</label>
                </fieldset>
                <input type="submit" name="submit-calificacion" id="submit-calificacion" value="Calificar">
            </div>
        </div>
        <div id="popupEgresados" class="absolute-center" style="display: none;">
            <div id="overlayCalificacion"></div>
            <div class="popupReportes">
                <a href="#" id="closeBtn" class="closePopUp">
                    <i class="close-icon" data-icon="9"></i>
                </a>
                <a href="#" id="volverBtn" class="closePopUp">
                    <i class="volver-icon" data-icon="l"></i>
                </a>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <img src="img/analytics.png" alt="">
                            <h2>Reporte Emprendedores Egresados <br> - Fase 1 -</h2>
                        </div>
                        <!--div class="col-xs-6">
                            <h3><strong>??</strong></h3>
                        </div>
                        <div class="col-xs-6 borderLeft">
                            <h3><strong>??%</strong></h3>
                        </div>
                        <div class="col-xs-12">
                            <p>Emprendedores que asistieron a todas las sesiones de la <strong>Fase 1</strong></p>
                        </div-->
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#egresadosDT" aria-controls="egresadosDT" role="tab" data-toggle="tab">Desarrollo Temprano</a></li>
                                <li role="presentation"><a href="#egresadosCon" aria-controls="egresadosCon" role="tab" data-toggle="tab">Consolidación</a></li>
                            </ul>
                        </div>
                        <div class="col-xs-12">
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="egresadosDT">
                                    <p><strong id="cant_emprendedores_egresados_dt"></strong></p><span></span>
                                    <div class="module library reportes">
                                        <table>
                                            <thead>
                                                <tr>
                                                    <th>Nombre</th>
                                                    <th>Sede</th>
                                                    <th>E-mail</th>
                                                </tr>
                                            </thead>
                                            <tbody id="table_egresados_dt">
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade in" id="egresadosCon"><p><strong id="cant_emprendedores_egresados_con"></strong></p><span></span>
                                <div class="module library reportes">
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Sede</th>
                                                <th>E-mail</th>
                                            </tr>
                                        </thead>
                                        <tbody id="table_egresados_con">
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="popupEgresados2" class="absolute-center" style="display: none;">
        <div id="overlayCalificacion"></div>
        <div class="popupReportes">
            <a href="#" id="closeBtn" class="closePopUp">
                <i class="close-icon" data-icon="9"></i>
            </a>
            <a href="#" id="volverBtn" class="closePopUp">
                <i class="volver-icon" data-icon="l"></i>
            </a>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <img src="img/analytics.png" alt="">
                        <h2>Reporte Emprendedores Egresados <br> - Fase 2 -</h2>
                    </div>
                    <div class="col-xs-6">
                        <h3><strong>??</strong></h3>
                    </div>
                    <div class="col-xs-6 borderLeft">
                        <h3><strong>??%</strong></h3>
                    </div>
                    <div class="col-xs-12">
                        <p>Emprendedores que asistieron a todas las sesiones de la <strong>Fase 2</strong></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#egresadosDT2" aria-controls="egresadosDT2" role="tab" data-toggle="tab">Desarrollo Temprano</a></li>
                            <li role="presentation"><a href="#egresadosCon2" aria-controls="egresadosCon2" role="tab" data-toggle="tab">Consolidación</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-12">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="egresadosDT2">
                                <p><strong>70</strong></p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem aliquid veritatis.</span>
                                <div class="module library reportes">
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Sede</th>
                                                <th>E-mail</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td data-label="Nombre: "><a href="leccion.html">Nombre y Apellido</a></td>
                                                <td data-label="Sede: ">Sede</td>
                                                <td data-label="E-mail: " style="white-space: nowrap;">Email@gmail.com</td>
                                            </tr>
                                            <tr>
                                                <td data-label="Nombre: "><a href="leccion.html">Nombre y Apellido</a></td>
                                                <td data-label="Sede: ">Sede</td>
                                                <td data-label="E-mail: " style="white-space: nowrap;">Email@gmail.com</td>
                                            </tr>
                                            <tr>
                                                <td data-label="Nombre: "><a href="leccion.html">Nombre y Apellido</a></td>
                                                <td data-label="Sede: ">Sede</td>
                                                <td data-label="E-mail: " style="white-space: nowrap;">Email@gmail.com</td>
                                            </tr>
                                            <tr>
                                                <td data-label="Nombre: "><a href="leccion.html">Nombre y Apellido</a></td>
                                                <td data-label="Sede: ">Sede</td>
                                                <td data-label="E-mail: " style="white-space: nowrap;">Email@gmail.com</td>
                                            </tr>
                                            <tr>
                                                <td data-label="Nombre: "><a href="leccion.html">Nombre y Apellido</a></td>
                                                <td data-label="Sede: ">Sede</td>
                                                <td data-label="E-mail: " style="white-space: nowrap;">Email@gmail.com</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade in" id="egresadosCon2"><p><strong>80</strong></p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem aliquid veritatis.</span>
                            <div class="module library reportes">
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Nombre</th>
                                            <th>Sede</th>
                                            <th>E-mail</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td data-label="Nombre: "><a href="leccion.html">Nombre y Apellido</a></td>
                                            <td data-label="Sede: ">Sede</td>
                                            <td data-label="E-mail: " style="white-space: nowrap;">Email@gmail.com</td>
                                        </tr>
                                        <tr>
                                            <td data-label="Nombre: "><a href="leccion.html">Nombre y Apellido</a></td>
                                            <td data-label="Sede: ">Sede</td>
                                            <td data-label="E-mail: " style="white-space: nowrap;">Email@gmail.com</td>
                                        </tr>
                                        <tr>
                                            <td data-label="Nombre: "><a href="leccion.html">Nombre y Apellido</a></td>
                                            <td data-label="Sede: ">Sede</td>
                                            <td data-label="E-mail: " style="white-space: nowrap;">Email@gmail.com</td>
                                        </tr>
                                        <tr>
                                            <td data-label="Nombre: "><a href="leccion.html">Nombre y Apellido</a></td>
                                            <td data-label="Sede: ">Sede</td>
                                            <td data-label="E-mail: " style="white-space: nowrap;">Email@gmail.com</td>
                                        </tr>
                                        <tr>
                                            <td data-label="Nombre: "><a href="leccion.html">Nombre y Apellido</a></td>
                                            <td data-label="Sede: ">Sede</td>
                                            <td data-label="E-mail: " style="white-space: nowrap;">Email@gmail.com</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="popupMentores" class="absolute-center" style="display:none;">
    <div id="overlayCalificacion"></div>
    <div class="popupReportes">
        <a href="#" id="closeBtn" class="closePopUp">
            <i class="close-icon" data-icon="9"></i>
        </a>
        <a href="#" id="volverBtn" class="closePopUp">
            <i class="volver-icon" data-icon="l"></i>
        </a>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <img src="img/analytics.png" alt="">
                    <h2>Mentores Formados</h2>
                </div>
                <div class="col-xs-12">
                    <h2><strong>??</strong></h2>
                    <p>Mentores que finalizaron la capacitación presencial y que están dados de alta con rol Mentor.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#mentoresRecibidosDT" aria-controls="mentoresRecibidosDT" role="tab" data-toggle="tab">Desarrollo Temprano</a></li>
                        <li role="presentation"><a href="#mentoresRecibidosCon" aria-controls="mentoresRecibidosCon" role="tab" data-toggle="tab">Consolidación</a></li>
                    </ul>
                </div>
                <div class="col-xs-12">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="mentoresRecibidosDT">
                            <p><strong id="cant_mentores_formados_dt"></strong></p><span>Total de mentores para <strong>Desarrollo Temprano</strong>.</span>
                            <div class="module library reportes">
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Nombre</th>
                                            <th>Sede</th>
                                            <th>E-mail</th>
                                        </tr>
                                    </thead>
                                    <tbody id="mentoresFormadosDT">
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade in" id="mentoresRecibidosCon"><p><strong id="cant_mentores_formados_con"></strong></p><span>Total de mentores para <strong>Consolidación</strong>.</span>
                        <div class="module library reportes">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Sede</th>
                                        <th>E-mail</th>
                                    </tr>
                                </thead>
                                <tbody id="mentoresFormadosCON">
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div id="popupMentorias" class="absolute-center" style="display: none;">
<div id="overlayCalificacion"></div>
<div class="popupReportes">
    <a href="#" id="closeBtn" class="closePopUp">
        <i class="close-icon" data-icon="9"></i>
    </a>
    <a href="#" id="volverBtn" class="closePopUp">
        <i class="volver-icon" data-icon="l"></i>
    </a>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <img src="img/analytics.png" alt="">
                <h2>Procesos de Mentoría <br>en funcionamiento</h2>
            </div>
            <div class="col-xs-12">
                <h2><strong id="cant_grupos_creados_mentorias"></strong></h2>
                <p>Cantidad de grupos creados.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#gruposMentoriaDT" aria-controls="gruposMentoriaDT" role="tab" data-toggle="tab">Desarrollo Temprano</a></li>
                    <li role="presentation"><a href="#gruposMentoriaCon" aria-controls="gruposMentoriaCon" role="tab" data-toggle="tab">Consolidación</a></li>
                </ul>
            </div>
            <div class="col-xs-12">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="gruposMentoriaDT">
                        <p><strong id="cant_grupos_creados_mentorias_dt"></strong></p><span>Total de mentores para <strong>Desarrollo Temprano</strong>.</span>
                        <div class="module library reportes">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Grupo</th>
                                        <th>Mentor</th>
                                        <th>Emprendedor</th>
                                        <th>Emprendedor</th>
                                        <th>Emprendedor</th>
                                    </tr>
                                </thead>
                                <tbody id="mentoriasActivasDT">
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade in" id="gruposMentoriaCon"><p><strong id="cant_grupos_creados_mentorias_dt"></strong></p><span>Total de mentores para <strong>Consolidación</strong>.</span>
                    <div class="module library reportes">
                        <table>
                            <thead>
                                <tr>
                                    <th>Mentor</th>
                                    <th>Emprendedor</th>
                                    <th>Emprendedor</th>
                                    <th>Emprendedor</th>
                                </tr>
                            </thead>
                            <tbody id="mentoriasActivasCON">
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<div id="popupEmprendedoresSesion1" class="absolute-center" style="display: none;">
<div id="overlayCalificacion"></div>
<div class="popupReportes">
<a href="#" id="closeBtn" class="closePopUp">
    <i class="close-icon" data-icon="9"></i>
</a>
<a href="#" id="volverBtn" class="closePopUp">
    <i class="volver-icon" data-icon="l"></i>
</a>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <img src="img/analytics.png" alt="">
            <h2>Emprendedores que completaron<br>la 1° sesión</h2>
        </div>
        <div class="col-xs-6">
            <h3><strong>??</strong></h3>
            <p>Emprendedores que completaron la<span style="white-space: nowrap;"> 1° sesión</span><strong><br> (Total)</strong></p>
        </div>
        <div class="col-xs-6 borderLeft">
            <h3><strong>??%</strong></h3>
            <p>Emprendedores que completaron la <span style="white-space: nowrap;"> 1° sesión</span><br><strong>(Porcentaje Global)</strong></p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#emprededores1sesionDT" aria-controls="emprededores1sesionDT" role="tab" data-toggle="tab">Desarrollo Temprano</a></li>
                <li role="presentation"><a href="#emprededores1sesionCon" aria-controls="emprededores1sesionCon" role="tab" data-toggle="tab">Consolidación</a></li>
            </ul>
        </div>
        <div class="col-xs-12">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="emprededores1sesionDT">
                    <p><strong>70%</strong></p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem aliquid veritatis.</span>
                    <div class="module library reportes">
                        <table>
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Sede</th>
                                    <th>E-mail</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td data-label="Nombre: "><a href="leccion.html">Nombre y Apellido</a></td>
                                    <td data-label="Sede: ">Sede</td>
                                    <td data-label="E-mail: " style="white-space: nowrap;">Email@gmail.com</td>
                                </tr>
                                <tr>
                                    <td data-label="Nombre: "><a href="leccion.html">Nombre y Apellido</a></td>
                                    <td data-label="Sede: ">Sede</td>
                                    <td data-label="E-mail: " style="white-space: nowrap;">Email@gmail.com</td>
                                </tr>
                                <tr>
                                    <td data-label="Nombre: "><a href="leccion.html">Nombre y Apellido</a></td>
                                    <td data-label="Sede: ">Sede</td>
                                    <td data-label="E-mail: " style="white-space: nowrap;">Email@gmail.com</td>
                                </tr>
                                <tr>
                                    <td data-label="Nombre: "><a href="leccion.html">Nombre y Apellido</a></td>
                                    <td data-label="Sede: ">Sede</td>
                                    <td data-label="E-mail: " style="white-space: nowrap;">Email@gmail.com</td>
                                </tr>
                                <tr>
                                    <td data-label="Nombre: "><a href="leccion.html">Nombre y Apellido</a></td>
                                    <td data-label="Sede: ">Sede</td>
                                    <td data-label="E-mail: " style="white-space: nowrap;">Email@gmail.com</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade in" id="emprededores1sesionCon"><p><strong>80%</strong></p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem aliquid veritatis.</span>
                <div class="module library reportes">
                    <table>
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Sede</th>
                                <th>E-mail</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td data-label="Nombre: "><a href="leccion.html">Nombre y Apellido</a></td>
                                <td data-label="Sede: ">Sede</td>
                                <td data-label="E-mail: " style="white-space: nowrap;">Email@gmail.com</td>
                            </tr>
                            <tr>
                                <td data-label="Nombre: "><a href="leccion.html">Nombre y Apellido</a></td>
                                <td data-label="Sede: ">Sede</td>
                                <td data-label="E-mail: " style="white-space: nowrap;">Email@gmail.com</td>
                            </tr>
                            <tr>
                                <td data-label="Nombre: "><a href="leccion.html">Nombre y Apellido</a></td>
                                <td data-label="Sede: ">Sede</td>
                                <td data-label="E-mail: " style="white-space: nowrap;">Email@gmail.com</td>
                            </tr>
                            <tr>
                                <td data-label="Nombre: "><a href="leccion.html">Nombre y Apellido</a></td>
                                <td data-label="Sede: ">Sede</td>
                                <td data-label="E-mail: " style="white-space: nowrap;">Email@gmail.com</td>
                            </tr>
                            <tr>
                                <td data-label="Nombre: "><a href="leccion.html">Nombre y Apellido</a></td>
                                <td data-label="Sede: ">Sede</td>
                                <td data-label="E-mail: " style="white-space: nowrap;">Email@gmail.com</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<div id="popupGenero" class="absolute-center" style="display: none;">
<div id="overlayCalificacion"></div>
<div class="popupReportes">
<a href="#" id="closeBtn" class="closePopUp">
<i class="close-icon" data-icon="9"></i>
</a>
<a href="#" id="volverBtn" class="closePopUp">
<i class="volver-icon" data-icon="l"></i>
</a>
<div class="container">
<div class="row">
    <div class="col-xs-12">
        <img src="img/analytics.png" alt="">
        <h2>Porcentajes entre hombres y mujeres</h2>
    </div>
    <div class="col-xs-12">
        <span>Mentoria</span>
    </div>
    <div class="col-xs-6 ">
        <h3 style="color:#FF6B1F;"><strong id="mentoresM"></strong></h3>
    </div>
    <div class="col-xs-6 borderLeft ">
        <h3><strong id="mentoresF"></strong></h3>
    </div>
    <div class="col-xs-12 ">
        <span>Emprendedores</span>
    </div>
    <div class="col-xs-6 ">
        <h3 style="color:#FF6B1F;"><strong id="emprendedoresM"></strong></h3>
    </div>
    <div class="col-xs-6 borderLeft ">
        <h3><strong id="emprendedoresF"></strong></h3>
    </div>
    <div class="col-xs-12 ">
        <span>Mentores Desarrollo Temprano</span>
    </div>
    <div class="col-xs-6 ">
        <h3 style="color:#FF6B1F;"><strong id="mentoresConM"></strong></h3>
    </div>
    <div class="col-xs-6 borderLeft ">
        <h3><strong id="mentoresConF"></strong></h3>
    </div>
    <div class="col-xs-12 ">
        <span>Mentores Consolidación</span>
    </div>
    <div class="col-xs-6 ">
        <h3 style="color:#FF6B1F;"><strong id="emprendedoresConM"></strong></h3>
    </div>
    <div class="col-xs-6 borderLeft ">
        <h3><strong id="emprendedoresConF"></strong></h3>
    </div>
    <div class="col-xs-12 ">
        <span>Emprendedores Desarrollo Temprano</span>
    </div>
    <div class="col-xs-6 ">
        <h3 style="color:#FF6B1F;"><strong id="mentoresDtM"></strong></h3>
    </div>
    <div class="col-xs-6 borderLeft ">
        <h3><strong id="mentoresDtF"></strong></h3>
    </div>
    <div class="col-xs-12  ">
        <span>Emprendedores Consolidación</span>
    </div>
    <div class="col-xs-6 ">
        <h3 style="color:#FF6B1F;"><strong id="emprendedoresDtM"></strong></h3>
    </div>
    <div class="col-xs-6 borderLeft ">
        <h3><strong id="emprendedoresDtF"></strong></h3>
    </div>
</div>
</div>
</div>
</div>
</div>
<div id="popupRubros" class="absolute-center" style="display: none;">
<div id="overlayCalificacion"></div>
<div class="popupReportes">
<a href="#" id="closeBtn" class="closePopUp">
<i class="close-icon" data-icon="9"></i>
</a>
<a href="#" id="volverBtn" class="closePopUp">
<i class="volver-icon" data-icon="l"></i>
</a>
<div class="container">
<div class="row">
<div class="col-xs-12">
    <img src="img/analytics.png" alt="">
    <h2>Diversidad Productiva</h2>
</div>
<div class="col-xs-12">
    <div class="module library reportes">
        <table>
            <thead>
                <tr>
                    <th>Rubro</th>
                    <th>Porcentaje</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td data-label="Rubro: "><a href="leccion.html">Rubro</a></td>
                    <td data-label="Porcentaje: ">34%</td>
                </tr>
                <tr>
                    <td data-label="Rubro: "><a href="leccion.html">Rubro</a></td>
                    <td data-label="Porcentaje: ">34%</td>
                </tr>
                <tr>
                    <td data-label="Rubro: "><a href="leccion.html">Rubro</a></td>
                    <td data-label="Porcentaje: ">34%</td>
                </tr>
                <tr>
                    <td data-label="Rubro: "><a href="leccion.html">Rubro</a></td>
                    <td data-label="Porcentaje: ">34%</td>
                </tr>
                <tr>
                    <td data-label="Rubro: "><a href="leccion.html">Rubro</a></td>
                    <td data-label="Porcentaje: ">34%</td>
                </tr>
                <tr>
                    <td data-label="Rubro: "><a href="leccion.html">Rubro</a></td>
                    <td data-label="Porcentaje: ">34%</td>
                </tr>
                <tr>
                    <td data-label="Rubro: "><a href="leccion.html">Rubro</a></td>
                    <td data-label="Porcentaje: ">34%</td>
                </tr>
                <tr>
                    <td data-label="Rubro: "><a href="leccion.html">Rubro</a></td>
                    <td data-label="Porcentaje: ">34%</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
<div id="popupTerritoriales" class="absolute-center" style="display: none;">
<div id="overlayCalificacion"></div>
<div class="popupReportes">
<a href="#" id="closeBtn" class="closePopUp">
<i class="close-icon" data-icon="9"></i>
</a>
<a href="#" id="volverBtn" class="closePopUp">
<i class="volver-icon" data-icon="l"></i>
</a>
<div class="container">
<div class="row">
<div class="col-xs-12">
<img src="img/analytics.png" alt="">
<h2>Alcance territorial</h2>
</div>
<div class="col-xs-12">
<h2><strong>??</strong></h2>
<p><strong>Sedes Activas</strong></p>
</div>
<div class="col-xs-12">
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#emprededoresTerritorio" aria-controls="emprededoresTerritorio" role="tab" data-toggle="tab">Emprendedores</a></li>
    <li role="presentation"><a href="#mentoresTerritorio" aria-controls="mentoresTerritorio" role="tab" data-toggle="tab">Mentores</a></li>
</ul>
</div>
<div class="col-xs-12">
<ul class="filtros">
    <li>
        <select name="filtroMentoria" id="filtroMentoria" class="selectize">
            <option value="">Filtrar por Mentoria</option>
            <option value="Desarrollo Temprano">Desarrollo Temprano</option>
            <option value="Consolidacion">Consolidación</option>
        </select>
    </li>
    <li>
        <select name="filtroProvincia" id="filtroProvincia" class="selectize">
            <option value="">Filtrar por provincia</option>
            <option value="Buenos Aires">Buenos Aires</option>
            <option value="Buenos Aires">Capital Federal</option>
            <option value="Santa Fe">Santa Fe</option>
            <option value="Córdoba">Córdoba</option>
            <option value="La Pampa">La Pampa</option>
            <option value="Chubut">Chubut</option>
        </select>
    </li>
    <li>
        <select name="filtroLocalidad" id="filtroLocalidad" class="selectize">
            <option value="">Filtrar por localidad</option>
            <option value="Opcion 1">Opcion 1</option>
            <option value="Opcion 2">Opcion 2</option>
            <option value="Opcion 3">Opcion 3</option>
        </select>
    </li>
</ul>
</div>
<div class="col-xs-12" >
<div class="tab-content">
    <div role="tabpanel" class="tab-pane fade in active" id="emprededoresTerritorio">
        <div class="module library reportes">
            <table>
                <thead >
                    <tr class="panel-title">
                        <th><a  role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseBuenosAires" aria-expanded="true" aria-controls="collapseBuenosAires">Buenos Aires</a></th>
                        <th></th>
                        <th><a  role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseBuenosAires" aria-expanded="true" aria-controls="collapseBuenosAires">50%</a></th>
                        <th><a  role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseBuenosAires" aria-expanded="true" aria-controls="collapseBuenosAires"><i data-icon="T"></i></a></th>
                    </tr>
                </thead>
                <tbody  id="collapseBuenosAires" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">15</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">32</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">40</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">35</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">8</td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
            <table>
                <thead>
                    <tr class="panel-title">
                        <th><a  role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSantaFe" aria-expanded="true" aria-controls="collapseSantaFe">Santa Fe</a></th>
                        <th></th>
                        <th>20%</th>
                        <th><a  role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSantaFe" aria-expanded="true" aria-controls="collapseSantaFe"><i data-icon="T"></i></a></th>
                    </tr>
                </thead>
                <tbody  id="collapseSantaFe" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">15</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">32</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">40</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">35</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">8</td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
            <table>
                <thead>
                    <tr class="panel-title">
                        <th><a  role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseCordoba" aria-expanded="true" aria-controls="collapseCordoba">Cordoba</a></th>
                        <th></th>
                        <th>15%</th>
                        <th><a  role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseCordoba" aria-expanded="true" aria-controls="collapseCordoba"><i data-icon="T"></i></a></th>
                    </tr>
                </thead>
                <tbody  id="collapseCordoba" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">15</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">32</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">40</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">35</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">8</td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
            <table>
                <thead>
                    <tr class="panel-title">
                        <th><a  role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseLaPampa" aria-expanded="true" aria-controls="collapseLaPampa">La Pampa</a></th>
                        <th></th>
                        <th>10%</th>
                        <th><a  role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseLaPampa" aria-expanded="true" aria-controls="collapseLaPampa"><i data-icon="T"></i></a></th>
                    </tr>
                </thead>
                <tbody  id="collapseLaPampa" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">15</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">32</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">40</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">35</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">8</td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
            <table>
                <thead>
                    <tr class="panel-title">
                        <th><a  role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseChubut" aria-expanded="true" aria-controls="collapseOne">Chubut</a></th>
                        <th></th>
                        <th>5%</th>
                        <th><a  role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseChubut" aria-expanded="true" aria-controls="collapseOne"><i data-icon="T"></i></a></th>
                    </tr>
                </thead>
                <tbody  id="collapseChubut" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">15</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">32</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">40</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">35</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">8</td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div role="tabpanel" class="tab-pane fade in" id="mentoresTerritorio">
        <div class="module library reportes">
            <table>
                <thead >
                    <tr class="panel-title">
                        <th><a  role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseBuenosAiresMentores" aria-expanded="true" aria-controls="collapseBuenosAiresMentores">Buenos Aires</a></th>
                        <th></th>
                        <th>60%</th>
                        <th><a  role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseBuenosAiresMentores" aria-expanded="true" aria-controls="collapseBuenosAiresMentores"><i data-icon="T"></i></a></th>
                    </tr>
                </thead>
                <tbody  id="collapseBuenosAiresMentores" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">15</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">32</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">40</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">35</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">8</td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
            <table>
                <thead>
                    <tr class="panel-title">
                        <th><a  role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSantaFeMentores" aria-expanded="true" aria-controls="collapseSantaFeMentores">Santa Fe</a></th>
                        <th></th>
                        <th>15%</th>
                        <th><a  role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSantaFeMentores" aria-expanded="true" aria-controls="collapseSantaFeMentores"><i data-icon="T"></i></a></th>
                    </tr>
                </thead>
                <tbody  id="collapseSantaFeMentores" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">15</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">32</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">40</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">35</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">8</td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
            <table>
                <thead>
                    <tr class="panel-title">
                        <th><a  role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseCordobaMentores" aria-expanded="true" aria-controls="collapseCordobaMentores">Cordoba</a></th>
                        <th></th>
                        <th>15%</th>
                        <th><a  role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseCordobaMentores" aria-expanded="true" aria-controls="collapseCordobaMentores"><i data-icon="T"></i></a></th>
                    </tr>
                </thead>
                <tbody  id="collapseCordobaMentores" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">15</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">32</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">40</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">35</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">8</td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
            <table>
                <thead>
                    <tr class="panel-title">
                        <th><a  role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseLaPampaMentores" aria-expanded="true" aria-controls="collapseLaPampaMentores">La Pampa</a></th>
                        <th></th>
                        <th>5%</th>
                        <th><a  role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseLaPampaMentores" aria-expanded="true" aria-controls="collapseLaPampaMentores"><i data-icon="T"></i></a></th>
                    </tr>
                </thead>
                <tbody  id="collapseLaPampaMentores" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">15</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">32</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">40</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">35</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">8</td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
            <table>
                <thead>
                    <tr class="panel-title">
                        <th><a  role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseChubutMentores" aria-expanded="true" aria-controls="collapseChubutMentores">Chubut</a></th>
                        <th></th>
                        <th>5%</th>
                        <th><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseChubutMentores" aria-expanded="true" aria-controls="collapseChubutMentores"><i data-icon="T"></i></a></th>
                    </tr>
                </thead>
                <tbody  id="collapseChubutMentores" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">15</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">32</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">40</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">35</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td data-label="Localidad: ">Localidad</td>
                        <td data-label="Sede: ">Sede</td>
                        <td data-label="Cantidad: ">8</td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <button class="btn descargarDatosTerritorio">Descargar Datos</button>
</div>
</div>
</div>
</div>
</div>
</div>
<div id="popupSatisfaccionEmpr" class="absolute-center" style="display: none;">
    <div id="overlayCalificacion"></div>
    <div class="popupReportes">
        <a href="#" id="closeBtn" class="closePopUp">
        <i class="close-icon" data-icon="9"></i>
        </a>
        <a href="#" id="volverBtn" class="closePopUp">
        <i class="volver-icon" data-icon="l"></i>
        </a>
        <div class="container">
            <div class="row">
                

                <div class="col-xs-12">
                    <img src="img/analytics.png" alt="">
                    <h2>Grado de satisfacción de los usuarios</h2>
                </div>
                <div class="col-xs-12">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#satisfaccionDt" aria-controls="satisfaccionDt" role="tab" data-toggle="tab">Desarrollo Temprano</a></li>
                        <li role="presentation"><a href="#satisfaccionConsolidacion" aria-controls="satisfaccionConsolidacion" role="tab" data-toggle="tab">Consolidación</a></li>
                    </ul>
                </div>
                <div class="col-xs-12">
                    <ul class="filtros">
                        <li>
                            <select name="filtroProvincia" id="filtroProvincia" class="selectize">
                                <option value="">Filtrar por provincia</option>
                                <option value="Buenos Aires">Buenos Aires</option>
                                <option value="Buenos Aires">Capital Federal</option>
                                <option value="Santa Fe">Santa Fe</option>
                                <option value="Córdoba">Córdoba</option>
                                <option value="La Pampa">La Pampa</option>
                                <option value="Chubut">Chubut</option>
                            </select>
                        </li>
                        <li>
                            <select name="filtroLocalidad" id="filtroLocalidad" class="selectize">
                                <option value="">Filtrar por localidad</option>
                                <option value="Opcion 1">Opcion 1</option>
                                <option value="Opcion 2">Opcion 2</option>
                                <option value="Opcion 3">Opcion 3</option>
                            </select>
                        </li>
                        <li>
                            <select name="filtroLocalidad" id="filtroLocalidad" class="selectize">
                                <option value="">Filtrar por Sede</option>
                                <option value="sede 1">sede 1</option>
                                <option value="sede 2">sede 2</option>
                                <option value="sede 3">sede 3</option>
                            </select>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-12" >
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="satisfaccionDt">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                            Emprendedor #1
                                             <i data-icon="T" class="pull-right"></i>
                                        </a>
                                        </h4>
                                    </div>
                                    <div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="collapse1">
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                <strong>Estructura y contenidos de cada reunión.</strong>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <strong>Utilidad de los temas trabajados.</strong>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <strong>Cumplimiento de los objetivos del plan de trabajo.</strong>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <strong>Desempeño del mentor.</strong>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <strong>Material de apoyo.</strong>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </li>
                                            <li class="list-group-item"><strong>Valoración general del programa:</strong> Lorem ipsum dolor sit amet, consectetur adipisicing elit. In iusto cumque quibusdam optio, fugit praesentium adipisci numquam cupiditate porro vero qui culpa aspernatur earum, debitis minima eius ea tempore, doloribus!</li>
                                            <li class="list-group-item"><strong>Comentario, sugerencia o aspecto de mejora para futuras ediciones:</strong> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</li>
                                            <li class="list-group-item"><button class="btn">Descargar Reporte</button></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading2">
                                        <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
                                            Emprendedor #2
                                            <i data-icon="T" class="pull-right"></i>
                                        </a>
                                        </h4>
                                    </div>
                                    <div id="collapse2" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading2">
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                <strong>Estructura y contenidos de cada reunión.</strong>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <strong>Utilidad de los temas trabajados.</strong>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <strong>Cumplimiento de los objetivos del plan de trabajo.</strong>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <strong>Desempeño del mentor.</strong>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <strong>Material de apoyo.</strong>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </li>
                                            <li class="list-group-item"><strong>Valoración general del programa:</strong> Lorem ipsum dolor sit amet, consectetur adipisicing elit. In iusto cumque quibusdam optio, fugit praesentium adipisci numquam cupiditate porro vero qui culpa aspernatur earum, debitis minima eius ea tempore, doloribus!</li>
                                            <li class="list-group-item"><strong>Comentario, sugerencia o aspecto de mejora para futuras ediciones:</strong> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</li>
                                            <li class="list-group-item"><button class="btn">Descargar Reporte</button></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                        <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
                                            Emprendedor #3
                                            <i data-icon="T" class="pull-right"></i>
                                        </a>
                                        </h4>
                                    </div>
                                    <div id="collapse3" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading3">
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                <strong>Estructura y contenidos de cada reunión.</strong>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <strong>Utilidad de los temas trabajados.</strong>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <strong>Cumplimiento de los objetivos del plan de trabajo.</strong>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <strong>Desempeño del mentor.</strong>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <strong>Material de apoyo.</strong>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </li>
                                            <li class="list-group-item"><strong>Valoración general del programa:</strong> Lorem ipsum dolor sit amet, consectetur adipisicing elit. In iusto cumque quibusdam optio, fugit praesentium adipisci numquam cupiditate porro vero qui culpa aspernatur earum, debitis minima eius ea tempore, doloribus!</li>
                                            <li class="list-group-item"><strong>Comentario, sugerencia o aspecto de mejora para futuras ediciones:</strong> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</li>
                                            <li class="list-group-item"><button class="btn">Descargar Reporte</button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade in" id="satisfaccionConsolidacion">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseM1" aria-expanded="true" aria-controls="collapseM1">
                                            Emprendedor #1
                                            <i data-icon="T" class="pull-right"></i>
                                        </a>
                                        </h4>
                                    </div>
                                    <div id="collapseM1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="collapseM1">
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                <strong>Estructura y contenidos de cada reunión.</strong>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <strong>Utilidad de los temas trabajados.</strong>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <strong>Cumplimiento de los objetivos del plan de trabajo.</strong>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <strong>Desempeño del mentor.</strong>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <strong>Material de apoyo.</strong>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </li>
                                            <li class="list-group-item"><strong>Valoración general del programa:</strong> Lorem ipsum dolor sit amet, consectetur adipisicing elit. In iusto cumque quibusdam optio, fugit praesentium adipisci numquam cupiditate porro vero qui culpa aspernatur earum, debitis minima eius ea tempore, doloribus!</li>
                                            <li class="list-group-item"><strong>Comentario, sugerencia o aspecto de mejora para futuras ediciones:</strong> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</li>
                                            <li class="list-group-item"><button class="btn">Descargar Reporte</button></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading2">
                                        <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseM2" aria-expanded="false" aria-controls="collapseM2">
                                            Emprendedor #2
                                            <i data-icon="T" class="pull-right"></i>
                                        </a>
                                        </h4>
                                    </div>
                                    <div id="collapseM2" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading2">
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                <strong>Estructura y contenidos de cada reunión.</strong>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <strong>Utilidad de los temas trabajados.</strong>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <strong>Cumplimiento de los objetivos del plan de trabajo.</strong>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <strong>Desempeño del mentor.</strong>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <strong>Material de apoyo.</strong>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </li>
                                            <li class="list-group-item"><strong>Valoración general del programa:</strong> Lorem ipsum dolor sit amet, consectetur adipisicing elit. In iusto cumque quibusdam optio, fugit praesentium adipisci numquam cupiditate porro vero qui culpa aspernatur earum, debitis minima eius ea tempore, doloribus!</li>
                                            <li class="list-group-item"><strong>Comentario, sugerencia o aspecto de mejora para futuras ediciones:</strong> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</li>
                                            <li class="list-group-item"><button class="btn">Descargar Reporte</button></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                        <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseM3" aria-expanded="false" aria-controls="collapseM3">
                                            Emprendedor #3
                                            <i data-icon="T" class="pull-right"></i>
                                        </a>
                                        </h4>
                                    </div>
                                    <div id="collapseM3" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading3">
                                       <ul class="list-group">
                                            <li class="list-group-item">
                                                <strong>Estructura y contenidos de cada reunión.</strong>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <strong>Utilidad de los temas trabajados.</strong>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <strong>Cumplimiento de los objetivos del plan de trabajo.</strong>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <strong>Desempeño del mentor.</strong>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <strong>Material de apoyo.</strong>
                                                <div class="ratingResult">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </li>
                                            <li class="list-group-item"><strong>Valoración general del programa:</strong> Lorem ipsum dolor sit amet, consectetur adipisicing elit. In iusto cumque quibusdam optio, fugit praesentium adipisci numquam cupiditate porro vero qui culpa aspernatur earum, debitis minima eius ea tempore, doloribus!</li>
                                            <li class="list-group-item"><strong>Comentario, sugerencia o aspecto de mejora para futuras ediciones:</strong> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</li>
                                            <li class="list-group-item"><button class="btn">Descargar Reporte</button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<div id="popupMentoresEnFormacion" class="absolute-center" style="display: none;">
    <div id="overlayCalificacion"></div>
    <div class="popupReportes">
    <a href="#" id="closeBtn" class="closePopUp">
        <i class="close-icon" data-icon="9"></i>
    </a>
    <a href="#" id="volverBtn" class="closePopUp">
        <i class="volver-icon" data-icon="l"></i>
    </a>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <img src="img/analytics.png" alt="">
                <h2>Mentores en Formación</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#mentoresFormacionDt" aria-controls="mentoresFormacionDt" role="tab" data-toggle="tab">Desarrollo Temprano</a></li>
                    <li role="presentation"><a href="#mentoresFormacionCon" aria-controls="mentoresFormacionCon" role="tab" data-toggle="tab">Consolidación</a></li>
                </ul>
            </div>
            <div class="col-xs-12">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="mentoresFormacionDt">
                        <p><strong id="cant_mentores_formacion_dt"></strong></p><span>Mentores en formación <strong>(Desarrollo Temprano)</strong>.</span>
                        <div class="module library reportes">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Sede</th>
                                        <th>E-mail</th>
                                    </tr>
                                </thead>
                                <tbody id="mentoresEnFormacionDT">
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade in" id="mentoresFormacionCon"><p><strong id="cant_mentores_formacion_con"></strong></p><span>Mentores en formación <strong>(Consolidación)</strong>.</span>
                    <div class="module library reportes">
                        <table>
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Sede</th>
                                    <th>E-mail</th>
                                </tr>
                            </thead>
                            <tbody id="mentoresEnFormacionCON">
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script>
window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')
</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="js/plugins.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.js"></script>
<script src="js/chartist.min.js"></script>
<script src="js/imagesloaded.pkgd.min.js"></script>
<script src="js/main.js"></script>
<script src="scripts/header.js"></script>
<script src="scripts/reportes-admin.js"></script>
</body>
</html>