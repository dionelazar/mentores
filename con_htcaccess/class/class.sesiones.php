<?php 
/**
* 
*/
require_once 'class.helpers.php';

class Sesiones
{
	
	function getSesiones()
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_sesiones_mentor_DT @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2001. ';
			Errores::insErrorDeQuery('Error 2001. ', $sql, $params);
		}else{
			$sesiones = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miSesion = new Sesiones();
				$miSesion->mensaje = utf8_encode($row['mensaje']);
				$miSesion->id_sesion = $row['id_sesion'];
				$miSesion->id_grupo = $row['id_grupo'];
				$miSesion->desc = utf8_encode($row['desc']);
				$miSesion->emprendedores = utf8_encode($row['emprendedores']);

				$sesiones[$i] = $miSesion;
				$i++;
			}			

			Conection::CerrarConexion($conn);
			echo json_encode($sesiones);
		}
	}

	function getSesionesDT()
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_sesiones_DT @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2001.1 ';
			Errores::insErrorDeQuery('Error 2001. ', $sql, $params);
		}else{
			$sesiones = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miSesion = new Sesiones();
				$miSesion->mensaje = utf8_encode($row['mensaje']);
				$miSesion->id_sesion = $row['id_sesion'];
				$miSesion->id_grupo = $row['id_grupo'];
				$miSesion->desc = utf8_encode($row['desc']);
				$miSesion->comentario = utf8_encode($row['comentario']);
				$miSesion->mentor = utf8_encode($row['mentor']);
				$miSesion->sn_califico = $row['sn_califico'];

				$sesiones[$i] = $miSesion;
				$i++;
			}			

			Conection::CerrarConexion($conn);
			echo json_encode($sesiones);
		}
	}

	function getSesionesMentorCon()
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_sesiones_mentor_con @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2001.2 ';
			Errores::insErrorDeQuery('Error 2001. ', $sql, $params);
		}else{
			$sesiones = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miSesion = new Sesiones();
				$miSesion->mensaje = utf8_encode($row['mensaje']);
				$miSesion->id_sesion = $row['id_sesion'];
				$miSesion->id_grupo = $row['id_grupo'];
				$miSesion->desc = utf8_encode($row['desc']);
				$miSesion->emprendedores = utf8_encode($row['emprendedores']);
				$miSesion->sn_califico = $row['sn_califico'];

				$sesiones[$i] = $miSesion;
				$i++;
			}			

			Conection::CerrarConexion($conn);
			echo json_encode($sesiones);
		}
	}

	function getSesionesCON()
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_sesiones_CON @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2001.3 ';
			Errores::insErrorDeQuery('Error 2001.3 ', $sql, $params);
		}else{
			$sesiones = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miSesion = new Sesiones();
				$miSesion->mensaje = utf8_encode($row['mensaje']);
				$miSesion->id_sesion = $row['id_sesion'];
				$miSesion->id_grupo = $row['id_grupo'];
				$miSesion->desc = utf8_encode($row['desc']);
				$miSesion->comentario = utf8_encode($row['comentario']);
				$miSesion->mentor = utf8_encode($row['mentor']);
				$miSesion->sn_califico = $row['sn_califico'];

				$sesiones[$i] = $miSesion;
				$i++;
			}			

			Conection::CerrarConexion($conn);
			echo json_encode($sesiones);
		}
	}

	function getFormCalificacionMentorDT($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_sesion = $post['id_sesion'];
		$id_grupo = $post['id_grupo'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_form_calificacion_mentor_DT";
		$params = array($id_usuario, $id_sesion, $id_grupo);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2002. ';
			Errores::insErrorDeQuery('Error 2002. ', $sql, $params);
		}else{			
			$miSesion = new Sesiones();
			
			$miSesion->emprendedores = Self::getEmprendedoresDeGrupo($id_grupo);

			$preguntas = array();
			$i = 0;
			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC )){
				$miPregunta = new Sesiones();

				$miPregunta->id_pregunta = $row['id_pregunta'];
				$miPregunta->num_pregunta = $row['num_pregunta'];
				$miPregunta->pregunta = utf8_encode($row['pregunta']);
				$miPregunta->respuestas = Self::getRespuestas($miPregunta->id_pregunta);
				$preguntas[$i] = $miPregunta;
				$i++;
			}

			$miSesion->preguntas = $preguntas;

			Conection::CerrarConexion($conn);
			echo json_encode($miSesion);
		}
	}

	function getFormCalificacionDT($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_sesion = $post['id_sesion'];
		$id_grupo = $post['id_grupo'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_form_calificacion_DT";
		$params = array($id_usuario, $id_sesion, $id_grupo);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2002.1 ';
			Errores::insErrorDeQuery('Error 2002.1 ', $sql, $params);
		}else{			
			$miSesion = new Sesiones();

			$preguntas = array();
			$i = 0;
			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC )){
				$miPregunta = new Sesiones();

				$miPregunta->id_pregunta = $row['id_pregunta'];
				$miPregunta->num_pregunta = $row['num_pregunta'];
				$miPregunta->pregunta = utf8_encode($row['pregunta']);
				$miPregunta->respuestas = Self::getRespuestas($miPregunta->id_pregunta);
				$preguntas[$i] = $miPregunta;
				$i++;
			}

			$miSesion->preguntas = $preguntas;

			Conection::CerrarConexion($conn);
			echo json_encode($miSesion);
		}
	}

	function getFormCalificacionMentorCON($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_sesion = $post['id_sesion'];
		$id_grupo = $post['id_grupo'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_form_calificacion_mentor_con";
		$params = array($id_usuario, $id_sesion, $id_grupo);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2002.2 ';
			Errores::insErrorDeQuery('Error 2002.2 ', $sql, $params);
		}else{			
			$miSesion = new Sesiones();
			
			$miSesion->emprendedores = Self::getEmprendedoresDeGrupo($id_grupo);

			$preguntas = array();
			$i = 0;
			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC )){
				$miPregunta = new Sesiones();

				$miPregunta->id_pregunta = $row['id_pregunta'];
				$miPregunta->num_pregunta = $row['num_pregunta'];
				$miPregunta->pregunta = utf8_encode($row['pregunta']);
				$miPregunta->respuestas = Self::getRespuestas($miPregunta->id_pregunta);
				$preguntas[$i] = $miPregunta;
				$i++;
			}

			$miSesion->preguntas = $preguntas;

			Conection::CerrarConexion($conn);
			echo json_encode($miSesion);
		}
	}

	function getFormCalificacionCON($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_sesion = $post['id_sesion'];
		$id_grupo = $post['id_grupo'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_form_calificacion_CON";
		$params = array($id_usuario, $id_sesion, $id_grupo);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2002.3 ';
			Errores::insErrorDeQuery('Error 2002.3 ', $sql, $params);
		}else{			
			$miSesion = new Sesiones();

			$preguntas = array();
			$i = 0;
			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC )){
				$miPregunta = new Sesiones();

				$miPregunta->id_pregunta = $row['id_pregunta'];
				$miPregunta->num_pregunta = $row['num_pregunta'];
				$miPregunta->pregunta = utf8_encode($row['pregunta']);
				$miPregunta->respuestas = Self::getRespuestas($miPregunta->id_pregunta);
				$preguntas[$i] = $miPregunta;
				$i++;
			}

			$miSesion->preguntas = $preguntas;

			Conection::CerrarConexion($conn);
			echo json_encode($miSesion);
		}
	}

	function getComprobarCalificacion($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_sesion = $post['id_sesion'];
		$id_grupo = $post['id_grupo'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_comprobar_calificacion @id_usuario = ?, @id_sesion = ?, @id_grupo = ?";
		$params = array($id_usuario, $id_sesion, $id_grupo);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2003. ';
			Errores::insErrorDeQuery('Error 2003. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$miSesion = new Sesiones();
			$miSesion->mensaje = utf8_encode($row['mensaje']);
			$miSesion->promedio = $row['promedio'];

			Conection::CerrarConexion($conn);
			echo json_encode($miSesion);
		}
	}

	function getEmprendedoresDeGrupo($id_grupo)
	{
		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_emprendedores_from_grupo @id_grupo = ?";
		$params = array($id_grupo);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2004. ';
			Errores::insErrorDeQuery('Error 2004. ', $sql, $params);
		}else{
			$sesiones = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miSesion = new Sesiones();
				$miSesion->id_usuario = $row['id_usuario'];
				$miSesion->nombre = utf8_encode($row['nombre']);
				$miSesion->documento = $row['documento'];

				$sesiones[$i] = $miSesion;
				$i++;
			}			

			Conection::CerrarConexion($conn);
			return $sesiones;
		}
	}

	function getRespuestas($id_pregunta)
	{
		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_opciones_pregunta @id_pregunta = ?";
		$params = array($id_pregunta);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2005. ';
			Errores::insErrorDeQuery('Error 2005. ', $sql, $params);
		}else{
			$respuestas = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miRespuesta = new Sesiones();
				$miRespuesta->id_respuesta = $row['id_respuesta'];
				$miRespuesta->respuesta = utf8_encode($row['respuesta']);

				$respuestas[$i] = $miRespuesta;
				$i++;
			}			

			Conection::CerrarConexion($conn);
			return $respuestas;
		}
	}

	function getGruposReporte($post)
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_grupos_reporte @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2006. ';
			Errores::insErrorDeQuery('Error 2006. ', $sql, $params);
		}else{
			$grupos = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miGrupo = new Sesiones();
				$miGrupo->id_grupo = $row['id_grupo'];
				$miGrupo->id_tipo_grupo = $row['id_tipo_grupo'];
				$miGrupo->promedio = $row['promedio'];
				$miGrupo->mentor = utf8_encode($row['mentor']);
				$miGrupo->grupo = utf8_encode($row['grupo']);

				$grupos[$i] = $miGrupo;
				$i++;
			}

			Conection::CerrarConexion($conn);
			return $grupos;
		}
	}

	function getCalificacionesEmprendedores($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_grupo = $post['id_grupo'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_calificaciones_reporte @id_usuario = ?, @id_grupo = ?";
		$params = array($id_usuario, $id_grupo);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2007. ';
			Errores::insErrorDeQuery('Error 2007. ', $sql, $params);
		}else{
			$calificaciones = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miCalificacion = new Sesiones();
				$miCalificacion->promedio = $row['promedio'];
				$miCalificacion->comentario = utf8_encode($row['comentario']);
				$miCalificacion->nombre = utf8_encode($row['nombre']);

				$calificaciones[$i] = $miCalificacion;
				$i++;
			}			

			Conection::CerrarConexion($conn);
			return $calificaciones;
		}
	}

	function getGruposPorPais($post)
	{
		$id_pais = $post['id_pais'];
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_grupos_por_pais @id_usuario = ?, @id_pais = ?";
		$params = array($id_usuario, $id_pais);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2008. ';
			Errores::insErrorDeQuery('Error 2008. ', $sql, $params);
		}else{
			$grupos = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miGrupo = new Sesiones();
				$miGrupo->id_grupo = $row['id_grupo'];
				$miGrupo->id_tipo_grupo = $row['id_tipo_grupo'];
				$miGrupo->promedio = $row['promedio'];
				$miGrupo->mentor = utf8_encode($row['mentor']);
				$miGrupo->grupo = utf8_encode($row['grupo']);

				$grupos[$i] = $miGrupo;
				$i++;
			}

			Conection::CerrarConexion($conn);
			return $grupos;
		}
	}

	function getGruposPorProvincia($post)
	{
		$id_pais = $post['id_pais'];
		$id_provincia = $post['id_provincia'];
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_grupos_por_provincia @id_usuario = ?, @id_pais = ?, @id_provincia = ?";
		$params = array($id_usuario, $id_pais, $id_provincia);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2009. ';
			Errores::insErrorDeQuery('Error 2009. ', $sql, $params);
		}else{
			$grupos = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miGrupo = new Sesiones();
				$miGrupo->id_grupo = $row['id_grupo'];
				$miGrupo->id_tipo_grupo = $row['id_tipo_grupo'];
				$miGrupo->promedio = $row['promedio'];
				$miGrupo->mentor = utf8_encode($row['mentor']);
				$miGrupo->grupo = utf8_encode($row['grupo']);

				$grupos[$i] = $miGrupo;
				$i++;
			}

			Conection::CerrarConexion($conn);
			return $grupos;
		}
	}

	function getGruposPorCiudad($post)
	{
		$id_pais = $post['id_pais'];
		$id_provincia = $post['id_provincia'];
		$id_ciudad = $post['id_ciudad'];
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_grupos_por_ciudad @id_usuario = ?, @id_pais = ?, @id_provincia = ?, @id_ciudad = ?";
		$params = array($id_usuario, $id_pais, $id_provincia, $id_ciudad);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2010. ';
			Errores::insErrorDeQuery('Error 2010. ', $sql, $params);
		}else{
			$grupos = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miGrupo = new Sesiones();
				$miGrupo->id_grupo = $row['id_grupo'];
				$miGrupo->id_tipo_grupo = $row['id_tipo_grupo'];
				$miGrupo->promedio = $row['promedio'];
				$miGrupo->mentor = utf8_encode($row['mentor']);
				$miGrupo->grupo = utf8_encode($row['grupo']);

				$grupos[$i] = $miGrupo;
				$i++;
			}

			Conection::CerrarConexion($conn);
			return $grupos;
		}
	}

	function getGruposPorMentoria($post)
	{
		$id_mentoria = $post['id_mentoria'];
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_grupos_por_mentoria @id_usuario = ?, @id_mentoria = ?";
		$params = array($id_usuario, $id_mentoria);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2011. ';
			Errores::insErrorDeQuery('Error 2011. ', $sql, $params);
		}else{
			$grupos = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miGrupo = new Sesiones();
				$miGrupo->id_grupo = $row['id_grupo'];
				$miGrupo->id_tipo_grupo = $row['id_tipo_grupo'];
				$miGrupo->promedio = $row['promedio'];
				$miGrupo->mentor = utf8_encode($row['mentor']);
				$miGrupo->grupo = utf8_encode($row['grupo']);

				$grupos[$i] = $miGrupo;
				$i++;
			}

			Conection::CerrarConexion($conn);
			return $grupos;
		}
	}

/****************************************************************************************************************************************************/

	function calificar($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_sesion = $post['id_sesion'];
		$id_grupo = $post['id_grupo'];
		$asistencias = isset($post['asistencias']) ? $post['asistencias'] : '';
		$resp1 = $post['resp1'];
		$resp2 = $post['resp2'];
		$stars = $post['stars'];
		$comentario = $post['comentario'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_ins_calificacion @id_usuario = ?, @id_sesion = ?, @id_grupo = ?, @stars = ?, @comentario = ?";
		$params = array($id_usuario, $id_sesion, $id_grupo, $stars, $comentario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2101. ';
			Errores::insErrorDeQuery('Error 2101. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$miSesion = new Sesiones();
			$miSesion->mensaje = utf8_encode($row['mensaje']);
			$miSesion->cod_mensaje = $row['cod_mensaje'];

			if($miSesion->cod_mensaje == 0){
				Conection::CerrarConexion($conn);
				echo json_encode($miSesion);
				return;
			}

			if($asistencias != ''){
				$asis = explode( ',', $asistencias);
				$i = 0;
				$misAsistencias = array();
				foreach ($asis as $key => $value) {
					$misAsistencias[$i] = Self::insAsistencia($id_sesion, $id_grupo, $value);
					$i++;
				}
				$miSesion->asistencias = $misAsistencias;
			}

			$resp = explode( ',', $resp1);
			$i = 0;
			$misrespuestas = array();
			foreach ($resp as $key => $value) {
				$misrespuestas[$i] = Self::insRespuesta($id_sesion, $id_grupo, $id_usuario, $value);
				$i++;
			}
			$miSesion->respuestas = $misrespuestas;

			$resp2 = explode( ',', $resp2);
			$i = 0;
			$misrespuestas2 = array();
			foreach ($resp2 as $key => $value) {
				$misrespuestas2[$i] = Self::insRespuesta($id_sesion, $id_grupo, $id_usuario, $value);
				$i++;
			}
			$miSesion->respuestas2 = $misrespuestas2;

			Conection::CerrarConexion($conn);
			echo json_encode($miSesion);
		}
	}

	function insAsistencia($id_sesion, $id_grupo, $id_usuario)
	{
		$conn = Conection::Conexion();
		$sql = "EXEC sp_ins_asistencia @id_sesion = ?, @id_grupo = ?, @id_usuario = ?";
		$params = array($id_sesion, $id_grupo, $id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2102. ';
			Errores::insErrorDeQuery('Error 2102. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$miSesion = new Sesiones();
			$miSesion->mensaje = utf8_encode($row['mensaje']);
			$miSesion->cod_mensaje = $row['cod_mensaje'];

			Conection::CerrarConexion($conn);
			return $miSesion;
		}
	}

	function insRespuesta($id_sesion, $id_grupo, $id_usuario, $id_respuesta	)
	{
		$conn = Conection::Conexion();
		$sql = "EXEC sp_ins_repuesta @id_sesion = ?, @id_grupo = ?, @id_usuario = ?, @id_respuesta = ?";
		$params = array($id_sesion, $id_grupo, $id_usuario, $id_respuesta);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2103. ';
			Errores::insErrorDeQuery('Error 2103. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$miSesion = new Sesiones();
			$miSesion->mensaje = utf8_encode($row['mensaje']);
			$miSesion->cod_mensaje = $row['cod_mensaje'];

			Conection::CerrarConexion($conn);
			return $miSesion;
		}
	}

} 
?>