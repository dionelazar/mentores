<?php 
/**
* 
*/
require_once 'class.helpers.php';

class Localidades
{
	function getPaises($post)
	{
		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_paises";
		$params = array();
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 4001. ';
			Errores::insErrorDeQuery('Error 4001. ', $sql, $params);
		}else{
			$paises = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miPais = new Localidades();
				$miPais->id_pais = $row['id_pais'];
				$miPais->pais = utf8_encode($row['pais']);

				$paises[$i] = $miPais;
				$i++;
			}			

			Conection::CerrarConexion($conn);
			echo json_encode($paises);
		}
	}

	function getProvincias($post)
	{
		$id_pais = $post['id_pais'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_provincias @id_pais = ?";
		$params = array($id_pais);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 4002. ';
			Errores::insErrorDeQuery('Error 4002. ', $sql, $params);
		}else{
			$provincias = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miProvincia = new Localidades();
				$miProvincia->id_pais = $row['id_pais'];
				$miProvincia->id_provincia = $row['id_provincia'];
				$miProvincia->provincia = utf8_encode($row['provincia']);

				$provincias[$i] = $miProvincia;
				$i++;
			}			

			Conection::CerrarConexion($conn);
			echo json_encode($provincias);
		}
	}

	function getCiudades($post)
	{
		$id_pais = $post['id_pais'];
		$id_provincia = $post['id_provincia'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_municipio @id_pais = ?, @id_provincia = ?";
		$params = array($id_pais, $id_provincia);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 4003. ';
			Errores::insErrorDeQuery('Error 4003. ', $sql, $params);
		}else{
			$ciudades = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miCiudad = new Localidades();
				$miCiudad->id_pais = $row['id_pais'];
				$miCiudad->id_provincia = $row['id_provincia'];
				$miCiudad->id_ciudad = $row['id_ciudad'];
				$miCiudad->ciudad = utf8_encode($row['ciudad']);

				$ciudades[$i] = $miCiudad;
				$i++;
			}			

			Conection::CerrarConexion($conn);
			echo json_encode($ciudades);
		}
	}
}

?>