<?php 
/**
* 
*/
require_once 'class.helpers.php';

class Usuarios
{
	
	function login($post)
	{
		$usuario = $post['usuario'];
		$password = $post['password'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_login @usuario = ?, @password = ?";
		$params = array($usuario, $password);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1001. ';
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			
			$response = new Usuarios();
			$response->mensaje = utf8_encode($row['mensaje']);
			$response->cod_mensaje = $row['cod_mensaje'];

			if($response->cod_mensaje != -1){
				echo json_encode($response);
				return;
			}

			$id_usuario   = $row['id_usuario'];
			$tipo_usuario = $row['id_tipo_usuario'];
			$nombre = utf8_encode($row['nombre']);

			setcookie('id_usuario', Encriptacion::encriptar($id_usuario), time()+31556926 ,'/' );
			setcookie('tipo_usuario', Encriptacion::encriptar($tipo_usuario), time()+31556926 ,'/' );
			setcookie('nombre', $nombre, time()+31556926 ,'/' );

			switch ($tipo_usuario) 
			{
				case 1:
					$response->redirect = 'gestion-usuarios';
					break;
				
				case 2:
					$response->redirect = 'home-mentorDT';
					break;

				case 3:
					$response->redirect = 'home-mentorCON';
					break;

				case 4:
					$response->redirect = 'home-DT';
					break;

				case 5:
					$response->redirect = 'home-CON';
					break;
			}

			Conection::CerrarConexion($conn);
			echo json_encode($response);
			return;
		}
	}

	function getUsuarios()
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_usuarios_lista @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1002. ';
			Errores::insErrorDeQuery('Error 1002. ', $sql, $params);
		}else{
			$usuarios = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miUsuario = new Usuarios();
				$miUsuario->id_usuario = $row['id_usuario'];
				$miUsuario->nombre = utf8_encode($row['nombre']);
				$miUsuario->cargo = utf8_encode($row['cargo']);
				$miUsuario->ult_login = $row['ult_login'] == null ? 'Sin actividad' : $row['ult_login']->format('d/m/y');
				$miUsuario->sesiones = $row['sesiones'];
				$miUsuario->email = utf8_encode($row['email']);
				$miUsuario->password = utf8_encode($row['password']);
				$miUsuario->estado = $row['estado'];

				$usuarios[$i] = $miUsuario;
				$i++;
			}			

			Conection::CerrarConexion($conn);
			echo json_encode($usuarios);
		}
	}

	function getMentoresEnFormacion($post)
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_mentores_en_formacion @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1003. ';
			Errores::insErrorDeQuery('Error 1003. ', $sql, $params);
		}else{
			$usuarios = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miUsuario = new Usuarios();
				$miUsuario->id_usuario = $row['id_usuario'];
				$miUsuario->nombre = utf8_encode($row['nombre']);
				$miUsuario->id_tipo_usuario = $row['id_tipo_usuario'];
				$miUsuario->email = utf8_encode($row['email']);
				$miUsuario->provincia = utf8_encode($row['provincia']);
				$miUsuario->ciudad = utf8_encode($row['ciudad']);

				$usuarios[$i] = $miUsuario;
				$i++;
			}			

			Conection::CerrarConexion($conn);
			echo json_encode($usuarios);
		}
	}


	/********************************************************************************************************************************************************/

	function insMentoresExcel($file)
	{
		//guardo el excel en la carpeta "archivos_excel/"
		$ruta = 'files/';
		$archivoExcel = $file['name'];
		$archivoExcel_tmp = $file['tmp_name'];
		move_uploaded_file($archivoExcel_tmp, $ruta.$archivoExcel);

		$extention = pathinfo($ruta.$archivoExcel,PATHINFO_EXTENSION);

		$rString = Helpers::generateRandomString();
		$file = $ruta.$rString.'.'.$extention;
		rename($ruta.$archivoExcel, $file);

		include_once(__DIR__.'/../assets/reader.php');

		$connection = new Spreadsheet_Excel_Reader(); // our main object
		if(is_bool( $connection->read($file) ) )
		{
			unlink($file);
			die();
		}

		$totalRows = $connection->sheets[0]['numRows'];
		$totalCols = $connection->sheets[0]['numCols'];

		$response = array();
		$j = 0;

		for ($i=2; $i <= $totalRows; $i++)
		{
			if( !isset($connection->sheets[0]["cells"][$i][1]) )
				break;

			if ($connection->sheets[0]["cells"][$i][1] == 'DATE') 
				$i++;

			$date                      = $connection->sheets[0]["cells"][$i][1];
				$dateTime = new DateTime("1899-12-30 + $date days");
				$date = $dateTime->format("Ymd");
			$nombre                    = $connection->sheets[0]["cells"][$i][2];
			$tipo_dni                  = $connection->sheets[0]["cells"][$i][3];
			$dni                       = $connection->sheets[0]["cells"][$i][4];
			$nacimiento                = $connection->sheets[0]["cells"][$i][5];
				$nacTime = new DateTime("1899-12-30 + $nacimiento days");
				$nacimiento = $nacTime->format("Ymd");
			$sexo                      = $connection->sheets[0]["cells"][$i][6];
			$pais                      = $connection->sheets[0]["cells"][$i][7];
			$provincia                 = $connection->sheets[0]["cells"][$i][8];
			$ciudad                    = $connection->sheets[0]["cells"][$i][9];
			$direccion                 = $connection->sheets[0]["cells"][$i][10];
			$cp                        = $connection->sheets[0]["cells"][$i][11];
			$tel                       = $connection->sheets[0]["cells"][$i][12];
			$cel                       = $connection->sheets[0]["cells"][$i][13];
			$mail                      = $connection->sheets[0]["cells"][$i][14];
			$skype                     = $connection->sheets[0]["cells"][$i][15];
			$linkedin                  = $connection->sheets[0]["cells"][$i][16];
			$comentarios               = $connection->sheets[0]["cells"][$i][17];
			$estudio                   = $connection->sheets[0]["cells"][$i][18];
			$situacion_laboral         = $connection->sheets[0]["cells"][$i][19];
			$desc_laboral              = $connection->sheets[0]["cells"][$i][20];
			$nombre_emprendimiento     = $connection->sheets[0]["cells"][$i][21];
			$tiempo_emprendimiento     = $connection->sheets[0]["cells"][$i][22];
			$empleados                 = $connection->sheets[0]["cells"][$i][23];
			$pagina_web                = $connection->sheets[0]["cells"][$i][24];
			$actividad_tiempo_libre    = $connection->sheets[0]["cells"][$i][25];
			$sn_emprendio              = $connection->sheets[0]["cells"][$i][26];
			$sn_emprendimiento_activo  = $connection->sheets[0]["cells"][$i][27];
			$desc_emprendimiento       = $connection->sheets[0]["cells"][$i][28];
			$experiencia_mentor        = $connection->sheets[0]["cells"][$i][29];
			$desc_experiencia_mentor   = $connection->sheets[0]["cells"][$i][30];
			$motivacion                = $connection->sheets[0]["cells"][$i][31];
			$aporte_al_programa        = $connection->sheets[0]["cells"][$i][32];
			$tipo_emprendimiento       = $connection->sheets[0]["cells"][$i][33];
			$tiempo_al_programa        = $connection->sheets[0]["cells"][$i][34];
			$como_se_entero            = $connection->sheets[0]["cells"][$i][35];
			$rol                       = $connection->sheets[0]["cells"][$i][36];

			$arrayName = array( 
								'linea'					  => $i,
								'date'                    => $date,
								'nombre'                  => utf8_encode($nombre),
								'tipo_dni'                => $tipo_dni,
								'dni'                     => $dni,
								'nacimiento'              => $nacimiento,
								'sexo'                    => $sexo,
								'pais'                    => utf8_encode($pais),
								'provincia'               => utf8_encode($provincia),
								'ciudad'                  => utf8_encode($ciudad),
								'direccion'               => utf8_encode($direccion),
								'cp'                      => $cp,
								'tel'                     => $tel,
								'cel'                     => $cel,
								'mail'                    => $mail,
								'skype'                   => $skype,
								'linkedin'                => $linkedin,
								'comentarios'             => utf8_encode($comentarios),
								'estudio'                 => utf8_encode($estudio),
								'situacion_laboral'       => utf8_encode($situacion_laboral),
								'desc_laboral'            => utf8_encode($desc_laboral),
								'nombre_emprendimiento'   => utf8_encode($nombre_emprendimiento),
								'tiempo_emprendimiento'   => utf8_encode($tiempo_emprendimiento),
								'empleados'               => $empleados,
								'pagina_web'              => $pagina_web,
								'actividad_tiempo_libre'  => utf8_encode($actividad_tiempo_libre),
								'sn_emprendio'            => utf8_encode($sn_emprendio),
								'sn_emprendimiento_activo'=> utf8_encode($sn_emprendimiento_activo),
								'desc_emprendimiento'     => utf8_encode($desc_emprendimiento),
								'experiencia_mentor'      => utf8_encode($experiencia_mentor),
								'desc_experiencia_mentor' => utf8_encode($desc_experiencia_mentor),
								'motivacion'              => utf8_encode($motivacion),
								'aporte_al_programa'      => utf8_encode($aporte_al_programa),
								'tipo_emprendimiento'     => utf8_encode($tipo_emprendimiento),
								'tiempo_al_programa'      => $tiempo_al_programa,
								'como_se_entero'          => utf8_encode($como_se_entero),
								'rol'                     => utf8_encode($rol),
									);
			$conn = Conection::conexion();

			//print_r($arrayName);

			$respuesta = Self::insUsuario($i, $date, $nombre, $tipo_dni, $dni, $nacimiento, $sexo, $pais, $provincia, $ciudad, $direccion, $cp, $tel, $cel, $mail, $skype, $linkedin, $comentarios, $estudio, $situacion_laboral, $desc_laboral, $nombre_emprendimiento, $tiempo_emprendimiento, $empleados, $pagina_web, $actividad_tiempo_libre, $sn_emprendio, $sn_emprendimiento_activo, $desc_emprendimiento, $experiencia_mentor, $desc_experiencia_mentor, $motivacion, $aporte_al_programa, $tipo_emprendimiento, $tiempo_al_programa, $como_se_entero, $rol);
			
			$response[$j] = $respuesta;
			$j++;
		}
		echo json_encode($response);
	}

	function insUsuario($linea, $date, $nombre, $tipo_dni, $dni, $nacimiento, $sexo, $pais, $provincia, $ciudad, $direccion, $cp, $tel, $cel, $mail, $skype, $linkedin, $comentarios, $estudio, $situacion_laboral, $desc_laboral, $nombre_emprendimiento, $tiempo_emprendimiento, $empleados, $pagina_web, $actividad_tiempo_libre, $sn_emprendio, $sn_emprendimiento_activo, $desc_emprendimiento, $experiencia_mentor, $desc_experiencia_mentor, $motivacion, $aporte_al_programa, $tipo_emprendimiento, $tiempo_al_programa, $como_se_entero, $rol)
	{
		$conn = Conection::Conexion();
		$sql = "EXEC sp_ins_mentor @date = ?, @nombre = ?, @tipo_dni = ?, @dni = ?, @nacimiento = ?, @sexo = ?, @pais = ?, @provincia = ?, @ciudad = ?, @direccion = ?, @cp = ?, @tel = ?, @cel = ?, @mail = ?, @skype = ?, @linkedin = ?, @comentarios = ?, @estudio = ?, @situacion_laboral = ?, @desc_laboral = ?, @nombre_emprendimiento = ?, @tiempo_emprendimiento = ?, @empleados = ?, @pagina_web = ?, @actividad_tiempo_libre = ?, @sn_emprendio = ?, @sn_emprendimiento_activo = ?, @desc_emprendimiento = ?, @experiencia_mentor = ?, @desc_experiencia_mentor = ?, @motivacion = ?, @aporte_al_programa = ?, @tipo_emprendimiento = ?, @tiempo_al_programa = ?, @como_se_entero = ?, @rol = ?";
		$params = array($date, $nombre, $tipo_dni, $dni, $nacimiento, $sexo, $pais, $provincia, $ciudad, $direccion, $cp, $tel, $cel, $mail, $skype, $linkedin, $comentarios, $estudio, $situacion_laboral, $desc_laboral, $nombre_emprendimiento, $tiempo_emprendimiento, $empleados, $pagina_web, $actividad_tiempo_libre, $sn_emprendio, $sn_emprendimiento_activo, $desc_emprendimiento, $experiencia_mentor, $desc_experiencia_mentor, $motivacion, $aporte_al_programa, $tipo_emprendimiento, $tiempo_al_programa, $como_se_entero, $rol);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			return 'Error 1101. ';
			Errores::insErrorDeQuery('Error 1101. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$response = new Usuarios();
			$response->cod_mensaje = $row['cod_mensaje'];
			$response->mensaje = utf8_encode($row['mensaje']);
			$response->linea = $linea;

			Conection::CerrarConexion($conn);
			return $response;
		}
	}

	function bloquearUsuario($post)
	{
		$id_usuario = $post['id_usuario'];
		$id_admin = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_upd_bloquear_usuario @id_usuario = ?, @id_admin = ?";
		$params = array($id_usuario, $id_admin);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			return 'Error 1201. ';
			Errores::insErrorDeQuery('Error 1201. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$response = new Usuarios();
			$response->cod_mensaje = $row['cod_mensaje'];
			$response->mensaje = utf8_encode($row['mensaje']);

			Conection::CerrarConexion($conn);
			echo json_encode( $response );
		}
	}

	function desbloquearUsuario($post)
	{
		$id_usuario = $post['id_usuario'];
		$id_admin = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_upd_desbloquear_usuario @id_usuario = ?, @id_admin = ?";
		$params = array($id_usuario, $id_admin);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			return 'Error 1202. ';
			Errores::insErrorDeQuery('Error 1202. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$response = new Usuarios();
			$response->cod_mensaje = $row['cod_mensaje'];
			$response->mensaje = utf8_encode($row['mensaje']);

			Conection::CerrarConexion($conn);
			echo json_encode( $response );
		}
	}

	function logout()
	{
		Cookies::destroyCookies();
	}
}
?>