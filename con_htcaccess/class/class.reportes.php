<?php 
/**
* 
*/
require_once 'class.helpers.php';

class Reportes
{
	
	function getEstadisticasNumeros($post)
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_estadisticas_numeros @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 5001. ';
			Errores::insErrorDeQuery('Error 5001. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$estadisticas = new Reportes();
			$estadisticas->mentores_formacion = $row['mentores_formacion'];
			$estadisticas->mentores_formados = $row['mentores_formados'];
			$estadisticas->mentorias_activas = $row['mentorias_activas'];
			$estadisticas->emprendedores_en_proceso = $row['emprendedores_en_proceso'];
			$estadisticas->emprendedores_egresados = $row['emprendedores_egresados'];
			$estadisticas->alcance_territorial = $row['alcance_territorial'];
			$estadisticas->atencion_por_participante = $row['atencion_por_participante'];
			$estadisticas->impacto_grado_satisfaccion = $row['impacto_grado_satisfaccion'];
			$estadisticas->generoM = $row['generoM'];
			$estadisticas->generoF = $row['generoF'];
			$estadisticas->diversidad_productiva = $row['diversidad_productiva'];
	
			Conection::CerrarConexion($conn);
			echo json_encode($estadisticas);
		}
	}

	function getReporteGenero($post)
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_reporte_genero @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 5003. ';
			Errores::insErrorDeQuery('Error 5002. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$reporte = new Reportes();

			$reporte->mentoresM = $row['mentoresM'];
			$reporte->mentoresF = $row['mentoresF'];
			$reporte->emprendedoresM = $row['emprendedoresM'];
			$reporte->emprendedoresF = $row['emprendedoresF'];
			$reporte->mentoresConM = $row['mentoresConM'];
			$reporte->mentoresConF = $row['mentoresConF'];
			$reporte->emprendedoresConM = $row['emprendedoresConM'];
			$reporte->emprendedoresConF = $row['emprendedoresConF'];
			$reporte->mentoresDtM = $row['mentoresDtM'];
			$reporte->mentoresDtF = $row['mentoresDtF'];
			$reporte->emprendedoresDtM = $row['emprendedoresDtM'];
			$reporte->emprendedoresDtF = $row['emprendedoresDtF'];

			Conection::CerrarConexion($conn);
			echo json_encode($reporte);
		}
	}

	function getMentoresEnFormacion($post)
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_mentores_en_formacion @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 5003. ';
			Errores::insErrorDeQuery('Error 5003. ', $sql, $params);
		}else{
			$usuarios = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miUsuario = new Reportes();
				$miUsuario->id_usuario = $row['id_usuario'];
				$miUsuario->nombre = utf8_encode($row['nombre']);
				$miUsuario->id_tipo_usuario = $row['id_tipo_usuario'];
				$miUsuario->email = utf8_encode($row['email']);
				$miUsuario->provincia = utf8_encode($row['provincia']);
				$miUsuario->ciudad = utf8_encode($row['ciudad']);

				$usuarios[$i] = $miUsuario;
				$i++;
			}			

			Conection::CerrarConexion($conn);
			echo json_encode($usuarios);
		}
	}

	function getMentoresFormados($post)
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_mentores_formados @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 5004. ';
			Errores::insErrorDeQuery('Error 5004. ', $sql, $params);
		}else{
			$usuarios = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miUsuario = new Reportes();
				$miUsuario->id_usuario = $row['id_usuario'];
				$miUsuario->nombre = utf8_encode($row['nombre']);
				$miUsuario->id_tipo_usuario = $row['id_tipo_usuario'];
				$miUsuario->email = utf8_encode($row['email']);
				$miUsuario->provincia = utf8_encode($row['provincia']);
				$miUsuario->ciudad = utf8_encode($row['ciudad']);

				$usuarios[$i] = $miUsuario;
				$i++;
			}			

			Conection::CerrarConexion($conn);
			echo json_encode($usuarios);
		}
	}

	function getMentoriasActivas($post)
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_mentorias_activas @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 5005. ';
			Errores::insErrorDeQuery('Error 5005. ', $sql, $params);
		}else{
			$grupos = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miGrupo = new Reportes();
				$miGrupo->mentor = utf8_encode($row['mentor']);
				$miGrupo->emprendedores = utf8_encode($row['emprendedores']);
				$miGrupo->grupo = utf8_encode($row['grupo']);
				$miGrupo->id_tipo_usuario = $row['id_tipo_usuario'];

				$grupos[$i] = $miGrupo;
				$i++;
			}			

			Conection::CerrarConexion($conn);
			echo json_encode($grupos);
		}
	}

	function getEmprendedoresEgresados($post)
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_emprendedores_egresados @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 5006. ';
			Errores::insErrorDeQuery('Error 5006. ', $sql, $params);
		}else{
			$usuarios = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miUsuario = new Reportes();
				$miUsuario->id_usuario = $row['id_usuario'];
				$miUsuario->id_tipo_usuario = $row['id_tipo_usuario'];
				$miUsuario->nombre = utf8_encode($row['nombre']);
				$miUsuario->email = utf8_encode($row['email']);
				$miUsuario->pais = utf8_encode($row['pais']);
				$miUsuario->provincia = utf8_encode($row['provincia']);
				$miUsuario->ciudad = utf8_encode($row['ciudad']);

				$usuarios[$i] = $miUsuario;
				$i++;
			}			

			Conection::CerrarConexion($conn);
			echo json_encode($usuarios);
		}
	}

}
?>