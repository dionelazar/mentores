<?php 
/**
* 
*/
class Errores
{
	function insErrorDeQuery($mensaje, $query, $parametros)
	{
		$id_usuario = Cookies::getIdUsuario();
		
		$conn = Conection::Conexion();
		$sql = "EXEC sp_ins_error @mensaje = ?, @tipo_error = ?, @query = ?, @params = ?, @id_usuario = ?";
		$params = array($mensaje, 'Error de query', $query, $parametros, $id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 001. '.$sql.'<br>';
			print_r($params);
		}else{
			Conection::CerrarConexion($conn);
			return;
		}
	}
}
?>