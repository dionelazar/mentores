CREATE PROC [dbo].[sp_get_calificaciones_emprendedor]
(
	 @id_usuario INT
	,@id_emprendedor INT
)
AS BEGIN
SET NOCOUNT ON
	IF EXISTS(SELECT 1 FROM calificaciones WHERE id_emprendedor = @id_emprendedor) BEGIN
		SELECT   '' 'mensaje'
				,-1 'cod_mensaje'
				,c.id_sesion 'id_sesion'
				,c.id_grupo 'id_grupo'
				,c.txt_comentario 'comentario'
				,c.puntuacion 'puntuacion'
				,g.txt_desc 'grupo'
		FROM calificaciones c
		JOIN grupos g ON g.id_grupo = c.id_grupo
		WHERE id_emprendedor = @id_emprendedor 
		  AND id_usuario = @id_usuario
		ORDER BY id_sesion
	END ELSE BEGIN
		SELECT   'No hay calificaciones del emprendedor' 'mensaje'
				,0 'cod_mensaje'
	END
END

GO

ALTER PROC [dbo].[sp_ins_calificacion]
(
	 @id_usuario INT
	,@id_sesion INT
	,@id_grupo INT
	,@stars FLOAT
	,@comentario VARCHAR(MAX)
	,@id_emprendedor INT
)
AS BEGIN
SET NOCOUNT ON
	DECLARE @id_calificacion INT
	DECLARE @hoy SMALLDATETIME

	SELECT @id_calificacion = ISNULL(MAX(id_calificacion), 0) + 1 FROM calificaciones
	SET @hoy = GETDATE()
	
	IF EXISTS(SELECT 1 FROM calificaciones WHERE id_usuario = @id_usuario AND id_grupo = @id_grupo AND id_sesion = @id_sesion AND id_emprendedor = @id_emprendedor) BEGIN
		SELECT  0  'cod_mensaje'
		      ,'Ya has calificado esta sesión' 'mensaje'
	END ELSE BEGIN
		-- si no existe una calificacion de la sesión y grupo
		-- o el n° de sesión es exactamente 1 mayor al último calificado
		IF     @id_sesion = 1 --NOT EXISTS(SELECT 1 FROM calificaciones WHERE id_grupo = @id_grupo AND id_sesion = @id_sesion) 
			OR @id_sesion = (SELECT MAX(id_sesion) + 1 FROM calificaciones WHERE id_grupo = @id_grupo AND id_usuario = @id_usuario)
		BEGIN
			INSERT INTO calificaciones (id_calificacion, id_usuario, id_sesion, id_grupo, puntuacion, txt_comentario, url_imagen, fecha, id_emprendedor)
							    VALUES (@id_calificacion, @id_usuario, @id_sesion, @id_grupo, @stars, @comentario, null, @hoy, @id_emprendedor)

			SELECT  -1  'cod_mensaje'
				   ,'Se cargó correctamente la calificación. ¡Gracias!' 'mensaje'
		END ELSE BEGIN
			  SELECT  0  'cod_mensaje'
					,'Tienes que calificar las anteriores sesiones' 'mensaje'
		END
	END
END

GO

ALTER PROC [dbo].[sp_get_form]
(
	 @id_usuario INT
	,@id_sesion INT
	,@id_grupo INT
	,@id_emprendedor INT
)
AS BEGIN
SET NOCOUNT ON
	DECLARE @id_tipo_usuario INT

	SELECT @id_tipo_usuario = id_tipo_usuario FROM usuarios WHERE id_usuario = @id_usuario
	IF		   @id_sesion = 1 --NOT EXISTS(SELECT 1 FROM calificaciones WHERE id_grupo = @id_grupo AND id_sesion = @id_sesion) 
			OR @id_sesion = (SELECT MAX(id_sesion) + 1 FROM calificaciones WHERE id_grupo = @id_grupo AND id_usuario = @id_usuario AND id_emprendedor = @id_emprendedor) BEGIN
		IF @id_tipo_usuario = 2 BEGIN -- mentor dt
			IF @id_sesion IN (1,2,3) BEGIN
				SELECT p.id_pregunta
					  ,p.num_pregunta
					  ,p.txt_desc 'pregunta'
					  ,-1 'cod_mensaje'
				FROM preguntas p
				WHERE id_pregunta in (7, 8)
			END ELSE IF @id_sesion IN (4,5,6) BEGIN
				SELECT p.id_pregunta
					  ,p.num_pregunta
					  ,p.txt_desc 'pregunta'
					  ,-1 'cod_mensaje'
				FROM preguntas p
				WHERE id_pregunta in (9, 10)
			END

		END ELSE IF @id_tipo_usuario = 3 BEGIN -- mentor con
			SELECT p.id_pregunta
				  ,p.num_pregunta
				  ,p.txt_desc 'pregunta'
				  ,-1 'cod_mensaje'
			FROM preguntas p
			WHERE id_pregunta in (11, 12)

		END ELSE IF @id_tipo_usuario = 4 BEGIN -- DT
			IF @id_sesion IN (1,2,3) BEGIN
				SELECT p.id_pregunta
					  ,p.num_pregunta
					  ,p.txt_desc 'pregunta'
					  ,-1 'cod_mensaje'
				FROM preguntas p
				WHERE id_pregunta in (1, 2)
			END ELSE IF @id_sesion IN (4,5,6) BEGIN
				SELECT p.id_pregunta
					  ,p.num_pregunta
					  ,p.txt_desc 'pregunta'
					  ,-1 'cod_mensaje'
				FROM preguntas p
				WHERE id_pregunta in (3, 4)
			END
		END ELSE IF @id_tipo_usuario = 5 BEGIN -- con
			SELECT p.id_pregunta
				  ,p.num_pregunta
				  ,p.txt_desc 'pregunta'
				  ,-1 'cod_mensaje'
			FROM preguntas p
			WHERE id_pregunta in (5, 6)
		END
	END ELSE BEGIN
		SELECT  0  'cod_mensaje'
			  ,'Tienes que calificar las sesiones anteriores para avanzar' 'mensaje'
	END
END

GO

ALTER PROC [dbo].[sp_get_sesiones_mentor_con]
(
	@id_usuario INT
)
AS BEGIN
SET NOCOUNT ON
	DECLARE @id_grupo INT

	SELECT @id_grupo = id_grupo FROM usuarios WHERE id_usuario = @id_usuario

	IF @id_grupo IS NULL BEGIN
		SELECT 'El usuario no tiene un grupo asignado' 'mensaje'
		      ,0 'cod_mensaje'
	END ELSE BEGIN
		SELECT '' 'mensaje'
		      ,-1 'cod_mensaje'
			  ,s.id_sesion 'id_sesion'
			  ,s.id_grupo 'id_grupo'
			  ,s.txt_desc 'desc'
			  ,(SELECT  Distinct Stuff((Select ', ' + txt_nombre From usuarios WHERE id_grupo = s.id_grupo AND id_tipo_usuario = 5 For Xml Path ('')), 1, 1, '')) 'emprendedores'
			  ,ISNULL((SELECT TOP 1 -1 FROM calificaciones WHERE id_usuario = @id_usuario AND id_sesion = s.id_sesion AND id_grupo = s.id_grupo), 0)'sn_califico'
			  --,falaría el tema de las calificaciones
		FROM sesiones s
		WHERE id_grupo in (SELECT id_grupo FROM grupos WHERE id_mentor = @id_usuario)
		ORDER BY id_grupo, id_sesion
	END
END

GO

ALTER PROC [dbo].[sp_get_sesiones_mentor_DT]
(
	@id_usuario INT
)
AS BEGIN
SET NOCOUNT ON
	DECLARE @id_grupo INT

	SELECT @id_grupo = id_grupo FROM usuarios WHERE id_usuario = @id_usuario

	IF @id_grupo IS NULL BEGIN
		SELECT 'El usuario no tiene un grupo asignado' 'mensaje'
		      ,0 'cod_mensaje'
	END ELSE BEGIN
		SELECT '' 'mensaje'
		      ,-1 'cod_mensaje'
			  ,s.id_sesion 'id_sesion'
			  ,s.id_grupo 'id_grupo'
			  ,s.txt_desc 'desc'
			  ,(SELECT  Distinct Stuff((Select ', ' + txt_nombre From usuarios WHERE id_grupo = s.id_grupo AND id_tipo_usuario = 4 For Xml Path ('')), 1, 1, '')) 'emprendedores'
			  ,ISNULL((SELECT TOP 1 -1 FROM calificaciones WHERE id_usuario = @id_usuario AND id_sesion = s.id_sesion AND id_grupo = s.id_grupo), 0)'sn_califico'
			  --,falaría el tema de las calificaciones
		FROM sesiones s
		WHERE id_grupo in (SELECT id_grupo FROM grupos WHERE id_mentor = @id_usuario)
		ORDER BY id_grupo, id_sesion
	END
END

GO


ALTER PROC [dbo].[sp_get_sesiones_DT]
(
	@id_usuario INT
)
AS BEGIN
SET NOCOUNT ON
	DECLARE @id_grupo INT
	DECLARE @id_mentor INT
	DECLARE @nombre_mentor VARCHAR(200)

	SELECT @id_grupo = id_grupo FROM usuarios WHERE id_usuario = @id_usuario

	IF @id_grupo IS NULL BEGIN
		SELECT 'El usuario no tiene un grupo asignado' 'mensaje'
		      ,0 'cod_mensaje'
	END ELSE BEGIN
		SELECT @id_mentor = id_usuario, @nombre_mentor = txt_nombre FROM  usuarios WHERE id_grupo = @id_grupo AND id_tipo_usuario = 2

		SELECT '' 'mensaje'
		      ,-1 'cod_mensaje'
			  ,s.id_sesion 'id_sesion'
			  ,s.id_grupo 'id_grupo'
			  ,s.txt_desc 'desc'
			  ,ISNULL((SELECT TOP 1 txt_comentario FROM calificaciones WHERE id_sesion = s.id_sesion AND id_grupo = s.id_grupo AND id_usuario = @id_mentor), '') 'comentario'
			  ,@nombre_mentor 'mentor'
			  ,ISNULL((SELECT TOP 1 -1 FROM calificaciones WHERE id_usuario = @id_usuario AND id_sesion = s.id_sesion AND id_grupo = s.id_grupo), 0)'sn_califico'
			  --,faltaría el tema de las calificaciones
		FROM sesiones s
		WHERE id_grupo in (@id_grupo)
		ORDER BY id_grupo, id_sesion
	END
END

GO

ALTER PROC [dbo].[sp_get_sesiones_CON]
(
	@id_usuario INT
)
AS BEGIN
SET NOCOUNT ON
	DECLARE @id_grupo INT
	DECLARE @id_mentor INT
	DECLARE @nombre_mentor VARCHAR(200)

	SELECT @id_grupo = id_grupo FROM usuarios WHERE id_usuario = @id_usuario

	IF @id_grupo IS NULL BEGIN
		SELECT 'El usuario no tiene un grupo asignado' 'mensaje'
		      ,0 'cod_mensaje'
	END ELSE BEGIN
		SELECT @id_mentor = id_usuario, @nombre_mentor = txt_nombre FROM  usuarios WHERE id_grupo = @id_grupo AND id_tipo_usuario = 3

		SELECT '' 'mensaje'
		      ,-1 'cod_mensaje'
			  ,s.id_sesion 'id_sesion'
			  ,s.id_grupo 'id_grupo'
			  ,s.txt_desc 'desc'
			  ,ISNULL((SELECT TOP 1 txt_comentario FROM calificaciones WHERE id_sesion = s.id_sesion AND id_grupo = s.id_grupo AND id_usuario = @id_mentor), '') 'comentario'
			  ,@nombre_mentor 'mentor'
			  ,ISNULL((SELECT TOP 1 -1 FROM calificaciones WHERE id_usuario = @id_usuario AND id_sesion = s.id_sesion AND id_grupo = s.id_grupo), 0)'sn_califico'
			  --,faltaría el tema de las calificaciones
		FROM sesiones s
		WHERE id_grupo in (@id_grupo)
		ORDER BY id_grupo, id_sesion
	END
END

GO

UPDATE calificaciones SET id_emprendedor = id_usuario WHERE id_usuario IN (SELECT u.id_usuario 
																			FROM calificaciones c
																			JOIN usuarios u ON c.id_usuario = u.id_usuario
																			WHERE u.id_tipo_usuario IN (4,5))

GO