<?php 
    require __DIR__.'/share/header-mentor-con.php';
?>
    <div class="content programa">
        <!-- TODO EL CONTENIDO (LO QUE NO ES MENÚ NI HEADER) -->
        <div class="wrapper">
            <div class="col100">
                <ul class="tabs tabs-2 oneTab clearfix">
                    <li class="current">
                        <a href="#0">
                            <h2>Programa de Mentorías</h2>
                        </a>
                    </li>
                </ul>
                <div class="container home">
                    <div class="sesionA show">
                        <div class="timeline-item" name='Primera Fase'>
                            <h3>• Cada mentor tiene asignado: <strong><br class="visibleMobile">4 emprendedores.</strong></h3>
                            <img src="img/icon10.png" alt="">
                            <h3>• Cada emprendedor tendrá: <strong><br class="visibleMobile">3 encuentros (mensuales) de 2 hs.</strong></h3>
                            <div class="images">
                                <img src="img/icon8.png" alt="">
                                <img src="img/icon8.png" alt="">
                                <img src="img/icon8.png" alt="">
                            </div>
                            <h3>• Cada mentor tendrá: <strong><br class="visibleMobile">2 hs de seguimiento acompañamiento mensual</strong> con el responsable de sede.</h3>
                            <img src="img/icon6.png" alt="">
                        </div>
                        <div class="timeline-item" name='Segunda Fase'>
                            <h3>• Cada mentor tiene asignado: <strong><br class="visibleMobile">2 emprendedores.</strong></h3>
                            <img src="img/icon5.png" alt="">
                            <h3>• Cada emprendedor tendrá: <strong><br class="visibleMobile">3 encuentros (mensuales) de 2 hs.</strong></h3>
                            <div class="images">
                                <img src="img/icon8.png" alt="">
                                <img src="img/icon8.png" alt="">
                                <img src="img/icon8.png" alt="">
                            </div>
                            <h3>• Cada mentor tendrá: <strong><br class="visibleMobile">2 hs de seguimiento acompañamiento mensual con el responsable de sede.</strong></h3>
                            <img src="img/icon6.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/masonry.pkgd.min.js"></script>
    <script src="js/select2.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="js/main.js"></script>
</body>

</html>