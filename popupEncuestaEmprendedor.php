<div id="popupEncuesta" class="absolute-center" style="" hidden="">
    <div id="overlayCalificacion"></div>
    <div class="popupCalificacion popupSatisfaccion">
        <a href="#" id="closeBtn" class="closePopUp">
            <i class="close-icon" data-icon="9"></i>
        </a>
        <h3><strong>Bienvenido a la Red de Mentores. Te pedimos que completes un Cuestionario antes de que empieces el proceso de mentoría</strong></h3>
            <label for="industria">Industria</label>
            <select name="insdustria" id="industria">

            </select>
            <!--label for="Ventas anuales">Ventas anuales 2017:
                <input type="number" name="Ventas anuales" id="Ventas-anuales" placeholder="Ventas anuales">
            </label>
            <label for="Cantidad de clientes">Cantidad de clientes:
                <input type="number" name="Cantidad de clientes" id="cantidad-clientes" placeholder="Cantidad de clientes">
            </label>
            <label for="Cantidad de empleados">Cantidad de empleados:
                <input type="number" name="Cantidad de empleados" id="cantidad-empleados" placeholder="Cantidad de empleados">
            </label-->
        <br>
        <label for="propuesta-valor">¿Has definido la propuesta de valor diferencial de tu producto/servicio?</label>
        <fieldset class="label100">
            <input type="radio" name="propuesta-valor" id="propuesta-valor-1" value="1">
            <label for="propuesta-valor-1">a)  No lo he pensado aún.</label>
            <input type="radio" name="propuesta-valor" id="propuesta-valor-2" value="2">
            <label for="propuesta-valor-2">b)  No sé cómo encarar este tema.</label>
            <input type="radio" name="propuesta-valor" id="propuesta-valor-4" value="3">
            <label for="propuesta-valor-4">c)  Estoy trabajando en eso.</label>
            <input type="radio" name="propuesta-valor" id="propuesta-valor-5" value="4">
            <label for="propuesta-valor-5">d)  Sí, aunque requeriría actualización o revisión. </label>
            <input type="radio" name="propuesta-valor" id="propuesta-valor-6" value="5">
            <label for="propuesta-valor-6">e)  Si, ya lo tengo hecho.  </label>
        </fieldset>
        <br>

        <label for="plan-negocio">¿Tenés un plan de negocios completo?</label>
        <fieldset class="label100">
            <input type="radio" name="plan-negocio" id="plan-negocio-1" value="1">
            <label for="plan-negocio-1">a)  No lo he pensado aún.</label>
            <input type="radio" name="plan-negocio" id="plan-negocio-2" value="2">
            <label for="plan-negocio-2">b)  No sé cómo encarar este tema.</label>
            <input type="radio" name="plan-negocio" id="plan-negocio-4" value="3">
            <label for="plan-negocio-4">c)  Estoy trabajando en eso.</label>
            <input type="radio" name="plan-negocio" id="plan-negocio-5" value="4">
            <label for="plan-negocio-5">d)  Sí, aunque requeriría actualización o revisión.</label>
            <input type="radio" name="plan-negocio" id="plan-negocio-6" value="5">
            <label for="plan-negocio-6">e)  Si, ya lo tengo hecho. </label>
        </fieldset>
        <br>

        <label for="calendario">¿Has definido un calendario de actividades clave a llevar a cabo en 2018?</label>
        <fieldset class="label100">
            <input type="radio" name="calendario" id="calendario-1" value="1">
            <label for="calendario-1">a)  No lo he pensado aún.</label>
            <input type="radio" name="calendario" id="calendario-2" value="2">
            <label for="calendario-2">b)  No sé cómo encarar este tema.</label>
            <input type="radio" name="calendario" id="calendario-4" value="3">
            <label for="calendario-4">c)  Estoy trabajando en eso.</label>
            <input type="radio" name="calendario" id="calendario-5" value="4">
            <label for="calendario-5">d)  Sí, aunque requeriría actualización o revisión.</label>
            <input type="radio" name="calendario" id="calendario-6" value="5">
            <label for="calendario-6">e)  Si, ya lo tengo hecho. </label>
        </fieldset>
        <br>

        <label for="estructura-legal">¿Has definido la estructura legal de tu organización?</label>
        <fieldset class="label100">
            <input type="radio" name="estructura-legal" id="estructura-legal-1" value="1">
            <label for="estructura-legal-1">a)  No lo he pensado aún.</label>
            <input type="radio" name="estructura-legal" id="estructura-legal-2" value="2">
            <label for="estructura-legal-2">b)  No sé cómo encarar este tema.</label>
            <input type="radio" name="estructura-legal" id="estructura-legal-4" value="3">
            <label for="estructura-legal-4">c)  Estoy trabajando en eso.</label>
            <input type="radio" name="estructura-legal" id="estructura-legal-5" value="4">
            <label for="estructura-legal-5">d)  Si, ya he formalizado mi emprendimiento. </label>
        </fieldset>
        <br>

        <label for="publico-objetivo">¿Identificaste el público objetivo de tu producto/servicio?</label>
        <fieldset class="label100">
            <input type="radio" name="publico-objetivo" id="publico-objetivo-1" value="1">
            <label for="publico-objetivo-1">a)  No lo he pensado aún.</label>
            <input type="radio" name="publico-objetivo" id="publico-objetivo-2" value="2">
            <label for="publico-objetivo-2">b)  No sé cómo encarar este tema.</label>
            <input type="radio" name="publico-objetivo" id="publico-objetivo-4" value="3">
            <label for="publico-objetivo-4">c)  Estoy trabajando en eso.</label>
            <input type="radio" name="publico-objetivo" id="publico-objetivo-5" value="4">
            <label for="publico-objetivo-5">d)  Sí, aunque requeriría actualización o revisión. </label>
            <input type="radio" name="publico-objetivo" id="publico-objetivo-6" value="5">
            <label for="publico-objetivo-6">e)  Si, ya lo tengo hecho. </label>
        </fieldset>
        <br>

        <label for="canales-venta">¿Identificaste los mejores canales de venta para llegar a dicho público?</label>
        <fieldset class="label100">
            <input type="radio" name="canales-venta" id="canales-venta-1" value="1">
            <label for="canales-venta-1">a)  No lo he pensado aún.</label>
            <input type="radio" name="canales-venta" id="canales-venta-2" value="2">
            <label for="canales-venta-2">b)  No sé cómo encarar este tema.</label>
            <input type="radio" name="canales-venta" id="canales-venta-4" value="3">
            <label for="canales-venta-4">c)  Estoy trabajando en eso.</label>
            <input type="radio" name="canales-venta" id="canales-venta-5" value="4">
            <label for="canales-venta-5">d)  Sí, aunque requeriría actualización o revisión.</label>
            <input type="radio" name="canales-venta" id="canales-venta-6" value="5">
            <label for="canales-venta-6">e)  Si, ya lo tengo hecho. </label>
        </fieldset>
        <br>

        <label for="estrategia-marketing">¿Tenés una estrategia de marketing (difusión, publicidad, redes sociales)?</label>
        <fieldset class="label100">
            <input type="radio" name="estrategia-marketing" id="estrategia-marketing-1" value="1">
            <label for="estrategia-marketing-1">a)  No lo he pensado aún.</label>
            <input type="radio" name="estrategia-marketing" id="estrategia-marketing-2" value="2">
            <label for="estrategia-marketing-2">b)  No sé cómo encarar este tema.</label>
            <input type="radio" name="estrategia-marketing" id="estrategia-marketing-4" value="3">
            <label for="estrategia-marketing-4">c)  Estoy trabajando en eso.</label>
            <input type="radio" name="estrategia-marketing" id="estrategia-marketing-5" value="4">
            <label for="estrategia-marketing-5">d)  Sí, aunque requeriría actualización o revisión.</label>
            <input type="radio" name="estrategia-marketing" id="estrategia-marketing-6" value="5">
            <label for="estrategia-marketing-6">e)  Si, ya lo tengo hecho. </label>
        </fieldset>
        <br>

        <label for="administracion">¿Tenés un registro claro de la administración de tu emprendimiento? (costos, ingresos, flujo de efectivo, etc.)</label>
        <fieldset class="label100">
            <input type="radio" name="administracion" id="administracion-1" value="1">
            <label for="administracion-1">a)  No lo he pensado aún.</label>
            <input type="radio" name="administracion" id="administracion-2" value="2">
            <label for="administracion-2">b)  No sé cómo encarar este tema.</label>
            <input type="radio" name="administracion" id="administracion-4" value="3">
            <label for="administracion-4">c)  Estoy trabajando en eso.</label>
            <input type="radio" name="administracion" id="administracion-5" value="4">
            <label for="administracion-5">d)  Sí, aunque requeriría actualización o revisión.</label>
            <input type="radio" name="administracion" id="administracion-6" value="5">
            <label for="administracion-6">e)  Si, ya lo tengo hecho. </label>
        </fieldset>
        <br>

        <label for="situacion-financiamiento">¿En qué situación te encontrás respecto a la necesidad de financiamiento?</label>
        <fieldset class="label100">
            <input type="radio" name="situacion-financiamiento" id="situacion-financiamiento-1" value="1">
            <label for="situacion-financiamiento-1">a)  N/A – no necesito financiamiento.</label>
            <input type="radio" name="situacion-financiamiento" id="situacion-financiamiento-2" value="2">
            <label for="situacion-financiamiento-2">b)  No tengo en claro si necesito financiamiento, o cuánto necesito.</label>
            <input type="radio" name="situacion-financiamiento" id="situacion-financiamiento-4" value="3">
            <label for="situacion-financiamiento-4">c)  Tengo necesidad de conseguir financiamiento pero desconozco las alternativas.</label>
            <input type="radio" name="situacion-financiamiento" id="situacion-financiamiento-5" value="4">
            <label for="situacion-financiamiento-5">d)  Estoy buscando financiamiento pero aún no lo conseguí.</label>
            <input type="radio" name="situacion-financiamiento" id="situacion-financiamiento-6" value="5">
        </fieldset>
        <br>

        <label for="plan-operaciones">¿Tenés un plan de operaciones? (que atienda la calidad y la eficiencia de los productos/servicios que brindás)</label>
        <fieldset class="label100">
            <input type="radio" name="plan-operaciones" id="plan-operaciones-1" value="1">
            <label for="plan-operaciones-1">a)  No lo he pensado aún.</label>
            <input type="radio" name="plan-operaciones" id="plan-operaciones-2" value="2">
            <label for="plan-operaciones-2">b)  No sé cómo encarar este tema.</label>
            <input type="radio" name="plan-operaciones" id="plan-operaciones-4" value="3">
            <label for="plan-operaciones-4">c)  Estoy trabajando en eso.</label>
            <input type="radio" name="plan-operaciones" id="plan-operaciones-5" value="4">
            <label for="plan-operaciones-5">d)  Sí, aunque requeriría actualización o revisión.</label>
            <input type="radio" name="plan-operaciones" id="plan-operaciones-6" value="5">
            <label for="plan-operaciones-6">e)  Si, ya lo tengo hecho. </label>
        </fieldset>
        <br>

        <input type="button" class="closePopUp label100" name="submit-calificacion" id="submit-calificacion" value="Calificar" onclick="insEncuestaEmprendedor()">
    </div>
</div>