<?php 
/**
* 
*/
require_once 'class.helpers.php';
require_once (__DIR__.'/../assets/PHPExcel/Classes/PHPExcel.php');

class Excels
{
	function createExcel($data)
	{
		$rString = Helpers::generateRandomString();

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()
		   ->setCreator("Amedia SRL")
		   ->setLastModifiedBy("Mentores en Red")
		   ->setTitle("Mentores en Red")
		   ->setSubject("Mentores en Red")
		   ->setDescription("Mentores en Red")
		   ->setKeywords("Mentores en Red");

	    $objPHPExcel->setActiveSheetIndex(0);

	    $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Mensaje');
	    $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'DNI');

	    foreach ($data as $key => $value) 
	    {
	    	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$value->linea, "$value->mensaje");
	    	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$value->linea, "$value->dni");
	    }

		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="excel.xls"');
		header('Cache-Control: max-age=0');
		$writer->save(__DIR__.'/../files/'.$rString.'.xls');

		$mensaje = New Excels();
		$mensaje->cod_mensaje = 2;
		$mensaje->mensaje = '';
		$mensaje->file = $rString.'.xls';
		return $mensaje;
	}

	function createExcelReporteGenero($data)
	{
		$rString = Helpers::generateRandomString();

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()
		   ->setCreator("Amedia SRL")
		   ->setLastModifiedBy("Mentores en Red")
		   ->setTitle("Reporte de Género")
		   ->setSubject("Mentores en Red")
		   ->setDescription("Mentores en Red")
		   ->setKeywords("Mentores en Red");

	    $objPHPExcel->setActiveSheetIndex(0);

	    $objPHPExcel->getActiveSheet()->SetCellValue('B2', "Representación de Género");
	    $objPHPExcel->getActiveSheet()->SetCellValue('C3', "Mujeres (%)");
	    $objPHPExcel->getActiveSheet()->SetCellValue('D3', "Hombres (%)");

	    $objPHPExcel->getActiveSheet()->SetCellValue('B4', "Mentor DT");
	    $objPHPExcel->getActiveSheet()->SetCellValue('B5', "Mentor Consolidación");

	    $objPHPExcel->getActiveSheet()->SetCellValue('B7', "Emprendedor DT");
	    $objPHPExcel->getActiveSheet()->SetCellValue('B8', "Emprendedor Consolidación");

	    $objPHPExcel->getActiveSheet()->SetCellValue('B10', "Emprendedores");
	    $objPHPExcel->getActiveSheet()->SetCellValue('B11', "Mentores");

	    $objPHPExcel->getActiveSheet()->SetCellValue('B13', "TOTAL");


		$objPHPExcel->getActiveSheet()->SetCellValue('C4', "$data->mentoresDtF");
		$objPHPExcel->getActiveSheet()->SetCellValue('D4', "$data->mentoresDtM");

		$objPHPExcel->getActiveSheet()->SetCellValue('C5', "$data->mentoresConF");
		$objPHPExcel->getActiveSheet()->SetCellValue('D5', "$data->mentoresConM");

		$objPHPExcel->getActiveSheet()->SetCellValue('C7', "$data->emprendedoresDtF");
		$objPHPExcel->getActiveSheet()->SetCellValue('D7', "$data->emprendedoresDtM");

		$objPHPExcel->getActiveSheet()->SetCellValue('C8', "$data->emprendedoresConF");
		$objPHPExcel->getActiveSheet()->SetCellValue('D8', "$data->emprendedoresConM");

		$objPHPExcel->getActiveSheet()->SetCellValue('C10', "$data->emprendedoresF");
		$objPHPExcel->getActiveSheet()->SetCellValue('D10', "$data->emprendedoresM");

		$objPHPExcel->getActiveSheet()->SetCellValue('C11', "$data->mentoresF");
		$objPHPExcel->getActiveSheet()->SetCellValue('D11', "$data->mentoresM");

		$objPHPExcel->getActiveSheet()->SetCellValue('C13', "$data->percentF");
		$objPHPExcel->getActiveSheet()->SetCellValue('D13', "$data->percentM");


		$objPHPExcel->getActiveSheet()->getStyle('B2')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('C3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('D3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B4')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B5')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B7')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B8')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B10')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B11')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B13')->getFont()->setBold(true);

		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="excel.xls"');
		header('Cache-Control: max-age=0');
		$writer->save(__DIR__.'/../files/'.$rString.'.xls');

		$mensaje = New Excels();
		$mensaje->cod_mensaje = 2;
		$mensaje->mensaje = '';
		$mensaje->file = $rString.'.xls';
		return $mensaje;
	}

	function createExcelMentoresEnFormacion($data)
	{
		$rString = Helpers::generateRandomString();

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()
		   ->setCreator("Amedia SRL")
		   ->setLastModifiedBy("Mentores en Red")
		   ->setTitle("Reporte de mentores en formación")
		   ->setSubject("Mentores en Red")
		   ->setDescription("Mentores en Red")
		   ->setKeywords("Mentores en Red");

	    $objPHPExcel->setActiveSheetIndex(0);

	    $objPHPExcel->getActiveSheet()->SetCellValue('A1', "Mentores en formación");

		$objPHPExcel->getActiveSheet()->SetCellValue('A3', "Nombre");
		$objPHPExcel->getActiveSheet()->SetCellValue('B3', "Rol");
		$objPHPExcel->getActiveSheet()->SetCellValue('C3', "Email");
		$objPHPExcel->getActiveSheet()->SetCellValue('D3', "DNI");
		$objPHPExcel->getActiveSheet()->SetCellValue('E3', "Sede");

		$tipo_usuario = array('', 'Administrador', 'Mentor Desarrollo Temprano', 'Mentor Consolidacion', 'Emprendedor Desarrollo temprano', 'Emprendedor Consolidación', 'Ministerio');
	    $i = 4;
	    foreach ($data as $key => $value) 
	    {
	    	$t_usuario = $tipo_usuario[$value->id_tipo_usuario];

			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$i, "$value->nombre");
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$i, "$t_usuario");
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.$i, "$value->email");
			$objPHPExcel->getActiveSheet()->SetCellValue('D'.$i, "$value->dni");
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.$i, "$value->sede");
			$i++;
	    }

	    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
	    $objPHPExcel->getActiveSheet()
			        ->getStyle('A3:E3')
			        ->getFill()
			        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
			        ->getStartColor()
			        ->setRGB('ADADAD');

		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="excel.xls"');
		header('Cache-Control: max-age=0');
		$writer->save(__DIR__.'/../files/'.$rString.'.xls');

		$mensaje = New Excels();
		$mensaje->cod_mensaje = 2;
		$mensaje->mensaje = '';
		$mensaje->file = $rString.'.xls';

		return $mensaje;
	}

	function createExcelMentoresFormados($data)
	{
		$rString = Helpers::generateRandomString();

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()
		   ->setCreator("Amedia SRL")
		   ->setLastModifiedBy("Mentores en Red")
		   ->setTitle("Reporte de mentores en formación")
		   ->setSubject("Mentores en Red")
		   ->setDescription("Mentores en Red")
		   ->setKeywords("Mentores en Red");

	    $objPHPExcel->setActiveSheetIndex(0);

	    $objPHPExcel->getActiveSheet()->SetCellValue('A1', "Mentores formados");

		$objPHPExcel->getActiveSheet()->SetCellValue('A3', "Nombre");
		$objPHPExcel->getActiveSheet()->SetCellValue('B3', "Rol");
		$objPHPExcel->getActiveSheet()->SetCellValue('C3', "Email");
		$objPHPExcel->getActiveSheet()->SetCellValue('D3', "DNI");
		$objPHPExcel->getActiveSheet()->SetCellValue('E3', "Sede");

		$tipo_usuario = array('', 'Administrador', 'Mentor Desarrollo Temprano', 'Mentor Consolidacion', 'Emprendedor Desarrollo temprano', 'Emprendedor Consolidación', 'Ministerio');
	    $i = 4;
	    foreach ($data as $key => $value) 
	    {
	    	$t_usuario = $tipo_usuario[$value->id_tipo_usuario];

			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$i, "$value->nombre");
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$i, "$t_usuario");
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.$i, "$value->email");
			$objPHPExcel->getActiveSheet()->SetCellValue('D'.$i, "$value->dni");
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.$i, "$value->sede");
			$i++;
	    }

	    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
	    $objPHPExcel->getActiveSheet()
			        ->getStyle('A3:E3')
			        ->getFill()
			        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
			        ->getStartColor()
			        ->setRGB('ADADAD');

		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="excel.xls"');
		header('Cache-Control: max-age=0');
		$writer->save(__DIR__.'/../files/'.$rString.'.xls');

		$mensaje = New Excels();
		$mensaje->cod_mensaje = 2;
		$mensaje->mensaje = '';
		$mensaje->file = $rString.'.xls';

		return $mensaje;
	}

	function createExcelMentoriasActivas($data)
	{
		$rString = Helpers::generateRandomString();

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()
		   ->setCreator("Amedia SRL")
		   ->setLastModifiedBy("Mentores en Red")
		   ->setTitle("Reporte de mentorías activas")
		   ->setSubject("Mentores en Red")
		   ->setDescription("Mentores en Red")
		   ->setKeywords("Mentores en Red");

	    $objPHPExcel->setActiveSheetIndex(0);

	    $objPHPExcel->getActiveSheet()->SetCellValue('A1', "Reporte de mentorías activas Desarrollo Temprano");

		$objPHPExcel->getActiveSheet()->SetCellValue('A3', "Mentor");
		$objPHPExcel->getActiveSheet()->SetCellValue('B3', "Dni Mentor");
		$objPHPExcel->getActiveSheet()->SetCellValue('C3', "Emprendedores");
		$objPHPExcel->getActiveSheet()->SetCellValue('D3', "Dni Emprendedores");
		$objPHPExcel->getActiveSheet()->SetCellValue('E3', "Grupo");
		$objPHPExcel->getActiveSheet()->SetCellValue('F3', "Rol");
		$objPHPExcel->getActiveSheet()->SetCellValue('G3', "Promedio calificación");
		$objPHPExcel->getActiveSheet()->SetCellValue('H3', "Sesiones realizadas");
		$objPHPExcel->getActiveSheet()->SetCellValue('I3', "Promedio calificación del mentor");

		$tipo_usuario = array('', 'Administrador', 'Mentor Desarrollo Temprano', 'Mentor Consolidacion', 'Emprendedor Desarrollo temprano', 'Emprendedor Consolidación', 'Ministerio');
	    $i = 4;
	    foreach ($data as $key => $value) 
	    {
	    	$t_usuario = $tipo_usuario[$value->id_tipo_usuario];

			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$i, "$value->mentor");
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$i, "$value->dni_mentor");
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.$i, "$value->emprendedores");
			$objPHPExcel->getActiveSheet()->SetCellValue('D'.$i, "$value->dni_emprendedores");
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.$i, "$value->grupo");
			$objPHPExcel->getActiveSheet()->SetCellValue('F'.$i, "$t_usuario");
			$objPHPExcel->getActiveSheet()->SetCellValue('G'.$i, "$value->promedio_calificacion");
			$objPHPExcel->getActiveSheet()->SetCellValue('H'.$i, "$value->cant_sesiones");
			$objPHPExcel->getActiveSheet()->SetCellValue('I'.$i, "$value->promedio_mentor");
			$i++;
	    }

	    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
	    $objPHPExcel->getActiveSheet()
			        ->getStyle('A3:I3')
			        ->getFill()
			        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
			        ->getStartColor()
			        ->setRGB('ADADAD');

		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="excel.xls"');
		header('Cache-Control: max-age=0');
		$writer->save(__DIR__.'/../files/'.$rString.'.xls');

		$mensaje = New Excels();
		$mensaje->cod_mensaje = 2;
		$mensaje->mensaje = '';
		$mensaje->file = $rString.'.xls';

		return $mensaje;
	}

	function createExcelMentoriasActivasCON($data)
	{
		$rString = Helpers::generateRandomString();

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()
		   ->setCreator("Amedia SRL")
		   ->setLastModifiedBy("Mentores en Red")
		   ->setTitle("Reporte de mentorías activas")
		   ->setSubject("Mentores en Red")
		   ->setDescription("Mentores en Red")
		   ->setKeywords("Mentores en Red");

	    $objPHPExcel->setActiveSheetIndex(0);

	    $objPHPExcel->getActiveSheet()->SetCellValue('A1', "Reporte de mentorías activas Consolidación");

		$objPHPExcel->getActiveSheet()->SetCellValue('A3', "Mentor");
		$objPHPExcel->getActiveSheet()->SetCellValue('B3', "Dni Mentor");
		$objPHPExcel->getActiveSheet()->SetCellValue('C3', "Emprendedores");
		$objPHPExcel->getActiveSheet()->SetCellValue('D3', "Dni Emprendedores");
		$objPHPExcel->getActiveSheet()->SetCellValue('E3', "Valoración Mentor");
		$objPHPExcel->getActiveSheet()->SetCellValue('F3', "Valoración Emprendedor");
		$objPHPExcel->getActiveSheet()->SetCellValue('G3', "Sesiones emprendedor");

		$tipo_usuario = array('', 'Administrador', 'Mentor Desarrollo Temprano', 'Mentor Consolidacion', 'Emprendedor Desarrollo temprano', 'Emprendedor Consolidación', 'Ministerio');
	    $i = 4;
	    foreach ($data as $key => $value) 
	    {
	    	$t_usuario = $tipo_usuario[$value->id_tipo_usuario];

			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$i, "$value->mentor");
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$i, "$value->dni_mentor");
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.$i, "$value->emprendedores");
			$objPHPExcel->getActiveSheet()->SetCellValue('D'.$i, "$value->dni_emprendedores");
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.$i, "$value->promedio_mentor");
			$objPHPExcel->getActiveSheet()->SetCellValue('F'.$i, "$value->promedio_emprendedor");
			$objPHPExcel->getActiveSheet()->SetCellValue('G'.$i, "$value->asistencias");
			$i++;
	    }

	    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
	    $objPHPExcel->getActiveSheet()
			        ->getStyle('A3:G3')
			        ->getFill()
			        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
			        ->getStartColor()
			        ->setRGB('ADADAD');

		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="excel.xls"');
		header('Cache-Control: max-age=0');
		$writer->save(__DIR__.'/../files/'.$rString.'.xls');

		$mensaje = New Excels();
		$mensaje->cod_mensaje = 2;
		$mensaje->mensaje = '';
		$mensaje->file = $rString.'.xls';

		return $mensaje;
	}

	function createExcelEmprendedoresEnProceso($data)
	{
		$rString = Helpers::generateRandomString();

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()
		   ->setCreator("Amedia SRL")
		   ->setLastModifiedBy("Mentores en Red")
		   ->setTitle("Emprendedores en proceso de formación")
		   ->setSubject("Mentores en Red")
		   ->setDescription("Mentores en Red")
		   ->setKeywords("Mentores en Red");

	    $objPHPExcel->setActiveSheetIndex(0);

	    $objPHPExcel->getActiveSheet()->SetCellValue('A1', "Emprendedores en proceso de formación");

		$objPHPExcel->getActiveSheet()->SetCellValue('A3', "Nombre");
		$objPHPExcel->getActiveSheet()->SetCellValue('B3', "Email");
		$objPHPExcel->getActiveSheet()->SetCellValue('C3', "Rol");
		$objPHPExcel->getActiveSheet()->SetCellValue('D3', "Dni");
		$objPHPExcel->getActiveSheet()->SetCellValue('E3', "Sede");
		$objPHPExcel->getActiveSheet()->SetCellValue('F3', "Nombre mentor");
		$objPHPExcel->getActiveSheet()->SetCellValue('G3', "Dni mentor");
		$objPHPExcel->getActiveSheet()->SetCellValue('H3', "Sesiones realizadas");

		$tipo_usuario = array('', 'Administrador', 'Mentor Desarrollo Temprano', 'Mentor Consolidacion', 'Emprendedor Desarrollo temprano', 'Emprendedor Consolidación', 'Ministerio');
	    $i = 4;
	    foreach ($data as $key => $value) 
	    {
	    	$t_usuario = $tipo_usuario[$value->id_tipo_usuario];

			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$i, "$value->nombre");
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$i, "$value->email");
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.$i, "$value->rol");
			$objPHPExcel->getActiveSheet()->SetCellValue('D'.$i, "$value->dni");
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.$i, "$value->sede");
			$objPHPExcel->getActiveSheet()->SetCellValue('F'.$i, "$value->nombre_mentor");
			$objPHPExcel->getActiveSheet()->SetCellValue('G'.$i, "$value->dni_mentor");
			$objPHPExcel->getActiveSheet()->SetCellValue('H'.$i, "$value->sesiones");
			$i++;
	    }

	    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
	    $objPHPExcel->getActiveSheet()
			        ->getStyle('A3:H3')
			        ->getFill()
			        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
			        ->getStartColor()
			        ->setRGB('ADADAD');

		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="excel.xls"');
		header('Cache-Control: max-age=0');
		$writer->save(__DIR__.'/../files/'.$rString.'.xls');

		$mensaje = New Excels();
		$mensaje->cod_mensaje = 2;
		$mensaje->mensaje = '';
		$mensaje->file = $rString.'.xls';

		return $mensaje;
	}

	function createExcelEncuesasSatisfaccion($data)
	{
		$rString = Helpers::generateRandomString();

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()
		   ->setCreator("Amedia SRL")
		   ->setLastModifiedBy("Mentores en Red")
		   ->setTitle("Reporte de encuestas de satisfacción")
		   ->setSubject("Mentores en Red")
		   ->setDescription("Mentores en Red")
		   ->setKeywords("Mentores en Red");

	    $objPHPExcel->setActiveSheetIndex(0);

		$objPHPExcel->getActiveSheet()->SetCellValue('A1', "Usuario");
		$objPHPExcel->getActiveSheet()->SetCellValue('B1', "Nombre");
		$objPHPExcel->getActiveSheet()->SetCellValue('C1', "DNI");
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', "Estructura");
		$objPHPExcel->getActiveSheet()->SetCellValue('E1', "Utilidad");
		$objPHPExcel->getActiveSheet()->SetCellValue('F1', "Cumplimiento");
		$objPHPExcel->getActiveSheet()->SetCellValue('G1', "Interaccion");
		$objPHPExcel->getActiveSheet()->SetCellValue('H1', "Desempeno");
		$objPHPExcel->getActiveSheet()->SetCellValue('I1', "Material");
		$objPHPExcel->getActiveSheet()->SetCellValue('J1', "Valoracion");
		$objPHPExcel->getActiveSheet()->SetCellValue('K1', "Comentario");
		$objPHPExcel->getActiveSheet()->SetCellValue('L1', "Fecha");
		$objPHPExcel->getActiveSheet()->SetCellValue('M1', "Rol");
		$objPHPExcel->getActiveSheet()->SetCellValue('N1', "Sede");

		$tipo_usuario = array('', 'Administrador', 'Mentor Desarrollo Temprano', 'Mentor Consolidacion', 'Emprendedor Desarrollo temprano', 'Emprendedor Consolidación', 'Ministerio');
	    $i = 2;
	    foreach ($data as $key => $value) 
	    {
	    	$t_usuario = $tipo_usuario[$value->id_tipo_usuario];

			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$i, "$value->usuario");
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$i, "$value->nombre");
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.$i, "$value->dni");
			$objPHPExcel->getActiveSheet()->SetCellValue('D'.$i, "$value->estructura");
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.$i, "$value->utilidad");
			$objPHPExcel->getActiveSheet()->SetCellValue('F'.$i, "$value->cumplimiento");
			$objPHPExcel->getActiveSheet()->SetCellValue('G'.$i, "$value->interaccion");
			$objPHPExcel->getActiveSheet()->SetCellValue('H'.$i, "$value->desempeno");
			$objPHPExcel->getActiveSheet()->SetCellValue('I'.$i, "$value->material");
			$objPHPExcel->getActiveSheet()->SetCellValue('J'.$i, "$value->valoracion");
			$objPHPExcel->getActiveSheet()->SetCellValue('K'.$i, "$value->comentario");
			$objPHPExcel->getActiveSheet()->SetCellValue('L'.$i, "$value->fecha");
			$objPHPExcel->getActiveSheet()->SetCellValue('M'.$i, "$t_usuario");
			$objPHPExcel->getActiveSheet()->SetCellValue('N'.$i, "$value->sede");
			$i++;
	    }

	    $objPHPExcel->getActiveSheet()
			        ->getStyle('A1:N1')
			        ->getFill()
			        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
			        ->getStartColor()
			        ->setRGB('ADADAD');

		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="excel.xls"');
		header('Cache-Control: max-age=0');
		$writer->save(__DIR__.'/../files/'.$rString.'.xls');

		$mensaje = New Excels();
		$mensaje->cod_mensaje = 2;
		$mensaje->mensaje = '';
		$mensaje->file = $rString.'.xls';

		return $mensaje;
	}

	function createExcelImpactoDesarrollo($data)
	{
		$rString = Helpers::generateRandomString();

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()
		   ->setCreator("Amedia SRL")
		   ->setLastModifiedBy("Mentores en Red")
		   ->setTitle("Impacto - desarrollo de propuesta del emprendimiento ")
		   ->setSubject("Mentores en Red")
		   ->setDescription("Mentores en Red")
		   ->setKeywords("Mentores en Red");

	    $objPHPExcel->setActiveSheetIndex(0);

	    $objPHPExcel->getActiveSheet()->SetCellValue('A1', "Impacto - desarrollo de propuesta del emprendimiento");

		$objPHPExcel->getActiveSheet()->SetCellValue('A3', "Emprendedor");
		$objPHPExcel->getActiveSheet()->SetCellValue('B3', "DNI");
		$objPHPExcel->getActiveSheet()->SetCellValue('C3', "Sede");
		$objPHPExcel->getActiveSheet()->SetCellValue('D3', "Diagnóstico inicial");
		$objPHPExcel->getActiveSheet()->SetCellValue('E3', "Diagnóstico final");
		$objPHPExcel->getActiveSheet()->SetCellValue('F3', "% de impacto");
		$objPHPExcel->getActiveSheet()->SetCellValue('G3', "Rol");

		$tipo_usuario = array('', 'Administrador', 'Mentor Desarrollo Temprano', 'Mentor Consolidacion', 'Emprendedor Desarrollo temprano', 'Emprendedor Consolidación', 'Ministerio');
	    $i = 4;
	    foreach ($data as $key => $value) 
	    {
	    	$t_usuario = $tipo_usuario[$value->id_tipo_usuario];

			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$i, "$value->emprendedor");
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$i, "$value->dni");
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.$i, "$value->sede");
			$objPHPExcel->getActiveSheet()->SetCellValue('D'.$i, "$value->diagnostico_inicial");
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.$i, "$value->diagnostico_final");
			$objPHPExcel->getActiveSheet()->SetCellValue('F'.$i, "$value->impacto");
			$objPHPExcel->getActiveSheet()->SetCellValue('G'.$i, "$t_usuario");
			$i++;
	    }

	    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
	    $objPHPExcel->getActiveSheet()
			        ->getStyle('A3:G3')
			        ->getFill()
			        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
			        ->getStartColor()
			        ->setRGB('ADADAD');

		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="excel.xls"');
		header('Cache-Control: max-age=0');
		$writer->save(__DIR__.'/../files/'.$rString.'.xls');

		$mensaje = New Excels();
		$mensaje->cod_mensaje = 2;
		$mensaje->mensaje = '';
		$mensaje->file = $rString.'.xls';

		return $mensaje;
	}

	function createExcelEstadoAtencionUsuarios($data)
	{
		$rString = Helpers::generateRandomString();

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()
		   ->setCreator("Amedia SRL")
		   ->setLastModifiedBy("Mentores en Red")
		   ->setTitle("Estado de atención por participante ")
		   ->setSubject("Mentores en Red")
		   ->setDescription("Mentores en Red")
		   ->setKeywords("Mentores en Red");

	    $objPHPExcel->setActiveSheetIndex(0);

	    $objPHPExcel->getActiveSheet()->SetCellValue('A1', "Estado de atención por participante");

		$objPHPExcel->getActiveSheet()->SetCellValue('A3', "Emprendedor");
		$objPHPExcel->getActiveSheet()->SetCellValue('B3', "Mentor");
		$objPHPExcel->getActiveSheet()->SetCellValue('C3', "Valoración promedio emprendedor");
		$objPHPExcel->getActiveSheet()->SetCellValue('D3', "Sesiones");
		$objPHPExcel->getActiveSheet()->SetCellValue('E3', "Rol");
		$objPHPExcel->getActiveSheet()->SetCellValue('F3', "Estado");
		$objPHPExcel->getActiveSheet()->SetCellValue('G3', "DNI");

		$tipo_usuario = array('', 'Administrador', 'Mentor Desarrollo Temprano', 'Mentor Consolidacion', 'Emprendedor Desarrollo temprano', 'Emprendedor Consolidación', 'Ministerio');
	    $i = 4;
	    foreach ($data as $key => $value) 
	    {
	    	$t_usuario = $tipo_usuario[$value->id_tipo_usuario];
	    	$sesiones = $value->sesiones . '/' . $value->total_sesiones;

			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$i, "$value->emprendedor");
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$i, "$value->mentor");
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.$i, "$value->valoracion");
			$objPHPExcel->getActiveSheet()->SetCellValue('D'.$i, "$value->sesiones");
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.$i, "$t_usuario");
			$objPHPExcel->getActiveSheet()->SetCellValue('F'.$i, "$value->activo");
			$objPHPExcel->getActiveSheet()->SetCellValue('G'.$i, "$value->dni");
			$i++;
	    }

	    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
	    $objPHPExcel->getActiveSheet()
			        ->getStyle('A3:G3')
			        ->getFill()
			        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
			        ->getStartColor()
			        ->setRGB('ADADAD');

		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="excel.xls"');
		header('Cache-Control: max-age=0');
		$writer->save(__DIR__.'/../files/'.$rString.'.xls');

		$mensaje = New Excels();
		$mensaje->cod_mensaje = 2;
		$mensaje->mensaje = '';
		$mensaje->file = $rString.'.xls';

		return $mensaje;
	}

	function createExcelListaUsuarios($data)
	{
		$rString = Helpers::generateRandomString();

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()
		   ->setCreator("Amedia SRL")
		   ->setLastModifiedBy("Mentores en Red")
		   ->setTitle("Lista usuarios ")
		   ->setSubject("Mentores en Red")
		   ->setDescription("Mentores en Red")
		   ->setKeywords("Mentores en Red");

	    $objPHPExcel->setActiveSheetIndex(0);

	    $objPHPExcel->getActiveSheet()->SetCellValue('A1', "Lista usuarios");

		$objPHPExcel->getActiveSheet()->SetCellValue('A3', "nombre");
		$objPHPExcel->getActiveSheet()->SetCellValue('B3', "cargo");
		$objPHPExcel->getActiveSheet()->SetCellValue('C3', "ult_login");
		$objPHPExcel->getActiveSheet()->SetCellValue('D3', "email");
		$objPHPExcel->getActiveSheet()->SetCellValue('E3', "dni");
		$objPHPExcel->getActiveSheet()->SetCellValue('F3', "sede");
		$objPHPExcel->getActiveSheet()->SetCellValue('G3', "estado");

		$tipo_usuario = array('', 'Administrador', 'Mentor Desarrollo Temprano', 'Mentor Consolidacion', 'Emprendedor Desarrollo temprano', 'Emprendedor Consolidación', 'Ministerio');
	    $i = 4;
	    foreach ($data as $key => $value) 
	    {
	    	$estado = $value->estado == -1 ? 'Activo' : 'Inactivo';

			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$i, "$value->nombre");
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$i, "$value->cargo");
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.$i, "$value->ult_login");
			$objPHPExcel->getActiveSheet()->SetCellValue('D'.$i, "$value->email");
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.$i, "$value->dni");
			$objPHPExcel->getActiveSheet()->SetCellValue('F'.$i, "$value->sede");
			$objPHPExcel->getActiveSheet()->SetCellValue('G'.$i, "$estado");
			$i++;
	    }

	    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
	    $objPHPExcel->getActiveSheet()
			        ->getStyle('A3:G3')
			        ->getFill()
			        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
			        ->getStartColor()
			        ->setRGB('ADADAD');

		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="excel.xls"');
		header('Cache-Control: max-age=0');
		$writer->save(__DIR__.'/../files/'.$rString.'.xls');

		$mensaje = New Excels();
		$mensaje->cod_mensaje = 2;
		$mensaje->mensaje = '';
		$mensaje->file = $rString.'.xls';

		return $mensaje;
	}

	function createExcelDiversidadProductiva($data)
	{
		$rString = Helpers::generateRandomString();

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()
		   ->setCreator("Amedia SRL")
		   ->setLastModifiedBy("Mentores en Red")
		   ->setTitle("Diversidad Productiva ")
		   ->setSubject("Mentores en Red")
		   ->setDescription("Mentores en Red")
		   ->setKeywords("Mentores en Red");

	    $objPHPExcel->setActiveSheetIndex(0);

	    $objPHPExcel->getActiveSheet()->SetCellValue('A1', "Diversidad Productiva");

		$objPHPExcel->getActiveSheet()->SetCellValue('A3', "Rubro");
		$objPHPExcel->getActiveSheet()->SetCellValue('B3', "Porcentaje");

		$tipo_usuario = array('', 'Administrador', 'Mentor Desarrollo Temprano', 'Mentor Consolidacion', 'Emprendedor Desarrollo temprano', 'Emprendedor Consolidación', 'Ministerio');
	    $i = 4;
	    foreach ($data as $key => $value) 
	    {
			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$i, "$value->rubro");
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$i, "$value->perRubro");
			$i++;
	    }

	    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
	    $objPHPExcel->getActiveSheet()
			        ->getStyle('A3:B3')
			        ->getFill()
			        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
			        ->getStartColor()
			        ->setRGB('ADADAD');

		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="excel.xls"');
		header('Cache-Control: max-age=0');
		$writer->save(__DIR__.'/../files/'.$rString.'.xls');

		$mensaje = New Excels();
		$mensaje->cod_mensaje = 2;
		$mensaje->mensaje = '';
		$mensaje->file = $rString.'.xls';

		return $mensaje;
	}

	function createExcelSedesOperativas($data)
	{
		$rString = Helpers::generateRandomString();

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()
		   ->setCreator("Amedia SRL")
		   ->setLastModifiedBy("Mentores en Red")
		   ->setTitle("Sedes Operativas")
		   ->setSubject("Mentores en Red")
		   ->setDescription("Mentores en Red")
		   ->setKeywords("Mentores en Red");

	    $objPHPExcel->setActiveSheetIndex(0);

	    $objPHPExcel->getActiveSheet()->SetCellValue('A1', "Sedes Operativas");

		$objPHPExcel->getActiveSheet()->SetCellValue('A3', "Sede");
		$objPHPExcel->getActiveSheet()->SetCellValue('B3', "Cantidad de mentores");
		$objPHPExcel->getActiveSheet()->SetCellValue('C3', "Cantidad de emprendedores");

	    $i = 4;
	    foreach ($data as $key => $value) 
	    {
			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$i, "$value->sede");
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$i, "$value->mentores");
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.$i, "$value->emprendedores");
			$i++;
	    }

	    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
	    $objPHPExcel->getActiveSheet()
			        ->getStyle('A3:C3')
			        ->getFill()
			        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
			        ->getStartColor()
			        ->setRGB('ADADAD');

		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="excel.xls"');
		header('Cache-Control: max-age=0');
		$writer->save(__DIR__.'/../files/'.$rString.'.xls');

		$mensaje = New Excels();
		$mensaje->cod_mensaje = 2;
		$mensaje->mensaje = '';
		$mensaje->file = $rString.'.xls';

		return $mensaje;
	}

	function createExcelMentores($data)
	{
		$rString = Helpers::generateRandomString();

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()
		   ->setCreator("Amedia SRL")
		   ->setLastModifiedBy("Mentores en Red")
		   ->setTitle("Mentores")
		   ->setSubject("Mentores en Red")
		   ->setDescription("Mentores en Red")
		   ->setKeywords("Mentores en Red");

	    $objPHPExcel->setActiveSheetIndex(0);

	    $objPHPExcel->getActiveSheet()->SetCellValue('A1', "Mentores");

		$objPHPExcel->getActiveSheet()->SetCellValue('A3', "Nombre");
		$objPHPExcel->getActiveSheet()->SetCellValue('B3', "Sede");
		$objPHPExcel->getActiveSheet()->SetCellValue('C3', "Expertise");
		$objPHPExcel->getActiveSheet()->SetCellValue('D3', "Rubro");
		$objPHPExcel->getActiveSheet()->SetCellValue('E3', "Linkedin");
		$objPHPExcel->getActiveSheet()->SetCellValue('F3', "Email");

	    $i = 4;
	    foreach ($data as $key => $value) 
	    {
			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$i, "$value->nombre");
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$i, "$value->sede");
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.$i, "$value->expertise");
			$objPHPExcel->getActiveSheet()->SetCellValue('D'.$i, "$value->rubro");
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.$i, "$value->linkedin");
			$objPHPExcel->getActiveSheet()->SetCellValue('F'.$i, "$value->email");
			$i++;
	    }

	    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
	    $objPHPExcel->getActiveSheet()
			        ->getStyle('A3:F3')
			        ->getFill()
			        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
			        ->getStartColor()
			        ->setRGB('ADADAD');

		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="excel.xls"');
		header('Cache-Control: max-age=0');
		$writer->save(__DIR__.'/../files/'.$rString.'.xls');

		$mensaje = New Excels();
		$mensaje->cod_mensaje = 2;
		$mensaje->mensaje = '';
		$mensaje->file = $rString.'.xls';

		return $mensaje;
	}

	function createExcelEmprendedoresEgresados($data)
	{
		$rString = Helpers::generateRandomString();

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()
		   ->setCreator("Amedia SRL")
		   ->setLastModifiedBy("Mentores en Red")
		   ->setTitle("Mentores")
		   ->setSubject("Mentores en Red")
		   ->setDescription("Mentores en Red")
		   ->setKeywords("Mentores en Red");

	    $objPHPExcel->setActiveSheetIndex(0);

	    $objPHPExcel->getActiveSheet()->SetCellValue('A1', "Emprendedores Egresados");

		$objPHPExcel->getActiveSheet()->SetCellValue('A3', "Nombre");
		$objPHPExcel->getActiveSheet()->SetCellValue('B3', "Email");
		$objPHPExcel->getActiveSheet()->SetCellValue('C3', "Sede");
		$objPHPExcel->getActiveSheet()->SetCellValue('D3', "DNI");
		$objPHPExcel->getActiveSheet()->SetCellValue('E3', "Mentor");

	    $i = 4;
	    foreach ($data as $key => $value) 
	    {
			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$i, "$value->nombre");
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$i, "$value->email");
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.$i, "$value->sede");
			$objPHPExcel->getActiveSheet()->SetCellValue('D'.$i, "$value->dni");
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.$i, "$value->mentor");
			$i++;
	    }

	    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
	    $objPHPExcel->getActiveSheet()
			        ->getStyle('A3:E3')
			        ->getFill()
			        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
			        ->getStartColor()
			        ->setRGB('ADADAD');

		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="excel.xls"');
		header('Cache-Control: max-age=0');
		$writer->save(__DIR__.'/../files/'.$rString.'.xls');

		$mensaje = New Excels();
		$mensaje->cod_mensaje = 2;
		$mensaje->mensaje = '';
		$mensaje->file = $rString.'.xls';

		return $mensaje;
	}

	function createExcelDetallesUsuario($data)
	{
		$rString = Helpers::generateRandomString();

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()
		   ->setCreator("Amedia SRL")
		   ->setLastModifiedBy("Mentores en Red")
		   ->setTitle("Mentores")
		   ->setSubject("Mentores en Red")
		   ->setDescription("Mentores en Red")
		   ->setKeywords("Mentores en Red");

	    $objPHPExcel->setActiveSheetIndex(0);

		$objPHPExcel->getActiveSheet()->SetCellValue('A3', "Nombre");
		$objPHPExcel->getActiveSheet()->SetCellValue('B3', "DNI");
		$objPHPExcel->getActiveSheet()->SetCellValue('C3', "Sistema Gestion");
		$objPHPExcel->getActiveSheet()->SetCellValue('D3', "Nombre Comercial");
		$objPHPExcel->getActiveSheet()->SetCellValue('E3', "Descripcion Actividad Principal");
		$objPHPExcel->getActiveSheet()->SetCellValue('F3', "Tiempo Comenzaste");
		$objPHPExcel->getActiveSheet()->SetCellValue('G3', "Rubro");
		$objPHPExcel->getActiveSheet()->SetCellValue('H3', "Etapa");
		$objPHPExcel->getActiveSheet()->SetCellValue('I3', "Propuesta Valor");
		$objPHPExcel->getActiveSheet()->SetCellValue('J3', "Conocimiento Mercado");
		$objPHPExcel->getActiveSheet()->SetCellValue('K3', "Estrategia Salida");
		$objPHPExcel->getActiveSheet()->SetCellValue('L3', "Analisis Competencia");
		$objPHPExcel->getActiveSheet()->SetCellValue('M3', "Fidelizacion Clientes");
		$objPHPExcel->getActiveSheet()->SetCellValue('N3', "Expansion Nuevos Mercados");
		$objPHPExcel->getActiveSheet()->SetCellValue('O3', "Analisis Actores Clave");
		$objPHPExcel->getActiveSheet()->SetCellValue('P3', "Consolidacion Punto de Equilibrio");
		$objPHPExcel->getActiveSheet()->SetCellValue('Q3', "Estabilidad Financiera");
		$objPHPExcel->getActiveSheet()->SetCellValue('R3', "Acceso Financiamiento");
		$objPHPExcel->getActiveSheet()->SetCellValue('S3', "Administracion Insumos");
		$objPHPExcel->getActiveSheet()->SetCellValue('T3', "Gestion Proveedores");
		$objPHPExcel->getActiveSheet()->SetCellValue('U3', "Diseño de Sistema Produccion");
		$objPHPExcel->getActiveSheet()->SetCellValue('V3', "Escalabilidad Produccion");
		$objPHPExcel->getActiveSheet()->SetCellValue('W3', "Cumplimiento de Requisitos Legales");
		$objPHPExcel->getActiveSheet()->SetCellValue('X3', "Diseño de sistemas Administrativos");
		$objPHPExcel->getActiveSheet()->SetCellValue('Y3', "Diseño de sistemas Planeamiento");
		$objPHPExcel->getActiveSheet()->SetCellValue('Z3', "Estructura de Personal");

	    $i = 4;
	    foreach ($data as $key => $value) 
	    {
			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$i, "$value->nombre");
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$i, "$value->dni");
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.$i, "$value->sistemaGestion");
			$objPHPExcel->getActiveSheet()->SetCellValue('D'.$i, "$value->nombreComercial");
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.$i, "$value->descripcionActividadPrincipal");
			$objPHPExcel->getActiveSheet()->SetCellValue('F'.$i, "$value->tiempoComenzaste");
			$objPHPExcel->getActiveSheet()->SetCellValue('G'.$i, "$value->rubro");
			$objPHPExcel->getActiveSheet()->SetCellValue('H'.$i, "$value->etapa");
			$objPHPExcel->getActiveSheet()->SetCellValue('I'.$i, "$value->propuestaValor");
			$objPHPExcel->getActiveSheet()->SetCellValue('J'.$i, "$value->conocimientoMercado");
			$objPHPExcel->getActiveSheet()->SetCellValue('K'.$i, "$value->estrategiaSalida");
			$objPHPExcel->getActiveSheet()->SetCellValue('L'.$i, "$value->analisisCompetencia");
			$objPHPExcel->getActiveSheet()->SetCellValue('M'.$i, "$value->fidelizacionClientes");
			$objPHPExcel->getActiveSheet()->SetCellValue('N'.$i, "$value->expansionNuevosMercados");
			$objPHPExcel->getActiveSheet()->SetCellValue('O'.$i, "$value->analisisActoresClave");
			$objPHPExcel->getActiveSheet()->SetCellValue('P'.$i, "$value->consolidacionPuntoEquilibrio");
			$objPHPExcel->getActiveSheet()->SetCellValue('Q'.$i, "$value->estabilidadFinanciera");
			$objPHPExcel->getActiveSheet()->SetCellValue('R'.$i, "$value->accesoFinanciamiento");
			$objPHPExcel->getActiveSheet()->SetCellValue('S'.$i, "$value->administracionInsumos");
			$objPHPExcel->getActiveSheet()->SetCellValue('T'.$i, "$value->gestionProveedores");
			$objPHPExcel->getActiveSheet()->SetCellValue('U'.$i, "$value->disenoSistemaProduccion");
			$objPHPExcel->getActiveSheet()->SetCellValue('V'.$i, "$value->escalabilidadProduccion");
			$objPHPExcel->getActiveSheet()->SetCellValue('W'.$i, "$value->cumplimientoRequisitosLegales");
			$objPHPExcel->getActiveSheet()->SetCellValue('X'.$i, "$value->disenoSistemasAdministrativos");
			$objPHPExcel->getActiveSheet()->SetCellValue('Y'.$i, "$value->disenoSistemasPlaneamiento");
			$objPHPExcel->getActiveSheet()->SetCellValue('Z'.$i, "$value->estructuraPersonal");
			$i++;
	    }

	    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
	    $objPHPExcel->getActiveSheet()
			        ->getStyle('A3:Z3')
			        ->getFill()
			        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
			        ->getStartColor()
			        ->setRGB('ADADAD');

		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="excel.xls"');
		header('Cache-Control: max-age=0');
		$writer->save(__DIR__.'/../files/'.$rString.'.xls');

		$mensaje = New Excels();
		$mensaje->cod_mensaje = 2;
		$mensaje->mensaje = '';
		$mensaje->file = $rString.'.xls';

		return $mensaje;
	}
}
?>