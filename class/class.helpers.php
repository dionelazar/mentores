<?php 
/**
* 
*/
require_once 'class.encrypt.php';
require_once 'class.cookies.php';
require_once 'class.error.php';
require_once 'class.conection.php';
require_once 'class.excels.php';

class Helpers
{
	function insertarError($errorConNumero, $queryEjecutada, $params)
	{
		if(is_array($params))
	    	$paramsString = implode(',', $params);
	    else
	    	$paramsString = $params;
	    
	    Errores::insErrorDeQuery($errorConNumero, $queryEjecutada, $paramsString);
	}

	public function jsonEncodeError($cod_mensaje, $mensaje)
	{
		$data = new Helpers();
		$data->cod_mensaje = $cod_mensaje;
		$data->mensaje     = $mensaje;
		echo json_encode($data);
	}

	public function returnError($cod_mensaje, $mensaje)
	{
		$data = new Helpers();
		$data->cod_mensaje = $cod_mensaje;
		$data->mensaje     = $mensaje;
		return $data;
	}

	public function getDia($fecha)
	{

	}

	public function getNumDia($fecha)
	{
		
	}

	public function generateRandomString($length = 10) 
	{
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
}
?>