<?php 
/**
* 
*/
require_once 'class.helpers.php';

class Reportes
{
	
	function getEstadisticasNumeros($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_sede = $post['id_sede'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_estadisticas_numeros @id_usuario = ?, @id_sede = ?";
		$params = array($id_usuario, $id_sede);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 5001. ';
			Errores::insErrorDeQuery('Error 5001. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$estadisticas = new Reportes();
			$estadisticas->mentores_formacion = $row['mentores_formacion'];
			$estadisticas->mentores_formados = $row['mentores_formados'];
			$estadisticas->mentorias_activas = $row['mentorias_activas'];
			$estadisticas->emprendedores_en_proceso = $row['emprendedores_en_proceso'];
			$estadisticas->emprendedores_egresados = $row['emprendedores_egresados'];
			$estadisticas->alcance_territorial = $row['alcance_territorial'];
			$estadisticas->atencion_por_participante = $row['atencion_por_participante'];
			$estadisticas->impacto_grado_satisfaccion = $row['impacto_grado_satisfaccion'];
			$estadisticas->generoM = $row['generoM'];
			$estadisticas->generoF = $row['generoF'];
			$estadisticas->diversidad_productiva = $row['diversidad_productiva'];
	
			Conection::CerrarConexion($conn);
			echo json_encode($estadisticas);
		}
	}

	function getReporteGenero($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_sede = $post['id_sede'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_reporte_genero @id_usuario = ?, @id_sede = ?";
		$params = array($id_usuario, $id_sede);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 5002. ';
			Errores::insErrorDeQuery('Error 5002. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$reporte = new Reportes();

			$reporte->mentoresM = $row['mentoresM'];
			$reporte->mentoresF = $row['mentoresF'];
			$reporte->emprendedoresM = $row['emprendedoresM'];
			$reporte->emprendedoresF = $row['emprendedoresF'];
			$reporte->mentoresConM = $row['mentoresConM'];
			$reporte->mentoresConF = $row['mentoresConF'];
			$reporte->emprendedoresConM = $row['emprendedoresConM'];
			$reporte->emprendedoresConF = $row['emprendedoresConF'];
			$reporte->mentoresDtM = $row['mentoresDtM'];
			$reporte->mentoresDtF = $row['mentoresDtF'];
			$reporte->emprendedoresDtM = $row['emprendedoresDtM'];
			$reporte->emprendedoresDtF = $row['emprendedoresDtF'];
			$reporte->percentM = $row['percentM'];
			$reporte->percentF = $row['percentF'];

			Conection::CerrarConexion($conn);
			$excel = Excels::createExcelReporteGenero($reporte);
			$reporte->excel = $excel;

			echo json_encode($reporte);
		}
	}

	function getMentoresEnFormacion($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_sede = $post['id_sede'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_mentores_en_formacion @id_usuario = ?, @id_sede = ?";
		$params = array($id_usuario, $id_sede);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 5003. ';
			Errores::insErrorDeQuery('Error 5003. ', $sql, $params);
		}else{
			$usuarios = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miUsuario = new Reportes();
				$miUsuario->id_usuario = $row['id_usuario'];
				$miUsuario->nombre = utf8_encode($row['nombre']);
				$miUsuario->id_tipo_usuario = $row['id_tipo_usuario'];
				$miUsuario->email = utf8_encode($row['email']);
				$miUsuario->provincia = utf8_encode($row['provincia']);
				$miUsuario->ciudad = utf8_encode($row['ciudad']);
				$miUsuario->dni = $row['dni'];
				$miUsuario->sede = utf8_encode($row['sede']);

				$usuarios[$i] = $miUsuario;
				$i++;
			}			

			Conection::CerrarConexion($conn);

			$excel = Excels::createExcelMentoresEnFormacion($usuarios);
			$usuarios[count($usuarios)] = $excel;

			echo json_encode($usuarios);
		}
	}

	function getMentoresFormados($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_sede = $post['id_sede'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_mentores_formados @id_usuario = ?, @id_sede = ?";
		$params = array($id_usuario, $id_sede);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 5004. ';
			Errores::insErrorDeQuery('Error 5004. ', $sql, $params);
		}else{
			$usuarios = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miUsuario = new Reportes();
				$miUsuario->id_usuario = $row['id_usuario'];
				$miUsuario->nombre = utf8_encode($row['nombre']);
				$miUsuario->id_tipo_usuario = $row['id_tipo_usuario'];
				$miUsuario->email = utf8_encode($row['email']);
				$miUsuario->provincia = utf8_encode($row['provincia']);
				$miUsuario->ciudad = utf8_encode($row['ciudad']);
				$miUsuario->dni = $row['dni'];
				$miUsuario->sede = utf8_encode($row['sede']);

				$usuarios[$i] = $miUsuario;
				$i++;
			}			

			Conection::CerrarConexion($conn);

			$excel = Excels::createExcelMentoresFormados($usuarios);
			$usuarios[count($usuarios)] = $excel;

			echo json_encode($usuarios);
		}
	}

	function getMentoriasActivasDT($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_sede = $post['id_sede'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_mentorias_activas @id_usuario = ?, @id_sede = ?";
		$params = array($id_usuario, $id_sede);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 5005. ';
			Errores::insErrorDeQuery('Error 5005. ', $sql, $params);
		}else{
			$grupos = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miGrupo = new Reportes();
				$miGrupo->mentor = utf8_encode($row['mentor']);
				$miGrupo->dni_mentor = $row['dni_mentor'];
				$miGrupo->emprendedores = utf8_encode($row['emprendedores']);
				$miGrupo->dni_emprendedores = $row['dni_emprendedores'];
				$miGrupo->grupo = utf8_encode($row['grupo']);
				$miGrupo->id_tipo_usuario = $row['id_tipo_usuario'];
				$miGrupo->promedio_calificacion = !is_null($row['promedio_calificacion']) ? round($row['promedio_calificacion'], 2) : 'Sin calificaciones';
				$miGrupo->promedio_mentor = !is_null($row['promedio_mentor']) ? round($row['promedio_mentor'], 2) : 'Sin calificaciones';
				$miGrupo->cant_sesiones = $row['cant_sesiones'];

				$grupos[$i] = $miGrupo;
				$i++;
			}			

			Conection::CerrarConexion($conn);

			$excel = Excels::createExcelMentoriasActivas($grupos);
			$grupos[count($grupos)] = $excel;

			echo json_encode($grupos);
		}
	}

	function getMentoriasActivasCON($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_sede = $post['id_sede'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_mentorias_activas_con @id_usuario = ?, @id_sede = ?";
		$params = array($id_usuario, $id_sede);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 5005.1 ';
			Errores::insErrorDeQuery('Error 5005.1 ', $sql, $params);
		}else{
			$grupos = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miGrupo = new Reportes();
				$miGrupo->mentor = utf8_encode($row['mentor']);
				$miGrupo->dni_mentor = $row['dni_mentor'];
				$miGrupo->emprendedores = utf8_encode($row['emprendedor']);
				$miGrupo->dni_emprendedores = $row['dni_emprendedor'];
				$miGrupo->grupo = utf8_encode($row['grupo']);
				$miGrupo->id_tipo_usuario = $row['id_tipo_usuario'];
				$miGrupo->promedio_mentor = !is_null($row['promedio_calificacion_mentor']) ? round($row['promedio_calificacion_mentor'], 2) : 'Sin calificaciones';
				$miGrupo->promedio_emprendedor = !is_null($row['promedio_calificacion_emprendedor']) ? round($row['promedio_calificacion_emprendedor'], 2) : 'Sin calificaciones';
				$miGrupo->asistencias = $row['asistencias'];

				$grupos[$i] = $miGrupo;
				$i++;
			}			

			Conection::CerrarConexion($conn);

			$excel = Excels::createExcelMentoriasActivasCON($grupos);
			$grupos[count($grupos)] = $excel;

			echo json_encode($grupos);
		}
	}

	function getEmprendedoresEgresados($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_sede = $post['id_sede'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_emprendedores_egresados @id_usuario = ?, @id_sede = ?";
		$params = array($id_usuario, $id_sede);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 5006. ';
			Errores::insErrorDeQuery('Error 5006. ', $sql, $params);
		}else{
			$usuarios = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miUsuario = new Reportes();
				$miUsuario->id_usuario = $row['id_usuario'];
				$miUsuario->id_tipo_usuario = $row['id_tipo_usuario'];
				$miUsuario->nombre = utf8_encode($row['nombre']);
				$miUsuario->email = utf8_encode($row['email']);
				$miUsuario->pais = utf8_encode($row['pais']);
				$miUsuario->provincia = utf8_encode($row['provincia']);
				$miUsuario->ciudad = utf8_encode($row['ciudad']);
				$miUsuario->sede = utf8_encode($row['sede']);
				$miUsuario->dni = $row['dni'];
				$miUsuario->mentor = utf8_encode($row['mentor']);

				$usuarios[$i] = $miUsuario;
				$i++;
			}			

			Conection::CerrarConexion($conn);

			$excel = Excels::createExcelEmprendedoresEgresados($usuarios);
			$usuarios[count($usuarios)] = $excel;

			echo json_encode($usuarios);
		}
	}

	function getReunionSeguimientoAdmin($post)
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_reunion_seguimiento_admin";
		//$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 5005. ';
			Errores::insErrorDeQuery('Error 5005. ', $sql, $params);
		}else{
			$usuarios = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miUsuario = new Reportes();
				$miUsuario->id_usuario = $row['id_usuario'];
				$miUsuario->id_tipo_usuario = $row['id_tipo_usuario'];
				$miUsuario->id_sesion = $row['id_sesion'];
				$miUsuario->id_grupo = $row['id_grupo'];
				$miUsuario->calificacion = $row['calificacion'];
				$miUsuario->fecha = $row['fecha'] == null ? '' : $row['fecha']->format('d/m/y');
				$miUsuario->nombre = utf8_encode($row['nombre']);
				$miUsuario->email = utf8_encode($row['email']);
				$miUsuario->grupo = utf8_encode($row['grupo']);
				$miUsuario->provincia = utf8_encode($row['provincia']);
				$miUsuario->ciudad = utf8_encode($row['ciudad']);

				$usuarios[$i] = $miUsuario;
				$i++;
			}			

			Conection::CerrarConexion($conn);
			echo json_encode($usuarios);
		}
	}

	function getSesionesReunionesSeguimientoMentor($post)
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_reunion_seguimiento_mentor @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 5006. ';
			Errores::insErrorDeQuery('Error 5006. ', $sql, $params);
		}else{
			$sesiones = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miSesion = new Reportes();
				$miSesion->id_sesion = $row['id_sesion'];
				$miSesion->id_grupo = $row['id_grupo'];
				$miSesion->fecha = $row['fecha'] == null ? '' : $row['fecha']->format('d/m/y');
				$miSesion->sesion = utf8_encode($row['sesion']);
				$miSesion->grupo = utf8_encode($row['grupo']);

				$sesiones[$i] = $miSesion;
				$i++;
			}			

			Conection::CerrarConexion($conn);
			echo json_encode($sesiones);
		}
	}

	function getEncuestasSatisfaccion($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_sede = $post['id_sede'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_usuarios_calificaron_satisfaccion @id_usuario = ?, @id_sede = ?";
		$params = array($id_usuario, $id_sede);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 5007. ';
			Errores::insErrorDeQuery('Error 5007. ', $sql, $params);
		}else{
			$usuarios = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miUsuario = new Reportes();
				$miUsuario->id_usuario = $row['id_usuario'];
				$miUsuario->id_tipo_usuario = $row['id_tipo_usuario'];
				$miUsuario->dni = $row['dni'];
				$miUsuario->usuario = utf8_encode($row['usuario']);		
				$miUsuario->nombre = utf8_encode($row['nombre']);
				$miUsuario->estructura = $row['estructura'];
				$miUsuario->utilidad = $row['utilidad'];
				$miUsuario->cumplimiento = $row['cumplimiento'];
				$miUsuario->interaccion = $row['interaccion'];
				$miUsuario->desempeno = $row['desempeno'];
				$miUsuario->material = $row['material'];
				$miUsuario->valoracion = $row['valoracion'];
				$miUsuario->comentario = utf8_encode($row['comentario']);
				$miUsuario->sede = utf8_encode($row['sede']);
				$miUsuario->fecha = $row['fecha'] == null ? '' : $row['fecha']->format('d/m/y');

				$usuarios[$i] = $miUsuario;
				$i++;
			}
			Conection::CerrarConexion($conn);

			$excel = Excels::createExcelEncuesasSatisfaccion($usuarios);
			$usuarios[count($usuarios)] = $excel;

			echo json_encode($usuarios);
		}
	}

	function getEmprendedoresProceso($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_sede = $post['id_sede'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_reporte_usuarios_proceso @id_usuario = ?, @id_sede = ?";
		$params = array($id_usuario, $id_sede);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 5008. ';
			Errores::insErrorDeQuery('Error 5008. ', $sql, $params);
		}else{
			$usuarios = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miUsuario = new Reportes();
				$miUsuario->id_usuario = $row['id_usuario'];
				$miUsuario->nombre = utf8_encode($row['nombre']);
				$miUsuario->id_tipo_usuario = $row['id_tipo_usuario'];
				$miUsuario->email = utf8_encode($row['email']);
				$miUsuario->dni = $row['dni'];
				$miUsuario->rol = utf8_encode($row['rol']);
				$miUsuario->sede = utf8_encode($row['sede']);
				$miUsuario->nombre_mentor = utf8_encode($row['nombre_mentor']);
				$miUsuario->dni_mentor = $row['dni_mentor'];
				$miUsuario->sesiones = $row['cant_sesiones'];

				$usuarios[$i] = $miUsuario;
				$i++;
			}			

			Conection::CerrarConexion($conn);

			$excel = Excels::createExcelEmprendedoresEnProceso($usuarios);
			$usuarios[count($usuarios)] = $excel;

			echo json_encode($usuarios);
		}
	}

	function getSesionesAtencion($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_sede = $post['id_sede'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_reporte_atencion_participante @id_usuario = ?, @id_sede = ?";
		$params = array($id_usuario, $id_sede);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 5009. ';
			Errores::insErrorDeQuery('Error 5009. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );

			$sesiones = new Reportes();
			$sesiones->DTperSes1  = $row['DTperSes1'];
			$sesiones->DTperSes2  = $row['DTperSes2'];
			$sesiones->DTperSes3  = $row['DTperSes3'];
			$sesiones->DTperSes4  = $row['DTperSes4'];
			$sesiones->DTperSes5  = $row['DTperSes5'];
			$sesiones->DTperSes6  = $row['DTperSes6'];
			$sesiones->CONperSes1 = $row['CONperSes1'];
			$sesiones->CONperSes2 = $row['CONperSes2'];
			$sesiones->CONperSes3 = $row['CONperSes3'];

			Conection::CerrarConexion($conn);
			echo json_encode($sesiones);
		}
	}

	function getUsuariosEstadoAtencion($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_sede = $post['id_sede'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_reporte_atencion_participante_usuarios @id_usuario = ?, @id_sede = ?";
		$params = array($id_usuario, $id_sede);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 5010. ';
			Errores::insErrorDeQuery('Error 5010. ', $sql, $params);
		}else{
			$usuarios = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miUsuario = new Reportes();
				
				if($row['id_rol'] == 2 || $row['id_rol'] == 3)
					$miUsuario->emprendedor = '-';
				else
					$miUsuario->emprendedor = utf8_encode($row['emprendedor']);

				$miUsuario->mentor = utf8_encode($row['mentor']);
				$miUsuario->dni = $row['dni'];
				$miUsuario->valoracion = $row['valoracion'] == 0 ? 'sin valoración' : $row['valoracion'];
				$miUsuario->sesiones = $row['sesiones'];
				$miUsuario->total_sesiones = $row['total_sesiones'];
				$miUsuario->id_tipo_usuario = $row['id_rol'];
				$miUsuario->activo = $row['sn_activo'] == -1 ? 'Activo' : 'Abandonó';

				$usuarios[$i] = $miUsuario;
				$i++;
			}			

			Conection::CerrarConexion($conn);

			$excel = Excels::createExcelEstadoAtencionUsuarios($usuarios);
			$usuarios[count($usuarios)] = $excel;

			echo json_encode($usuarios);
		}
	}

	function getDiversidadProductiva($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_sede = $post['id_sede'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_reporte_diversidad_productiva @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 5011. ';
			Errores::insErrorDeQuery('Error 5011. ', $sql, $params);
		}else{
			$rubros = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miRubro = new Reportes();

				$miRubro->rubro = utf8_encode($row['rubro']);
				$miRubro->perRubro = $row['perRubro'];

				$rubros[$i] = $miRubro;
				$i++;
			}			

			Conection::CerrarConexion($conn);

			$excel = Excels::createExcelDiversidadProductiva($rubros);
			$rubros[count($rubros)] = $excel;

			echo json_encode($rubros);
		}
	}

	function getImpactoDesarrollo($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_sede = $post['id_sede'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_impacto_desarrollo @id_usuario = ?, @id_sede = ?";
		$params = array($id_usuario, $id_sede);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 5012. ';
			Errores::insErrorDeQuery('Error 5012. ', $sql, $params);
		}else{
			$usuarios = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miUsuario = new Reportes();

				$miUsuario->emprendedor = utf8_encode($row['nombre']);
				$miUsuario->sede = utf8_encode($row['sede']);
				$miUsuario->dni = $row['dni'];
				$miUsuario->diagnostico_inicial = $row['diagnostico_inicial'];
				$miUsuario->diagnostico_final = $row['diagnostico_final'];
				$miUsuario->impacto = round($row['impacto'], 2);
				$miUsuario->id_tipo_usuario = $row['id_tipo_usuario'];

				$usuarios[$i] = $miUsuario;
				$i++;
			}

			Conection::CerrarConexion($conn);

			$excel = Excels::createExcelImpactoDesarrollo($usuarios);
			$usuarios[count($usuarios)] = $excel;

			echo json_encode($usuarios);
		}
	}

	function getSedesOperativas($post)
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_sedes_operativas @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 5013. ';
			Errores::insErrorDeQuery('Error 5013. ', $sql, $params);
		}else{
			$sedes = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miSede = new Reportes();

				$miSede->sede = utf8_encode($row['sede']);
				$miSede->mentores = $row['mentores'];
				$miSede->emprendedores = $row['emprendedores'];

				$sedes[$i] = $miSede;
				$i++;
			}

			Conection::CerrarConexion($conn);

			$excel = Excels::createExcelSedesOperativas($sedes);
			$sedes[count($sedes)] = $excel;

			echo json_encode($sedes);
		}
	}

	function getEmprendedoresPropuestosFase2($post)
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_emprendedores_propuestos_fase_2 @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 5014. ';
			Errores::insErrorDeQuery('Error 5014. ', $sql, $params);
		}else{
			$usuarios = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miUsuario = new Reportes();

				$miUsuario->id_mentor = $row['id_mentor'];
				$miUsuario->mentor = utf8_encode($row['mentor']);
				$miUsuario->id_emprendedor = $row['id_emprendedor'];
				$miUsuario->emprendedor = utf8_encode($row['emprendedor']);
				$miUsuario->dni_emprendedor = $row['dni_emprendedor'];
				$miUsuario->rol = utf8_encode($row['rol']);
				$miUsuario->sede = utf8_encode($row['sede']);
				$miUsuario->asistencias = $row['asistencias'];
				$miUsuario->fecha = $row['fecha']->format('d/m/y');
				switch ($row['sn_aceptado']) {
					case -1:
						$miUsuario->sn_aceptado = '<span style="color:green;">Aceptado</span>';
						break;

					case 0:
						$miUsuario->sn_aceptado = '<span>Propuesto</span>';
						break;

					case 2:
						$miUsuario->sn_aceptado = '<span style="color:red;">Rechazado</span>';
						break;
					
					default:
						$miUsuario->sn_aceptado = '<span>Propuesto</span>';
						break;
				}

				$usuarios[$i] = $miUsuario;
				$i++;
			}

			Conection::CerrarConexion($conn);

			echo json_encode($usuarios);
		}
	}

	function getUsuariosDelGrupo($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_grupo = $post['id_grupo'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_reporte_usuarios_by_grupo @id_usuario = ?, @id_grupo = ?";
		$params = array($id_usuario, $id_grupo);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 5015. ';
			Errores::insErrorDeQuery('Error 5015. ', $sql, $params);
		}else{
			$usuarios = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miUsuario = new Reportes();

				$miUsuario->id_usuario = $row['id_usuario'];
				$miUsuario->nombre = utf8_encode($row['nombre']);
				$miUsuario->dni = $row['dni'];
				$miUsuario->rol = utf8_encode($row['rol']);
				$miUsuario->id_rol = $row['id_rol'];
				$miUsuario->cant_sesiones = $row['cant_sesiones'];

				$usuarios[$i] = $miUsuario;
				$i++;
			}

			Conection::CerrarConexion($conn);

			echo json_encode($usuarios);
		}
	}

	function getSesionesDelUsuario($post)
	{
		require_once 'class.sesiones.php';
		$sesiones = new Sesiones();

		switch ($post['id_rol']) {
			case 2:
				return $sesiones->getSesiones($post);
				break;
			
			case 3:
				return $sesiones->getSesionesMentorCon($post);
				break;

			case 4:
				return $sesiones->getSesionesDT($post);
				break;

			case 5:
				return $sesiones->getSesionesCON($post);
				break;

			default:
				return array('mensaje' => 'error');
				break;
		}
	}


	/************************************************************************************************************************************************************************/

	function calificarReunionSeguimiento($post)
	{
		$id_sesion = $post['id_sesion'];
		$id_grupo = $post['id_grupo'];
		$resp1 = $post['resp1'];
		$resp2 = $post['resp2'];
		$stars = $post['stars'];
		$comentario = $post['comentario'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_ins_calificacion_seguimiento @id_sesion = ?, @id_grupo = ?, @resp1 = ?, @resp2 = ?, @stars = ?, @comentario = ?";
		$params = array($id_sesion, $id_grupo, $resp1, $resp2, $stars, $comentario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 5101. ';
			Errores::insErrorDeQuery('Error 5101. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );

			$response = new Reportes();
			$response->cod_mensaje = $row['cod_mensaje'];
			$response->mensaje = utf8_encode($row['mensaje']);

			Conection::CerrarConexion($conn);
			echo json_encode($response);
		}
	}

	function marcarAsistencia($post)
	{
		$id_sesion = $post['id_sesion'];
		$id_grupo = $post['id_grupo'];
		$id_usuario = $post['id_usuario'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_ins_asistencia_seguimiento @id_sesion = ?, @id_grupo = ?, @id_usuario = ?";
		$params = array($id_sesion, $id_grupo, $id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 5102. ';
			Errores::insErrorDeQuery('Error 5102. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );

			$response = new Reportes();
			$response->cod_mensaje = $row['cod_mensaje'];
			$response->mensaje = utf8_encode($row['mensaje']);

			Conection::CerrarConexion($conn);
			echo json_encode($response);
		}
	}

	function desmarcarAsistencia($post)
	{
		$id_sesion = $post['id_sesion'];
		$id_grupo = $post['id_grupo'];
		$id_usuario = $post['id_usuario'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_upd_desmarcar_asistencia_seguimiento @id_sesion = ?, @id_grupo = ?, @id_usuario = ?";
		$params = array($id_sesion, $id_grupo, $id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 5102.1 ';
			Errores::insErrorDeQuery('Error 5102.1 ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );

			$response = new Reportes();
			$response->cod_mensaje = $row['cod_mensaje'];
			$response->mensaje = utf8_encode($row['mensaje']);

			Conection::CerrarConexion($conn);
			echo json_encode($response);
		}
	}

}
?>