<?php 
/**
* 
*/
class Errores
{
	function insErrorDeQuery($mensaje, $query, $parametros)
	{
		$query = Self::parseQuery($query, $parametros);

		$id_usuario = Cookies::getIdUsuario();
		$parametros = implode (", ", $parametros);
		$conn = Conection::Conexion();
		$sql = "EXEC sp_ins_error @mensaje = ?, @tipo_error = ?, @query = ?, @params = ?, @id_usuario = ?";
		$params = array($mensaje, 'Error de query', $query, $parametros, $id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 001. '.$sql.'<br>';
			print_r($params);
		}else{
			Conection::CerrarConexion($conn);
			return;
		}
	}

	public function parseQuery($query, $params)
	{
		foreach (array_fill(0, count($params), '?') as $key => $wildcard) 
		{
			if( intval($params[$key]) ){
				if ( intval($params[$key]) < 1000 )
					$query = substr_replace($query, $params[$key], strpos($query, $wildcard), strlen($wildcard));
				else
					$query = substr_replace($query, "'".$params[$key]."'", strpos($query, $wildcard), strlen($wildcard));
			}else{
		    	$query = substr_replace($query, "'".$params[$key]."'", strpos($query, $wildcard), strlen($wildcard));
			}
		}
		
		return $query;
	}
}
?>