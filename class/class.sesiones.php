<?php 
/**
* 
*/
require_once 'class.helpers.php';

class Sesiones
{
	
	function getSesiones($post)//mentorDT
	{
		if(isset($post['id_usuario']))
			$id_usuario = $post['id_usuario'];
		else
			$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_sesiones_mentor_DT @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2001. ';
			Errores::insErrorDeQuery('Error 2001. ', $sql, $params);
		}else{
			$sesiones = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miSesion = new Sesiones();
				$miSesion->mensaje = utf8_encode($row['mensaje']);
				$miSesion->id_sesion = $row['id_sesion'];
				$miSesion->id_grupo = $row['id_grupo'];
				$miSesion->desc = utf8_encode($row['desc']);
				$miSesion->emprendedores = utf8_encode($row['emprendedores']);
				$miSesion->sn_califico = $row['sn_califico'];

				$sesiones[$i] = $miSesion;
				$i++;
			}			

			Conection::CerrarConexion($conn);
			echo json_encode($sesiones);
		}
	}

	function getSesionesDT($post)
	{
		if(isset($post['id_usuario']))
			$id_usuario = $post['id_usuario'];
		else
			$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_sesiones_DT @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2001.1 ';
			Errores::insErrorDeQuery('Error 2001.1 ', $sql, $params);
		}else{
			$sesiones = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miSesion = new Sesiones();
				$miSesion->mensaje = utf8_encode($row['mensaje']);
				$miSesion->id_sesion = $row['id_sesion'];
				$miSesion->id_grupo = $row['id_grupo'];
				$miSesion->desc = utf8_encode($row['desc']);
				$miSesion->comentario = utf8_encode($row['comentario']);
				$miSesion->mentor = utf8_encode($row['mentor']);
				$miSesion->sn_califico = $row['sn_califico'];

				$sesiones[$i] = $miSesion;
				$i++;
			}			

			Conection::CerrarConexion($conn);
			echo json_encode($sesiones);
		}
	}

	function getSesionesMentorCon($post)
	{
		if(isset($post['id_usuario']))
			$id_usuario = $post['id_usuario'];
		else
			$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_sesiones_mentor_con @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2001.2 ';
			Errores::insErrorDeQuery('Error 2001.2 ', $sql, $params);
		}else{
			$sesiones = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miSesion = new Sesiones();
				$miSesion->mensaje = utf8_encode($row['mensaje']);
				$miSesion->id_sesion = $row['id_sesion'];
				$miSesion->id_grupo = $row['id_grupo'];
				$miSesion->desc = utf8_encode($row['desc']);
				$miSesion->emprendedores = utf8_encode($row['emprendedores']);
				$miSesion->sn_califico = $row['sn_califico'];

				$sesiones[$i] = $miSesion;
				$i++;
			}			

			Conection::CerrarConexion($conn);
			echo json_encode($sesiones);
		}
	}

	function getSesionesCON($post)
	{
		if(isset($post['id_usuario']))
			$id_usuario = $post['id_usuario'];
		else
			$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_sesiones_CON @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2001.3 ';
			Errores::insErrorDeQuery('Error 2001.3 ', $sql, $params);
		}else{
			$sesiones = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miSesion = new Sesiones();
				$miSesion->mensaje = utf8_encode($row['mensaje']);
				$miSesion->id_sesion = $row['id_sesion'];
				$miSesion->id_grupo = $row['id_grupo'];
				$miSesion->desc = utf8_encode($row['desc']);
				$miSesion->comentario = utf8_encode($row['comentario']);
				$miSesion->mentor = utf8_encode($row['mentor']);
				$miSesion->sn_califico = $row['sn_califico'];

				$sesiones[$i] = $miSesion;
				$i++;
			}			

			Conection::CerrarConexion($conn);
			echo json_encode($sesiones);
		}
	}

	function getFormCalificacionMentorDT($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_sesion = $post['id_sesion'];
		$id_grupo = $post['id_grupo'];
		$id_emprendedor = isset($post['id_emprendedor']) ? $post['id_emprendedor'] : 0;

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_form @id_usuario = ?, @id_sesion = ?, @id_grupo = ?, @id_emprendedor = ?";
		$params = array($id_usuario, $id_sesion, $id_grupo, $id_emprendedor);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2002. ';
			Errores::insErrorDeQuery('Error 2002. ', $sql, $params);
		}else{			
			$miSesion = new Sesiones();
			
			$miSesion->emprendedores = Self::getEmprendedoresDeGrupo($id_grupo);

			$preguntas = array();
			$i = 0;
			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC )){
				$miPregunta = new Sesiones();

				$miPregunta->cod_mensaje = $row['cod_mensaje'];
				if($miPregunta->cod_mensaje == 0){
					$miPregunta->mensaje = utf8_encode($row['mensaje']);
					$preguntas[$i] = $miPregunta;
					break;
				}
				$miPregunta->id_pregunta = $row['id_pregunta'];
				$miPregunta->num_pregunta = $row['num_pregunta'];
				$miPregunta->pregunta = utf8_encode($row['pregunta']);
				$miPregunta->respuestas = Self::getRespuestas($miPregunta->id_pregunta);
				$preguntas[$i] = $miPregunta;
				$i++;
			}

			$miSesion->preguntas = $preguntas;

			Conection::CerrarConexion($conn);
			echo json_encode($miSesion);
		}
	}

	function getFormCalificacionDT($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_sesion = $post['id_sesion'];
		$id_grupo = $post['id_grupo'];
		$id_emprendedor = $id_usuario;

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_form @id_usuario = ?, @id_sesion = ?, @id_grupo = ?, @id_emprendedor = ?";
		$params = array($id_usuario, $id_sesion, $id_grupo, $id_emprendedor);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2002.1 ';
			Errores::insErrorDeQuery('Error 2002.1 ', $sql, $params);
		}else{			
			$miSesion = new Sesiones();

			$preguntas = array();
			$i = 0;
			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC )){
				$miPregunta = new Sesiones();

				$miPregunta->cod_mensaje = $row['cod_mensaje'];
				if($miPregunta->cod_mensaje == 0){
					$miPregunta->mensaje = utf8_encode($row['mensaje']);
					$preguntas[$i] = $miPregunta;
					break;
				}
				$miPregunta->id_pregunta = $row['id_pregunta'];
				$miPregunta->num_pregunta = $row['num_pregunta'];
				$miPregunta->pregunta = utf8_encode($row['pregunta']);
				$miPregunta->respuestas = Self::getRespuestas($miPregunta->id_pregunta);
				$preguntas[$i] = $miPregunta;				
				$i++;
			}

			$miSesion->preguntas = $preguntas;

			Conection::CerrarConexion($conn);
			echo json_encode($miSesion);
		}
	}

	function getFormCalificacionMentorCON($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_sesion = $post['id_sesion'];
		$id_grupo = $post['id_grupo'];
		$id_emprendedor = $post['id_emprendedor'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_form @id_usuario = ?, @id_sesion = ?, @id_grupo = ?, @id_emprendedor = ?";
		$params = array($id_usuario, $id_sesion, $id_grupo, $id_emprendedor);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2002.2 ';
			Errores::insErrorDeQuery('Error 2002.2 ', $sql, $params);
		}else{			
			$miSesion = new Sesiones();
			
			$miSesion->emprendedores = Self::getEmprendedoresDeGrupo($id_grupo);

			$preguntas = array();
			$i = 0;
			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC )){
				$miPregunta = new Sesiones();

				$miPregunta->cod_mensaje = $row['cod_mensaje'];
				if($miPregunta->cod_mensaje == 0){
					$miPregunta->mensaje = utf8_encode($row['mensaje']);
					$preguntas[$i] = $miPregunta;
					break;
				}
				$miPregunta->id_pregunta = $row['id_pregunta'];
				$miPregunta->num_pregunta = $row['num_pregunta'];
				$miPregunta->pregunta = utf8_encode($row['pregunta']);
				$miPregunta->respuestas = Self::getRespuestas($miPregunta->id_pregunta);
				$preguntas[$i] = $miPregunta;
				$i++;
			}

			$miSesion->preguntas = $preguntas;

			Conection::CerrarConexion($conn);
			echo json_encode($miSesion);
		}
	}

	function getFormCalificacionCON($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_sesion = $post['id_sesion'];
		$id_grupo = $post['id_grupo'];
		$id_emprendedor = $id_usuario;

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_form @id_usuario = ?, @id_sesion = ?, @id_grupo = ?, @id_emprendedor = ?";
		$params = array($id_usuario, $id_sesion, $id_grupo, $id_emprendedor);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2002.3 ';
			Errores::insErrorDeQuery('Error 2002.3 ', $sql, $params);
		}else{			
			$miSesion = new Sesiones();
			$preguntas = array();
			$i = 0;
			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miPregunta = new Sesiones();

				$miPregunta->cod_mensaje = $row['cod_mensaje'];
				if($miPregunta->cod_mensaje == 0){
					$miPregunta->mensaje = utf8_encode($row['mensaje']);
					$preguntas[$i] = $miPregunta;
					break;
				}
				$miPregunta->id_pregunta = $row['id_pregunta'];
				$miPregunta->num_pregunta = $row['num_pregunta'];
				$miPregunta->pregunta = utf8_encode($row['pregunta']);
				$miPregunta->respuestas = Self::getRespuestas($miPregunta->id_pregunta);
				$preguntas[$i] = $miPregunta;
				$i++;
			}

			$miSesion->preguntas = $preguntas;

			Conection::CerrarConexion($conn);
			echo json_encode($miSesion);
		}
	}

	function getFormCalificacionSeguimiento($post)
	{
		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_form_seguimiento";
		$stmt = sqlsrv_query($conn, $sql);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2002.4 ';
			Errores::insErrorDeQuery('Error 2002.4 ', $sql, $params);
		}else{			
			$miSesion = new Sesiones();

			$preguntas = array();
			$i = 0;
			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miPregunta = new Sesiones();

				$miPregunta->id_pregunta = $row['id_pregunta'];
				$miPregunta->num_pregunta = $row['num_pregunta'];
				$miPregunta->pregunta = utf8_encode($row['pregunta']);
				$miPregunta->respuestas = Self::getRespuestas($miPregunta->id_pregunta);
				$preguntas[$i] = $miPregunta;
				$i++;
			}

			$miSesion->preguntas = $preguntas;

			Conection::CerrarConexion($conn);
			echo json_encode($miSesion);
		}
	}

	function getComprobarCalificacion($post)
	{
		if(isset($post['id_usuario']))
			$id_usuario = $post['id_usuario'];
		else
			$id_usuario = Cookies::getIdUsuario();

		$id_sesion = $post['id_sesion'];
		$id_grupo = $post['id_grupo'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_comprobar_calificacion @id_usuario = ?, @id_sesion = ?, @id_grupo = ?";
		$params = array($id_usuario, $id_sesion, $id_grupo);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2003. ';
			Errores::insErrorDeQuery('Error 2003. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$miSesion = new Sesiones();
			$miSesion->mensaje = utf8_encode($row['mensaje']);
			$miSesion->comentario = isset($row['comentario']) ? utf8_encode($row['comentario']) : '';
			$miSesion->promedio = $row['promedio'];

			Conection::CerrarConexion($conn);
			echo json_encode($miSesion);
		}
	}

	function getEmprendedoresDeGrupo($id_grupo)
	{
		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_emprendedores_from_grupo @id_grupo = ?";
		$params = array($id_grupo);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2004. ';
			Errores::insErrorDeQuery('Error 2004. ', $sql, $params);
		}else{
			$sesiones = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miSesion = new Sesiones();
				$miSesion->id_usuario = $row['id_usuario'];
				$miSesion->nombre = utf8_encode($row['nombre']);
				$miSesion->documento = $row['documento'];
				$miSesion->sn_abandono = $row['sn_abandono'];

				$sesiones[$i] = $miSesion;
				$i++;
			}			

			Conection::CerrarConexion($conn);
			return $sesiones;
		}
	}

	function getEmprendedoresDeGrupoByIdGrupo($post)
	{
		$id_grupo = $post['id_grupo'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_emprendedores_from_grupo @id_grupo = ?";
		$params = array($id_grupo);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2004. ';
			Errores::insErrorDeQuery('Error 2004. ', $sql, $params);
		}else{
			$sesiones = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miSesion = new Sesiones();
				$miSesion->id_usuario = $row['id_usuario'];
				$miSesion->nombre = utf8_encode($row['nombre']);
				$miSesion->documento = $row['documento'];
				$miSesion->sn_abandono = $row['sn_abandono'];

				$sesiones[$i] = $miSesion;
				$i++;
			}			

			Conection::CerrarConexion($conn);
			return $sesiones;
		}
	}

	function getEmprendedoresDeGrupoById($post)
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_emprendedores_from_id @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2004. ';
			Errores::insErrorDeQuery('Error 2004. ', $sql, $params);
		}else{
			$sesiones = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miSesion = new Sesiones();
				$miSesion->id_usuario = $row['id_usuario'];
				$miSesion->nombre = utf8_encode($row['nombre']);
				$miSesion->documento = $row['documento'];
				$miSesion->sn_abandono = $row['sn_abandono'];

				$sesiones[$i] = $miSesion;
				$i++;
			}			

			Conection::CerrarConexion($conn);
			return $sesiones;
		}
	}

	function getRespuestas($id_pregunta)
	{
		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_opciones_pregunta @id_pregunta = ?";
		$params = array($id_pregunta);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2005. ';
			Errores::insErrorDeQuery('Error 2005. ', $sql, $params);
		}else{
			$respuestas = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miRespuesta = new Sesiones();
				$miRespuesta->id_respuesta = $row['id_respuesta'];
				$miRespuesta->respuesta = utf8_encode($row['respuesta']);

				$respuestas[$i] = $miRespuesta;
				$i++;
			}			

			Conection::CerrarConexion($conn);
			return $respuestas;
		}
	}

	function getGruposReporte($post)
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_grupos_reporte @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2006. ';
			Errores::insErrorDeQuery('Error 2006. ', $sql, $params);
		}else{
			$grupos = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miGrupo = new Sesiones();
				$miGrupo->id_grupo = $row['id_grupo'];
				$miGrupo->id_tipo_grupo = $row['id_tipo_grupo'];
				$miGrupo->promedio = $row['promedio'];
				$miGrupo->mentor = utf8_encode($row['mentor']);
				$miGrupo->grupo = utf8_encode($row['grupo']);
				$miGrupo->imagen = $row['imagen'];

				$grupos[$i] = $miGrupo;
				$i++;
			}

			Conection::CerrarConexion($conn);
			return $grupos;
		}
	}

	function getCalificacionesEmprendedores($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_grupo = $post['id_grupo'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_calificaciones_reporte @id_usuario = ?, @id_grupo = ?";
		$params = array($id_usuario, $id_grupo);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2007. ';
			Errores::insErrorDeQuery('Error 2007. ', $sql, $params);
		}else{
			$calificaciones = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miCalificacion = new Sesiones();
				$miCalificacion->promedio = $row['promedio'];
				$miCalificacion->comentario = utf8_encode($row['comentario']) == '' ? 'Calificación pendiente' : utf8_encode($row['comentario']);
				$miCalificacion->nombre = utf8_encode($row['nombre']);

				$calificaciones[$i] = $miCalificacion;
				$i++;
			}			

			Conection::CerrarConexion($conn);
			return $calificaciones;
		}
	}

	function getGruposPorPais($post)
	{
		$id_pais = $post['id_pais'];
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_grupos_por_pais @id_usuario = ?, @id_pais = ?";
		$params = array($id_usuario, $id_pais);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2008. ';
			Errores::insErrorDeQuery('Error 2008. ', $sql, $params);
		}else{
			$grupos = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miGrupo = new Sesiones();
				$miGrupo->id_grupo = $row['id_grupo'];
				$miGrupo->id_tipo_grupo = $row['id_tipo_grupo'];
				$miGrupo->promedio = $row['promedio'];
				$miGrupo->mentor = utf8_encode($row['mentor']);
				$miGrupo->grupo = utf8_encode($row['grupo']);

				$grupos[$i] = $miGrupo;
				$i++;
			}

			Conection::CerrarConexion($conn);
			return $grupos;
		}
	}

	function getGruposPorProvincia($post)
	{
		$id_pais = $post['id_pais'];
		$id_provincia = $post['id_provincia'];
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_grupos_por_provincia @id_usuario = ?, @id_pais = ?, @id_provincia = ?";
		$params = array($id_usuario, $id_pais, $id_provincia);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2009. ';
			Errores::insErrorDeQuery('Error 2009. ', $sql, $params);
		}else{
			$grupos = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miGrupo = new Sesiones();
				$miGrupo->id_grupo = $row['id_grupo'];
				$miGrupo->id_tipo_grupo = $row['id_tipo_grupo'];
				$miGrupo->promedio = $row['promedio'];
				$miGrupo->mentor = utf8_encode($row['mentor']);
				$miGrupo->grupo = utf8_encode($row['grupo']);

				$grupos[$i] = $miGrupo;
				$i++;
			}

			Conection::CerrarConexion($conn);
			return $grupos;
		}
	}

	function getGruposPorCiudad($post)
	{
		$id_pais = $post['id_pais'];
		$id_provincia = $post['id_provincia'];
		$id_ciudad = $post['id_ciudad'];
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_grupos_por_ciudad @id_usuario = ?, @id_pais = ?, @id_provincia = ?, @id_ciudad = ?";
		$params = array($id_usuario, $id_pais, $id_provincia, $id_ciudad);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2010. ';
			Errores::insErrorDeQuery('Error 2010. ', $sql, $params);
		}else{
			$grupos = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miGrupo = new Sesiones();
				$miGrupo->id_grupo = $row['id_grupo'];
				$miGrupo->id_tipo_grupo = $row['id_tipo_grupo'];
				$miGrupo->promedio = $row['promedio'];
				$miGrupo->mentor = utf8_encode($row['mentor']);
				$miGrupo->grupo = utf8_encode($row['grupo']);

				$grupos[$i] = $miGrupo;
				$i++;
			}

			Conection::CerrarConexion($conn);
			return $grupos;
		}
	}

	function getGruposPorMentoria($post)
	{
		$id_mentoria = $post['id_mentoria'];
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_grupos_por_mentoria @id_usuario = ?, @id_mentoria = ?";
		$params = array($id_usuario, $id_mentoria);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2011. ';
			Errores::insErrorDeQuery('Error 2011. ', $sql, $params);
		}else{
			$grupos = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miGrupo = new Sesiones();
				$miGrupo->id_grupo = $row['id_grupo'];
				$miGrupo->id_tipo_grupo = $row['id_tipo_grupo'];
				$miGrupo->promedio = $row['promedio'];
				$miGrupo->mentor = utf8_encode($row['mentor']);
				$miGrupo->grupo = utf8_encode($row['grupo']);

				$grupos[$i] = $miGrupo;
				$i++;
			}

			Conection::CerrarConexion($conn);
			return $grupos;
		}
	}

	function getGruposPorSede($post)
	{
		$id_sede = $post['id_sede'];
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_grupos_por_sede @id_usuario = ?, @id_sede = ?";
		$params = array($id_usuario, $id_sede);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2012. ';
			Errores::insErrorDeQuery('Error 2012. ', $sql, $params);
		}else{
			$grupos = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miGrupo = new Sesiones();
				$miGrupo->id_grupo = $row['id_grupo'];
				$miGrupo->id_tipo_grupo = $row['id_tipo_grupo'];
				$miGrupo->promedio = $row['promedio'];
				$miGrupo->mentor = utf8_encode($row['mentor']);
				$miGrupo->grupo = utf8_encode($row['grupo']);

				$grupos[$i] = $miGrupo;
				$i++;
			}

			Conection::CerrarConexion($conn);
			return $grupos;
		}
	}

	function getRubros($post)
	{
		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_rubros";
		$stmt = sqlsrv_query($conn, $sql);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2013. ';
			Errores::insErrorDeQuery('Error 2013. ', $sql, $params);
		}else{
			$rubros = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miRubro = new Sesiones();
				$miRubro->id_rubro = $row['id_rubro'];
				$miRubro->rubro = utf8_encode($row['rubro']);

				$rubros[$i] = $miRubro;
				$i++;
			}

			Conection::CerrarConexion($conn);
			return $rubros;
		}
	}

	function getComprobarEncuestaEmprendedor($post)
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_comprobar_encuesta_emprendedor @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2014. ';
			Errores::insErrorDeQuery('Error 2014. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$response = new Sesiones();
			$response->cod_mensaje = $row['cod_mensaje'];
			$response->mensaje = utf8_encode($row['mensaje']);

			Conection::CerrarConexion($conn);
			echo json_encode( $response );
		}
	}

	function getCalificacionesEmprendedor($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_emprendedor = $post['id_emprendedor'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_calificaciones_emprendedor @id_usuario = ?, @id_emprendedor = ?";
		$params = array($id_usuario, $id_emprendedor);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2015. ';
			Errores::insErrorDeQuery('Error 2015. ', $sql, $params);
		}else{
			$sesiones = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miSesion = new Sesiones();
				$miSesion->mensaje = utf8_encode($row['mensaje']);
				$miSesion->cod_mensaje = $row['cod_mensaje'];
				
				if($miSesion->cod_mensaje == 0){
					$sesiones[$i] = $miSesion;
					break;
				}
				
				$miSesion->id_sesion = $row['id_sesion'];
				$miSesion->id_grupo = $row['id_grupo'];
				$miSesion->comentario = utf8_encode($row['comentario']);
				$miSesion->puntuacion = $row['puntuacion'];
				$miSesion->grupo = (int) filter_var($row['grupo'], FILTER_SANITIZE_NUMBER_INT);
				$miSesion->id_usuario = $row['id_usuario'];
				$miSesion->id_emprendedor = $row['id_emprendedor'];
				$miSesion->propio = $id_usuario == $miSesion->id_usuario ? -1 : 0;

				$sesiones[$i] = $miSesion;
				$i++;
			}

			Conection::CerrarConexion($conn);
			echo json_encode( $sesiones );
		}
	}

	function getCalificacionesGrupo($post)
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_calificaciones_grupo @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2016. ';
			Errores::insErrorDeQuery('Error 2016. ', $sql, $params);
		}else{
			$sesiones = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miSesion = new Sesiones();
				$miSesion->mensaje = utf8_encode($row['mensaje']);
				$miSesion->cod_mensaje = $row['cod_mensaje'];
				
				if($miSesion->cod_mensaje == 0){
					$sesiones[$i] = $miSesion;
					break;
				}
				
				$miSesion->id_sesion = $row['id_sesion'];
				$miSesion->id_grupo = $row['id_grupo'];
				$miSesion->comentario = utf8_encode($row['comentario']);
				$miSesion->puntuacion = $row['puntuacion'];
				$miSesion->grupo = (int) filter_var($row['grupo'], FILTER_SANITIZE_NUMBER_INT);
				$miSesion->id_usuario = $row['id_usuario'];
				$miSesion->usuario = utf8_encode($row['usuario']);
				$miSesion->id_emprendedor = $row['id_emprendedor'];
				$miSesion->propio = $id_usuario == $miSesion->id_usuario ? -1 : 0;

				$sesiones[$i] = $miSesion;
				$i++;
			}

			Conection::CerrarConexion($conn);
			echo json_encode( $sesiones );
		}
	}

	function getCalificacion($post)
	{
		if(isset($post['id_usuario']))
			$id_usuario = $post['id_usuario'];
		else
			$id_usuario = Cookies::getIdUsuario();
		
		$id_sesion = $post['id_sesion'];
		$id_grupo = $post['id_grupo'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_calificacion @id_usuario = ?, @id_sesion = ?, @id_grupo = ?";
		$params = array($id_usuario, $id_sesion, $id_grupo);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion( $conn );
			echo 'Error 2017. ';
			Errores::insErrorDeQuery('Error 2017. ', $sql, $params);
		}else{
			$i = 0;
			$calificaciones = array();

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miCalificacion = new Sesiones();
			
				$miCalificacion->id_calificacion = $row['id_calificacion'];
				$miCalificacion->id_usuario      = $row['id_usuario'];
				$miCalificacion->id_sesion       = $row['id_sesion'];
				$miCalificacion->id_grupo        = $row['id_grupo'];
				$miCalificacion->puntuacion      = $row['puntuacion'];
				$miCalificacion->txt_comentario  = utf8_encode($row['txt_comentario']);
				$miCalificacion->fecha           = $row['fecha']->format('d/m/y');
				$miCalificacion->id_emprendedor  = $row['id_emprendedor'];
				$miCalificacion->txt_nombre      = utf8_encode($row['txt_nombre']);
				$miCalificacion->asistencia      = $row['asistencia'];
				$miCalificacion->respuestas = Self::getMisRespuestas(array('id_usuario' => $miCalificacion->id_usuario, 
																		   'id_sesion'  => $miCalificacion->id_sesion, 
																		   'id_grupo'   => $miCalificacion->id_grupo) );

				$calificaciones[$i] = $miCalificacion;
				$i++;
			}

			Conection::CerrarConexion( $conn );
			echo json_encode( $calificaciones );
		}
	}

	function getMisRespuestas($arr)
	{
		$id_usuario = $arr['id_usuario'];
		$id_sesion  = $arr['id_sesion'];
		$id_grupo   = $arr['id_grupo'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_mis_respuestas @id_usuario = ?, @id_sesion = ?, @id_grupo = ?";
		$params = array($id_usuario, $id_sesion, $id_grupo);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion( $conn );
			echo 'Error 2018. ';
			Errores::insErrorDeQuery('Error 2018. ', $sql, $params);
		}else{
			$i = 0;
			$respuestas = array();

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miRespuesta = new Sesiones();
				
				$miRespuesta->id              = $row['id'];
				$miRespuesta->id_respuesta    = $row['id_respuesta'];
				$miRespuesta->id_usuario      = $row['id_usuario'];
				$miRespuesta->id_sesion       = $row['id_sesion'];
				$miRespuesta->id_grupo        = $row['id_grupo'];
				$miRespuesta->id_pregunta     = $row['id_pregunta'];
				$miRespuesta->respuesta       = utf8_encode($row['respuesta']);
				$miRespuesta->pregunta        = utf8_encode($row['pregunta']);

				$respuestas[$i] = $miRespuesta;
				$i++;
			}

			Conection::CerrarConexion( $conn );
			return $respuestas;
		}
	}

	function getCalificacionesGrupoAdmin($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_grupo   = $post['id_grupo'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_calificaciones_grupo_admin @id_usuario = ?, @id_grupo = ?";
		$params = array($id_usuario, $id_grupo);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion( $conn );
			echo 'Error 2019. ';
			Errores::insErrorDeQuery('Error 2019. ', $sql, $params);
		}else{
			$i = 0;
			$response = array();

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miCalificacion = new Sesiones();
				
				$miCalificacion->id_calificacion  = $row['id_calificacion'];
				$miCalificacion->id_sesion        = $row['id_sesion'];
				$miCalificacion->puntuacion       = $row['puntuacion'];
				$miCalificacion->comentario       = utf8_encode($row['comentario']);
				$miCalificacion->fecha            = $row['fecha']->format('d/m/y');
				$miCalificacion->usuario          = utf8_encode($row['usuario']);
				$miCalificacion->pregunta         = utf8_encode($row['pregunta']);
				$miCalificacion->respuesta        = utf8_encode($row['respuesta']);

				$response[$i] = $miCalificacion;
				$i++;
			}

			Conection::CerrarConexion( $conn );
			return $response;
		}
	}

	function getEmprendedoresAptosFase2($post)
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_emprendedores_para_fase_2 @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion( $conn );
			echo 'Error 2020. ';
			Errores::insErrorDeQuery('Error 2020. ', $sql, $params);
		}else{
			$i = 0;
			$response = array();

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miUsuario = new Sesiones();
				
				$miUsuario->id_usuario  = $row['id_usuario'];
				$miUsuario->dni        = $row['dni'];
				$miUsuario->email       = $row['email'];
				$miUsuario->nombre       = utf8_encode($row['nombre']);

				$response[$i] = $miUsuario;
				$i++;
			}

			Conection::CerrarConexion( $conn );
			return $response;
		}
	}

	function getSesionesFase2()
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_sesiones_fase_2 @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2021';
			Errores::insErrorDeQuery('Error 221', $sql, $params);
		}else{
			$sesiones = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miSesion = new Sesiones();
				$miSesion->mensaje = utf8_encode($row['mensaje']);
				$miSesion->id_sesion = $row['id_sesion'];
				$miSesion->id_grupo = $row['id_grupo'];
				$miSesion->desc = utf8_encode($row['desc']);
				$miSesion->emprendedores = utf8_encode($row['emprendedores']);
				$miSesion->sn_califico = $row['sn_califico'];

				$sesiones[$i] = $miSesion;
				$i++;
			}			

			Conection::CerrarConexion($conn);
			echo json_encode($sesiones);
		}
	}

	function getEmprendedoresFase2($post)
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_emprendedores_fase_2 @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2022';
			Errores::insErrorDeQuery('Error 2022', $sql, $params);
		}else{
			$sesiones = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miSesion = new Sesiones();
				$miSesion->id_usuario = $row['id_usuario'];
				$miSesion->nombre = utf8_encode($row['nombre']);
				$miSesion->documento = $row['documento'];
				$miSesion->sn_abandono = $row['sn_abandono'];

				$sesiones[$i] = $miSesion;
				$i++;
			}			

			Conection::CerrarConexion($conn);
			return $sesiones;
		}
	}

	function getFormCalificacionMentorFase2($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_sesion = $post['id_sesion'];
		$id_grupo = $post['id_grupo'];
		$id_emprendedor = $post['id_emprendedor'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_form_fase_2 @id_usuario = ?, @id_sesion = ?, @id_grupo = ?, @id_emprendedor = ?";
		$params = array($id_usuario, $id_sesion, $id_grupo, $id_emprendedor);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2023';
			Errores::insErrorDeQuery('Error 2023', $sql, $params);
		}else{			
			$miSesion = new Sesiones();
			
			$miSesion->emprendedores = Self::getEmprendedoresDeGrupo($id_grupo);

			$preguntas = array();
			$i = 0;
			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC )){
				$miPregunta = new Sesiones();

				$miPregunta->cod_mensaje = $row['cod_mensaje'];
				if($miPregunta->cod_mensaje == 0){
					$miPregunta->mensaje = utf8_encode($row['mensaje']);
					$preguntas[$i] = $miPregunta;
					break;
				}
				$miPregunta->id_pregunta = $row['id_pregunta'];
				$miPregunta->num_pregunta = $row['num_pregunta'];
				$miPregunta->pregunta = utf8_encode($row['pregunta']);
				$miPregunta->respuestas = Self::getRespuestas($miPregunta->id_pregunta);
				$preguntas[$i] = $miPregunta;
				$i++;
			}

			$miSesion->preguntas = $preguntas;

			Conection::CerrarConexion($conn);
			echo json_encode($miSesion);
		}
	}

	function getCalificacionesEmprendedorFase2($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_emprendedor = $post['id_emprendedor'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_calificaciones_emprendedor_fase_2 @id_usuario = ?, @id_emprendedor = ?";
		$params = array($id_usuario, $id_emprendedor);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2024. ';
			Errores::insErrorDeQuery('Error 2024. ', $sql, $params);
		}else{
			$sesiones = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miSesion = new Sesiones();
				$miSesion->mensaje = utf8_encode($row['mensaje']);
				$miSesion->cod_mensaje = $row['cod_mensaje'];
				
				if($miSesion->cod_mensaje == 0){
					$sesiones[$i] = $miSesion;
					break;
				}
				
				$miSesion->id_sesion = $row['id_sesion'];
				$miSesion->id_grupo = $row['id_grupo'];
				$miSesion->comentario = utf8_encode($row['comentario']);
				$miSesion->puntuacion = $row['puntuacion'];
				$miSesion->grupo = (int) filter_var($row['grupo'], FILTER_SANITIZE_NUMBER_INT);
				$miSesion->id_usuario = $row['id_usuario'];
				$miSesion->id_emprendedor = $row['id_emprendedor'];
				$miSesion->propio = $id_usuario == $miSesion->id_usuario ? -1 : 0;

				$sesiones[$i] = $miSesion;
				$i++;
			}

			Conection::CerrarConexion($conn);
			echo json_encode( $sesiones );
		}
	}

	function comprobarAceptadoFase2()
	{
		$id_usuario = Cookies::getIdUsuario();
		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_comprobar_aceptado_fase_2 @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2025. ';
			Errores::insErrorDeQuery('Error 2025. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$response = new Sesiones();
			$response->mensaje = utf8_encode($row['mensaje']);
			$response->cod_mensaje = $row['cod_mensaje'];
			$response->estado = $row['estado'];

			Conection::CerrarConexion($conn);
			return $response ;
		}
	}

	function getSesionesFase2Emprendedor()
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_sesiones_fase_2_emprendedores @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2001.3 ';
			Errores::insErrorDeQuery('Error 2001.3 ', $sql, $params);
		}else{
			$sesiones = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miSesion = new Sesiones();
				$miSesion->mensaje = utf8_encode($row['mensaje']);
				$miSesion->id_sesion = $row['id_sesion'];
				$miSesion->id_grupo = $row['id_grupo'];
				$miSesion->desc = utf8_encode($row['desc']);
				$miSesion->comentario = utf8_encode($row['comentario']);
				$miSesion->mentor = utf8_encode($row['mentor']);
				$miSesion->sn_califico = $row['sn_califico'];

				$sesiones[$i] = $miSesion;
				$i++;
			}			

			Conection::CerrarConexion($conn);
			echo json_encode($sesiones);
		}
	}

	function getFormCalificacionEmprendedorFase2($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_sesion = $post['id_sesion'];
		$id_grupo = $post['id_grupo'];
		$id_emprendedor = $id_usuario;

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_form_fase_2 @id_usuario = ?, @id_sesion = ?, @id_grupo = ?, @id_emprendedor = ?";
		$params = array($id_usuario, $id_sesion, $id_grupo, $id_emprendedor);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2002.3 ';
			Errores::insErrorDeQuery('Error 2002.3 ', $sql, $params);
		}else{			
			$miSesion = new Sesiones();
			$preguntas = array();
			$i = 0;
			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miPregunta = new Sesiones();

				$miPregunta->cod_mensaje = $row['cod_mensaje'];
				if($miPregunta->cod_mensaje == 0){
					$miPregunta->mensaje = utf8_encode($row['mensaje']);
					$preguntas[$i] = $miPregunta;
					break;
				}
				$miPregunta->id_pregunta = $row['id_pregunta'];
				$miPregunta->num_pregunta = $row['num_pregunta'];
				$miPregunta->pregunta = utf8_encode($row['pregunta']);
				$miPregunta->respuestas = Self::getRespuestas($miPregunta->id_pregunta);
				$preguntas[$i] = $miPregunta;
				$i++;
			}

			$miSesion->preguntas = $preguntas;

			Conection::CerrarConexion($conn);
			echo json_encode($miSesion);
		}
	}
	

/****************************************************************************************************************************************************/

	function calificar($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_sesion = $post['id_sesion'];
		$id_grupo = $post['id_grupo'];
		$id_emprendedor = isset($post['id_emprendedor']) ? $post['id_emprendedor'] : $id_usuario;
		$asistencias = isset($post['asistencias']) ? $post['asistencias'] : '';
		$resp1 = isset($post['resp1']) ? $post['resp1'] : '';
		$resp2 = isset($post['resp2']) ? $post['resp2'] : '';
		$stars = isset($post['stars']) ? $post['stars'] : '';
		$comentario = isset($post['comentario']) ? utf8_decode($post['comentario']) : '';

		$conn = Conection::Conexion();
		$sql = "EXEC sp_ins_calificacion @id_usuario = ?, @id_sesion = ?, @id_grupo = ?, @stars = ?, @comentario = ?, @id_emprendedor = ?";
		$params = array($id_usuario, $id_sesion, $id_grupo, $stars, $comentario, $id_emprendedor);

		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2101. ';
			Errores::insErrorDeQuery('Error 2101. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$miSesion = new Sesiones();
			$miSesion->mensaje = utf8_encode($row['mensaje']);
			$miSesion->cod_mensaje = $row['cod_mensaje'];

			if($miSesion->cod_mensaje == 0){
				Conection::CerrarConexion($conn);
				echo json_encode($miSesion);
				return;
			}

			if($asistencias != ''){
				if( !is_array( $asistencias) )
					$asistencias = explode( ',', $asistencias);

				$i = 0;
				$misAsistencias = array();
				foreach ($asistencias as $key => $value) {
					$misAsistencias[$i] = Self::insAsistencia($id_sesion, $id_grupo, $value);
					$i++;
				}
				$miSesion->asistencias = $misAsistencias;
			}

			$resp = explode( ',', $resp1);
			$i = 0;
			$misrespuestas = array();
			foreach ($resp as $key => $value) {
				$misrespuestas[$i] = Self::insRespuesta($id_sesion, $id_grupo, $id_usuario, $value);
				$i++;
			}
			$miSesion->respuestas = $misrespuestas;

			$resp2 = explode( ',', $resp2);
			$i = 0;
			$misrespuestas2 = array();
			foreach ($resp2 as $key => $value) {
				$misrespuestas2[$i] = Self::insRespuesta($id_sesion, $id_grupo, $id_usuario, $value);
				$i++;
			}
			$miSesion->respuestas2 = $misrespuestas2;

			Conection::CerrarConexion($conn);
			return json_encode($miSesion);
		}
	}

	function calificarMentorDT($post)
	{
		$emprendedores = Self::getEmprendedoresDeGrupo($post['id_grupo']);

		$responses = array();
		$i = 0;

		foreach ($emprendedores as $key => $value)
		{
			$post['id_emprendedor'] = $value->id_usuario;
			$responses[$i] = json_decode(Self::calificar($post));
			$i++;
		}
		return json_encode($responses);
	}

	function insAsistencia($id_sesion, $id_grupo, $id_usuario)
	{
		$conn = Conection::Conexion();
		$sql = "EXEC sp_ins_asistencia @id_sesion = ?, @id_grupo = ?, @id_usuario = ?";
		$params = array($id_sesion, $id_grupo, $id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2102. ';
			Errores::insErrorDeQuery('Error 2102. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$miSesion = new Sesiones();
			$miSesion->mensaje = utf8_encode($row['mensaje']);
			$miSesion->cod_mensaje = $row['cod_mensaje'];

			Conection::CerrarConexion($conn);
			return $miSesion;
		}
	}

	function insRespuesta($id_sesion, $id_grupo, $id_usuario, $id_respuesta	)
	{
		$conn = Conection::Conexion();
		$sql = "EXEC sp_ins_repuesta @id_sesion = ?, @id_grupo = ?, @id_usuario = ?, @id_respuesta = ?";
		$params = array($id_sesion, $id_grupo, $id_usuario, $id_respuesta);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2103. ';
			Errores::insErrorDeQuery('Error 2103. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$miSesion = new Sesiones();
			$miSesion->mensaje = utf8_encode($row['mensaje']);
			$miSesion->cod_mensaje = $row['cod_mensaje'];

			Conection::CerrarConexion($conn);
			return $miSesion;
		}
	}

	function calificarSatisfaccion($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$estructura    = $post['sstarEstr'];
		$utilidad      = $post['sstarUtil'];
		$cumplimiento  = $post['sstarCumplimiento'];
		$valor         = $post['sstarValor'];
		$desemp        = $post['sstarDesemp'];
		$material      = $post['sstarMaterial'];
		$valoracion    = $post['puntajeValoracion'];
		$detalle       = utf8_decode($post['detalleSatisfaccion'] );
		$comentario    = utf8_decode($post['comentarioSatisfaccion'] );

		$conn = Conection::Conexion();
		$sql = "EXEC sp_ins_encuesta_satisfaccion @id_usuario = ?, @estructura = ?, @utilidad = ?, @cumplimiento = ?, @valor = ?, @desemp = ?, @material = ?, @valoracion = ?, @detalle = ?, @comentario = ?"; 
		$params = array($id_usuario, $estructura, $utilidad, $cumplimiento, $valor, $desemp, $material, $valoracion, $detalle, $comentario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2104. ';
			Errores::insErrorDeQuery('Error 2104. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$miSesion = new Sesiones();
			$miSesion->mensaje = utf8_encode($row['mensaje']);
			$miSesion->cod_mensaje = $row['cod_mensaje'];

			Conection::CerrarConexion($conn);
			echo json_encode( $miSesion );
		}
	}

	function calificarSatisfaccionMentor($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$relevancia = $post['sstarRelevancia'];
		$profundidad = $post['sstarProfundidad'];
		$metodologia = $post['sstarMetodologia'];
		$facDominio = $post['sstarFacDominio'];
		$facClaridad = $post['sstarFacClaridad'];
		$duracion = $post['sstarDuracion'];
		$ambiente = $post['sstarAmbiente'];
		$comentario = utf8_decode($post['comentarioSatisfaccion']);

		$conn = Conection::Conexion();
		$sql = "EXEC sp_ins_encuesta_satisfaccion_mentor @id_usuario = ?, @relevancia = ?, @profundidad = ?, @metodologia = ?, @facDominio = ?, @facClaridad = ?, @duracion = ?, @ambiente = ?, @comentario = ?"; 
		$params = array($id_usuario, $relevancia, $profundidad, $metodologia, $facDominio, $facClaridad, $duracion, $ambiente, $comentario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2104.1 ';
			Errores::insErrorDeQuery('Error 2104.1 ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$miSesion = new Sesiones();
			$miSesion->mensaje = utf8_encode($row['mensaje']);
			$miSesion->cod_mensaje = $row['cod_mensaje'];

			Conection::CerrarConexion($conn);
			echo json_encode( $miSesion );
		}
	}


	function comprobarEncuestaSatisfaccion()
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_sn_encuesta_satisfaccion @id_usuario = ?"; 
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2105. ';
			Errores::insErrorDeQuery('Error 2105. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$miSesion = new Sesiones();
			$miSesion->mensaje = utf8_encode($row['mensaje']);
			$miSesion->cod_mensaje = $row['cod_mensaje'];

			Conection::CerrarConexion($conn);
			echo json_encode( $miSesion );
		}
	}

	function comprobarCertificacion()
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_certificacion @id_usuario = ?"; 
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2105. ';
			Errores::insErrorDeQuery('Error 2105. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$miSesion = new Sesiones();
			$miSesion->mensaje = utf8_encode($row['mensaje']);
			$miSesion->cod_mensaje = $row['cod_mensaje'];
			if($miSesion->cod_mensaje == -1){
				$miSesion->mentor = utf8_encode($row['mentor']);
				$miSesion->usuario = utf8_encode($row['usuario']);

			}

			Conection::CerrarConexion($conn);
			echo json_encode( $miSesion );
		}
	}

	function insEncuestaEmprendedor($post)
	{
		$id_usuario               = Cookies::getIdUsuario();
		$id_rubro                 = $post['id_rubro'];
		$propuesta_valor          = utf8_decode($post['propuesta_valor']);
		$plan_negocio             = utf8_decode($post['plan_negocio']);
		$calendario               = utf8_decode($post['calendario']);
		$estructura_legal         = utf8_decode($post['estructura_legal']);
		$publico_objetivo         = utf8_decode($post['publico_objetivo']);
		$canales_venta            = utf8_decode($post['canales_venta']);
		$estrategia_marketing     = utf8_decode($post['estrategia_marketing']);
		$administracion           = utf8_decode($post['administracion']);
		$situacion_financiamiento = utf8_decode($post['situacion_financiamiento']);
		$plan_operaciones         = utf8_decode($post['plan_operaciones']);

		$conn = Conection::Conexion();
		$sql = "EXEC sp_ins_encuesta_emprendedor_2 @id_usuario = ? , @id_rubro = ? , @propuesta_valor = ?, @plan_negocio = ?, @calendario = ?, @estructura_legal = ?, @publico_objetivo = ?, @canales_venta = ?, @estrategia_marketing = ?, @administracion = ?, @situacion_financiamiento = ?, @plan_operaciones = ?";
		$params = array($id_usuario, $id_rubro, $propuesta_valor, $plan_negocio, $calendario, $estructura_legal, $publico_objetivo, $canales_venta, $estrategia_marketing, $administracion, $situacion_financiamiento, $plan_operaciones );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2106. ';
			Errores::insErrorDeQuery('Error 2106. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$miSesion = new Sesiones();
			$miSesion->mensaje = utf8_encode($row['mensaje']);
			$miSesion->cod_mensaje = $row['cod_mensaje'];

			Conection::CerrarConexion($conn);
			echo json_encode( $miSesion );
		}
	}

	function enviarEncuestaMentor($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$nombre = utf8_decode($post['nombre']);
		$id_provincia = $post['id_provincia'];
		$id_municipio = $post['id_municipio'];
		$email = $post['email'];
		$linkedin = utf8_decode($post['linkedin']);
		$id_rubro = $post['id_rubro'];
		$expertises = $post['expertises'];
		$id_expertise = $expertises[0];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_ins_validacion_datos_mentor @id_usuario = ? , @nombre = ? , @id_provincia = ?, @id_ciudad = ?, @email = ?, @linkedin = ?, @id_rubro = ?, @id_expertise = ?";
		$params = array($id_usuario, $nombre, $id_provincia, $id_municipio, $email, $linkedin, $id_rubro, $id_expertise );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2107. ';
			Errores::insErrorDeQuery('Error 2107. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$response = new Sesiones();
			$response->mensaje = utf8_encode($row['mensaje']);
			$response->cod_mensaje = $row['cod_mensaje'];

			if($response->cod_mensaje == -1){
				foreach ($expertises as $key => $value) {
					$response->expertise = Self::insExpertise($id_usuario, $value);
				}
			}

			Conection::CerrarConexion($conn);
			echo json_encode( $response );
		}
	}

	function insExpertise($id_usuario, $id_expertise)
	{
		$conn = Conection::Conexion();
		$sql = "EXEC sp_ins_area_expertise @id_usuario = ? , @id_expertise = ?";
		$params = array($id_usuario, $id_expertise );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2108. ';
			Errores::insErrorDeQuery('Error 2108. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$response = new Sesiones();
			$response->mensaje = utf8_encode($row['mensaje']);
			$response->cod_mensaje = $row['cod_mensaje'];

			Conection::CerrarConexion($conn);
			return $response;
		}
	}

	function insEmprendedoresFase2($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$emprendedores = $post['emprendedores'];

		$response = array();

		foreach ($emprendedores as $key => $value) {
			$r = Self::insEmprendedorFase2($id_usuario, $value);
			array_push($response, $r);
		}
		
		return $response;
	}

	function insEmprendedorFase2($id_mentor, $id_emprendedor)
	{
		$conn = Conection::Conexion();
		$sql = "EXEC sp_ins_emprendedor_fase_2 @id_mentor = ? , @id_emprendedor = ?";
		$params = array($id_mentor, $id_emprendedor );
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2109. ';
			Errores::insErrorDeQuery('Error 2109. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$response = new Sesiones();
			$response->mensaje = utf8_encode($row['mensaje']);
			$response->cod_mensaje = $row['cod_mensaje'];

			Conection::CerrarConexion($conn);
			return $response;
		}
	}

	function insSesionesFase2($id_mentor = null)
	{
		if ($id_mentor == null)
			$id_mentor = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_ins_sesiones_fase_2 @id_mentor = ?";
		$params = array($id_mentor);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 2110. ';
			Errores::insErrorDeQuery('Error 2110. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$response = new Sesiones();
			$response->mensaje = utf8_encode($row['mensaje']);
			$response->cod_mensaje = $row['cod_mensaje'];

			Conection::CerrarConexion($conn);
			return $response;
		}
	}
} 
?>