<?php 
/**
* 
*/
require_once 'class.helpers.php';

class Usuarios
{
	
	function login($post)
	{
		$usuario = $post['usuario'];
		$password = $post['password'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_login @usuario = ?, @password = ?";
		$params = array($usuario, $password);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1001. ';
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			
			$response = new Usuarios();
			$response->mensaje = utf8_encode($row['mensaje']);
			$response->cod_mensaje = $row['cod_mensaje'];

			if($response->cod_mensaje != -1){
				echo json_encode($response);
				return;
			}

			$id_usuario   = $row['id_usuario'];
			$tipo_usuario = $row['id_tipo_usuario'];
			$nombre = utf8_encode($row['nombre']);
			$response->tipo_usuario = $row['id_tipo_usuario'];

			setcookie('id_usuario', Encriptacion::encriptar($id_usuario), time()+315569260 ,'/' );
			setcookie('tipo_usuario', Encriptacion::encriptar($tipo_usuario), time()+315569260 ,'/' );
			setcookie('nombre', $nombre, time()+315569260 ,'/' );

			switch ($tipo_usuario) 
			{
				case 1:
				case 7:
					$response->redirect = 'gestion-usuarios.php';
					break;
				
				case 2:
					$response->redirect = 'home-mentorDT.php';
					break;

				case 3:
					$response->redirect = 'home-mentorConsolidacion.php';
					break;

				case 4:
					$response->redirect = 'home-desarrolloTemp.php';
					break;

				case 5:
					$response->redirect = 'home-consolidacion.php';
					break;

				case 8:
					$response->redirect = 'red-de-mentores.php';
					break;
			}

			Conection::CerrarConexion($conn);
			echo json_encode($response);
			return;
		}
	}

	function getUsuarios()
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_usuarios_lista @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1002. ';
			Errores::insErrorDeQuery('Error 1002. ', $sql, $params);
		}else{
			$usuarios = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miUsuario = new Usuarios();
				$miUsuario->id_usuario = $row['id_usuario'];
				$miUsuario->nombre = utf8_encode($row['nombre']);
				$miUsuario->cargo = utf8_encode($row['cargo']);
				$miUsuario->ult_login = $row['ult_login'] == null ? 'Sin actividad' : $row['ult_login']->format('d/m/y');
				$miUsuario->sesiones = $row['sesiones'];
				$miUsuario->email = utf8_encode($row['email']);
				$miUsuario->password = utf8_encode($row['password']);
				$miUsuario->dni = utf8_encode($row['dni']);
				$miUsuario->sede = utf8_encode($row['sede']);
				$miUsuario->estado = $row['estado'];

				$usuarios[$i] = $miUsuario;
				$i++;
			}			

			$excel = Excels::createExcelListaUsuarios($usuarios);
			$usuarios[count($usuarios)] = $excel;

			Conection::CerrarConexion($conn);
			echo json_encode($usuarios);
		}
	}

	function getUsuariosPorSede($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_sede = $post['id_sede'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_usuarios_por_sede @id_usuario = ?, @id_sede = ?";
		$params = array($id_usuario, $id_sede);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1002.2 ';
			Errores::insErrorDeQuery('Error 1002.2 ', $sql, $params);
		}else{
			$usuarios = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miUsuario = new Usuarios();
				$miUsuario->id_usuario = $row['id_usuario'];
				$miUsuario->nombre = utf8_encode($row['nombre']);
				$miUsuario->cargo = utf8_encode($row['cargo']);
				$miUsuario->ult_login = $row['ult_login'] == null ? 'Sin actividad' : $row['ult_login']->format('d/m/y');
				$miUsuario->sesiones = $row['sesiones'];
				$miUsuario->email = utf8_encode($row['email']);
				$miUsuario->password = utf8_encode($row['password']);
				$miUsuario->dni = utf8_encode($row['dni']);
				$miUsuario->sede = utf8_encode($row['sede']);
				$miUsuario->estado = $row['estado'];

				$usuarios[$i] = $miUsuario;
				$i++;
			}

			$excel = Excels::createExcelListaUsuarios($usuarios);
			$usuarios[count($usuarios)] = $excel;

			Conection::CerrarConexion($conn);
			echo json_encode($usuarios);
		}
	}

	function getMentoresEnFormacion($post)
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_mentores_en_formacion @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1003. ';
			Errores::insErrorDeQuery('Error 1003. ', $sql, $params);
		}else{
			$usuarios = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miUsuario = new Usuarios();
				$miUsuario->id_usuario = $row['id_usuario'];
				$miUsuario->nombre = utf8_encode($row['nombre']);
				$miUsuario->id_tipo_usuario = $row['id_tipo_usuario'];
				$miUsuario->email = utf8_encode($row['email']);
				$miUsuario->provincia = utf8_encode($row['provincia']);
				$miUsuario->ciudad = utf8_encode($row['ciudad']);

				$usuarios[$i] = $miUsuario;
				$i++;
			}			

			Conection::CerrarConexion($conn);
			echo json_encode($usuarios);
		}
	}

	function getMisDatos()
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_datos_usuario @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1004. ';
			Errores::insErrorDeQuery('Error 1004. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );

			$miUsuario = new Usuarios();
			$miUsuario->id_usuario = $row['id_usuario'];
			$miUsuario->nombre = utf8_encode($row['nombre']);
			$miUsuario->id_tipo_usuario = $row['id_tipo_usuario'];
			$miUsuario->tipo_usuario = utf8_encode($row['tipo_usuario']);
			$miUsuario->email = utf8_encode($row['email']);
			$miUsuario->password = utf8_encode($row['password']);
			$miUsuario->ult_actividad = date('m/d/Y');
			$miUsuario->linkedin = utf8_encode($row['linkedin']);
		    $miUsuario->id_pais = $row['id_pais'];
		    $miUsuario->id_provincia = $row['id_provincia'];
		    $miUsuario->id_municipio = $row['id_ciudad'];
		    $miUsuario->id_rubro = $row['id_rubro'];
		    $miUsuario->id_expertise = $row['id_expertise'];
		    $miUsuario->imagen = $row['imagen'];

			Conection::CerrarConexion($conn);
			return $miUsuario;
		}
	}

	function getSede($post)
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_sede_usuario @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1005. ';
			Errores::insErrorDeQuery('Error 1005. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );

			$miUsuario = new Usuarios();
			$miUsuario->cod_mensaje = $row['cod_mensaje'];
			$miUsuario->mensaje = utf8_encode($row['mensaje']);
			if($miUsuario->cod_mensaje == -1){
				$miUsuario->pais = utf8_encode($row['pais']);
				$miUsuario->provincia = utf8_encode($row['provincia']);
				$miUsuario->ciudad = utf8_encode($row['ciudad']);
			}

			Conection::CerrarConexion($conn);
			echo json_encode($miUsuario);
		}
	}

	function getIdUsuarioByRandomString($randomString)
	{
		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_usuario_by_randomstring @randomString = ?";
		$params = array($randomString);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1005. ';
			Errores::insErrorDeQuery('Error 1005. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );

			$response = new Usuarios();
			$response->cod_mensaje = $row['cod_mensaje'];
			$response->mensaje = utf8_encode($row['mensaje']);
			$response->id_usuario = $row['id_usuario'];
			setcookie('id_usuario', Encriptacion::encriptar($response->id_usuario), time()+31556926 ,'/' );

			Conection::CerrarConexion($conn);
			return $response ;
		}
	}

	function getUsuarioEspecifico($post)
	{
		$id_usuario = $post['id_usuario'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_usuario_by_id @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1006. ';
			Errores::insErrorDeQuery('Error 1006. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );

			$response = new Usuarios();
			$response->nombre = utf8_encode($row['nombre']);
			$response->email = utf8_encode($row['email']);
			$response->dni = utf8_encode($row['dni']);
			$response->password = utf8_encode($row['password']);
			$response->id_rol = $row['id_rol'];
			$response->id_pais = $row['id_pais'];
			$response->id_provincia = $row['id_provincia'];
			$response->id_ciudad = $row['id_ciudad'];
			$response->id_sede = $row['id_sede'];

			Conection::CerrarConexion($conn);
			echo json_encode( $response );
		}
	}

	function getRoles($post)
	{
		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_roles";
		$params = array();
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1007. ';
			Errores::insErrorDeQuery('Error 1007. ', $sql, $params);
		}else{
			$i = 0;
			$roles = array();
			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miRol = new Usuarios();
			
				$miRol->id_rol = $row['id_rol'];
				$miRol->rol = utf8_encode($row['rol']);

				$roles[$i] = $miRol;
				$i++;
			}

			Conection::CerrarConexion($conn);
			echo json_encode( $roles );
		}
	}

	function getSedes($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		
		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_sedes @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1008. ';
			Errores::insErrorDeQuery('Error 1008. ', $sql, $params);
		}else{
			$i = 0;
			$sedes = array();
			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miSede = new Usuarios();
			
				$miSede->id_sede = $row['id_sede'];
				$miSede->sede = utf8_encode($row['sede']);

				$sedes[$i] = $miRol;
				$i++;
			}

			Conection::CerrarConexion($conn);
			echo json_encode( $sedes );
		}
	}

	function getUsuariosCoordinador()
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_usuarios_coordinador @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1009. ';
			Errores::insErrorDeQuery('Error 1009. ', $sql, $params);
		}else{
			$usuarios = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miUsuario = new Usuarios();
				$miUsuario->id_usuario = $row['id_usuario'];
				$miUsuario->nombre = utf8_encode($row['nombre']);
				$miUsuario->sedes = utf8_encode($row['sedes']);
				$miUsuario->ult_login = $row['ult_login'] == null ? 'Sin actividad' : $row['ult_login']->format('d/m/y');
				$miUsuario->sesiones = $row['sesiones'];
				$miUsuario->email = utf8_encode($row['email']);
				$miUsuario->password = utf8_encode($row['password']);
				$miUsuario->dni = utf8_encode($row['dni']);
				$miUsuario->estado = $row['estado'];

				$usuarios[$i] = $miUsuario;
				$i++;
			}			

			Conection::CerrarConexion($conn);
			echo json_encode($usuarios);
		}
	}

	function getDetallesUsuario($post)
	{
		$id_usuario = $post['id_usuario'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_detalles_usuario @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1010. ';
			Errores::insErrorDeQuery('Error 1010. ', $sql, $params);
		}else{
			$usuarios = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miUsuario = new Usuarios();

				$miUsuario->cod_mensaje = $row['cod_mensaje'];
				$miUsuario->mensaje = utf8_encode($row['mensaje']);

				if($miUsuario->cod_mensaje == -1)
				{
					$miUsuario->nombreComercial = utf8_encode($row['nombreComercial']);
					$miUsuario->descripcionActividadPrincipal = utf8_encode($row['descripcionActividadPrincipal']);
					$miUsuario->tiempoComenzaste = utf8_encode($row['tiempoComenzaste']);
					$miUsuario->rubro = utf8_encode($row['rubro']);
					$miUsuario->etapa = utf8_encode($row['etapa']);
					$miUsuario->propuestaValor = $row['propuestaValor'];
					$miUsuario->conocimientoMercado = $row['conocimientoMercado'];
					$miUsuario->estrategiaSalida = $row['estrategiaSalida'];
					$miUsuario->analisisCompetencia = $row['analisisCompetencia'];
					$miUsuario->fidelizacionClientes = $row['fidelizacionClientes'];
					$miUsuario->expansionNuevosMercados = $row['expansionNuevosMercados'];
					$miUsuario->analisisActoresClave = $row['analisisActoresClave'];
					$miUsuario->consolidacionPuntoEquilibrio = $row['consolidacionPuntoEquilibrio'];
					$miUsuario->estabilidadFinanciera = $row['estabilidadFinanciera'];
					$miUsuario->accesoFinanciamiento = $row['accesoFinanciamiento'];
					$miUsuario->administracionInsumos = $row['administracionInsumos'];
					$miUsuario->gestionProveedores = $row['gestionProveedores'];
					$miUsuario->disenoSistemaProduccion = $row['disenoSistemaProduccion'];
					$miUsuario->escalabilidadProduccion = $row['escalabilidadProduccion'];
					$miUsuario->cumplimientoRequisitosLegales = $row['cumplimientoRequisitosLegales'];
					$miUsuario->disenoSistemasAdministrativos = $row['disenoSistemasAdministrativos'];
					$miUsuario->disenoSistemasPlaneamiento = $row['disenoSistemasPlaneamiento'];
					$miUsuario->estructuraPersonal = $row['estructuraPersonal'];
					$miUsuario->sistemaGestion = $row['sistemaGestion'];

					$miUsuario->dni = $row['dni'];
					$miUsuario->nombre = utf8_encode($row['nombre']);
					$miUsuario->rol = utf8_encode($row['rol']);
					$miUsuario->sede = utf8_encode($row['sede']);
					$miUsuario->actividad = utf8_encode($row['actividad']);
					$miUsuario->pass = utf8_encode($row['pass']);
				}

				$usuarios[$i] = $miUsuario;
				$i++;
			}

			Conection::CerrarConexion($conn);

			$excel = Excels::createExcelDetallesUsuario($usuarios);
			$usuarios[count($usuarios)] = $excel;

			echo json_encode($usuarios);
		}
	}

	function getExpertise($post)
	{
		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_expertise";
		$stmt = sqlsrv_query($conn, $sql);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1011. ';
			Errores::insErrorDeQuery('Error 1011. ', $sql, $params);
		}else{
			$expertises = array();
			$i = 0;

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miExpertise = new Usuarios();

				$miExpertise->id_expertise = $row['id_expertise'];
				$miExpertise->expertise    = utf8_encode($row['expertise']);

				$expertises[$i] = $miExpertise;
				$i++;
			}

			Conection::CerrarConexion($conn);
			echo json_encode($expertises);
		}
	}

	function comprobarEncuestaMentorValidacion($post)
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_comprobar_encuesta_mentor_validacion @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1012. ';
			Errores::insErrorDeQuery('Error 1012. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$response = new Usuarios();

			$response->cod_mensaje = $row['cod_mensaje'];
			$response->mensaje     = utf8_encode($row['mensaje']);

			Conection::CerrarConexion($conn);
			echo json_encode( $response ); 
		}
	}

	function getMentorRelacionamiento($post)
	{
		$dni_mentor = $post['dni_mentor'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_mentor_relacionamiento @dni_mentor = ?";
		$params = array($dni_mentor);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1012. ';
			Errores::insErrorDeQuery('Error 1012. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$usuario = new Usuarios();

			$usuario->cod_mensaje = $row['cod_mensaje'];
			$usuario->mensaje = utf8_encode($row['mensaje']);

			if($usuario->cod_mensaje == -1)
			{
				$usuario->id_usuario = $row['id_usuario'];
				$usuario->nombre     = utf8_encode($row['txt_nombre']);
				$usuario->rol        = utf8_encode($row['rol']);
				$usuario->emprendedores = Self::getEmprendedoresRelacionamiento($post);
			}

			Conection::CerrarConexion($conn);
			echo json_encode($usuario);
		}
	}

	function getEmprendedoresRelacionamiento($post)
	{
		$dni_mentor = $post['dni_mentor'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_emprendedores_relacionados @dni_mentor = ?";
		$params = array($dni_mentor);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1013. ';
			Errores::insErrorDeQuery('Error 1013. ', $sql, $params);
		}else{
			$usuarios = array();
			$i = 0;
				
			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$usuario = new Usuarios();

				if($row['cod_mensaje'] != -1){
					$usuario->cod_mensaje = $row['cod_mensaje'];
					$usuario->mensaje = utf8_encode($row['mensaje']);
					break;
				}

				$usuario->id_usuario = $row['id_usuario'];
				$usuario->nombre     = utf8_encode($row['txt_nombre']);
				$usuario->dni        = utf8_encode($row['dni']);
				$usuario->rol        = utf8_encode($row['rol']);
				$usuario->id_grupo   = $row['id_grupo'];
				$usuario->grupo      = utf8_encode($row['grupo']);

				$usuarios[$i] = $usuario;
				$i++;
			}
			
			Conection::CerrarConexion($conn);
			return $usuarios;
		}
	}

	function getEmprendedorRelacionamiento($post)
	{
		$dni_emprendedor = $post['dni_emprendedor'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_emprendedor_relacionamiento @dni_emprendedor = ?";
		$params = array($dni_emprendedor);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1012. ';
			Errores::insErrorDeQuery('Error 1012. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$usuario = new Usuarios();

			$usuario->cod_mensaje = $row['cod_mensaje'];
			$usuario->mensaje = utf8_encode($row['mensaje']);

			if($usuario->cod_mensaje == -1)
			{
				$usuario->id_usuario = $row['id_usuario'];
				$usuario->nombre     = utf8_encode($row['txt_nombre']);
				$usuario->rol        = utf8_encode($row['rol']);
				$usuario->mentor     = utf8_encode($row['mentor']);
				$usuario->dni_mentor = $row['dni_mentor'];
				$usuario->cant_calificaciones_propias = $row['cant_calificaciones_propias'];
				$usuario->cant_calificaciones_mentor = $row['cant_calificaciones_mentor'];
			}

			Conection::CerrarConexion($conn);
			echo json_encode($usuario);
		}
	}

	function getMentores($post)
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_mentores @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1013. ';
			Errores::insErrorDeQuery('Error 1013. ', $sql, $params);
		}else{
			$usuarios = array();
			$i = 0;
			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miUsuario = new Usuarios();

				$miUsuario->nombre     = utf8_encode($row['nombre']);
				$miUsuario->sede     = utf8_encode($row['sede']);
				$miUsuario->expertise        = utf8_encode($row['expertise']);
				$miUsuario->rubro     = utf8_encode($row['rubro']);
				$miUsuario->linkedin     = utf8_encode($row['linkedin']);
				$miUsuario->dni = $row['dni'];
				$miUsuario->email = $row['email'];
				$miUsuario->imagen = $row['imagen'];

				$usuarios[$i] = $miUsuario;
				$i++;
			}

			require_once 'class.excels.php';
			$excel = Excels::createExcelMentores($usuarios);
			$usuarios[count($usuarios)] = $excel;

			Conection::CerrarConexion($conn);
			echo json_encode($usuarios);
		}
	}

	function getMentoresPorSede($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_sede = $post['id_sede'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_mentores_por_sede @id_usuario = ?, @id_sede = ?";
		$params = array($id_usuario, $id_sede);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1014. ';
			Errores::insErrorDeQuery('Error 1014. ', $sql, $params);
		}else{
			$usuarios = array();
			$i = 0;
			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miUsuario = new Usuarios();

				$miUsuario->nombre     = utf8_encode($row['nombre']);
				$miUsuario->sede     = utf8_encode($row['sede']);
				$miUsuario->expertise        = utf8_encode($row['expertise']);
				$miUsuario->rubro     = utf8_encode($row['rubro']);
				$miUsuario->linkedin     = utf8_encode($row['linkedin']);
				$miUsuario->dni = $row['dni'];
				$miUsuario->email = $row['email'];

				$usuarios[$i] = $miUsuario;
				$i++;
			}

			require_once 'class.excels.php';
			$excel = Excels::createExcelMentores($usuarios);
			$usuarios[count($usuarios)] = $excel;

			Conection::CerrarConexion($conn);
			echo json_encode($usuarios);
		}
	}

	function getMentoresPorRubro($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_rubro = $post['id_rubro'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_mentores_por_rubro @id_usuario = ?, @id_rubro = ?";
		$params = array($id_usuario, $id_rubro);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1015. ';
			Errores::insErrorDeQuery('Error 1015. ', $sql, $params);
		}else{
			$usuarios = array();
			$i = 0;
			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miUsuario = new Usuarios();

				$miUsuario->nombre     = utf8_encode($row['nombre']);
				$miUsuario->sede     = utf8_encode($row['sede']);
				$miUsuario->expertise        = utf8_encode($row['expertise']);
				$miUsuario->rubro     = utf8_encode($row['rubro']);
				$miUsuario->linkedin     = utf8_encode($row['linkedin']);
				$miUsuario->dni = $row['dni'];
				$miUsuario->email = $row['email'];

				$usuarios[$i] = $miUsuario;
				$i++;
			}

			require_once 'class.excels.php';
			$excel = Excels::createExcelMentores($usuarios);
			$usuarios[count($usuarios)] = $excel;

			Conection::CerrarConexion($conn);
			echo json_encode($usuarios);
		}
	}

	function getMentoresPorExpertise($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_expertise = $post['id_expertise'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_mentores_por_expertise @id_usuario = ?, @id_expertise = ?";
		$params = array($id_usuario, $id_expertise);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1016. ';
			Errores::insErrorDeQuery('Error 1016. ', $sql, $params);
		}else{
			$usuarios = array();
			$i = 0;
			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miUsuario = new Usuarios();

				$miUsuario->nombre     = utf8_encode($row['nombre']);
				$miUsuario->sede     = utf8_encode($row['sede']);
				$miUsuario->expertise        = utf8_encode($row['expertise']);
				$miUsuario->rubro     = utf8_encode($row['rubro']);
				$miUsuario->linkedin     = utf8_encode($row['linkedin']);
				$miUsuario->dni = $row['dni'];
				$miUsuario->email = $row['email'];

				$usuarios[$i] = $miUsuario;
				$i++;
			}

			require_once 'class.excels.php';
			$excel = Excels::createExcelMentores($usuarios);
			$usuarios[count($usuarios)] = $excel;

			Conection::CerrarConexion($conn);
			echo json_encode($usuarios);
		}
	}

	function getMentoresBuscar($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$text = $post['text'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_mentores_por_busqueda @id_usuario = ?, @text = ?";
		$params = array($id_usuario, $text);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1017. ';
			Errores::insErrorDeQuery('Error 1017. ', $sql, $params);
		}else{
			$usuarios = array();
			$i = 0;
			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miUsuario = new Usuarios();

				$miUsuario->nombre     = utf8_encode($row['nombre']);
				$miUsuario->sede     = utf8_encode($row['sede']);
				$miUsuario->expertise        = utf8_encode($row['expertise']);
				$miUsuario->rubro     = utf8_encode($row['rubro']);
				$miUsuario->linkedin     = utf8_encode($row['linkedin']);
				$miUsuario->dni = $row['dni'];
				$miUsuario->email = $row['email'];
				$miUsuario->imagen = $row['imagen'];

				$usuarios[$i] = $miUsuario;
				$i++;
			}

			require_once 'class.excels.php';
			$excel = Excels::createExcelMentores($usuarios);
			$usuarios[count($usuarios)] = $excel;

			Conection::CerrarConexion($conn);
			echo json_encode($usuarios);
		}
	}

	function getMentoresPorFiltros($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_expertise = $post['id_expertise'] == "" ? 0 : $post['id_expertise'];
		$id_rubro = $post['id_rubro'] == "" ? 0 : $post['id_rubro'];
		$id_sede = $post['id_sede'] == "" ? 0 : $post['id_sede'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_mentores_por_filtros @id_usuario = ?, @id_sede = ?, @id_rubro = ?, @id_expertise = ?";
		$params = array($id_usuario, $id_sede, $id_rubro, $id_expertise);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1018. ';
			Errores::insErrorDeQuery('Error 1018. ', $sql, $params);
		}else{
			$usuarios = array();
			$i = 0;
			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miUsuario = new Usuarios();

				$miUsuario->nombre     = utf8_encode($row['nombre']);
				$miUsuario->sede     = utf8_encode($row['sede']);
				$miUsuario->expertise        = utf8_encode($row['expertise']);
				$miUsuario->rubro     = utf8_encode($row['rubro']);
				$miUsuario->linkedin     = utf8_encode($row['linkedin']);
				$miUsuario->dni = $row['dni'];
				$miUsuario->email = $row['email'];
				$miUsuario->imagen = $row['imagen'];

				$usuarios[$i] = $miUsuario;
				$i++;
			}

			require_once 'class.excels.php';
			$excel = Excels::createExcelMentores($usuarios);
			$usuarios[count($usuarios)] = $excel;

			Conection::CerrarConexion($conn);
			echo json_encode($usuarios);
		}
	}

	function getEmprendedoresFase2($post)
	{
		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_emprendedores_mis_propuestos_fase_2 @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1019. ';
			Errores::insErrorDeQuery('Error 1019. ', $sql, $params);
		}else{
			$usuarios = array();
			$i = 0;
			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$miUsuario = new Usuarios();

				$miUsuario->id_usuario      = $row['id_emprendedor'];
				$miUsuario->emprendedor     = utf8_encode($row['emprendedor']);
				$miUsuario->dni = $row['dni'];
				switch ($row['estado']) {
					case -1:
						$miUsuario->sn_aceptado = '<span style="color:green;">Aceptado</span>';
						break;

					case 0:
						$miUsuario->sn_aceptado = '<span>Propuesto</span>';
						break;

					case 2:
						$miUsuario->sn_aceptado = '<span style="color:red;">Rechazado</span>';
						break;
					
					default:
						$miUsuario->sn_aceptado = '<span>Propuesto</span>';
						break;
				}
				
				$usuarios[$i] = $miUsuario;
				$i++;
			}

			Conection::CerrarConexion($conn);
			echo json_encode($usuarios);
		}
	}

	/*********************************************************************************************************************************************************/

	function updPassword($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$password = $post['password'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_upd_password @id_usuario = ?, @password = ?";
		$params = array($id_usuario, $password);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1201. ';
			Errores::insErrorDeQuery('Error 1201. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );

			$response = new Usuarios();
			$response->cod_mensaje = $row['cod_mensaje'];
			$response->mensaje = utf8_encode($row['mensaje']);

			Conection::CerrarConexion($conn);
			echo json_encode( $response );
		}
	}

	function editarUsuario($post)
	{
		$id_admin = Cookies::getIdUsuario();
		$id_usuario = $post['id_usuario'];
		$nombre = utf8_decode($post['nombre']);
		$email = $post['email'];
		$dni = $post['dni'];
		$password = $post['password'];
		$id_rol = $post['id_rol'];
		$id_provincia = $post['id_provincia'];
		$id_ciudad = $post['id_ciudad'];
		$id_sede = $post['id_sede'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_upd_usuario @id_admin = ?, @id_usuario = ?, @nombre = ?, @email = ?, @dni = ?, @password = ?, @id_rol = ?, @id_provincia = ?, @id_ciudad = ?, @id_sede = ?";
		$params = array($id_admin, $id_usuario, $nombre, $email, $dni, $password, $id_rol, $id_provincia, $id_ciudad, $id_sede);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1202. ';
			Errores::insErrorDeQuery('Error 1202. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );

			$response = new Usuarios();
			$response->cod_mensaje = $row['cod_mensaje'];
			$response->mensaje = utf8_encode($row['mensaje']);

			Conection::CerrarConexion($conn);
			echo json_encode( $response );
		}
	}

	function updAbandono($post)
	{
		$id_emprendedor = $post['id_emprendedor'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_upd_abandono @id_emprendedor = ?";
		$params = array($id_emprendedor);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1203. ';
			Errores::insErrorDeQuery('Error 1203. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );

			$response = new Usuarios();
			$response->cod_mensaje = $row['cod_mensaje'];
			$response->mensaje = utf8_encode($row['mensaje']);
			$response->sn_abandono = $row['sn_abandono'];

			Conection::CerrarConexion($conn);
			echo json_encode( $response );
		}
	}

	function updRol($post)
	{
		$id_usuario = $post['id_usuario'];
		$id_rol = $post['id_rol'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_upd_rol @id_usuario = ?, @id_rol = ?";
		$params = array($id_usuario, $id_rol);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1204. ';
			Errores::insErrorDeQuery('Error 1204. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );

			$response = new Usuarios();
			$response->cod_mensaje = $row['cod_mensaje'];
			$response->mensaje = utf8_encode($row['mensaje']);
			$response->mensaje2 = utf8_encode($row['mensaje_2']);

			Conection::CerrarConexion($conn);
			echo json_encode( $response );
		}
	}

	function updBorarRelacionamientoEmprendedor($post)
	{
		$id_usuario = $post['id_usuario'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_upd_borrar_relacionamiento_emprendedor @id_usuario = ?";
		$params = array($id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1205. ';
			Errores::insErrorDeQuery('Error 1205. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );

			$response = new Usuarios();
			$response->cod_mensaje = $row['cod_mensaje'];
			$response->mensaje = utf8_encode($row['mensaje']);

			Conection::CerrarConexion($conn);
			echo json_encode( $response );
		}
	}

	function updBorarRelacionamientoEmprendedorByDNI($linea, $dni_emprendedor)
	{
		$conn = Conection::Conexion();
		$sql = "EXEC sp_upd_borrar_relacionamiento_emprendedor_by_dni @dni_emprendedor = ?";
		$params = array($dni_emprendedor);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1206. ';
			Errores::insErrorDeQuery('Error 1206. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );

			$response = new Usuarios();
			$response->cod_mensaje = $row['cod_mensaje'];
			$response->mensaje = utf8_encode($row['mensaje']);
			$response->linea = $linea;
			$response->dni_emprendedor = $dni_emprendedor;

			Conection::CerrarConexion($conn);
			return $response;
		}
	}

	function updImagenPerfil($file)
	{
		$ruta = 'files/';
		$archivoExcel = $file['name'];
		$archivoExcel_tmp = $file['tmp_name'];
		move_uploaded_file($archivoExcel_tmp, $ruta.$archivoExcel);

		$extention = pathinfo($ruta.$archivoExcel,PATHINFO_EXTENSION);

		$rString = Helpers::generateRandomString();
		$file = $ruta.$rString.'.'.$extention;
		rename($ruta.$archivoExcel, $file);

		$id_usuario = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_upd_imagen_perfil @id_usuario = ?, @url_imagen = ?";
		$params = array($id_usuario, $file);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1207. ';
			Errores::insErrorDeQuery('Error 1207. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );

			$response = new Usuarios();
			$response->cod_mensaje = $row['cod_mensaje'];
			$response->mensaje = utf8_encode($row['mensaje']);
			$response->imagen = $file;

			Conection::CerrarConexion($conn);
			return $response;
		}
	}

	function aceptarEmprendedorFase2($post)
	{
		$id_emprendedor = $post['id_emprendedor'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_upd_aceptar_emprendedor_fase_2 @id_emprendedor = ?";
		$params = array($id_emprendedor);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1208. ';
			Errores::insErrorDeQuery('Error 1208. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );

			$response = new Usuarios();
			$response->cod_mensaje = $row['cod_mensaje'];
			$response->mensaje = utf8_encode($row['mensaje']);
			$response->id_mentor = $row['id_mentor'];

			if($response->cod_mensaje == -1)
			{
				require_once 'class.sesiones.php';
				$sesiones = new Sesiones();
				$s = $sesiones->insSesionesFase2($response->id_mentor);
				$response->sesiones = $s;
			}

			Conection::CerrarConexion($conn);
			return $response;
		}
	}

	function rechazarEmprendedorFase2($post)
	{
		$id_emprendedor = $post['id_emprendedor'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_upd_rechazar_emprendedor_fase_2 @id_emprendedor = ?";
		$params = array($id_emprendedor);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			echo 'Error 1209. ';
			Errores::insErrorDeQuery('Error 1209. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );

			$response = new Usuarios();
			$response->cod_mensaje = $row['cod_mensaje'];
			$response->mensaje = utf8_encode($row['mensaje']);

			Conection::CerrarConexion($conn);
			return $response;
		}
	}

	/********************************************************************************************************************************************************/

	function insMentoresExcel($file)
	{
		//guardo el excel en la carpeta "archivos_excel/"
		$ruta = 'files/';
		$archivoExcel = $file['name'];
		$archivoExcel_tmp = $file['tmp_name'];
		move_uploaded_file($archivoExcel_tmp, $ruta.$archivoExcel);

		$extention = pathinfo($ruta.$archivoExcel,PATHINFO_EXTENSION);

		$rString = Helpers::generateRandomString();
		$file = $ruta.$rString.'.'.$extention;
		rename($ruta.$archivoExcel, $file);

		include_once(__DIR__.'/../assets/reader.php');

		$connection = new Spreadsheet_Excel_Reader(); // our main object
		if(is_bool( $connection->read($file) ) )
		{
			unlink($file);
			die();
		}

		$totalRows = $connection->sheets[0]['numRows'];
		$totalCols = $connection->sheets[0]['numCols'];

		$response = array();
		$j = 0;

		for ($i=2; $i <= $totalRows; $i++)
		{
			if( !isset($connection->sheets[0]["cells"][$i][1]) )
				break;

			if ($connection->sheets[0]["cells"][$i][1] == 'DATE') 
				$i++;

			//echo '<strong>linea: '.$i.'</strong><br>';

			$date                      = isset($connection->sheets[0]["cells"][$i][1]) ? $connection->sheets[0]["cells"][$i][1] : '';//A
				try {
					$dateTime = new DateTime("1899-12-30 + $date days");
					$date = $dateTime->format("Ymd");
				} catch (Exception $e) {
					$dateTime = new DateTime();
					$date = $dateTime->format("Ymd");
				}
			$nombre                    = isset($connection->sheets[0]["cells"][$i][2]) ? $connection->sheets[0]["cells"][$i][2] : '';//B
			$tipo_dni                  = isset($connection->sheets[0]["cells"][$i][3]) ? $connection->sheets[0]["cells"][$i][3] : '';//C
			$dni                       = isset($connection->sheets[0]["cells"][$i][4]) ? $connection->sheets[0]["cells"][$i][4] : '';//D
			$nacimiento                = isset($connection->sheets[0]["cells"][$i][5]) ? $connection->sheets[0]["cells"][$i][5] : '';//E
				try{
					$nacTime = new DateTime("1899-12-30 + $nacimiento days");
					$nacimiento = $nacTime->format("Ymd");
				}catch (Exception $e){
					$nacTime = new DateTime();
					$nacimiento = $nacTime->format("Ymd");
				}
			$sexo                      = isset($connection->sheets[0]["cells"][$i][6]) ? $connection->sheets[0]["cells"][$i][6] : '';//F
			$pais                      = isset($connection->sheets[0]["cells"][$i][7]) ? $connection->sheets[0]["cells"][$i][7] : '';//G
			$provincia                 = isset($connection->sheets[0]["cells"][$i][8]) ? $connection->sheets[0]["cells"][$i][8] : '';//H
			$ciudad                    = isset($connection->sheets[0]["cells"][$i][9]) ? $connection->sheets[0]["cells"][$i][9] : '';//I
			$direccion                 = isset($connection->sheets[0]["cells"][$i][10]) ? $connection->sheets[0]["cells"][$i][10] : '';//J
			$cp                        = isset($connection->sheets[0]["cells"][$i][11]) ? $connection->sheets[0]["cells"][$i][11] : '';//K
			$tel 					   = isset($connection->sheets[0]["cells"][$i][12]) ? $connection->sheets[0]["cells"][$i][12] : '';//L
			$cel                       = isset($connection->sheets[0]["cells"][$i][13]) ? $connection->sheets[0]["cells"][$i][13] : '';//M
			$mail                      = isset($connection->sheets[0]["cells"][$i][14]) ? $connection->sheets[0]["cells"][$i][14] : '';//N
			$skype                     = isset($connection->sheets[0]["cells"][$i][15]) ? $connection->sheets[0]["cells"][$i][15] : '';//O
			$linkedin                  = isset($connection->sheets[0]["cells"][$i][16]) ? $connection->sheets[0]["cells"][$i][16] : '';//P
			$comentarios 			   = isset($connection->sheets[0]["cells"][$i][17]) ? $connection->sheets[0]["cells"][$i][17] : '';//Q
			$estudio                   = isset($connection->sheets[0]["cells"][$i][18]) ? $connection->sheets[0]["cells"][$i][18] : '';//R
			$situacion_laboral         = isset($connection->sheets[0]["cells"][$i][19]) ? $connection->sheets[0]["cells"][$i][19] : '';//S
			$desc_laboral              = isset($connection->sheets[0]["cells"][$i][20]) ? $connection->sheets[0]["cells"][$i][20] : '';//T
			$nombre_emprendimiento     = isset($connection->sheets[0]["cells"][$i][21]) ? $connection->sheets[0]["cells"][$i][21] : '';//U
			$tiempo_emprendimiento     = isset($connection->sheets[0]["cells"][$i][22]) ? $connection->sheets[0]["cells"][$i][22] : '';//V
			$empleados                 = isset($connection->sheets[0]["cells"][$i][23]) ? $connection->sheets[0]["cells"][$i][23] : '';//W
			$pagina_web 			   = isset($connection->sheets[0]["cells"][$i][24]) ? $connection->sheets[0]["cells"][$i][24] : '';//X
			$actividad_tiempo_libre    = isset($connection->sheets[0]["cells"][$i][25]) ? $connection->sheets[0]["cells"][$i][25] : '';//Y
			$sn_emprendio              = isset($connection->sheets[0]["cells"][$i][26]) ? $connection->sheets[0]["cells"][$i][26] : '';//Z
			$sn_emprendimiento_activo  = isset($connection->sheets[0]["cells"][$i][27]) ? $connection->sheets[0]["cells"][$i][27] : '';//AA
			$desc_emprendimiento 	   = isset($connection->sheets[0]["cells"][$i][28]) ? $connection->sheets[0]["cells"][$i][28] : '';//AB
			$experiencia_mentor        = isset($connection->sheets[0]["cells"][$i][29]) ? $connection->sheets[0]["cells"][$i][29] : '';//AC
			$desc_experiencia_mentor   = isset($connection->sheets[0]["cells"][$i][30]) ? $connection->sheets[0]["cells"][$i][30] : '';//AD
			$motivacion                = isset($connection->sheets[0]["cells"][$i][31]) ? $connection->sheets[0]["cells"][$i][31] : '';//AE
			$aporte_al_programa        = isset($connection->sheets[0]["cells"][$i][32]) ? $connection->sheets[0]["cells"][$i][32] : '';//AF
			$tipo_emprendimiento       = isset($connection->sheets[0]["cells"][$i][33]) ? $connection->sheets[0]["cells"][$i][33] : '';//AG
			$tiempo_al_programa        = isset($connection->sheets[0]["cells"][$i][34]) ? $connection->sheets[0]["cells"][$i][34] : '';//AH
			$como_se_entero            = isset($connection->sheets[0]["cells"][$i][35]) ? $connection->sheets[0]["cells"][$i][35] : '';//AI
			$rol                       = isset($connection->sheets[0]["cells"][$i][36]) ? $connection->sheets[0]["cells"][$i][36] : '';//AJ
			$SEDE                      = isset($connection->sheets[0]["cells"][$i][37]) ? $connection->sheets[0]["cells"][$i][37] : '';//AK

			$arrayName = array( 
								'linea'					  => $i,
								'date'                    => $date,
								'nombre'                  => utf8_encode($nombre),
								'tipo_dni'                => $tipo_dni,
								'dni'                     => $dni,
								'nacimiento'              => $nacimiento,
								'sexo'                    => $sexo,
								'pais'                    => utf8_encode($pais),
								'provincia'               => utf8_encode($provincia),
								'ciudad'                  => utf8_encode($ciudad),
								'direccion'               => utf8_encode($direccion),
								'cp'                      => $cp,
								'tel'                     => $tel,
								'cel'                     => $cel,
								'mail'                    => $mail,
								'skype'                   => $skype,
								'linkedin'                => $linkedin,
								'comentarios'             => utf8_encode($comentarios),
								'estudio'                 => utf8_encode($estudio),
								'situacion_laboral'       => utf8_encode($situacion_laboral),
								'desc_laboral'            => utf8_encode($desc_laboral),
								'nombre_emprendimiento'   => utf8_encode($nombre_emprendimiento),
								'tiempo_emprendimiento'   => utf8_encode($tiempo_emprendimiento),
								'empleados'               => $empleados,
								'pagina_web'              => $pagina_web,
								'actividad_tiempo_libre'  => utf8_encode($actividad_tiempo_libre),
								'sn_emprendio'            => utf8_encode($sn_emprendio),
								'sn_emprendimiento_activo'=> utf8_encode($sn_emprendimiento_activo),
								'desc_emprendimiento'     => utf8_encode($desc_emprendimiento),
								'experiencia_mentor'      => utf8_encode($experiencia_mentor),
								'desc_experiencia_mentor' => utf8_encode($desc_experiencia_mentor),
								'motivacion'              => utf8_encode($motivacion),
								'aporte_al_programa'      => utf8_encode($aporte_al_programa),
								'tipo_emprendimiento'     => utf8_encode($tipo_emprendimiento),
								'tiempo_al_programa'      => $tiempo_al_programa,
								'como_se_entero'          => utf8_encode($como_se_entero),
								'rol'                     => utf8_encode($rol),
								'SEDE'                    => utf8_encode($SEDE),
									);
			$conn = Conection::conexion();

			//echo '<pre>';print_r($arrayName);echo '</pre>';

			$respuesta = Self::insUsuario($i, $date, $nombre, $tipo_dni, $dni, $nacimiento, $sexo, $pais, $provincia, $ciudad, $direccion, $cp, $tel, $cel, $mail, $skype, $linkedin, $comentarios, $estudio, $situacion_laboral, $desc_laboral, $nombre_emprendimiento, $tiempo_emprendimiento, $empleados, $pagina_web, $actividad_tiempo_libre, $sn_emprendio, $sn_emprendimiento_activo, $desc_emprendimiento, $experiencia_mentor, $desc_experiencia_mentor, $motivacion, $aporte_al_programa, $tipo_emprendimiento, $tiempo_al_programa, $como_se_entero, $rol, $SEDE);
			
			$response[$j] = $respuesta;
			$j++;
		}

		require_once 'class.excels.php';

		$mensaje = Excels::createExcel($response);
		$response[$j] = $mensaje;

		echo json_encode($response);
	}

	function insEmprendedoresExcel($file)
	{
		$ruta = 'files/';
		$archivoExcel = $file['name'];
		$archivoExcel_tmp = $file['tmp_name'];
		move_uploaded_file($archivoExcel_tmp, $ruta.$archivoExcel);

		$extention = pathinfo($ruta.$archivoExcel,PATHINFO_EXTENSION);

		$rString = Helpers::generateRandomString();
		$file = $ruta.$rString.'.'.$extention;
		rename($ruta.$archivoExcel, $file);

		include_once(__DIR__.'/../assets/reader.php');

		$connection = new Spreadsheet_Excel_Reader(); // este es el objeto principal
		if(is_bool( $connection->read($file) ) )
		{//si falla se rompe
			unlink($file);
			die('Hubo un error, revise que el excel no tenga ningún problema');
		}

		$totalRows = $connection->sheets[0]['numRows'];
		$totalCols = $connection->sheets[0]['numCols'];

		$response = array();
		$j = 0;

		for ($i=2; $i <= $totalRows; $i++)
		{
			if( !isset($connection->sheets[0]["cells"][$i][1]) )
				break;

			$SEDE	                               = isset($connection->sheets[0]["cells"][$i][1]) ? $connection->sheets[0]["cells"][$i][1] : '';//A
			$date                      			   = isset($connection->sheets[0]["cells"][$i][2]) ? $connection->sheets[0]["cells"][$i][2] : '';//B
				try {
					$dateTime = new DateTime("1899-12-30 + $date days");
					$date = $dateTime->format("Ymd");
				} catch (Exception $e) {
					$dateTime = new DateTime();
					$date = $dateTime->format("Ymd");
				}
			$email                                 =  isset($connection->sheets[0]["cells"][$i][3] )  ?  $connection->sheets[0]["cells"][$i][3] : '';//C
			$nombre	                               =  isset($connection->sheets[0]["cells"][$i][4] )  ?  $connection->sheets[0]["cells"][$i][4] : '';//D
			$sedeCercana                           =  isset($connection->sheets[0]["cells"][$i][5] )  ?  $connection->sheets[0]["cells"][$i][5] : '';//E
			$nombreComercial	                   =  isset($connection->sheets[0]["cells"][$i][6] )  ?  $connection->sheets[0]["cells"][$i][6] : '';//F
			$razonSocial                           =  isset($connection->sheets[0]["cells"][$i][7] )  ?  $connection->sheets[0]["cells"][$i][7] : '';//G
			$web                                   =  isset($connection->sheets[0]["cells"][$i][8] )  ?  $connection->sheets[0]["cells"][$i][8] : '';//H
			$tipoOrganizacion                      =  isset($connection->sheets[0]["cells"][$i][9] )  ?  $connection->sheets[0]["cells"][$i][9] : '';//I
			$descripcionActividadPrincipal         =  isset($connection->sheets[0]["cells"][$i][10] ) ? $connection->sheets[0]["cells"][$i][10] : '';//J
			$tiempoComenzaste                      =  isset($connection->sheets[0]["cells"][$i][11] ) ? $connection->sheets[0]["cells"][$i][11] : '';//K
			$funcionesDesempenadas                 =  isset($connection->sheets[0]["cells"][$i][12] ) ? $connection->sheets[0]["cells"][$i][12] : '';//L
			$cantSocios                            =  isset($connection->sheets[0]["cells"][$i][13] ) ? $connection->sheets[0]["cells"][$i][13] : '';//M
			$cantPersonas                          =  isset($connection->sheets[0]["cells"][$i][14] ) ? $connection->sheets[0]["cells"][$i][14] : '';//N
			$rubro	                               =  isset($connection->sheets[0]["cells"][$i][15] ) ? $connection->sheets[0]["cells"][$i][15] : '';//O
			$financiamiento                        =  isset($connection->sheets[0]["cells"][$i][16] ) ? $connection->sheets[0]["cells"][$i][16] : '';//P
			$etapa                                 =  isset($connection->sheets[0]["cells"][$i][17] ) ? $connection->sheets[0]["cells"][$i][17] : '';//Q
			$propuestaValor                        =  isset($connection->sheets[0]["cells"][$i][18] ) ? $connection->sheets[0]["cells"][$i][18] : '';//R
			$conocimientoMercado                   =  isset($connection->sheets[0]["cells"][$i][19] ) ? $connection->sheets[0]["cells"][$i][19] : '';//S
			$estrategiaSalida                      =  isset($connection->sheets[0]["cells"][$i][20] ) ? $connection->sheets[0]["cells"][$i][20] : '';//T
			$analisisCompetencia                   =  isset($connection->sheets[0]["cells"][$i][21] ) ? $connection->sheets[0]["cells"][$i][21] : '';//U
			$fidelizacionClientes                  =  isset($connection->sheets[0]["cells"][$i][22] ) ? $connection->sheets[0]["cells"][$i][22] : '';//V
			$expansionNuevosMercados               =  isset($connection->sheets[0]["cells"][$i][23] ) ? $connection->sheets[0]["cells"][$i][23] : '';//W
			$analisisActoresClave                  =  isset($connection->sheets[0]["cells"][$i][24] ) ? $connection->sheets[0]["cells"][$i][24] : '';//X
			$consolidacionPuntoEquilibrio          =  isset($connection->sheets[0]["cells"][$i][25] ) ? $connection->sheets[0]["cells"][$i][25] : '';//Y
			$estabilidadFinanciera                 =  isset($connection->sheets[0]["cells"][$i][26] ) ? $connection->sheets[0]["cells"][$i][26] : '';//Z
			$accesoFinanciamiento                  =  isset($connection->sheets[0]["cells"][$i][27] ) ? $connection->sheets[0]["cells"][$i][27] : '';//AA
			$administracionInsumos                 =  isset($connection->sheets[0]["cells"][$i][28] ) ? $connection->sheets[0]["cells"][$i][28] : '';//AB
			$gestionProveedores                    =  isset($connection->sheets[0]["cells"][$i][29] ) ? $connection->sheets[0]["cells"][$i][29] : '';//AC
			$disenoSistemaProduccion               =  isset($connection->sheets[0]["cells"][$i][30] ) ? $connection->sheets[0]["cells"][$i][30] : '';//AD
			$escalabilidadProduccion               =  isset($connection->sheets[0]["cells"][$i][31] ) ? $connection->sheets[0]["cells"][$i][31] : '';//AE
			$cumplimientoRequisitosLegales         =  isset($connection->sheets[0]["cells"][$i][32] ) ? $connection->sheets[0]["cells"][$i][32] : '';//AF
			$disenoSistemasAdministrativos         =  isset($connection->sheets[0]["cells"][$i][33] ) ? $connection->sheets[0]["cells"][$i][33] : '';//AG
			$disenoSistemasPlaneamiento            =  isset($connection->sheets[0]["cells"][$i][34] ) ? $connection->sheets[0]["cells"][$i][34] : '';//AH
			$estructuraPersonal                    =  isset($connection->sheets[0]["cells"][$i][35] ) ? $connection->sheets[0]["cells"][$i][35] : '';//AI
			$sistemaGestion                        =  isset($connection->sheets[0]["cells"][$i][36] ) ? $connection->sheets[0]["cells"][$i][36] : '';//AJ
			$afirmacionesEmprendimientoSeisMeses   =  isset($connection->sheets[0]["cells"][$i][37] ) ? $connection->sheets[0]["cells"][$i][37] : '';//AK
			$afirmacionesEmprendimiento            =  isset($connection->sheets[0]["cells"][$i][38] ) ? $connection->sheets[0]["cells"][$i][38] : '';//AL
			$prioridadExito                        =  isset($connection->sheets[0]["cells"][$i][39] ) ? $connection->sheets[0]["cells"][$i][39] : '';//AM
			$visionFuturo                          =  isset($connection->sheets[0]["cells"][$i][40] ) ? $connection->sheets[0]["cells"][$i][40] : '';//AN
			$impactoSocial                         =  isset($connection->sheets[0]["cells"][$i][41] ) ? $connection->sheets[0]["cells"][$i][41] : '';//AO
			$impactoAmbiental                      =  isset($connection->sheets[0]["cells"][$i][42] ) ? $connection->sheets[0]["cells"][$i][42] : '';//AP
			$confianza                             =  isset($connection->sheets[0]["cells"][$i][43] ) ? $connection->sheets[0]["cells"][$i][43] : '';//AQ
			$descripcionDesafios                   =  isset($connection->sheets[0]["cells"][$i][44] ) ? $connection->sheets[0]["cells"][$i][44] : '';//AR
			$nivelEstudios                         =  isset($connection->sheets[0]["cells"][$i][45] ) ? $connection->sheets[0]["cells"][$i][45] : '';//AS
			$terciarioUniversitario				   =  isset($connection->sheets[0]["cells"][$i][46] ) ? $connection->sheets[0]["cells"][$i][46] : '';//AT
			$actividadesPersonales                 =  isset($connection->sheets[0]["cells"][$i][47] ) ? $connection->sheets[0]["cells"][$i][47] : '';//AU
			$otroProgramaMentoria                  =  isset($connection->sheets[0]["cells"][$i][48] ) ? $connection->sheets[0]["cells"][$i][48] : '';//AV
			$otroProgramaDelMinisterio             =  isset($connection->sheets[0]["cells"][$i][49] ) ? $connection->sheets[0]["cells"][$i][49] : '';//AW
			$interesEnParticipar                   =  isset($connection->sheets[0]["cells"][$i][50] ) ? $connection->sheets[0]["cells"][$i][50] : '';//AX
			$tiempoDedicacionAlPrograma            =  isset($connection->sheets[0]["cells"][$i][51] ) ? $connection->sheets[0]["cells"][$i][51] : '';//AY
			$comoTeEnteraste                       =  isset($connection->sheets[0]["cells"][$i][52] ) ? $connection->sheets[0]["cells"][$i][52] : '';//AZ
			$dni                                   =  isset($connection->sheets[0]["cells"][$i][53] ) ? $connection->sheets[0]["cells"][$i][53] : '';//BA
			$nacimiento                            =  isset($connection->sheets[0]["cells"][$i][54] ) ? $connection->sheets[0]["cells"][$i][54] : '';//BB
			try {
					if($dateTime = DateTime::createFromFormat('d/m/Y', $nacimiento)){
						$nacimiento = $dateTime->format('Ymd');
					}else{
						$dateTime = new DateTime();
						$nacimiento = $dateTime->format("Ymd");
					}
				} catch (Exception $e) {
					$dateTime = new DateTime();
					$nacimiento = $dateTime->format("Ymd");
				}
			$genero                                =  isset($connection->sheets[0]["cells"][$i][55] ) ? $connection->sheets[0]["cells"][$i][55] : '';//BC
			$nacionalidad                          =  isset($connection->sheets[0]["cells"][$i][56] ) ? $connection->sheets[0]["cells"][$i][56] : '';//BD
			$provincia                             =  isset($connection->sheets[0]["cells"][$i][57] ) ? $connection->sheets[0]["cells"][$i][57] : '';//BE
			$municipio                             =  isset($connection->sheets[0]["cells"][$i][58] ) ? $connection->sheets[0]["cells"][$i][58] : '';//BF
			$direccion                             =  isset($connection->sheets[0]["cells"][$i][59] ) ? $connection->sheets[0]["cells"][$i][59] : '';//BG
			$codPostal                             =  isset($connection->sheets[0]["cells"][$i][60] ) ? $connection->sheets[0]["cells"][$i][60] : '';//BH
			$telefono                              =  isset($connection->sheets[0]["cells"][$i][61] ) ? $connection->sheets[0]["cells"][$i][61] : '';//BI
			$celular                               =  isset($connection->sheets[0]["cells"][$i][62] ) ? $connection->sheets[0]["cells"][$i][62] : '';//BJ
			$skype                                 =  isset($connection->sheets[0]["cells"][$i][63] ) ? $connection->sheets[0]["cells"][$i][63] : '';//BK
			$comentarios                           =  isset($connection->sheets[0]["cells"][$i][64] ) ? $connection->sheets[0]["cells"][$i][64] : '';//BL
			$rol                                   =  isset($connection->sheets[0]["cells"][$i][65] ) ? $connection->sheets[0]["cells"][$i][65] : '';//BM

			$misDatos = array( 
								'SEDE'                                  => addslashes($SEDE),
								'date'                                  => addslashes($date),
								'email'                                 => addslashes($email),
								'nombre'                                => addslashes($nombre),
								'sedeCercana'                           => addslashes($sedeCercana),
								'nombreComercial'                       => addslashes($nombreComercial),
								'razonSocial'                           => addslashes($razonSocial),
								'web'                                   => addslashes($web),
								'tipoOrganizacion'                      => addslashes($tipoOrganizacion),
								'descripcionActividadPrincipal'         => addslashes($descripcionActividadPrincipal),
								'tiempoComenzaste'                      => addslashes($tiempoComenzaste),
								'funcionesDesempenadas'                 => addslashes($funcionesDesempenadas),
								'cantSocios'                            => addslashes($cantSocios),
								'cantPersonas'                          => addslashes($cantPersonas),
								'rubro'	                                => addslashes($rubro),
								'financiamiento'                        => addslashes($financiamiento),
								'etapa'                                 => addslashes($etapa),
								'propuestaValor'                        => addslashes($propuestaValor),
								'conocimientoMercado'                   => addslashes($conocimientoMercado),
								'estrategiaSalida'                      => addslashes($estrategiaSalida),
								'analisisCompetencia'                   => addslashes($analisisCompetencia),
								'fidelizacionClientes'                  => addslashes($fidelizacionClientes),
								'expansionNuevosMercados'               => addslashes($expansionNuevosMercados),
								'analisisActoresClave'                  => addslashes($analisisActoresClave),
								'consolidacionPuntoEquilibrio'          => addslashes($consolidacionPuntoEquilibrio),
								'estabilidadFinanciera'                 => addslashes($estabilidadFinanciera),
								'accesoFinanciamiento'                  => addslashes($accesoFinanciamiento),
								'administracionInsumos'                 => addslashes($administracionInsumos),
								'gestionProveedores'                    => addslashes($gestionProveedores),
								'disenoSistemaProduccion'               => addslashes($disenoSistemaProduccion),
								'escalabilidadProduccion'               => addslashes($escalabilidadProduccion),
								'cumplimientoRequisitosLegales'         => addslashes($cumplimientoRequisitosLegales),
								'disenoSistemasAdministrativos'         => addslashes($disenoSistemasAdministrativos),
								'disenoSistemasPlaneamiento'            => addslashes($disenoSistemasPlaneamiento),
								'estructuraPersonal'                    => addslashes($estructuraPersonal),
								'sistemaGestion'                        => addslashes($sistemaGestion),
								'afirmacionesEmprendimientoSeisMeses'   => addslashes($afirmacionesEmprendimientoSeisMeses),
								'afirmacionesEmprendimiento'            => addslashes($afirmacionesEmprendimiento),
								'prioridadExito'                        => addslashes($prioridadExito),
								'visionFuturo'                          => addslashes($visionFuturo),
								'impactoSocial'                         => addslashes($impactoSocial),
								'impactoAmbiental'                      => addslashes($impactoAmbiental),
								'confianza'                             => addslashes($confianza),
								'descripcionDesafios'                   => addslashes($descripcionDesafios),
								'nivelEstudios'                         => addslashes($nivelEstudios),
								'terciarioUniversitario'                => addslashes($terciarioUniversitario),
								'actividadesPersonales'                 => addslashes($actividadesPersonales),
								'otroProgramaMentoria'                  => addslashes($otroProgramaMentoria),
								'otroProgramaDelMinisterio'             => addslashes($otroProgramaDelMinisterio),
								'interesEnParticipar'                   => addslashes($interesEnParticipar),
								'tiempoDedicacionAlPrograma'            => addslashes($tiempoDedicacionAlPrograma),
								'comoTeEnteraste'                       => addslashes($comoTeEnteraste),
								'dni'                                   => addslashes($dni),
								'nacimiento'                            => addslashes($nacimiento),
								'genero'                                => addslashes($genero),
								'nacionalidad'                          => addslashes($nacionalidad),
								'provincia'                             => addslashes($provincia),
								'municipio'                             => addslashes($municipio),
								'direccion'                             => addslashes($direccion),
								'codPostal'                             => addslashes($codPostal),
								'telefono'                              => addslashes($telefono),
								'celular'                               => addslashes($celular),
								'skype'                                 => addslashes($skype),
								'comentarios'                           => addslashes($comentarios),
								'rol'                                   => addslashes($rol),
									);
			$conn = Conection::conexion();

			$respuesta = Self::insEmprendedor($misDatos, $i);

			$response[$j] = $respuesta;
			//$response[$j] = $misDatos;
			$j++;
		}

		require_once 'class.excels.php';
		$mensaje = Excels::createExcel($response);
		$response[$j] = $mensaje;
		if(json_encode($response))
			echo json_encode($response);
		else
			echo '<a href="files/'.$mensaje->file.'" download>Descargar excel</a>';
	}

	function insRelacionamientoExcel($file)
	{
		//BROWA
		//guardo el excel en la carpeta "archivos_excel/"
		$ruta = 'files/';
		$archivoExcel = $file['name'];
		$archivoExcel_tmp = $file['tmp_name'];
		move_uploaded_file($archivoExcel_tmp, $ruta.$archivoExcel);

		$extention = pathinfo($ruta.$archivoExcel,PATHINFO_EXTENSION);

		$rString = Helpers::generateRandomString();
		$file = $ruta.$rString.'.'.$extention;
		rename($ruta.$archivoExcel, $file);

		include_once(__DIR__.'/../assets/reader.php');

		$connection = new Spreadsheet_Excel_Reader(); // our main object
		if(is_bool( $connection->read($file) ) )
		{
			unlink($file);
			die();
		}

		$totalRows = $connection->sheets[0]['numRows'];
		$totalCols = $connection->sheets[0]['numCols'];

		$response = array();
		$j = 0;

		for ($i=2; $i <= $totalRows; $i++)
		{
			if( !isset($connection->sheets[0]["cells"][$i][1]) )
				break;

			if ($connection->sheets[0]["cells"][$i][1] == 'DATE') 
				$i++;

			$dni_emprendedor = isset($connection->sheets[0]["cells"][$i][1]) ? $connection->sheets[0]["cells"][$i][1] : '';
			$dni_mentor      = isset($connection->sheets[0]["cells"][$i][2]) ? $connection->sheets[0]["cells"][$i][2] : '';
			$grupo           = isset($connection->sheets[0]["cells"][$i][3]) ? $connection->sheets[0]["cells"][$i][3] : '';

			$arrayName = array( 
								'linea'			  => $i,
								'dni_emprendedor' => $dni_emprendedor,
								'dni_mentor'      => $dni_mentor,
								'grupo'           => $grupo,
									);
			$conn = Conection::conexion();

			//print_r($arrayName);

			$respuesta = Self::insRelacionamiento($i, $dni_emprendedor, $dni_mentor, $grupo);
			
			$response[$j] = $respuesta;
			$j++;
		}
		echo json_encode($response);
	}

	function updBorrarRelacionamientoExcel($file)
	{
		//BROWA
		//guardo el excel en la carpeta "archivos_excel/"
		$ruta = 'files/';
		$archivoExcel = $file['name'];
		$archivoExcel_tmp = $file['tmp_name'];
		move_uploaded_file($archivoExcel_tmp, $ruta.$archivoExcel);

		$extention = pathinfo($ruta.$archivoExcel,PATHINFO_EXTENSION);

		$rString = Helpers::generateRandomString();
		$file = $ruta.$rString.'.'.$extention;
		rename($ruta.$archivoExcel, $file);

		include_once(__DIR__.'/../assets/reader.php');

		$connection = new Spreadsheet_Excel_Reader(); // our main object
		if(is_bool( $connection->read($file) ) )
		{
			unlink($file);
			die();
		}

		$totalRows = $connection->sheets[0]['numRows'];
		$totalCols = $connection->sheets[0]['numCols'];

		$response = array();
		$j = 0;

		for ($i=1; $i <= $totalRows; $i++)
		{
			$dni_emprendedor = isset($connection->sheets[0]["cells"][$i][1]) ? $connection->sheets[0]["cells"][$i][1] : '';//A

			$arrayName = array( 
								'linea'			  => $i,
								'dni_emprendedor' => $dni_emprendedor,
									);
			$conn = Conection::conexion();

			//print_r($arrayName);

			$respuesta = Self::updBorarRelacionamientoEmprendedorByDNI($i, $dni_emprendedor);
			
			$response[$j] = $respuesta;
			$j++;
		}
		echo json_encode($response);
	}

	function insEmprendedor($datos, $linea)
	{
		$conn = Conection::Conexion();
		$sql = "EXEC sp_ins_emprendedor @SEDE = ?, @date = ?, @email = ?, @nombre = ?, @sedeCercana = ?, @nombreComercial = ?, @razonSocial = ?, @web = ?, @tipoOrganizacion = ?, @descripcionActividadPrincipal = ?, @tiempoComenzaste = ?, @funcionesDesempenadas = ?, @cantSocios = ?, @cantPersonas = ?, @rubro = ?, @financiamiento = ?, @etapa = ?, @propuestaValor = ?, @conocimientoMercado = ?, @estrategiaSalida = ?, @analisisCompetencia = ?, @fidelizacionClientes = ?, @expansionNuevosMercados = ?, @analisisActoresClave = ?, @consolidacionPuntoEquilibrio = ?, @estabilidadFinanciera = ?, @accesoFinanciamiento = ?, @administracionInsumos = ?, @gestionProveedores = ?, @disenoSistemaProduccion = ?, @escalabilidadProduccion = ?, @cumplimientoRequisitosLegales = ?, @disenoSistemasAdministrativos = ?, @disenoSistemasPlaneamiento = ?, @estructuraPersonal = ?, @sistemaGestion = ?, @afirmacionesEmprendimientoSeisMeses = ?, @afirmacionesEmprendimiento = ?, @prioridadExito = ?, @visionFuturo = ?, @impactoSocial = ?, @impactoAmbiental = ?, @confianza = ?, @descripcionDesafios = ?, @nivelEstudios = ?, @terciarioUniversitario = ?, @actividadesPersonales = ?, @otroProgramaMentoria = ?, @otroProgramaDelMinisterio = ?, @interesEnParticipar = ?, @tiempoDedicacionAlPrograma = ?, @comoTeEnteraste = ?, @dni = ?, @nacimiento = ?, @genero = ?, @nacionalidad = ?, @provincia = ?, @municipio = ?, @direccion = ?, @codPostal = ?, @telefono = ?, @celular = ?, @skype = ?, @comentarios = ?, @rol = ?";
		$params = array(
						$datos['SEDE'],
						$datos['date'],
						$datos['email'],
						$datos['nombre'],
						$datos['sedeCercana'],
						$datos['nombreComercial'],
						$datos['razonSocial'],
						$datos['web'],
						$datos['tipoOrganizacion'],
						$datos['descripcionActividadPrincipal'],
						$datos['tiempoComenzaste'],
						$datos['funcionesDesempenadas'],
						$datos['cantSocios'],
						$datos['cantPersonas'],
						$datos['rubro'],
						$datos['financiamiento'],
						$datos['etapa'],
						$datos['propuestaValor'],
						$datos['conocimientoMercado'],
						$datos['estrategiaSalida'],
						$datos['analisisCompetencia'],
						$datos['fidelizacionClientes'],
						$datos['expansionNuevosMercados'],
						$datos['analisisActoresClave'],
						$datos['consolidacionPuntoEquilibrio'],
						$datos['estabilidadFinanciera'],
						$datos['accesoFinanciamiento'],
						$datos['administracionInsumos'],
						$datos['gestionProveedores'],
						$datos['disenoSistemaProduccion'],
						$datos['escalabilidadProduccion'],
						$datos['cumplimientoRequisitosLegales'],
						$datos['disenoSistemasAdministrativos'],
						$datos['disenoSistemasPlaneamiento'],
						$datos['estructuraPersonal'],
						$datos['sistemaGestion'],
						$datos['afirmacionesEmprendimientoSeisMeses'],
						$datos['afirmacionesEmprendimiento'],
						$datos['prioridadExito'],
						$datos['visionFuturo'],
						$datos['impactoSocial'],
						$datos['impactoAmbiental'],
						$datos['confianza'],
						$datos['descripcionDesafios'],
						$datos['nivelEstudios'],
						$datos['terciarioUniversitario'],
						$datos['actividadesPersonales'],
						$datos['otroProgramaMentoria'],
						$datos['otroProgramaDelMinisterio'],
						$datos['interesEnParticipar'],
						$datos['tiempoDedicacionAlPrograma'],
						$datos['comoTeEnteraste'],
						$datos['dni'],
						$datos['nacimiento'],
						$datos['genero'],
						$datos['nacionalidad'],
						$datos['provincia'],
						$datos['municipio'],
						$datos['direccion'],
						$datos['codPostal'],
						$datos['telefono'],
						$datos['celular'],
						$datos['skype'],
						$datos['comentarios'],
						$datos['rol']
						);

		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			Errores::insErrorDeQuery('Error 1101. ', $sql, $params);
			$response = new Usuarios();
			$response->cod_mensaje = 0;
			$response->mensaje = 'Error 1101 ';
			$response->linea = $linea;
			$response->dni = $datos['dni'];
			return $response;
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$response = new Usuarios();
			$response->cod_mensaje = $row['cod_mensaje'];
			$response->mensaje = utf8_encode($row['mensaje']);
			$response->linea = $linea;
			$response->dni = $datos['dni'];

			Conection::CerrarConexion($conn);
			return $response;
		}
	}

	function insUsuario($linea, $date, $nombre, $tipo_dni, $dni, $nacimiento, $sexo, $pais, $provincia, $ciudad, $direccion, $cp, $tel, $cel, $mail, $skype, $linkedin, $comentarios, $estudio, $situacion_laboral, $desc_laboral, $nombre_emprendimiento, $tiempo_emprendimiento, $empleados, $pagina_web, $actividad_tiempo_libre, $sn_emprendio, $sn_emprendimiento_activo, $desc_emprendimiento, $experiencia_mentor, $desc_experiencia_mentor, $motivacion, $aporte_al_programa, $tipo_emprendimiento, $tiempo_al_programa, $como_se_entero, $rol, $sede)
	{
		$conn = Conection::Conexion();
		$sql = "EXEC sp_ins_mentor @date = ?, @nombre = ?, @tipo_dni = ?, @dni = ?, @nacimiento = ?, @sexo = ?, @pais = ?, @provincia = ?, @ciudad = ?, @direccion = ?, @cp = ?, @tel = ?, @cel = ?, @mail = ?, @skype = ?, @linkedin = ?, @comentarios = ?, @estudio = ?, @situacion_laboral = ?, @desc_laboral = ?, @nombre_emprendimiento = ?, @tiempo_emprendimiento = ?, @empleados = ?, @pagina_web = ?, @actividad_tiempo_libre = ?, @sn_emprendio = ?, @sn_emprendimiento_activo = ?, @desc_emprendimiento = ?, @experiencia_mentor = ?, @desc_experiencia_mentor = ?, @motivacion = ?, @aporte_al_programa = ?, @tipo_emprendimiento = ?, @tiempo_al_programa = ?, @como_se_entero = ?, @rol = ?, @sede = ?";
		$params = array($date, $nombre, $tipo_dni, $dni, $nacimiento, $sexo, $pais, $provincia, $ciudad, $direccion, $cp, $tel, $cel, $mail, $skype, $linkedin, $comentarios, $estudio, $situacion_laboral, $desc_laboral, $nombre_emprendimiento, $tiempo_emprendimiento, $empleados, $pagina_web, $actividad_tiempo_libre, $sn_emprendio, $sn_emprendimiento_activo, $desc_emprendimiento, $experiencia_mentor, $desc_experiencia_mentor, $motivacion, $aporte_al_programa, $tipo_emprendimiento, $tiempo_al_programa, $como_se_entero, $rol, $sede);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			Errores::insErrorDeQuery('Error 1102. ', $sql, $params);
			$response = new Usuarios();
			$response->cod_mensaje = 0;
			$response->mensaje = 'El usuario no pudo cargarse correctamente, revise los datos por favor. ';
			$response->dni = $dni;
			$response->linea = $linea;
			return $response;
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$response = new Usuarios();
			$response->cod_mensaje = $row['cod_mensaje'];
			$response->mensaje = utf8_encode($row['mensaje']);
			$response->dni = $dni;
			$response->linea = $linea;

			Conection::CerrarConexion($conn);
			return $response;
		}
	}

	function insRelacionamiento($linea, $dni_emprendedor, $dni_mentor, $grupo)
	{
		$conn = Conection::Conexion();
		$sql = "EXEC sp_relacionar_emprendedor_mentor @dni_emprendedor = ?, @dni_mentor = ?, @grupo = ?";
		$params = array($dni_emprendedor, $dni_mentor, $grupo);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			Errores::insErrorDeQuery('Error 1103. ', $sql, $params);
			$response = new Usuarios();
			$response->cod_mensaje = 0;
			$response->mensaje = 'Error 1103. ';
			$response->linea = $linea;
			return $response;
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$response = new Usuarios();
			$response->cod_mensaje = isset($row['cod_mensaje']) ? $row['cod_mensaje'] : 0;
			$response->mensaje = isset($row['mensaje']) ? utf8_encode($row['mensaje']) : '';
			$response->linea = $linea;

			Conection::CerrarConexion($conn);
			return $response;
		}
	}

	function insRelacionamientoGestor($post)
	{
		$dni_emprendedor = $post['dni_emprendedor'];
		$dni_mentor = $post['dni_mentor'];
		$grupo = $post['grupo'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_relacionar_emprendedor_mentor @dni_emprendedor = ?, @dni_mentor = ?, @grupo = ?";
		$params = array($dni_emprendedor, $dni_mentor, $grupo);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			Errores::insErrorDeQuery('Error 1103.2 ', $sql, $params);
			$response = new Usuarios();
			$response->cod_mensaje = 0;
			$response->mensaje = 'Error 1103.2 ';
			return $response;
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$response = new Usuarios();
			$response->cod_mensaje = isset($row['cod_mensaje']) ? $row['cod_mensaje'] : 0;
			$response->mensaje = isset($row['mensaje']) ? utf8_encode($row['mensaje']) : '';

			Conection::CerrarConexion($conn);
			return $response;
		}
	}

	function updSede($post)
	{
		$id_usuario = Cookies::getIdUsuario();
		$id_pais = $post['id_pais'];
		$id_provincia = $post['id_provincia'];
		$id_ciudad = $post['id_ciudad'];
		$id_sede = $post['id_sede'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_upd_sede @id_usuario = ?, @id_pais = ?, @id_provincia = ?, @id_municipio = ?, @id_sede = ?";
		$params = array($id_usuario, $id_pais, $id_provincia, $id_ciudad, $id_sede);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			return 'Error 1104. ';
			Errores::insErrorDeQuery('Error 1104. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$response = new Usuarios();
			$response->cod_mensaje = $row['cod_mensaje'];
			$response->mensaje = utf8_encode($row['mensaje']);

			Conection::CerrarConexion($conn);
			echo json_encode( $response );
		}
	}

	function recuperarPass($post)
	{
		$email = $post['email'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_get_user_by_mail @email = ?";
		$params = array($email);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			return 'Error 1105. ';
			Errores::insErrorDeQuery('Error 1105. ', $sql, $params);
		}else{
			require_once 'class.mails.php';
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$response = new Usuarios();
			$response->cod_mensaje = $row['cod_mensaje'];
			$response->mensaje = utf8_encode($row['mensaje']);
			
			$randomString = $row['randomString'];
			$response->email = Email::mail_recuperar_pass($randomString, $email);

			Conection::CerrarConexion($conn);
			echo json_encode( $response );
		}
	}

	function insUsuarioCoordinador($post)
	{
		$nombre = $post['nombre'];
		$email = $post['email'];
		$dni = $post['dni'];
		$id_provincia  = $post['id_provincia'];
		$id_ciudad = $post['id_ciudad'];

		$conn = Conection::Conexion();
		$sql = "EXEC sp_ins_usuario_coordinador @nombre = ?, @email = ?, @dni = ?, @id_provincia = ?, @id_ciudad = ?";
		$params = array($nombre, $email, $dni, $id_provincia, $id_ciudad);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			Errores::insErrorDeQuery('Error 1106. ', $sql, $params);
			return 'Error 1106. ';
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$response = new Usuarios();
			$response->cod_mensaje = $row['cod_mensaje'];
			$response->mensaje = utf8_encode($row['mensaje']);
			if($response->cod_mensaje == -1){
				$response->id_usuario = $row['id_usuario'];
				$sedes = $post['sedes'];
				$respSedes = array();
				$i = 0;
				foreach ($sedes as $key => $value) 
				{
					$myResp = new Usuarios();
					$myResp->prov = Self::insSedeCoordinador($value, $response->id_usuario);
					$respSedes[$i] = $myResp;
					$i++;
				}
				$response->sedes = $respSedes;
			}

			Conection::CerrarConexion($conn);
			echo json_encode( $response );
		}
	}

	function insSedeCoordinador($id_sede, $id_usuario)
	{
		$conn = Conection::Conexion();
		$sql = "EXEC sp_ins_sede_a_coordinador @id_sede = ?, @id_usuario = ?";
		$params = array($id_sede, $id_usuario);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false)
		{
			Conection::CerrarConexion($conn);
			Errores::insErrorDeQuery('Error 1107. ', $sql, $params);
			return 'Error 1107. ';
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$response = new Usuarios();
			$response->cod_mensaje = $row['cod_mensaje'];
			$response->mensaje = utf8_encode($row['mensaje']);

			Conection::CerrarConexion($conn);
			return $response;
		}
	}

	function bloquearUsuario($post)
	{
		$id_usuario = $post['id_usuario'];
		$id_admin = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_upd_bloquear_usuario @id_usuario = ?, @id_admin = ?";
		$params = array($id_usuario, $id_admin);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			return 'Error 1201. ';
			Errores::insErrorDeQuery('Error 1201. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$response = new Usuarios();
			$response->cod_mensaje = $row['cod_mensaje'];
			$response->mensaje = utf8_encode($row['mensaje']);

			Conection::CerrarConexion($conn);
			echo json_encode( $response );
		}
	}

	function desbloquearUsuario($post)
	{
		$id_usuario = $post['id_usuario'];
		$id_admin = Cookies::getIdUsuario();

		$conn = Conection::Conexion();
		$sql = "EXEC sp_upd_desbloquear_usuario @id_usuario = ?, @id_admin = ?";
		$params = array($id_usuario, $id_admin);
		$stmt = sqlsrv_query($conn, $sql, $params);

		if ($stmt === false) 
		{
			Conection::CerrarConexion($conn);
			return 'Error 1202. ';
			Errores::insErrorDeQuery('Error 1202. ', $sql, $params);
		}else{
			$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC );
			$response = new Usuarios();
			$response->cod_mensaje = $row['cod_mensaje'];
			$response->mensaje = utf8_encode($row['mensaje']);

			Conection::CerrarConexion($conn);
			echo json_encode( $response );
		}
	}

	function logout()
	{
		Cookies::destroyCookies();
	}
}
?>