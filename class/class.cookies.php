<?php 
/**
* Clase de seguridad con cookies
*/
class Cookies extends Encriptacion
{
	function getIdUsuario()
	{
		$id_usuario = Encriptacion::desencriptar($_COOKIE['id_usuario']);
		return intval($id_usuario);
	}

	function getDatosUser()
	{
		$datosUser = new Cookies();

		if(!isset($_COOKIE['tipo_usuario']))
			header('location:login.php');

		$datosUser->tipo_usuario = Encriptacion::desencriptar($_COOKIE['tipo_usuario']);
		$datosUser->id_usuario = Encriptacion::desencriptar($_COOKIE['id_usuario']);
		$datosUser->nombre = $_COOKIE['nombre'];

		return $datosUser;
	}

	function grabarCookies($params)
	{
		foreach ($params as $key => $value) 
		{
			setcookie($value->nombre, $value->valor, time()+31556926 ,'/' );
		}
	}

	function grabarCookiesLogin($id_usuario, $tipo_usuario, $nombre, $apellido, $id_catering)
	{
		setcookie('id_usuario', Encriptacion::encriptar($id_usuario), time()+31556926 ,'/' );
		setcookie('tipo_usuario', Encriptacion::encriptar($tipo_usuario), time()+31556926 ,'/' );
		setcookie('nombre', $nombre, time()+31556926 ,'/' );
		setcookie('apellido', $apellido, time()+31556926 ,'/' );
		setcookie('id_catering', Encriptacion::encriptar($id_catering), time()+31556926 ,'/' );
	}

	public function bloquearPaginaCatering()
	{
		$tipo_usuario = Encriptacion::desencriptar($_COOKIE['tipo_usuario']);
		if( is_int(intval($tipo_usuario)) )
		{
			if($tipo_usuario != 1)
			{
				if($tipo_usuario == 2 || $tipo_usuario == 4)
					header('location:home');
				else
					header('location:login');
			}
		}else{
			header('location:login');
		}
	}

	public function bloquearPaginaCliente()
	{
		$tipo_usuario = Encriptacion::desencriptar($_COOKIE['tipo_usuario']);
		if( is_int(intval($tipo_usuario)) )
		{
			if($tipo_usuario != 2 && $tipo_usuario != 4)
			{
				if($tipo_usuario == 1)
					header('location:catering');
				else
					header('location:login');
			}
		}else{
			header('location:login');
		}
	}

	function destroyCookies()
	{
		if (isset($_SERVER['HTTP_COOKIE'])) 
		{
		    $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
		    foreach($cookies as $cookie) {
		        $parts = explode('=', $cookie);
		        $name = trim($parts[0]);
		        setcookie($name, '', time()-1000);
		        setcookie($name, '', time()-1000, '/');
		    }
		}
	}
}
?>