<?php 
	require_once('class/class.helpers.php');

	if(!isset($_COOKIE['tipo_usuario']))
		header('location:login.php');

	$tipo_usuario = Encriptacion::desencriptar($_COOKIE['tipo_usuario']);

	if(!is_int(intval($tipo_usuario)))
		header('location:login.php');

	switch ($tipo_usuario) {
		case 1:
            $rol = 'admin';
			require 'share/header-admin.php';
			break;
		
		case 2:
            $rol = 'dt';
			require 'share/header-mentorDT.php';
			break;

		case 3:
            $rol = 'con';
			require 'share/header-mentor-con.php';
			break;

		case 7:
            $rol = 'admin';
			require 'share/header-admin.php';
			break;

        case 8:
            $rol = 'public';
            require 'share/header-publico.php';
            break;
		default:
			header('location:login.php');
			break;
	}
?>

<div class="content home">
	<h1>Red de mentores</h1>
	<div class="col100">
        <div class="module search-bar">
            <!-- BARRA DE BÚSQUEDA -->
            <div class="inner-cols clearfix">
                <div class="col50">
                    <form action="javascript:getMentoresBuscar()" name="search" id="search">
                        <input type="text" placeholder="Buscar usuarios" id="inputSearch">
                        <input type="submit" value="&#xe041;">
                    </form>
                </div>
                <div class="col50">
                    <select id="filtrarPorSede" onchange="getUsuariosPorFiltro()">
                        <option value="">Filtrar por sede</option>
                    </select>
                    <select id="filtrarPorRubro" onchange="getUsuariosPorFiltro()">
                        <option value="">Filtrar por Rubro</option>
                    </select>
                    <select id="filtrarPorExpertise" onchange="getUsuariosPorFiltro()">
                        <option value="">Filtrar por Expertise</option>
                    </select>
                </div>
            </div>
            <div class="col100">
                <a href="javascript:void(0)" class="btn" onclick="getMentores()">Resetear filtros</a>
                <br>
                <div id="divExcel" class="btn"></div>
            </div>
        </div>
    </div>

	<div class="col100" id="listaMentores">
                                
    </div>
<!-- TODO EL CONTENIDO (LO QUE NO ES MENÚ NI HEADER) -->
            
</div>

    <div id="popupSede" class="absolute-center" hidden="">
        <div id="overlayCalificacion"></div>
        <div class="popupSede">
            <a href="#" id="closeBtn" class="closePopUpSede pull-right">
                <i class="close-icon" data-icon="9"></i>
            </a>
            <div class="contentSede">
                <label for="sede">¿Cuál es tu sede de mentoría?</label>
                <br>
                <select name="selectSede" id="selectSede">
                    <option>Seleccioná una sede</option>
                </select><br>

                <label for="sede">¿Cuál es tu ciudad?</label>
                <br>
                <select name="selectProvincia" id="selectProvincia">
                    <option>Seleccioná una provincia</option>
                </select><br>
                <select name="selectCiudad" id="selectCiudad">
                    <option>Seleccioná una ciudad</option>
                </select><br>
                <input type="button" class="label100" value="Enviar" onclick="elegirSede()">
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/masonry.pkgd.min.js"></script>
    <script src="js/select2.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="js/main.js"></script>
    <script src="scripts/header-mentorDT.js"></script>
    <script>
        var myRol = '<?php echo $rol; ?>';
    </script>
    <script src="scripts/red-de-mentores.js"></script>
    </body>
</html>