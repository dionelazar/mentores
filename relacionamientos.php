<?php 
    require __DIR__.'/share/header-admin.php';
?>
	<div class="content">
		<div class="wrapper">
			<h1>Relacionamientos</h1>

            <div class="col100">
            	<!--div class="form-group">
            		<label for="">Filtrar por sede</label>
            		<select id="filtrarPorSede" class="form-control" onchange="filtrarPorSede()">
            			
            		</select>
            	</div-->

            	<div class="col50">
	            	<form action="javascript:buscarMentor()">
		            	<div class="form-group">
		            		<label for="">Dni mentor</label>
		            		<input type="text" class="form-control" id="dniMentor" placeholder="Ingrese el número de documento por favor">
		            	</div>
		            	<div class="form-group">
		            		<input type="submit" class="btn" value="Buscar">
		            	</div>
	            	</form>
            	</div>
            	<div class="col50">
            		<pre>
	            		<ul style="list-style-type:square" id="dataMentor">
							
						</ul>
					</pre>
					<div id="btnEditarMentor"></div>
            	</div>
            </div>

            <div class="col100">
            	<div class="col50">
            		<form action="javascript:buscarEmprendedor()">
		            	<div class="form-group">
		            		<label for="">Dni emprendedor</label>
		            		<input type="text" class="form-control" id="dniEmprendedor" placeholder="Ingrese el número de documento por favor">
		            	</div>
		            	<div class="form-group">
		            		<input type="submit" class="btn" value="Buscar">
		            	</div>
		            </form>
	            </div>

	            <div class="col50">
	            	<pre>
		            	<ul style="list-style-type:disc" id="dataEmprendedor">
							
						</ul>
					</pre>
					<div id="btnEditarEmprendedor"></div>
					<br>
					<div id="btnDesasignarEmprendedor"></div>
            	</div>
            </div>

            <div class="col100">
	            <div class="col50">
	            	<div class="form-group">
	                    <label for="">N° grupo</label>
	                    <select class="form-control" id="nGrupo">
	                        <option value=1>1</option>
	            			<option value=2>2</option>
                            <option value=3>3</option>
	                    </select>
	                </div>
	            </div>
	        </div>

            <div class="col100">
            	<p>Antes de relacionar mira con detenimiento los usuarios que quieres relacionar.</p>
            	<div class="form-group">
            		<input type="button" class="btn" value="Relacionar" onclick="relacionar()">
            	</div>
            </div>


            <div class="col100">
	            <hr>
	            <p>Si quieres borrar varios relacionamientos puedes importar un excel con los <strong>DNI de los emprendedores</strong>.</p>
	        	<input type="button" class="btn" onclick="javascript:$('#popupImportarUsuario').show()" value="Importar" />
        	</div>

        </div>
	</div>

	<div id="popUpCambioRol" class="absolute-center" hidden="">
        <div id="overlayCalificacion"></div>
        <div class="popupReportes">
            <a href="#" id="closeBtn" class="closePopUp" onclick="$('#popUpCambioRol').hide()">
                <i class="close-icon" data-icon="9"></i>
            </a>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h2>Cambiar rol</h2>
                        <hr>
                    </div>
                    <div class="col-xs-12">
                        <div class="tab-content">
                        	<p><strong>Rol actual: </strong><span id="rol_actual"></span></p>
                        	<label>Tené en cuenta que cambiar el rol afecta a todo lo que hizo el usuario. </label>
                        	<ul style="list-style-type:disc">
                        		<li>Si se cambia de mentor a emprendedor o viceversa se borrará el relacionamiento.</li>
                        		<li>
                        			Si un mentor pasa a ser emprendedor se perderá todo lo que hizo él y sus emprendedores
	                        		<ul style="list-style-type:disc">
	                        			<li>Se <strong>borrarán</strong> los grupos y sesiones que tenía asignados</li>
	                        			<li>Se <strong>borrará</strong> el relacionamiento con sus emprendedores</li>
	                        		</ul>
                        		</li>
                        		<li>Si un mentor desarrollo temprano pasa a mentor consolidación se <strong>borrará</strong> su segundo grupo y todas las sesiones mayores a la 3° sesión. Junto con todas las calificaciones de dichas sesiones</li>
                        		<li>Si un mentor consolidación pasa a mentor desarrollo temprano se creará un segundo grupo y sus dos grupos serán de 6 sesiones cada uno.</li>
                        	</ul>
                        	<hr>
                            <form action="javascript:cambiarRol()">
                            	<div class="form-group">
		                            <label for="">Roles</label>
		                            <select class="form-control" id="selectRoles">
		                                
		                            </select>
		                        </div>

		                        <div class="form-group">
		                        	<input type="submit" class="btn" value="Cambiar rol">
		                        </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="popupImportarUsuario" class="absolute-center" hidden="">
        <div id="overlayCalificacion"></div>
        <div class="popupReportes">
            <a href="#" id="closeBtn" class="closePopUp">
                <i class="close-icon" data-icon="9"></i>
            </a>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h2>Borrar relacionamientos</h2>
                        <hr>
                        <h3 style="color: #d62902">Para borrar relacionamientos debe descargar la planilla de ejemplo y completar los campos de cada usuario, respetando el formato de cada celda.</h3>
                        <small>El formato tiene que ser .xls</small>
                        <h4>Template:</h4>
                        <h5><a href="assets/Ejemplo-borrar-relacionamientos.xls" download="download">Ejemplo de borrado de relacionamientos</a></h5>
                    </div>
                    <form enctype="multipart/form-data" id="formImportadorExcel" action="javascript:void(0)">
                        <div class="col-xs-12">
                            <p>Seleccione su hoja de excel de usuarios</p>
                            <input name="myFileExcel" id="myFileExcel" type="file">
                        </div>
                        <div class="col-xs-12" style="text-align: center;">
                            <input type="button" name="importarRel" class="btn" id="importarRel" value="Borrar relacionamientos" onclick="importarBorrarRelacionamiento()" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script>
    window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')
    </script>
    <script src="js/plugins.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.js"></script>
    <script src="js/masonry.pkgd.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="js/main.js"></script>
    <script src="scripts/header.js"></script>
    <script src="scripts/relacionamientos.js"></script>
</body>

</html>