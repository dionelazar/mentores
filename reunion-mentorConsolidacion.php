<?php 
    require __DIR__.'/share/header-mentor-con.php';
?>
        <div class="content home">
            <!-- TODO EL CONTENIDO (LO QUE NO ES MENÚ NI HEADER) -->
            <div class="wrapper">
                <div class="col100">
                    <div class="">
                        <ul class="tabs tabs-2 oneTab clearfix">
                            <li class="current">
                                <a href="#0">
                                    <h2>Reuniones de seguimiento</h2>
                                </a>
                            </li>
                        </ul>
                        <div class="container home">
                            <div class="sesionA show" id="sesiones">
                                <!--div class="col-100 pad10">
                                    <div class="feature-panel">
                                        <ul class="breadcrumb sesion3 ">
                                            <li class="active">
                                                <a class="visibleDesktop" href="#">1ª Sesión</a>
                                                <a class="visibleSmInline" href="#">1ª</a>
                                            </li>
                                            <li class="active">
                                                <a class="visibleDesktop" href="#">2ª Sesión</a>
                                                <a class="visibleSmInline" href="#">2ª</a>
                                            </li>
                                            <li class="active">
                                                <a class="visibleDesktop" href="#">3ª Sesión</a>
                                                <a class="visibleSmInline" href="#">3ª</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.js"></script>
        <script src="js/imagesloaded.pkgd.min.js"></script>
        <script src="js/masonry.pkgd.min.js"></script>
        <script src="js/select2.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/bootbox.min.js"></script>
        <script src="js/main.js"></script>
        <script src="scripts/header.js"></script>
        <script src="scripts/reunion-mentorCon.js"></script>
    </body>
</html>