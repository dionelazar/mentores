<?php 
    require __DIR__.'/share/header-admin.php';
?>
	<div class="content">
        <!-- TODO EL CONTENIDO (LO QUE NO ES MENÚ NI HEADER) -->
        <div class="wrapper">
            <div class="col100">
                <div class="module library">
                	<h1>Emprendedores elegidos para fase 2</h1>

	            	<table>
	            		<thead>
	            			<th>Emprendedor</th>
	            			<th>Mentor</th>
	            			<th>Fecha</th>
	            			<th>DNI emprendedor</th>
							<th>Rol</th>
							<th>Sede</th>
							<th>Asistencias</th>
	            			<th>Aceptar</th>
	            			<th>Rechazar</th>
	            			<th>Estado</th>
	            		</thead>
	            		<tbody id="emprendedores">

	            		</tbody>
	            	</table>
				</div>
            </div>
        </div>
    </div>
	
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script>
window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')
</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="js/plugins.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.js"></script>
<script src="js/chartist.min.js"></script>
<script src="js/imagesloaded.pkgd.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootbox.min.js"></script>
<script src="js/main.js"></script>
<script src="scripts/header.js"></script>
<script src="scripts/emprendedores-fase-2.js"></script>

</body>
</html>