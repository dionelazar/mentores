<?php 
    require __DIR__.'/share/header-admin.php';
?>
        <div class="content home reunion">
            <!-- TODO EL CONTENIDO (LO QUE NO ES MENÚ NI HEADER) -->
            <div class="wrapper">
                <div class="col100">
                    <div class="module search-bar">
                        <!-- BARRA DE BÚSQUEDA -->
                        <div class="inner-cols clearfix">
                            <div class="col100">
                                <form action="javascript:void(0)" name="search" id="search">
                                    <input type="text" placeholder="Buscar sesiones, mentores, grupos o usuarios" id="inputSearch">
                                    <input type="submit" value="&#xe041;">
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <ul class="tabs tabs-2 clearfix">
                            <li id="homeTab1" class="current"><a href="#0">Desarrollo temprano</a></li>
                            <li id="homeTab2"><a href="#0">Consolidación</a></li>
                        </ul>
                        <div class="container home">
                            <div class="sesionA show" id="tabDT">

                            </div>
                           <div class="sesionB" id="tabCON">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="popupCalificacion" class="absolute-center">
    <div id="overlayCalificacion"></div>
    <div class="popupCalificacion">
        <a href="#" id="closeBtn" class="closePopUp">
            <i class="close-icon" data-icon="9"></i>
        </a>
        <div class="label100">
            <label for="rating">Calificá esta sesión</label>
        </div>
        <fieldset class="rating">
            <input type="radio" id="star5" name="rating" value="5" />
            <label class="full" for="star5" title="Awesome - 5 stars"></label>
            <input type="radio" id="star4half" name="rating" value="4.5" />
            <label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
            <input type="radio" id="star4" name="rating" value="4" />
            <label class="full" for="star4" title="Pretty good - 4 stars"></label>
            <input type="radio" id="star3half" name="rating" value="3.5" />
            <label class="half" for="star3half" title="Meh - 3.5 stars"></label>
            <input type="radio" id="star3" name="rating" value="3" />
            <label class="full" for="star3" title="Meh - 3 stars"></label>
            <input type="radio" id="star2half" name="rating" value="2.5" />
            <label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
            <input type="radio" id="star2" name="rating" value="2" />
            <label class="full" for="star2" title="Kinda bad - 2 stars"></label>
            <input type="radio" id="star1half" name="rating" value="1.5" />
            <label class="half" for="star1half" title="Meh - 1.5 stars"></label>
            <input type="radio" id="star1" name="rating" value="1" />
            <label class="full" for="star1" title="Sucks big time - 1 star"></label>
            <input type="radio" id="starhalf" name="rating" value="0.5" />
            <label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
        </fieldset>
        <fieldset class="label100">
                <label for="calificacion1"><div id="pregunta1"></div>
                <div id="respuestas1"></div>

            </fieldset>
            <fieldset class="label100">
                <label for="calificacion2"><div id="pregunta2"></div>
                <div id="respuestas2"></div>

            </fieldset>
        <fieldset class="label100">
            <label for="calificacion2">Describí brevemente como te pareció la sesión</label>
            <textarea class="label100" name="calificacion-texto" id="calificacion-texto"></textarea>
        </fieldset>
        <!--button class="cameraButton"><i data-icon="O"></i>Sacar una Selfie</button-->
        <input type="submit" class="label100" name="submit-calificacion" id="submit-calificacion" value="Calificar" onclick="calificar()" >
    </div>
</div>
        <div id="popupEvaluacion" class="absolute-center" style="display: none;">
            <div id="overlayCalificacion"></div>
            <div class="popupCalificacion popupSatisfaccion">
                <a href="#" id="closeBtn" class="closePopUp">
                    <i class="close-icon" data-icon="9"></i>
                </a>
                <div class="calificacion">
                <div class="label100">
                    <h3><strong>Emprendedor ####</strong></h3>
                    <small>Asistencia</small>
                </div>
                    <fieldset class="labelWCheck">
                        <input type="checkbox" name="puntaje-emprendedor" id="puntaje-emprendedor-1" value="1">
                        <label for="puntaje-emprendedor-1">1</label>
                        <input type="checkbox" name="puntaje-emprendedor" id="puntaje-emprendedor-2" value="2">
                        <label for="puntaje-emprendedor-2">2</label>
                        <input type="checkbox" name="puntaje-emprendedor" id="puntaje-emprendedor-3" value="3">
                        <label for="puntaje-emprendedor-3">3</label>
                    </fieldset>
                </div>
                <hr>
                <h4>Por favor, te pedimos que completes una breve evaluación sobre el emprendedor y su respectivo emprendimiento, en una escala de 1 a 5, siendo 5 el puntaje más alto. </h4>
                <hr>
                <div class="label100">
                    <label for="rating7">1. El emprendedor cumplió con los requisitos formales del proceso de mentoría.*</label>
                </div>
                <fieldset class="rating">
                    <input type="radio" id="rating7-1" name="rating7" value="5" />
                    <label class="full" for="rating7-1" title="Awesome - 5 stars"></label>
                    <input type="radio" id="rating7-2" name="rating7" value="4 and a half" />
                    <label class="half" for="rating7-2" title="Pretty good - 4.5 stars"></label>
                    <input type="radio" id="rating7-3" name="rating7" value="4" />
                    <label class="full" for="rating7-3" title="Pretty good - 4 stars"></label>
                    <input type="radio" id="rating7-4" name="rating7" value="3 and a half" />
                    <label class="half" for="rating7-4" title="Meh - 3.5 stars"></label>
                    <input type="radio" id="rating7-5" name="rating7" value="3" />
                    <label class="full" for="rating7-5" title="Meh - 3 stars"></label>
                    <input type="radio" id="rating7-6" name="rating7" value="2 and a half" />
                    <label class="half" for="rating7-6" title="Kinda bad - 2.5 stars"></label>
                    <input type="radio" id="rating7-7" name="rating7" value="2" />
                    <label class="full" for="rating7-7" title="Kinda bad - 2 stars"></label>
                    <input type="radio" id="rating7-8" name="rating7" value="1 and a half" />
                    <label class="half" for="rating7-8" title="Meh - 1.5 stars"></label>
                    <input type="radio" id="rating7-9" name="rating7" value="1" />
                    <label class="full" for="rating7-9" title="Sucks big time - 1 star"></label>
                    <input type="radio" id="rating7-10" name="rating7" value="half" />
                    <label class="half" for="rating7-10" title="Sucks big time - 0.5 stars"></label>
                </fieldset>
                <div class="label100">
                    <label for="rating8">2. El emprendedor tuvo una participación activa y demostró compromiso con el programa.*</label>
                </div><br>
                <fieldset class="rating">
                    <input type="radio" id="rating8-1" name="rating8" value="5" />
                    <label class="full" for="rating8-1" title="Awesome - 5 stars"></label>
                    <input type="radio" id="rating8-2" name="rating8" value="4 and a half" />
                    <label class="half" for="rating8-2" title="Pretty good - 4.5 stars"></label>
                    <input type="radio" id="rating8-3" name="rating8" value="4" />
                    <label class="full" for="rating8-3" title="Pretty good - 4 stars"></label>
                    <input type="radio" id="rating8-4" name="rating8" value="3 and a half" />
                    <label class="half" for="rating8-4" title="Meh - 3.5 stars"></label>
                    <input type="radio" id="rating8-5" name="rating8" value="3" />
                    <label class="full" for="rating8-5" title="Meh - 3 stars"></label>
                    <input type="radio" id="rating8-6" name="rating8" value="2 and a half" />
                    <label class="half" for="rating8-6" title="Kinda bad - 2.5 stars"></label>
                    <input type="radio" id="rating8-7" name="rating8" value="2" />
                    <label class="full" for="rating8-7" title="Kinda bad - 2 stars"></label>
                    <input type="radio" id="rating8-8" name="rating8" value="1 and a half" />
                    <label class="half" for="rating8-8" title="Meh - 1.5 stars"></label>
                    <input type="radio" id="rating8-9" name="rating8" value="1" />
                    <label class="full" for="rating8-9" title="Sucks big time - 1 star"></label>
                    <input type="radio" id="rating8-10" name="rating8" value="half" />
                    <label class="half" for="rating8-10" title="Sucks big time - 0.5 stars"></label>
                </fieldset><br>
                <div class="label100">
                    <label>3. ¿Cómo evaluás el cumplimiento de los objetivos de la mentoría?.*</label>
                </div>
                <div class="label100">
                    <label for="rating9"><h4>i. Identificar su mercado y segmento objetivo.</h4></label>
                </div>
                <fieldset class="rating">
                    <input type="radio" id="rating9-1" name="rating9" value="5" />
                    <label class="full" for="rating9-1" title="Awesome - 5 stars"></label>
                    <input type="radio" id="rating9-2" name="rating9" value="4 and a half" />
                    <label class="half" for="rating9-2" title="Pretty good - 4.5 stars"></label>
                    <input type="radio" id="rating9-3" name="rating9" value="4" />
                    <label class="full" for="rating9-3" title="Pretty good - 4 stars"></label>
                    <input type="radio" id="rating9-4" name="rating9" value="3 and a half" />
                    <label class="half" for="rating9-4" title="Meh - 3.5 stars"></label>
                    <input type="radio" id="rating9-5" name="rating9" value="3" />
                    <label class="full" for="rating9-5" title="Meh - 3 stars"></label>
                    <input type="radio" id="rating9-6" name="rating9" value="2 and a half" />
                    <label class="half" for="rating9-6" title="Kinda bad - 2.5 stars"></label>
                    <input type="radio" id="rating9-7" name="rating9" value="2" />
                    <label class="full" for="rating9-7" title="Kinda bad - 2 stars"></label>
                    <input type="radio" id="rating9-8" name="rating9" value="1 and a half" />
                    <label class="half" for="rating9-8" title="Meh - 1.5 stars"></label>
                    <input type="radio" id="rating9-9" name="rating9" value="1" />
                    <label class="full" for="rating9-9" title="Sucks big time - 1 star"></label>
                    <input type="radio" id="rating9-10" name="rating9" value="half" />
                    <label class="half" for="rating9-10" title="Sucks big time - 0.5 stars"></label>
                </fieldset><br>
                <div class="label100">
                    <label for="rating10"><h4>ii. Definir actividades claves del modelo de negocio.</h4> </label>
                </div>
                <fieldset class="rating">
                    <input type="radio" id="rating10-1" name="rating10" value="5" />
                    <label class="full" for="rating10-1" title="Awesome - 5 stars"></label>
                    <input type="radio" id="rating10-2" name="rating10" value="4 and a half" />
                    <label class="half" for="rating10-2" title="Pretty good - 4.5 stars"></label>
                    <input type="radio" id="rating10-3" name="rating10" value="4" />
                    <label class="full" for="rating10-3" title="Pretty good - 4 stars"></label>
                    <input type="radio" id="rating10-4" name="rating10" value="3 and a half" />
                    <label class="half" for="rating10-4" title="Meh - 3.5 stars"></label>
                    <input type="radio" id="rating10-5" name="rating10" value="3" />
                    <label class="full" for="rating10-5" title="Meh - 3 stars"></label>
                    <input type="radio" id="rating10-6" name="rating10" value="2 and a half" />
                    <label class="half" for="rating10-6" title="Kinda bad - 2.5 stars"></label>
                    <input type="radio" id="rating10-7" name="rating10" value="2" />
                    <label class="full" for="rating10-7" title="Kinda bad - 2 stars"></label>
                    <input type="radio" id="rating10-8" name="rating10" value="1 and a half" />
                    <label class="half" for="rating10-8" title="Meh - 1.5 stars"></label>
                    <input type="radio" id="rating10-9" name="rating10" value="1" />
                    <label class="full" for="rating10-9" title="Sucks big time - 1 star"></label>
                    <input type="radio" id="rating10-10" name="rating10" value="half" />
                    <label class="half" for="rating10-10" title="Sucks big time - 0.5 stars"></label>
                </fieldset>
                <div class="label100">
                    <label for="rating11"><h4>iii. Diseñar y ejecutar el plan de marketing y ventas.</h4></label>
                </div><br>
                <fieldset class="rating">
                    <input type="radio" id="rating11-1" name="rating11" value="5" />
                    <label class="full" for="rating11-1" title="Awesome - 5 stars"></label>
                    <input type="radio" id="rating11-2" name="rating11" value="4 and a half" />
                    <label class="half" for="rating11-2" title="Pretty good - 4.5 stars"></label>
                    <input type="radio" id="rating11-3" name="rating11" value="4" />
                    <label class="full" for="rating11-3" title="Pretty good - 4 stars"></label>
                    <input type="radio" id="rating11-4" name="rating11" value="3 and a half" />
                    <label class="half" for="rating11-4" title="Meh - 3.5 stars"></label>
                    <input type="radio" id="rating11-5" name="rating11" value="3" />
                    <label class="full" for="rating11-5" title="Meh - 3 stars"></label>
                    <input type="radio" id="rating11-6" name="rating11" value="2 and a half" />
                    <label class="half" for="rating11-6" title="Kinda bad - 2.5 stars"></label>
                    <input type="radio" id="rating11-7" name="rating11" value="2" />
                    <label class="full" for="rating11-7" title="Kinda bad - 2 stars"></label>
                    <input type="radio" id="rating11-8" name="rating11" value="1 and a half" />
                    <label class="half" for="rating11-8" title="Meh - 1.5 stars"></label>
                    <input type="radio" id="rating11-9" name="rating11" value="1" />
                    <label class="full" for="rating11-9" title="Sucks big time - 1 star"></label>
                    <input type="radio" id="rating11-10" name="rating11" value="half" />
                    <label class="half" for="rating11-10" title="Sucks big time - 0.5 stars"></label>
                </fieldset><br>
                <div class="label100">
                    <label for="rating12"><h4>iv. Detectar Socios clave/ Aliados estratégicos.</h4></label>
                </div><br>
                <fieldset class="rating">
                    <input type="radio" id="rating12-1" name="rating12" value="5" />
                    <label class="full" for="rating12-1" title="Awesome - 5 stars"></label>
                    <input type="radio" id="rating12-2" name="rating12" value="4 and a half" />
                    <label class="half" for="rating12-2" title="Pretty good - 4.5 stars"></label>
                    <input type="radio" id="rating12-3" name="rating12" value="4" />
                    <label class="full" for="rating12-3" title="Pretty good - 4 stars"></label>
                    <input type="radio" id="rating12-4" name="rating12" value="3 and a half" />
                    <label class="half" for="rating12-4" title="Meh - 3.5 stars"></label>
                    <input type="radio" id="rating12-5" name="rating12" value="3" />
                    <label class="full" for="rating12-5" title="Meh - 3 stars"></label>
                    <input type="radio" id="rating12-6" name="rating12" value="2 and a half" />
                    <label class="half" for="rating12-6" title="Kinda bad - 2.5 stars"></label>
                    <input type="radio" id="rating12-7" name="rating12" value="2" />
                    <label class="full" for="rating12-7" title="Kinda bad - 2 stars"></label>
                    <input type="radio" id="rating12-8" name="rating12" value="1 and a half" />
                    <label class="half" for="rating12-8" title="Meh - 1.5 stars"></label>
                    <input type="radio" id="rating12-9" name="rating12" value="1" />
                    <label class="full" for="rating12-9" title="Sucks big time - 1 star"></label>
                    <input type="radio" id="rating12-10" name="rating12" value="half" />
                    <label class="half" for="rating12-10" title="Sucks big time - 0.5 stars"></label>
                </fieldset><br>
                <div class="label100">
                    <label for="rating13"><h4>v. Elaborar el plan financiero.</h4></label>
                </div><br>
                <fieldset class="rating">
                    <input type="radio" id="rating13-1" name="rating13" value="5" />
                    <label class="full" for="rating13-1" title="Awesome - 5 stars"></label>
                    <input type="radio" id="rating13-2" name="rating13" value="4 and a half" />
                    <label class="half" for="rating13-2" title="Pretty good - 4.5 stars"></label>
                    <input type="radio" id="rating13-3" name="rating13" value="4" />
                    <label class="full" for="rating13-3" title="Pretty good - 4 stars"></label>
                    <input type="radio" id="rating13-4" name="rating13" value="3 and a half" />
                    <label class="half" for="rating13-4" title="Meh - 3.5 stars"></label>
                    <input type="radio" id="rating13-5" name="rating13" value="3" />
                    <label class="full" for="rating13-5" title="Meh - 3 stars"></label>
                    <input type="radio" id="rating13-6" name="rating13" value="2 and a half" />
                    <label class="half" for="rating13-6" title="Kinda bad - 2.5 stars"></label>
                    <input type="radio" id="rating13-7" name="rating13" value="2" />
                    <label class="full" for="rating13-7" title="Kinda bad - 2 stars"></label>
                    <input type="radio" id="rating13-8" name="rating13" value="1 and a half" />
                    <label class="half" for="rating13-8" title="Meh - 1.5 stars"></label>
                    <input type="radio" id="rating13-9" name="rating13" value="1" />
                    <label class="full" for="rating13-9" title="Sucks big time - 1 star"></label>
                    <input type="radio" id="rating13-10" name="rating13" value="half" />
                    <label class="half" for="rating13-10" title="Sucks big time - 0.5 stars"></label>
                </fieldset><br>
                <fieldset class="label100">
                    <label for="calificacion3"><h4>vi. Comentarios/consideraciones que desees agregar:</h4></label>
                    <textarea class="label100" name="calificacion3" id="calificacion3"></textarea>
                </fieldset>
                <input type="submit" class="closePopUp label100" name="submit-calificacion" id="submit-calificacion" value="Calificar">
            </div>
        </div>
    <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/masonry.pkgd.min.js"></script>
    <script src="js/select2.js"></script>
    <script src="js/featherlight.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="js/main.js"></script>
    <script src="scripts/header.js"></script>
    <script src="scripts/reunion-ministerio.js"></script>
</body>
</html>